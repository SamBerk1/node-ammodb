'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DirectorSchema extends Schema {
  up () {
    this.create('directors', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('name').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('directors')
  }
}

module.exports = DirectorSchema
