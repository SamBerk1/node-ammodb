'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IncomingEpisodeSchema extends Schema {
  up () {
    this.create('incoming_episodes', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('videoStatus')
      table.integer('qc')
      table.integer('captions')
      table.integer('subtitles')
      table.integer('artwork')
      table.integer('dubbing')
      table.integer('metadata')
      table.date('deliveryDate')
      table.date('launchDate')
      table.date('dueDate')
      table.integer('customerId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('incomingId').unsigned().references('id').inTable('incomings').onDelete('CASCADE')
      table.integer('seriesId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('seasonId').unsigned().references('id').inTable('seasons').onDelete('CASCADE')
      table.integer('episodeId').unsigned().references('id').inTable('episodes').onDelete('CASCADE')
      table.unique(['incomingId','seriesId','seasonId','episodeId'])
      table.timestamps()
    })
  }

  down () {
    this.drop('incoming_episodes')
  }
}

module.exports = IncomingEpisodeSchema
