'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesGenresSchema extends Schema {
  up () {
    this.create('titles_genres', (table) => {
      //pivot table for many to many realtion in Titles and Genres
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('genreId').unsigned().references('id').inTable('genres').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_genres')
  }
}

module.exports = TitlesGenresSchema
