'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LanguagesSchema extends Schema {
  up () {
    this.create('languages', (table) => {
      //pivot table for many to many realtion in Titles and Languages
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('code').unique()
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('languages')
  }
}

module.exports = LanguagesSchema
