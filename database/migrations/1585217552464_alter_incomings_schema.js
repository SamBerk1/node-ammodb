'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterIncomingsSchema extends Schema {
  up () {
    this.table('incomings', (table) => {
      // alter table
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
    })
  }

  down () {
    this.table('incomings', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterIncomingsSchema
