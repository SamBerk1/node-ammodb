'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterAlternateTitleLanguagesSchema extends Schema {
  up () {
    this.table('alternate_title_languages', (table) => {
      // alter table
      table.dropForeign('libraryId')
      table.dropForeign('languageId')
      table.foreign('libraryId').references('libraries.id').onDelete('CASCADE')
      table.foreign('languageId').references('languages.id').onDelete('CASCADE')
    })
  }

  down () {
    this.table('alternate_title_languages', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterAlternateTitleLanguagesSchema
