'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProducerSchema extends Schema {
  up () {
    this.create('producers', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('name').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('producers')
  }
}

module.exports = ProducerSchema
