'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterAlternateTitlesPlatformsUniqueConstraintSchema extends Schema {
  up () {
    this.table('alternate_titles_platforms', (table) => {
      // alter table
      table.unique(['alternateTitleId', 'platformId'],'alternateName_platform_unique')
    })
  }

  down () {
    this.table('alternate_titles_platforms', (table) => {
      // reverse alternations
      table.dropUnique(['alternateTitleId', 'platformId'],'alternateName_platform_unique')
    })
  }
}

module.exports = AlterAlternateTitlesPlatformsUniqueConstraintSchema
