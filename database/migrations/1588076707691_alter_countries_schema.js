'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterCountriesSchema extends Schema {
  up () {
    this.table('countries', (table) => {
      // alter table
      table.unique(['name','code'],'unique')

    })
  }

  down () {
    this.table('countries', (table) => {
      // reverse alternations
      table.dropUnique(['name','code'],'unique')
    })
  }
}

module.exports = AlterCountriesSchema
