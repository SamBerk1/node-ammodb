'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterHoldbacksSchema extends Schema {
  up () {
    this.table('holdbacks', (table) => {
      // alter table
      table.renameColumn('remarks', 'name')
      table.date('startDate')
      table.date('endDate')
      table.dropForeign('territoryId')
      table.dropColumn('territoryId')
      table.dropForeign('platformId')
      table.dropColumn('platformId')
    })
  }

  down () {
    this.table('holdbacks', (table) => {
      // reverse alternations
      table.dropColumn('startDate')
      table.dropColumn('endDate')
      table.renameColumn('name','remarks')
      table.foreign('territoryId').references('territories.id')
      table.foreign('platformId').references('platforms.id')
    })
  }
}

module.exports = AlterHoldbacksSchema
