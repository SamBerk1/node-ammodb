'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesSchema extends Schema {
  up () {
    this.table('title_statuses', (table) => {
      // alter table
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
    })
  }

  down () {
    this.table('title_statuses', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterTitleStatusesSchema
