'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterAdbreaksSchema extends Schema {
  up () {
    this.table('ad_breaks', (table) => {
      // alter table
      table.foreign('libraryId').references('libraries.id').onDelete('CASCADE')
    })
  }

  down () {
    this.table('ad_breaks', (table) => {
      // reverse alternations
      table.foreign('libraryId').references('libraries.id')
    })
  }
}

module.exports = AlterAdbreaksSchema
