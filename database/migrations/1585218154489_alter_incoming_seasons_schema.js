'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterIncomingSeasonsSchema extends Schema {
  up () {
    this.table('incoming_seasons', (table) => {
      // alter table
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
    })
  }

  down () {
    this.table('incoming_seasons', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterIncomingSeasonsSchema
