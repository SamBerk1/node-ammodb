'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OutgoingNotesSchema extends Schema {
  up () {
    this.create('outgoing_notes', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('outgoingId').unsigned().references('id').inTable('title_statuses').onDelete('CASCADE')
      table.integer('noteId').unsigned().references('id').inTable('notes').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('outgoing_notes')
  }
}

module.exports = OutgoingNotesSchema
