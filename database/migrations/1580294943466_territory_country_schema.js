'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TerritoryCountrySchema extends Schema {
  up () {
    this.create('territory_countries', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('territoryId').unsigned().references('id').inTable('territories').onDelete('CASCADE')
      table.string('country')
      table.timestamps()
    })
  }

  down () {
    this.drop('territory_countries')
  }
}

module.exports = TerritoryCountrySchema
