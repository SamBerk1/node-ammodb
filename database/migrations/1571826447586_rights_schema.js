'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RightsSchema extends Schema {
  up () {
    this.create('rights', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('name')
      table.string('description')
      table.timestamps()
    })
  }

  down () {
    this.drop('rights')
  }
}

module.exports = RightsSchema
