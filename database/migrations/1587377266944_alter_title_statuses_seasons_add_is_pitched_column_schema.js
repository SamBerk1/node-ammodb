'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesSeasonsAddIsPitchedColumnSchema extends Schema {
  up () {
    this.table('title_statuses_seasons', (table) => {
      // alter table
      table.boolean('isPitched').notNullable().defaultTo(false)
    })
  }

  down () {
    this.table('title_statuses_seasons', (table) => {
      // reverse alternations
      table.dropColumn('isPitched')
    })
  }
}

module.exports = AlterTitleStatusesSeasonsAddIsPitchedColumnSchema
