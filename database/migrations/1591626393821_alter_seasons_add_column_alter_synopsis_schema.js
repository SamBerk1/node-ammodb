'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterSeasonsAddColumnAlterSynopsisSchema extends Schema {
  up () {
    this.table('seasons', (table) => {
      // alter table
      table.string('alterSynopsis',400)
    })
  }

  down () {
    this.table('seasons', (table) => {
      // reverse alternations
      table.dropColumn('alterSynopsis')
    })
  }
}

module.exports = AlterSeasonsAddColumnAlterSynopsisSchema
