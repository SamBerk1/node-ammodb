'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitlesTerritoriesRightsSchema extends Schema {
  up () {
    this.table('titles_territories_rights', (table) => {
      // alter table
      table.integer('countryId').unsigned().references('id').inTable('territory_countries').onDelete('CASCADE')
    })
  }

  down () {
    this.table('titles_territories_rights', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterTitlesTerritoriesRightsSchema
