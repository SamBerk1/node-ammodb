'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesDirectorsSchema extends Schema {
  up () {
    this.create('titles_directors', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('directorId').unsigned().references('id').inTable('directors').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_directors')
  }
}

module.exports = TitlesDirectorsSchema


