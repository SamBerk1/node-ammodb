'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitlesTerritoriesRightsSchema extends Schema {
  up () {
    this.table('titles_territories_rights', (table) => {
      // alter table
      table.foreign('libraryId').references('id').inTable('libraries').onDelete('CASCADE')
      table.foreign('rightId').references('id').inTable('rights').onDelete('CASCADE')
      table.foreign('territoryId').references('id').inTable('territories').onDelete('CASCADE')
      table.unique(['libraryId','rightId','territoryId','countryId'],'uniqueConstraint')
    })
  }

  down () {
    this.table('titles_territories_rights', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterTitlesTerritoriesRightsSchema
