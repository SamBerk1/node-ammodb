'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterAlternateTitlesLanguagesSchema extends Schema {
  up () {
    this.table('alternate_title_languages', (table) => {
      // alter table
      table.string('imdb')
      table.string('alterSynopsis',200)
      table.integer('studioId').unsigned().references('id').inTable('studios')
      table.integer('ratingId').unsigned().references('id').inTable('ratings')
    })
  }

  down () {
    this.table('alternate_title_languages', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterAlternateTitlesLanguagesSchema
