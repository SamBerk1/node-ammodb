'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterEpisodesSchema extends Schema {
  up () {
    this.table('episodes', (table) => {
      // alter table
      table.text('alterSynopsis',['longtext'])
    })
  }

  down () {
    this.table('episodes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterEpisodesSchema
