'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SubscriberSchema extends Schema {
  up () {
    this.create('subscribers', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('email')
      table.integer('emailId').unsigned().references('id').inTable('emails').onDelete('CASCADE')
      table.unique(['email','emailId'])
      table.timestamps()
    })
  }

  down () {
    this.drop('subscribers')
  }
}

module.exports = SubscriberSchema
