'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterPlatformRightsSchema extends Schema {
  up () {
    this.table('platfroms_rights', (table) => {
      // alter table
      table.dropColumn('startDate');
      table.dropColumn('endDate');
    })
  }

  down () {
    this.table('platfroms_rights', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterPlatformRightsSchema
