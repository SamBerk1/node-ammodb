'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterLibrarySchema extends Schema {
  up () {
    this.table('libraries', (table) => {
      // alter table
      table.renameColumn('duration','runtime');
    })
  }

  down () {
    this.table('libraries', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterLibrarySchema
