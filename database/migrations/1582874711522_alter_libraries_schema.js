'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterLibrariesSchema extends Schema {
  up () {
    this.table('libraries', (table) => {
      // alter table
      table.dropForeign('seriesId')
      table.dropColumn('seriesId')
      table.dropColumn('country')
      table.dropColumn('season')
      table.dropColumn('episodeNo')
      table.dropForeign('directorId')
      table.dropColumn('directorId')
    })
  }

  down () {
    this.table('libraries', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterLibrariesSchema
