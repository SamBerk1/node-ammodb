'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterIncomingsSchema extends Schema {
  up () {
    this.table('incomings', (table) => {
      // alter table
      table.dropColumn('dueDate');
    })
  }

  down () {
    this.table('incomings', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterIncomingsSchema
