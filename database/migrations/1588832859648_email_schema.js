'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmailSchema extends Schema {
  up () {
    this.create('emails', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.text('title',['mediumtext'])
      table.text('subject',['text'])
      table.text('content',['longtext'])
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('emails')
  }
}

module.exports = EmailSchema
