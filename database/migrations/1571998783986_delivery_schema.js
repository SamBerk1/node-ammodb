'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DeliverySchema extends Schema {
  up () {
    this.create('deliveries', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.date('deliveryDate')
      table.date('launchDate')
      table.string('status')
      table.integer('libraryId').unsigned().references('id').inTable('libraries')
      table.timestamps()
    })
  }

  down () {
    this.drop('deliveries')
  }
}

module.exports = DeliverySchema
