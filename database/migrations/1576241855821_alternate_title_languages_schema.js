'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlternateTitleLanguagesSchema extends Schema {
  up () {
    this.create('alternate_title_languages', (table) => {
      //create table storing alternate names provided in library form
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('guId')
      table.string('alternateName')
      table.boolean('isParent')
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('languageId').unsigned().references('id').inTable('languages').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('alternate_title_languages')
  }
}

module.exports = AlternateTitleLanguagesSchema
