'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesSchema extends Schema {
  up () {
    this.table('title_statuses', (table) => {
      // alter table
      table.dropColumn('qc');
    })
  }

  down () {
    this.table('title_statuses', (table) => {
      // reverse alternations
      table.integer('qc')
    })
  }
}

module.exports = AlterTitleStatusesSchema
