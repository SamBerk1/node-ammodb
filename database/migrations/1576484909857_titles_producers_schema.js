'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesProducersSchema extends Schema {
  up () {
    this.create('titles_producers', (table) => {
      //pivot table for many to many realtion in Titles and Producers
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('producerId').unsigned().references('id').inTable('producers').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_producers')
  }
}

module.exports = TitlesProducersSchema
