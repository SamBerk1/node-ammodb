'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ActivityLogSchema extends Schema {
  up () {
    this.create('activity_logs', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('action')
      table.string('url')
      table.string('resource')
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('activity_logs')
  }
}

module.exports = ActivityLogSchema
