'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesTrailersSchema extends Schema {
  up () {
    this.create('titles_trailers', (table) => {
      //pivot table for many to many realtion in Titles and Trailers
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('trailerId').unsigned().references('id').inTable('trailers').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_trailers')
  }
}

module.exports = TitlesTrailersSchema
