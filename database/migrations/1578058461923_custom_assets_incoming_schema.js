'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomAssetsIncomingSchema extends Schema {
  up () {
    this.create('custom_assets_incomings', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('incomingId').unsigned().references('id').inTable('incomings').onDelete('CASCADE')
      table.string('name')
      table.string('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('custom_assets_incomings')
  }
}

module.exports = CustomAssetsIncomingSchema
