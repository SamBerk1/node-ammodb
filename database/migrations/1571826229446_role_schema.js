'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RoleSchema extends Schema {
  up () {
    this.create('roles', (table) => {
      //creates table roles
      table.engine('InnoDB') //set database Engine InnoDB 
      table.increments()
      table.string('name').notNullable().unique() //role name e.g Administrator
      table.string('slug').notNullable().unique() // admin
      table.timestamps()
    })
  }

  down () {
    this.drop('roles')
  }
}

module.exports = RoleSchema
