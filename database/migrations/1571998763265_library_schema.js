'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LibrarySchema extends Schema {
  up () {
    this.create('libraries', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('title').unique()
      table.string('year')
      table.string('duration')
      table.string('country')
      table.string('is') //movie or series
      table.string('season')
      table.integer('episodeNo')
      table.string('frameRate')
      table.string('synopsis',400)
      table.string('alterSynopsis',200)
      table.string('guId')
      table.string('imdb')
      table.date('releaseDate');
      table.integer('seriesId').unsigned().references('id').inTable('series')
      table.integer('formatId').unsigned().references('id').inTable('formats')
      table.integer('studioId').unsigned().references('id').inTable('studios')
      table.integer('screenerId').unsigned().references('id').inTable('screeners')
      table.integer('trailerId').unsigned().references('id').inTable('trailers')
      table.integer('directorId').unsigned().references('id').inTable('directors')
      table.integer('ratingId').unsigned().references('id').inTable('ratings')
      table.timestamps()
    })
  }

  down () {
    this.drop('libraries')
  }
}

module.exports = LibrarySchema
