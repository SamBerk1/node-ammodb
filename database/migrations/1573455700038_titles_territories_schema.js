'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesTerritoriesSchema extends Schema {
  up () {
    this.create('titles_territories', (table) => {
      //pivot table for many to many realtion in Titles and Territories
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('territoryId').unsigned().references('id').inTable('territories').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_territories')
  }
}

module.exports = TitlesTerritoriesSchema
