'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlatformsTerritoriesRightsSchema extends Schema {
  up () {
    this.create('platfroms_rights', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('platformId').unsigned().references('id').inTable('platforms').onDelete('CASCADE')
      table.integer('rightId').unsigned().references('id').inTable('rights').onDelete('CASCADE')
      table.integer('territoryId').unsigned().references('id').inTable('territories').onDelete('CASCADE')
      table.unique(['platformId','rightId','territoryId'])
      table.date('startDate');
      table.date('endDate');
      table.timestamps()
    })
  }

  down () {
    this.drop('platfroms_rights')
  }
}

module.exports = PlatformsTerritoriesRightsSchema
