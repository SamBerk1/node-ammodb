'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterLibrariesSchema extends Schema {
  up () {
    this.table('libraries', (table) => {
      // alter table
      table.text('synopsis',['longtext'])
    })
  }

  down () {
    this.table('libraries', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterLibrariesSchema
