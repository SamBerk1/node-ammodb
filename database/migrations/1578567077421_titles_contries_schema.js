'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesContriesSchema extends Schema {
  up () {
    this.create('titles_countries', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('countryId').unsigned().references('id').inTable('countries').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_countries')
  }
}

module.exports = TitlesContriesSchema
