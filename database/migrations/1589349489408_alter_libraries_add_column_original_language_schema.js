'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterLibrariesAddColumnOriginalLanguageSchema extends Schema {
  up () {
    this.table('libraries', (table) => {
      // alter table
      table.integer('originalLanguageId').unsigned().references('id').inTable('languages').onDelete('CASCADE')
    })
  }

  down () {
    this.table('libraries', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterLibrariesAddColumnOriginalLanguageSchema
