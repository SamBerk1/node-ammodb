'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterStudioSchema extends Schema {
  up () {
    this.table('studios', (table) => {
      // alter table
      table.string('contactNumber')
    })
  }

  down () {
    this.table('studios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterStudioSchema
