'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterLibrariesDropLibrariesTitleUniqueSchema extends Schema {
  up () {
    this.table('libraries', (table) => {
      // alter table
      table.dropUnique(['title'])
      table.unique(['guId'])
    })
  }

  down () {
    this.table('libraries', (table) => {
      // reverse alternations
      table.unique(['title'])
      table.dropUnique(['guId'])
    })
  }
}

module.exports = AlterLibrariesDropLibrariesTitleUniqueSchema
