'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IncomingSchema extends Schema {
  up () {
    this.create('incomings', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('videoStatus')
      table.integer('qc')
      table.integer('captions')
      table.integer('subtitles')
      table.integer('artwork')
      table.integer('dubbing')
      table.integer('metadata')
      table.date('deliveryDate')
      table.date('launchDate')
      table.date('dueDate')
      table.integer('customerId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('alternateTitleId').unsigned().references('id').inTable('alternate_title_languages').onDelete('CASCADE')
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.unique(['alternateTitleId', 'libraryId'])
      table.timestamps()
    })
  }

  down () {
    this.drop('incomings')
  }
}

module.exports = IncomingSchema
