'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlteraImagesSchema extends Schema {
  up () {
    this.table('images', (table) => {
      // alter table
      table.string('compressed')
      table.string('thumbnail')

    })
  }

  down () {
    this.table('images', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlteraImagesSchema
