'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterIncomingNotesSchema extends Schema {
  up () {
    this.table('incoming_notes', (table) => {
      // alter table
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
    })
  }

  down () {
    this.table('incoming_notes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterIncomingNotesSchema
