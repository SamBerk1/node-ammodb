'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomAssetsOutgoingSchema extends Schema {
  up () {
    this.create('custom_assets_outgoings', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('outgoingId').unsigned().references('id').inTable('title_statuses').onDelete('CASCADE')
      table.string('name')
      table.string('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('custom_assets_outgoings')
  }
}

module.exports = CustomAssetsOutgoingSchema
