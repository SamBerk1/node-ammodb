'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OutgoingSeasonNotesSchema extends Schema {
  up () {
    this.create('outgoing_season_notes', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('outgoingSeasonId').unsigned().references('id').inTable('title_statuses_seasons').onDelete('CASCADE')
      table.integer('noteId').unsigned().references('id').inTable('notes').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('outgoing_season_notes')
  }
}

module.exports = OutgoingSeasonNotesSchema
