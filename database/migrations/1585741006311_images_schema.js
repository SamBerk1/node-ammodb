'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ImagesSchema extends Schema {
  up () {
    this.create('images', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('path')
      table.string('name')
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('images')
  }
}

module.exports = ImagesSchema
