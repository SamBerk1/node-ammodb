'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesSeasonsAddColumnSunsetDateSchema extends Schema {
  up () {
    this.table('title_statuses_seasons', (table) => {
      // alter table
      table.date('sunSetDate')
      table.dropColumn('isPitched')
    })
  }

  down () {
    this.table('title_statuses_seasons', (table) => {
      // reverse alternations
      table.dropColumn('sunSetDate')
      table.boolean('isPitched').notNullable().defaultTo(false)
    })
  }
}

module.exports = AlterTitleStatusesSeasonsAddColumnSunsetDateSchema
