'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SubtitleSchema extends Schema {
  up () {
    this.create('subtitles', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('subtitleLanguage').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('subtitles')
  }
}

module.exports = SubtitleSchema
