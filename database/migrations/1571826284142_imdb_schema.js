'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ImdbSchema extends Schema {
  up () {
    this.create('imdbs', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('imdbUrl').unique()
      table.string('imdbRating')
      table.timestamps()
    })
  }

  down () {
    this.drop('imdbs')
  }
}

module.exports = ImdbSchema
