'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ScreenerSchema extends Schema {
  up () {
    this.create('screeners', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('screenerLink').unique()
      table.string('screenerPW')
      table.string('name').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('screeners')
  }
}

module.exports = ScreenerSchema
