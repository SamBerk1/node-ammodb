'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudioSchema extends Schema {
  up () {
    this.create('studios', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('title').unique()
      table.string('location')
      table.integer('contactNumber')
      table.string('contactEmail').unique()
      table.string('address')
      table.timestamps()
    })
  }

  down () {
    this.drop('studios')
  }
}

module.exports = StudioSchema
