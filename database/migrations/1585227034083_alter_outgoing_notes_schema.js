'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterOutgoingNotesSchema extends Schema {
  up () {
    this.table('outgoing_notes', (table) => {
      // alter table
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
    })
  }

  down () {
    this.table('outgoing_notes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterOutgoingNotesSchema
