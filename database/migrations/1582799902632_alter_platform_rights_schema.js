'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterPlatformRightsSchema extends Schema {
  up () {
    this.table('platfroms_rights', (table) => {
      // alter table
      table.dropForeign('platformId');
      table.dropForeign('rightId');
      table.dropForeign('territoryId')
      table.dropUnique(['platformId', 'rightId','territoryId']);
      table.foreign('platformId').references('id').inTable('platforms').onDelete('CASCADE')
      table.foreign('rightId').references('id').inTable('rights').onDelete('CASCADE')
      table.foreign('territoryId').references('id').inTable('territories').onDelete('CASCADE')
      table.unique(['platformId','rightId','territoryId','countryId'],'uniqueConstraint')
    })
  }

  down () {
    this.table('platfroms_rights', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterPlatformRightsSchema
