'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitleStatusSchema extends Schema {
  up () {
    this.create('title_statuses', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('videoStatus')
      table.integer('qc')
      table.integer('captions')
      table.integer('subtitles')
      table.integer('artwork')
      table.integer('dubbing')
      table.integer('metadata')
      table.date('deliveryDate')
      table.date('launchDate')
      table.date('dueDate')
      table.boolean('outgoing')
      table.integer('incomingId').unsigned().references('id').inTable('incomings').onDelete('CASCADE')
      table.integer('alternateTitleId').unsigned().references('id').inTable('alternate_title_languages').onDelete('CASCADE')
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('platformId').unsigned().references('id').inTable('platforms').onDelete('CASCADE')
      table.unique(['incomingId', 'libraryId','platformId'])
      table.timestamps()
    })
  }

  down () {
    this.drop('title_statuses')
  }
}

module.exports = TitleStatusSchema
