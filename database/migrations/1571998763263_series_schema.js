'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeriesSchema extends Schema {
  up () {
    this.create('series', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('title').unique()
      table.integer('seasons')
      table.string('guid')
      table.timestamps()
    })
  }

  down () {
    this.drop('series')
  }
}

module.exports = SeriesSchema
