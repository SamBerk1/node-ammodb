'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterPlatfromsRightsSchema extends Schema {
  up () {
    this.table('platfroms_rights', (table) => {
      // alter table
      table.integer('countryId').unsigned().references('id').inTable('territory_countries').onDelete('CASCADE')
    })
  }

  down () {
    this.table('platfroms_rights', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterPlatfromsRightsSchema
