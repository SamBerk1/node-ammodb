'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesCastsSchema extends Schema {
  up () {
    this.create('titles_casts', (table) => {
      //pivot table for many to many realtion in Titles and Casts
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('castId').unsigned().references('id').inTable('casts').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_casts')
  }
}

module.exports = TitlesCastsSchema
