'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitleStatusesSeasonsSchema extends Schema {
  up () {
    this.create('title_statuses_seasons', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('videoStatus')
      table.integer('captions')
      table.integer('subtitles')
      table.integer('artwork')
      table.integer('dubbing')
      table.integer('metadata')
      table.date('deliveryDate')
      table.date('launchDate')
      table.date('dueDate')
      table.boolean('outgoing')
      table.integer('incomingId').unsigned().references('id').inTable('incomings').onDelete('CASCADE')
      table.integer('seasonId').unsigned().references('id').inTable('seasons').onDelete('CASCADE')
      table.integer('incomingSeasonId').unsigned().references('id').inTable('incoming_seasons').onDelete('CASCADE')
      table.integer('alternateTitleId').unsigned().references('id').inTable('alternate_title_languages').onDelete('CASCADE')
      table.integer('seriesId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('platformId').unsigned().references('id').inTable('platforms').onDelete('CASCADE')
      table.unique(['incomingId', 'incomingSeasonId','seriesId','platformId','seasonId'],'uniqueConstraint')
      table.timestamps()
    })
  }

  down () {
    this.drop('title_statuses_seasons')
  }
}

module.exports = TitleStatusesSeasonsSchema
