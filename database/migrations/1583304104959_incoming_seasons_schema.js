'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IncomingSeasonsSchema extends Schema {
  up () {
    this.create('incoming_seasons', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('incomingId').unsigned().references('id').inTable('incomings').onDelete('CASCADE')
      table.integer('seriesId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('seasonId').unsigned().references('id').inTable('seasons').onDelete('CASCADE')
      table.integer('alternateTitleId').unsigned().references('id').inTable('alternate_title_languages').onDelete('CASCADE')
      table.unique(['incomingId','seriesId','seasonId'])
      table.timestamps()
    })
  }

  down () {
    this.drop('incoming_seasons')
  }
}

module.exports = IncomingSeasonsSchema
