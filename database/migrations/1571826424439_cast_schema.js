'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CastSchema extends Schema {
  up () {
    this.create('casts', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('actorName').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('casts')
  }
}

module.exports = CastSchema
