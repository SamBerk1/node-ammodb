'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TrailerSchema extends Schema {
  up () {
    this.create('trailers', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('trailerUrl').unique()
      table.string('name').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('trailers')
  }
}

module.exports = TrailerSchema
