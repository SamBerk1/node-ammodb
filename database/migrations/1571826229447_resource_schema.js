'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResourceSchema extends Schema {
  up () {
    this.create('resources', (table) => {
      //creates table resources
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('name').notNullable().unique() //resource name e.g User,Incoming, Librarie
      table.string('slug').notNullable().unique() // users,incomings, libraries
      table.timestamps()
    })
  }

  down () {
    this.drop('resources')
  }
}

module.exports = ResourceSchema
