'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterIncomingEpisodesSchema extends Schema {
  up () {
    this.table('incoming_episodes', (table) => {
      // alter table
      table.integer('incomingSeasonId').unsigned().references('id').inTable('incoming_seasons').onUpdate('CASCADE')
      table.dropColumn('qc')
      table.dropColumn('dueDate')
    })
  }

  down () {
    this.table('incoming_episodes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterIncomingEpisodesSchema
