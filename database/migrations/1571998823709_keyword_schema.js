'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class KeywordSchema extends Schema {
  up () {
    this.create('keywords', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('keyword').unique()
      table.integer('libraryId').unsigned().references('id').inTable('libraries')
      table.integer('platformId').unsigned().references('id').inTable('platforms')
      table.timestamps()
    })
  }

  down () {
    this.drop('keywords')
  }
}

module.exports = KeywordSchema
