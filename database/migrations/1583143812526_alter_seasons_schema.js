'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterSeasonsSchema extends Schema {
  up () {
    this.table('seasons', (table) => {
      // alter table
      table.integer('alternateTitleId').unsigned().references('id').inTable('alternate_title_languages').onDelete('CASCADE')
      table.string('imdb')
      table.integer('studioId').unsigned().references('id').inTable('studios')
      table.integer('ratingId').unsigned().references('id').inTable('ratings')
      table.dropUnique(['name','seriesId'])
      table.dropUnique(['number','seriesId'])
      table.unique(['name','number','seriesId','alternateTitleId'],'uniqueConstraint')
    })
  }

  down () {
    this.table('seasons', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterSeasonsSchema
