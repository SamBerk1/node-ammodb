'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterUsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      // alter table
      table.boolean('isNew').notNullable().defaultTo(true) //For new account this flag is used to redirect user to set new password
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterUsersSchema
