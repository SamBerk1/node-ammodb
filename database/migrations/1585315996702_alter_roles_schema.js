'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterRolesSchema extends Schema {
  up () {
    this.table('roles', (table) => {
      // alter table
      table.boolean('isStudio').notNullable().defaultTo(false) //0 false, 1 true. This falg is for identifying roles which are client roles and they have access to only their assocaited studio
    })
  }

  down () {
    this.table('roles', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterRolesSchema
