'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudioCutomersSchema extends Schema {
  up () {
    this.create('studio_customers', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
      table.unique(['userId','studioId'])
      table.timestamps()
    })
  }

  down () {
    this.drop('studio_customers')
  }
}

module.exports = StudioCutomersSchema
