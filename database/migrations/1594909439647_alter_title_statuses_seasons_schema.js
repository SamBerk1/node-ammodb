'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesSeasonsSchema extends Schema {
  up () {
    this.table('title_statuses_seasons', (table) => {
      // alter table
      table.integer('titleStatusId').unsigned().references('id').inTable('title_statuses').onDelete('CASCADE')
    })
  }

  down () {
    this.table('title_statuses_seasons', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterTitleStatusesSeasonsSchema
