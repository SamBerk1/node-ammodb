'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterCountriesUniqueSchema extends Schema {
  up () {
    this.table('countries', (table) => {
      // alter table
      table.dropUnique(['name','code'],'unique')
      table.unique(['code'],'uniqueCode')
      
    })
  }

  down () {
    this.table('countries', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterCountriesUniqueSchema
