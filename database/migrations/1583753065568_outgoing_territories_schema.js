'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OutgoingTerritoriesSchema extends Schema {
  up () {
    this.create('outgoing_territories', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('outgoingMovieId').unsigned().references('id').inTable('title_statuses').onDelete('CASCADE')
      table.integer('outgoingSeasonId').unsigned().references('id').inTable('title_statuses_seasons').onDelete('CASCADE')
      table.string('territory')
      table.unique(['outgoingMovieId','territory'])
      table.unique(['outgoingSeasonId','territory'])
      table.timestamps()
    })
  }

  down () {
    this.drop('outgoing_territories')
  }
}

module.exports = OutgoingTerritoriesSchema
