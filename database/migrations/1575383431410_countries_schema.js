'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CountriesSchema extends Schema {
  up () {
    this.create('countries', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string("code"), // coutry iso code e.g USA
      table.string("name") //country name
      table.timestamps()
    })
  }

  down () {
    this.drop('countries')
  }
}

module.exports = CountriesSchema
