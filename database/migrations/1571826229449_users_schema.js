'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.create('users', (table) => {
      //creates table users
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('role_id').unsigned().references('id').inTable('roles').notNullable() //reference of roles table
      table.string('fullname')
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UsersSchema
