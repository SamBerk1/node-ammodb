'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AwardSchema extends Schema {
  up () {
    this.create('awards', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('awardName').unique()
      table.string('awardType')
      table.timestamps()
    })
  }

  down () {
    this.drop('awards')
  }
}

module.exports = AwardSchema
