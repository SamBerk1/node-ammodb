'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterAlternateTitlesStatusesSeasonsSchema extends Schema {
  up () {
    this.table('title_statuses_seasons', (table) => {
      // alter table
      table.dropForeign('incomingId')
      table.dropForeign('seasonId')
      table.dropForeign('incomingSeasonId')
      table.dropForeign('alternateTitleId')
      table.dropForeign('seriesId')
      table.dropForeign('platformId')

      table.dropUnique(['incomingId', 'incomingSeasonId','seriesId','platformId','seasonId'],'uniqueConstraint')

      table.foreign('incomingId').references('incomings.id').onDelete('CASCADE')
      table.foreign('seasonId').references('seasons.id').onDelete('CASCADE')
      table.foreign('incomingSeasonId').references('incoming_seasons.id').onDelete('CASCADE')
      table.foreign('alternateTitleId').references('alternate_title_languages.id').onDelete('CASCADE')
      table.foreign('seriesId').references('libraries.id').onDelete('CASCADE')
      table.foreign('platformId').references('platforms.id').onDelete('CASCADE')
    })
  }

  down () {
    this.table('title_statuses_seasons', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterAlternateTitlesStatusesSeasonsSchema
