'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterKeywordsSchema extends Schema {
  up () {
    this.table('keywords', (table) => {
      // alter table
      table.dropForeign('libraryId')
    })
  }

  down () {
    this.table('keywords', (table) => {
      // reverse alternations
      table.foreign('libraryId').references('libraries.id')
    })
  }
}

module.exports = AlterKeywordsSchema
