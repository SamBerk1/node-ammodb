'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlatformSchema extends Schema {
  up () {
    this.create('platforms', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('name').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('platforms')
  }
}

module.exports = PlatformSchema
