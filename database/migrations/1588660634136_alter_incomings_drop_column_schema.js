'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterIncomingsDropColumnSchema extends Schema {
  up () {
    this.table('incomings', (table) => {
      // alter table
      table.dropColumn('deliveryDate')
      table.dropColumn('launchDate')
    })
  }

  down () {
    this.table('incomings', (table) => {
      // reverse alternations
      table.date('deliveryDate')
      table.date('launchDate')
    })
  }
}

module.exports = AlterIncomingsDropColumnSchema
