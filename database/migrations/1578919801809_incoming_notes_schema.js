'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IncomingNotesSchema extends Schema {
  up () {
    this.create('incoming_notes', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('incomingId').unsigned().references('id').inTable('incomings').onDelete('CASCADE')
      table.integer('noteId').unsigned().references('id').inTable('notes').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('incoming_notes')
  }
}

module.exports = IncomingNotesSchema
