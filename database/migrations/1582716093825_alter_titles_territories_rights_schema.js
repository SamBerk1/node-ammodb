'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitlesTerritoriesRightsSchema extends Schema {
  up () {
    this.table('titles_territories_rights', (table) => {
      // alter table
      table.dropForeign('libraryId');
      table.dropForeign('rightId');
      table.dropForeign('territoryId')
      table.dropUnique(['libraryId', 'rightId','territoryId']);
    })
  }

  down () {
    this.table('titles_territories_rights', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterTitlesTerritoriesRightsSchema
