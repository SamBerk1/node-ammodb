'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterIncomingEpisodesSchema extends Schema {
  up () {
    this.table('incoming_episodes', (table) => {
      // alter table
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
    })
  }

  down () {
    this.table('incoming_episodes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterIncomingEpisodesSchema
