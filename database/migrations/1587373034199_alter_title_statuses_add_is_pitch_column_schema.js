'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesAddIsPitchColumnSchema extends Schema {
  up () {
    this.table('title_statuses', (table) => {
      // alter table
      table.boolean('isPitched').notNullable().defaultTo(false)
    })
  }

  down () {
    this.table('title_statuses', (table) => {
      // reverse alternations
      table.dropColumn('isPitched')
    })
  }
}

module.exports = AlterTitleStatusesAddIsPitchColumnSchema
