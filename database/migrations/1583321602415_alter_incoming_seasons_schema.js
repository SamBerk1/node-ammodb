'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterIncomingSeasonsSchema extends Schema {
  up () {
    this.table('incoming_seasons', (table) => {
      // alter table
      table.integer('videoStatus')
      table.integer('captions')
      table.integer('subtitles')
      table.integer('artwork')
      table.integer('dubbing')
      table.integer('metadata')
      table.date('deliveryDate')
      table.date('launchDate')
    })
  }

  down () {
    this.table('incoming_seasons', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterIncomingSeasonsSchema
