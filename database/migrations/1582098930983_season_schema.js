'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeasonSchema extends Schema {
  up () {
    this.create('seasons', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('number')
      table.string('name')
      table.string('guId')
      table.integer('seriesId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.unique(['name','seriesId'])
      table.unique(['number','seriesId'])
      table.timestamps()
    })
  }

  down () {
    this.drop('seasons')
  }
}

module.exports = SeasonSchema
