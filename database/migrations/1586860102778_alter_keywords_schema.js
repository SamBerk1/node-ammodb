'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterKeywordsSchema extends Schema {
  up () {
    this.table('keywords', (table) => {
      // alter table
      table.dropForeign('platformId')
      table.dropColumn('platformId')
      table.integer('alternateTitleId').unsigned().references('id').inTable('alternate_title_languages').onDelete('CASCADE')
      table.integer('seasonId').unsigned().references('id').inTable('seasons').onDelete('CASCADE')
      table.integer('episodeId').unsigned().references('id').inTable('episodes').onDelete('CASCADE')
    })
  }

  down () {
    this.table('keywords', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterKeywordsSchema
