'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesSchema extends Schema {
  up () {
    this.table('title_statuses', (table) => {
      // alter table
      table.dropForeign('incomingId')
      table.dropForeign('libraryId')
      table.dropForeign('platformId')
      table.dropUnique(['incomingId', 'libraryId','platformId'])
      table.foreign('incomingId').references('id').inTable('incomings').onDelete('CASCADE')
      table.foreign('libraryId').references('id').inTable('libraries').onDelete('CASCADE')
      table.foreign('platformId').references('id').inTable('platforms').onDelete('CASCADE')
    })
  }

  down () {
    this.table('title_statuses', (table) => {
      // reverse alternations
      table.unique(['incomingId', 'libraryId','platformId'])
    })
  }
}

module.exports = AlterTitleStatusesSchema
