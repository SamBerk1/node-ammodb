'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterEpisodesSchema extends Schema {
  up () {
    this.table('episodes', (table) => {
      // alter table
      table.integer('alternateTitleId').unsigned().references('id').inTable('alternate_title_languages').onDelete('CASCADE')
      table.string('imdb')
      table.string('alterSynopsis',200)
      table.integer('studioId').unsigned().references('id').inTable('studios')
      table.integer('ratingId').unsigned().references('id').inTable('ratings')
      table.dropUnique(['number','seriesId','seasonId'])
      table.dropUnique(['name','seriesId','seasonId'])
      table.unique(['name','number','seriesId','seasonId','alternateTitleId'],'uniqueConstraint')
    })
  }

  down () {
    this.table('episodes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterEpisodesSchema
