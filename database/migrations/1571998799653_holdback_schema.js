'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HoldbackSchema extends Schema {
  up () {
    this.create('holdbacks', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('remarks')
      table.integer('territoryId').unsigned().references('id').inTable('territories')
      table.integer('libraryId').unsigned().references('id').inTable('libraries')
      table.integer('platformId').unsigned().references('id').inTable('platforms')
      table.timestamps()
    })
  }

  down () {
    this.drop('holdbacks')
  }
}

module.exports = HoldbackSchema
