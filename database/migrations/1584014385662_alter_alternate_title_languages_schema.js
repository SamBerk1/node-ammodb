'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterAlternateTitleLanguagesSchema extends Schema {
  up () {
    this.table('alternate_title_languages', (table) => {
      // alter table
      table.dropColumn('alterSynopsis')
    })
  }

  down () {
    this.table('alternate_title_languages', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterAlternateTitleLanguagesSchema
