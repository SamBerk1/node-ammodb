'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TerritorySchema extends Schema {
  up () {
    this.create('territories', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('territoryName').unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('territories')
  }
}

module.exports = TerritorySchema
