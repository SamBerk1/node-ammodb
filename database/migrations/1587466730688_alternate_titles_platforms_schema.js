'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlternateTitlesPlatformsSchema extends Schema {
  up () {
    this.create('alternate_titles_platforms', (table) => {
      //create table for resource pitch tracker
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.boolean('status').notNullable().defaultTo(false) // [{name:'Pitched',value:0},{name:'Rejected',value:1},{name:'Accepted',value:2},{name:'Under Review',value:3}]
      table.date('pitchDate')
      table.integer('platformId').unsigned().references('id').inTable('platforms').onDelete('CASCADE')
      table.integer('alternateTitleId').unsigned().references('id').inTable('alternate_title_languages').onDelete('CASCADE')
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('alternate_titles_platforms')
  }
}

module.exports = AlternateTitlesPlatformsSchema
