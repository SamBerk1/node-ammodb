'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterDropUniqueAlternateTitleLanguagesSchema extends Schema {
  up () {
    this.table('alternate_title_languages', (table) => {
      // alter table
      table.dropForeign('libraryId')
      table.dropForeign('languageId')
      table.dropUnique(['libraryId','languageId'])
    })
  }

  down () {
    this.table('alternate_title_languages', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterDropUniqueAlternateTitleLanguagesSchema
