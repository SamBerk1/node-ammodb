'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterAlternateTitleLanguagesSchema extends Schema {
  up () {
    this.table('alternate_title_languages', (table) => {
      // alter table
      table.unique(['guId'])
    })
  }

  down () {
    this.table('alternate_title_languages', (table) => {
      // reverse alternations
      table.dropUnique(['guId'])
    })
  }
}

module.exports = AlterAlternateTitleLanguagesSchema
