'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdBreakSchema extends Schema {
  up () {
    this.create('ad_breaks', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.string('time')
      table.string('timeFormated')
      table.string('adbreakFormat')
      table.integer('libraryId').unsigned().references('id').inTable('libraries')
      table.integer('platformId').unsigned().references('id').inTable('platforms')
      table.timestamps()
    })
  }

  down () {
    this.drop('ad_breaks')
  }
}

module.exports = AdBreakSchema
