'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterCustomAssetsIncomingsSchema extends Schema {
  up () {
    this.table('custom_assets_incomings', (table) => {
      // alter table
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('studioId').unsigned().references('id').inTable('studios').onDelete('CASCADE')
    })
  }

  down () {
    this.table('custom_assets_incomings', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterCustomAssetsIncomingsSchema
