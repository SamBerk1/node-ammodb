'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterSeasonsSchema extends Schema {
  up () {
    this.table('seasons', (table) => {
      // alter table
      table.integer('userId').unsigned().references('id').inTable('users').onDelete('CASCADE')
    })
  }

  down () {
    this.table('seasons', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterSeasonsSchema
