'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EpisodeSchema extends Schema {
  up () {
    this.create('episodes', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('number')
      table.string('name')
      table.string('guId')
      table.integer('seasonId').unsigned().references('id').inTable('seasons').onDelete('CASCADE')
      table.integer('seriesId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.unique(['number','seriesId','seasonId'])
      table.unique(['name','seriesId','seasonId'])
      table.timestamps()
    })
  }

  down () {
    this.drop('episodes')
  }
}

module.exports = EpisodeSchema
