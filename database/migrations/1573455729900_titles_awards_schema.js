'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesAwardsSchema extends Schema {
  up () {
    this.create('titles_awards', (table) => {
      //pivot table for many to many realtion in Titles and Awards
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('awardId').unsigned().references('id').inTable('awards').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_awards')
  }
}

module.exports = TitlesAwardsSchema
