'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterAlternateTitlesPlatformsSchema extends Schema {
  up () {
    this.table('alternate_titles_platforms', (table) => {
      // alter table
      table.dropForeign('alternateTitleId')
      table.dropForeign('platformId')
      table.dropUnique(['alternateTitleId', 'platformId'],'alternateName_platform_unique')
      table.dropColumn('alternateTitleId')
      table.dropColumn('platformId')
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.string('platform')
      table.unique(['libraryId', 'platform'],'unique')
    })
  }

  down () {
    this.table('alternate_titles_platforms', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterAlternateTitlesPlatformsSchema
