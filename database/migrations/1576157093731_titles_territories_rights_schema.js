'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesTerritoriesRightsSchema extends Schema {
  up () {
    this.create('titles_territories_rights', (table) => {
      //pivot table for many to many realtion in Titles,Territories and Rights
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('rightId').unsigned().references('id').inTable('rights').onDelete('CASCADE')
      table.integer('territoryId').unsigned().references('id').inTable('territories').onDelete('CASCADE')
      table.unique(['libraryId','rightId','territoryId'])
      table.date('startDate');
      table.date('endDate');
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_territories_rights')
  }
}

module.exports = TitlesTerritoriesRightsSchema
