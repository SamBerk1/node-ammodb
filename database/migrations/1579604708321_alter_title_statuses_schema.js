'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesSchema extends Schema {
  up () {
    this.table('title_statuses', (table) => {
      // alter table
      table.dropForeign('platformId');
      table.dropColumn('platformId');
      table.integer('platform_rightsId').unsigned().references('id').inTable('platfroms_rights').onDelete('CASCADE');
    })
  }

  down () {
    this.table('title_statuses', (table) => {
      // reverse alternations
      table.dropForeign('platform_rightsId');
      table.dropColumn('platform_rightsId')
    })
  }
}

module.exports = AlterTitleStatusesSchema
