'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterLibrariesAddColumnDurationSchema extends Schema {
  up () {
    this.table('libraries', (table) => {
      // alter table
      table.string('duration')
    })
  }

  down () {
    this.table('libraries', (table) => {
      // reverse alternations
      table.dropColumn('duration')
    })
  }
}

module.exports = AlterLibrariesAddColumnDurationSchema
