'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterStudioSchema extends Schema {
  up () {
    this.table('studios', (table) => {
      // alter table
      table.dropColumn('contactNumber')
    })
  }

  down () {
    this.table('studios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterStudioSchema
