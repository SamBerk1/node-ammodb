'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ExportTokenSchema extends Schema {
  up () {
    this.create('export_tokens', (table) => {
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.uuid('token')
      table.string('batch')
      table.timestamps()
    })
  }

  down () {
    this.drop('export_tokens')
  }
}

module.exports = ExportTokenSchema
