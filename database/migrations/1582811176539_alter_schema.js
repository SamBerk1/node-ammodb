'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterSchema extends Schema {
  up () {
    this.table('title_statuses', (table) => {
      // alter table
      table.dropForeign('libraryId');
      table.dropForeign('incomingId');
      table.dropForeign('platform_rightsId');
      table.dropUnique(['incomingId', 'libraryId','platform_rightsId']);
      table.dropColumn('platform_rightsId');
      table.foreign('libraryId').references('id').inTable('libraries').onDelete('CASCADE')
      table.foreign('incomingId').references('id').inTable('incomings').onDelete('CASCADE')
      table.integer('platformId').unsigned().references('id').inTable('platforms').onDelete('CASCADE')
      table.unique(['incomingId','libraryId','platformId'])
    })
  }

  down () {
    this.table('title_statuses', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterSchema
