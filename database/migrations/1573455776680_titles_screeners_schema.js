'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TitlesScreenersSchema extends Schema {
  up () {
    this.create('titles_screeners', (table) => {
      //pivot table for many to many realtion in Titles and Screener
      table.engine('InnoDB') //set database Engine InnoDB
      table.increments()
      table.integer('libraryId').unsigned().references('id').inTable('libraries').onDelete('CASCADE')
      table.integer('screenerId').unsigned().references('id').inTable('screeners').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('titles_screeners')
  }
}

module.exports = TitlesScreenersSchema
