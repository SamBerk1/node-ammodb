'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterTitleStatusesSchema extends Schema {
  up () {
    this.table('title_statuses', (table) => {
      // alter table
      table.dropForeign('libraryId');
      table.dropForeign('incomingId');
      table.dropUnique(['incomingId', 'libraryId','platformId']);
      table.foreign('libraryId').references('id').inTable('libraries').onDelete('CASCADE')
      table.foreign('incomingId').references('id').inTable('incomings').onDelete('CASCADE')
      table.unique(['incomingId','libraryId','platform_rightsId'])
    })
  }

  down () {
    this.table('title_statuses', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterTitleStatusesSchema
