'use strict'

/*
|--------------------------------------------------------------------------
| AdBasedStreamingSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class AdBasedStreamingSeeder {
  static  async run () {
    let rights = [
      {name:'Ad-Based Streaming'}
      ]
  
    await Factory.model('App/Models/Right').createMany(rights.length,rights)
  }
}

module.exports = AdBasedStreamingSeeder
