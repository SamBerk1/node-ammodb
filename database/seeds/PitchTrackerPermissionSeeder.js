'use strict'

/*
|--------------------------------------------------------------------------
| PitchTrackerPermissionSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Resource = use('Cerberus/Models/Resource')
const Role = use('App/Models/Role')

class PitchTrackerPermissionSeeder {
  static  async run () {
    let resources = ['pitchtracker']
    await Factory.model('Cerberus/Models/Resource').createMany(resources.length,resources)   
        
    let adminPermissions = [
      {
        "resource_id": (await Resource.findBy({slug:'pitchtracker'})).id,
        "role_id": (await Role.findBy({slug:'admin'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "1"
      }
    ]

    let managerPermissions = [
      {
        "resource_id": (await Resource.findBy({slug:'pitchtracker'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "0",
        "read": "1",
        "update": "0",
        "delete": "0"
      }
    ]

    let userPermissions = [
      {
        "resource_id": (await Resource.findBy({slug:'pitchtracker'})).id,
        "role_id": (await Role.findBy({slug:'user'})).id,
        "create": "0",
        "read": "1",
        "update": "0",
        "delete": "0"
      }
    ]

    let managerClientPermissions = [
      {
        "resource_id": (await Resource.findBy({slug:'pitchtracker'})).id,
        "role_id": (await Role.findBy({slug:'client_manager'})).id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      }
    ]

    let userClientPermissions = [
      {
        "resource_id": (await Resource.findBy({slug:'pitchtracker'})).id,
        "role_id": (await Role.findBy({slug:'client_user'})).id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      }
    ]

    await Factory.model('Cerberus/Models/Permission').createMany(adminPermissions.length,adminPermissions)
    await Factory.model('Cerberus/Models/Permission').createMany(managerPermissions.length,managerPermissions)
    await Factory.model('Cerberus/Models/Permission').createMany(userPermissions.length,userPermissions)
    await Factory.model('Cerberus/Models/Permission').createMany(managerClientPermissions.length,managerClientPermissions)
    await Factory.model('Cerberus/Models/Permission').createMany(userClientPermissions.length,userClientPermissions)

  }
}

module.exports = PitchTrackerPermissionSeeder
