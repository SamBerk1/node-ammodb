'use strict'

/*
|--------------------------------------------------------------------------
| UserCustomerPermissionSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Resource = use('Cerberus/Models/Resource')
const Role = use('App/Models/Role')

class UserCustomerPermissionSeeder {
  static  async run () {
    let role = await Role.findBy('slug','customer')
    role.slug = "client_user"
    role.name = 'Client User'
    role.isStudio = 1
    await role.save()

    await role.permissions().delete()

    let customerUserPermissions = [
      {
        "resource_id": (await Resource.findBy({slug:'financial'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'library'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'incoming'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "1",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'outgoing'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "1",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'projectManagement'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'customers'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'roles'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'permissions'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'users'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'activityLog'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'others'})).id,
        "role_id": role.id,
        "create": "0",
        "read": "0",
        "update": "0",
        "delete": "0"
      }
    ]

    await Factory.model('Cerberus/Models/Permission').createMany(customerUserPermissions.length,customerUserPermissions)
  }
}

module.exports = UserCustomerPermissionSeeder
