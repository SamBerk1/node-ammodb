'use strict'

/*
|--------------------------------------------------------------------------
| PlatformRightSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Platform = use('App/Models/Platform')
const PlatfromsRight = use('App/Models/PlatfromsRight')
const Territory = use('App/Models/Territory')
const Right = use('App/Models/Right')

class PlatformRightSeeder {
  static  async run () {


    let platformsData = [
      'Amazon Prime',
      'Comcast',
      'Here TV',
      'Hoopla',
      'Indemand',
      'Itunes',
      'Kanopy',
      'Mubi',
      'Out TV',
      'pluto TV',
      'Redbox (Physical)',
      'Redbox (Digital)',
      'Roku Channel',
      'Showtime',
      'Starz',
      'Tubi',
      'Vubiquity',
      'Vudu (movies on us)',
      'Vudu (new releases)',
      'Xbox',
      'Xumo',
      'Imdb TV'
    ]



    await Factory.model('App/Models/Platform').createMany(platformsData.length,platformsData)

    let globalTerritory = await Territory.findBy('territoryName','World Wide')


    let amazonPrime = await Platform.findBy('name','Amazon Prime')
    let amzonPrimeRights = [
      {platformId:amazonPrime.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','SVOD')).id},
      {platformId:amazonPrime.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','EST')).id},
      {platformId:amazonPrime.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','TVOD')).id}
    ]


    let Comcast = await Platform.findBy('name','Comcast')
    let ComcastRights = [
      {platformId:Comcast.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','EST')).id},
      {platformId:Comcast.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','PPV')).id},
      {platformId:Comcast.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','TVOD')).id}
    ]

    let Here_TV = await Platform.findBy('name','Here TV')
    let Here_TVRights = [
      {platformId:Here_TV.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','SVOD')).id}
    ]


    let Hoopla = await Platform.findBy('name','Hoopla')
    let HooplaRights = [
      {platformId:Hoopla.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','TVOD')).id}
    ]


    let Indemand = await Platform.findBy('name','Indemand')
    let IndemandRights = [
      {platformId:Indemand.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','PPV')).id}
    ]

    let Itunes = await Platform.findBy('name','Itunes')
    let ItunesRights = [
      {platformId:Itunes.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','EST')).id},
      {platformId:Itunes.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','TVOD')).id}
    ]

    let Kanopy = await Platform.findBy('name','Kanopy')
    let KanopyRights = [
      {platformId:Kanopy.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','Non-theatrical')).id}
    ]

    let Mubi = await Platform.findBy('name','Mubi')
    let MubiRights = [
      {platformId:Mubi.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','SVOD')).id}
    ]
    

    let out_TV = await Platform.findBy('name','Out TV')
    let out_TVRights = [
      {platformId:out_TV.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','SVOD')).id}
    ]


    let pluto_TV = await Platform.findBy('name','pluto TV')
    let pluto_TVRights = [
      {platformId:pluto_TV.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','AVOD')).id}
    ]


    let Redbox_Physical = await Platform.findBy('name','Redbox (Physical)')
    let Redbox_PhysicalRights = [
      {platformId:Redbox_Physical.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','DVD')).id}
    ]


    let Redbox_Digital = await Platform.findBy('name','Redbox (Digital)')
    let Redbox_DigitalRights = [
      {platformId:Redbox_Digital.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','AVOD')).id},
      {platformId:Redbox_Digital.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','EST')).id},
      {platformId:Redbox_Digital.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','TVOD')).id}
    ]

    
    let Roku_Channel = await Platform.findBy('name','Roku Channel')
    let Roku_ChannelRights = [
      {platformId:Roku_Channel.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','AVOD')).id}
    ]


    let Showtime = await Platform.findBy('name','Showtime')
    let ShowtimeRights = [
      {platformId:Showtime.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','PTV')).id},
      {platformId:Showtime.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','SVOD')).id}
    ]


    let Starz = await Platform.findBy('name','Starz')
    let StarzRights = [
      {platformId:Starz.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','PTV')).id},
      {platformId:Starz.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','SVOD')).id}
    ]

    let Tubi = await Platform.findBy('name','Tubi')
    let TubiRights = [
      {platformId:Tubi.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','AVOD')).id}
    ]


    let Vubiquity = await Platform.findBy('name','Vubiquity')
    let VubiquityRights = [
      {platformId:Vubiquity.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','EST')).id},
      {platformId:Vubiquity.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','PPV')).id},
      {platformId:Vubiquity.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','TVOD')).id}
    ]


    let Vudu_movies_on_us = await Platform.findBy('name','Vudu (movies on us)')
    let Vudu_movies_on_usRights = [
      {platformId:Vudu_movies_on_us.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','AVOD')).id}
    ]

    let Vudu_new_releases = await Platform.findBy('name','Vudu (new releases)')
    let Vudu_new_releasesRights = [
      {platformId:Vudu_new_releases.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','EST')).id},
      {platformId:Vudu_new_releases.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','TVOD')).id}
    ]


    let Xbox = await Platform.findBy('name','Xbox')
    let XboxRights = [
      {platformId:Xbox.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','EST')).id},
      {platformId:Xbox.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','TVOD')).id}
    ]


    let Xumo = await Platform.findBy('name','Xumo')
    let XumoRights = [
      {platformId:Xumo.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','AVOD')).id}
    ]

    let Imdb_TV = await Platform.findBy('name','Imdb TV')
    let Imdb_TVRights = [
      {platformId:Imdb_TV.id,territoryId:globalTerritory.id,rightId: (await Right.findBy('name','AVOD')).id}
    ]


    let platfromsRights = amzonPrimeRights.concat(ComcastRights).concat(Here_TVRights).concat(HooplaRights).concat(IndemandRights).concat(ItunesRights).concat(KanopyRights).concat(MubiRights).concat(out_TVRights).concat(pluto_TVRights).concat(Redbox_DigitalRights).concat(Redbox_PhysicalRights).concat(Roku_ChannelRights).concat(ShowtimeRights).concat(StarzRights).concat(TubiRights).concat(VubiquityRights).concat(Vudu_movies_on_usRights).concat(Vudu_new_releasesRights).concat(XboxRights).concat(XumoRights).concat(Imdb_TVRights)                   
    await Factory.model('App/Models/PlatfromsRight').createMany(platfromsRights.length,platfromsRights)


  }
}

module.exports = PlatformRightSeeder
