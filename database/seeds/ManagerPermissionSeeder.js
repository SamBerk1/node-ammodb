'use strict'

/*
|--------------------------------------------------------------------------
| ManagerPermissionSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Resource = use('Cerberus/Models/Resource')
const Role = use('App/Models/Role')

class ManagerPermissionSeeder {
  static  async run () {
    let roleData = [{role:'Manager',slug:'manager'}]

    await Factory.model('App/Models/Role').createMany(roleData.length,roleData)

    let managerPermissions = [
      {
        "resource_id": (await Resource.findBy({slug:'financial'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'library'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'incoming'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'outgoing'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'projectManagement'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'customers'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'roles'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'permissions'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'users'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'activityLog'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      },
      {
        "resource_id": (await Resource.findBy({slug:'others'})).id,
        "role_id": (await Role.findBy({slug:'manager'})).id,
        "create": "1",
        "read": "1",
        "update": "1",
        "delete": "0"
      }
    ]

    await Factory.model('Cerberus/Models/Permission').createMany(managerPermissions.length,managerPermissions)

  }
}

module.exports = ManagerPermissionSeeder
