'use strict'

/*
|--------------------------------------------------------------------------
| DbSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Resource = use('Cerberus/Models/Resource')
const Role = use('App/Models/Role')

class DbSeeder {
    static  async run () {
    let roleData = [{role:'Administrator',slug:'admin'},
                    {role:'Customer',slug:'customer'}
                    ]

  await Factory.model('App/Models/Role').createMany(roleData.length,roleData)

  await Factory.model('App/Models/User').create({role_id:(await Role.findBy({slug:'admin'})).id,email:'superadmin@test.com',password:'test$uperAdmin123'})
  let customer = await Factory.model('App/Models/User').create({role_id:(await Role.findBy({slug:'customer'})).id,email:'jhondoe@email.com',password:'12345678'})


  let studio = await Factory.model('App/Models/Studio').create({ title : 'DefaultStudio_' + customer.id,})
  await Factory.model('App/Models/StudioCustomer').create({ userId : customer.id, studioId : studio.id})
  

  let resources = ['financial','library','incoming','outgoing','projectManagement','customers','roles','permissions','users','activityLog','others']

  await Factory.model('Cerberus/Models/Resource').createMany(resources.length,resources)    

  let adminPermissions = [
    {
      "resource_id": (await Resource.findBy({slug:'financial'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'library'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'incoming'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'outgoing'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'projectManagement'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'customers'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'roles'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'permissions'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'users'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'activityLog'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'others'})).id,
      "role_id": (await Role.findBy({slug:'admin'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    }
  ]

  let customerPermissions = [
    {
      "resource_id": (await Resource.findBy({slug:'financial'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'library'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'incoming'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'outgoing'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    },
    {
      "resource_id": (await Resource.findBy({slug:'projectManagement'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "0",
      "read": "0",
      "update": "0",
      "delete": "0"
    },
    {
      "resource_id": (await Resource.findBy({slug:'customers'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "0",
      "read": "0",
      "update": "0",
      "delete": "0"
    },
    {
      "resource_id": (await Resource.findBy({slug:'roles'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "0",
      "read": "0",
      "update": "0",
      "delete": "0"
    },
    {
      "resource_id": (await Resource.findBy({slug:'permissions'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "0",
      "read": "0",
      "update": "0",
      "delete": "0"
    },
    {
      "resource_id": (await Resource.findBy({slug:'users'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "0",
      "read": "0",
      "update": "0",
      "delete": "0"
    },
    {
      "resource_id": (await Resource.findBy({slug:'activityLog'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "0",
      "read": "0",
      "update": "0",
      "delete": "0"
    },
    {
      "resource_id": (await Resource.findBy({slug:'others'})).id,
      "role_id": (await Role.findBy({slug:'customer'})).id,
      "create": "1",
      "read": "1",
      "update": "1",
      "delete": "1"
    }
  ]

  await Factory.model('Cerberus/Models/Permission').createMany(adminPermissions.length,adminPermissions)
  await Factory.model('Cerberus/Models/Permission').createMany(customerPermissions.length,customerPermissions)

  let isoLangs = [
    {"code":"ab",
    "name":"Abkhaz"
},
    {"code":"aa",
    "name":"Afar"
},
    {"code":"af",
    "name":"Afrikaans"
},
    {"code":"ak",
    "name":"Akan"
},
    {"code":"sq",
    "name":"Albanian"
},
    {"code":"am",
    "name":"Amharic"
},
    {"code":"ar",
    "name":"Arabic"
},
    {"code":"an",
    "name":"Aragonese"
},
    {"code":"hy",
    "name":"Armenian"
},
    {"code":"as",
    "name":"Assamese"
},
    {"code":"av",
    "name":"Avaric"
},
    {"code":"ae",
    "name":"Avestan"
},
    {"code":"ay",
    "name":"Aymara"
},
    {"code":"az",
    "name":"Azerbaijani"
},
    {"code":"bm",
    "name":"Bambara"
},
    {"code":"ba",
    "name":"Bashkir"
},
    {"code":"eu",
    "name":"Basque"
},
    {"code":"be",
    "name":"Belarusian"
},
    {"code":"bn",
    "name":"Bengali"
},
    {"code":"bh",
    "name":"Bihari"
},
    {"code":"bi",
    "name":"Bislama"
},
    {"code":"bs",
    "name":"Bosnian"
},
    {"code":"br",
    "name":"Breton"
},
    {"code":"bg",
    "name":"Bulgarian"
},
    {"code":"my",
    "name":"Burmese"
},
    {"code":"ca",
    "name":"Catalan; Valencian"
},
    {"code":"ch",
    "name":"Chamorro"
},
    {"code":"ce",
    "name":"Chechen"
},
    {"code":"ny",
    "name":"Chichewa; Chewa; Nyanja"
},
    {"code":"zh",
    "name":"Chinese"
},
    {"code":"cv",
    "name":"Chuvash"
},
    {"code":"kw",
    "name":"Cornish"
},
    {"code":"co",
    "name":"Corsican"
},
    {"code":"cr",
    "name":"Cree"
},
    {"code":"hr",
    "name":"Croatian"
},
    {"code":"cs",
    "name":"Czech"
},
    {"code":"da",
    "name":"Danish"
},
    {"code":"dv",
    "name":"Divehi; Dhivehi; Maldivian;"
},
    {"code":"nl",
    "name":"Dutch"
},
    {"code":"en",
    "name":"English"
},
    {"code":"eo",
    "name":"Esperanto"
},
    {"code":"et",
    "name":"Estonian"
},
    {"code":"ee",
    "name":"Ewe"
},
    {"code":"fo",
    "name":"Faroese"
},
    {"code":"fj",
    "name":"Fijian"
},
    {"code":"fi",
    "name":"Finnish"
},
    {"code":"fr",
    "name":"French"
},
    {"code":"ff",
    "name":"Fula; Fulah; Pulaar; Pular"
},
    {"code":"gl",
    "name":"Galician"
},
    {"code":"ka",
    "name":"Georgian"
},
    {"code":"de",
    "name":"German"
},
    {"code":"el",
    "name":"Greek, Modern"
},
    {"code":"gn",
    "name":"Guaraní"
},
    {"code":"gu",
    "name":"Gujarati"
},
    {"code":"ht",
    "name":"Haitian; Haitian Creole"
},
    {"code":"ha",
    "name":"Hausa"
},
    {"code":"he",
    "name":"Hebrew (modern)"
},
    {"code":"hz",
    "name":"Herero"
},
    {"code":"hi",
    "name":"Hindi"
},
    {"code":"ho",
    "name":"Hiri Motu"
},
    {"code":"hu",
    "name":"Hungarian"
},
    {"code":"ia",
    "name":"Interlingua"
},
    {"code":"id",
    "name":"Indonesian"
},
    {"code":"ie",
    "name":"Interlingue"
},
    {"code":"ga",
    "name":"Irish"
},
    {"code":"ig",
    "name":"Igbo"
},
    {"code":"ik",
    "name":"Inupiaq"
},
    {"code":"io",
    "name":"Ido"
},
    {"code":"is",
    "name":"Icelandic"
},
    {"code":"it",
    "name":"Italian"
},
    {"code":"iu",
    "name":"Inuktitut"
},
    {"code":"ja",
    "name":"Japanese"
},
    {"code":"jv",
    "name":"Javanese"
},
    {"code":"kl",
    "name":"Kalaallisut, Greenlandic"
},
    {"code":"kn",
    "name":"Kannada"
},
    {"code":"kr",
    "name":"Kanuri"
},
    {"code":"ks",
    "name":"Kashmiri"
},
    {"code":"kk",
    "name":"Kazakh"
},
    {"code":"km",
    "name":"Khmer"
},
    {"code":"ki",
    "name":"Kikuyu, Gikuyu"
},
    {"code":"rw",
    "name":"Kinyarwanda"
},
    {"code":"ky",
    "name":"Kirghiz, Kyrgyz"
},
    {"code":"kv",
    "name":"Komi"
},
    {"code":"kg",
    "name":"Kongo"
},
    {"code":"ko",
    "name":"Korean"
},
    {"code":"ku",
    "name":"Kurdish"
},
    {"code":"kj",
    "name":"Kwanyama, Kuanyama"
},
    {"code":"la",
    "name":"Latin"
},
    {"code":"lb",
    "name":"Luxembourgish, Letzeburgesch"
},
    {"code":"lg",
    "name":"Luganda"
},
    {"code":"li",
    "name":"Limburgish, Limburgan, Limburger"
},
    {"code":"ln",
    "name":"Lingala"
},  
    {
    "code":"latam",
    "name":"Latin American"
},
    {"code":"lo",
    "name":"Lao"
},
    {"code":"lt",
    "name":"Lithuanian"
},
    {"code":"lu",
    "name":"Luba-Katanga"
},
    {"code":"lv",
    "name":"Latvian"
},
    {"code":"gv",
    "name":"Manx"
},
    {"code":"mk",
    "name":"Macedonian"
},
    {"code":"mg",
    "name":"Malagasy"
},
    {"code":"ms",
    "name":"Malay"
},
    {"code":"ml",
    "name":"Malayalam"
},
    {"code":"mt",
    "name":"Maltese"
},
    {"code":"mi",
    "name":"Maori"
},
    {"code":"mr",
    "name":"Marathi (Marathi)"
},
    {"code":"mh",
    "name":"Marshallese"
},
    {"code":"mn",
    "name":"Mongolian"
},
    {"code":"na",
    "name":"Nauru"
},
    {"code":"nv",
    "name":"Navajo, Navaho"
},
    {"code":"nb",
    "name":"Norwegian Bokmal"
},
    {"code":"nd",
    "name":"North Ndebele"
},
    {"code":"ne",
    "name":"Nepali"
},
    {"code":"ng",
    "name":"Ndonga"
},
    {"code":"nn",
    "name":"Norwegian Nynorsk"
},
    {"code":"no",
    "name":"Norwegian"
},
    {"code":"ii",
    "name":"Nuosu"
},
    {"code":"nr",
    "name":"South Ndebele"
},
    {"code":"oc",
    "name":"Occitan"
},
    {"code":"oj",
    "name":"Ojibwe, Ojibwa"
},
    {"code":"cu",
    "name":"Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic"
},
    {"code":"om",
    "name":"Oromo"
},
    {"code":"or",
    "name":"Oriya"
},
    {"code":"os",
    "name":"Ossetian, Ossetic"
},
    {"code":"pa",
    "name":"Panjabi, Punjabi"
},
    {"code":"pi",
    "name":"Pali"
},
    {"code":"fa",
    "name":"Persian"
},
    {"code":"pl",
    "name":"Polish"
},
    {"code":"ps",
    "name":"Pashto, Pushto"
},
    {"code":"pt",
    "name":"Portuguese"
},
    {"code":"qu",
    "name":"Quechua"
},
    {"code":"rm",
    "name":"Romansh"
},
    {"code":"rn",
    "name":"Kirundi"
},
    {"code":"ro",
    "name":"Romanian, Moldavian, Moldovan"
},
    {"code":"ru",
    "name":"Russian"
},
    {"code":"sa",
    "name":"Sanskrit"
},
    {"code":"sc",
    "name":"Sardinian"
},
    {"code":"sd",
    "name":"Sindhi"
},
    {"code":"se",
    "name":"Northern Sami"
},
    {"code":"sm",
    "name":"Samoan"
},
    {"code":"sg",
    "name":"Sango"
},
    {"code":"sr",
    "name":"Serbian"
},
    {"code":"gd",
    "name":"Scottish Gaelic; Gaelic"
},
    {"code":"sn",
    "name":"Shona"
},
    {"code":"si",
    "name":"Sinhala, Sinhalese"
},
    {"code":"sk",
    "name":"Slovak"
},
    {"code":"sl",
    "name":"Slovene"
},
    {"code":"so",
    "name":"Somali"
},
    {"code":"st",
    "name":"Southern Sotho"
},
    {"code":"es",
    "name":"Spanish"
},
    {"code":"su",
    "name":"Sundanese"
},
    {"code":"sw",
    "name":"Swahili"
},
    {"code":"ss",
    "name":"Swati"
},
    {"code":"sv",
    "name":"Swedish"
},
    {"code":"ta",
    "name":"Tamil"
},
    {"code":"te",
    "name":"Telugu"
},
    {"code":"tg",
    "name":"Tajik"
},
    {"code":"th",
    "name":"Thai"
},
    {"code":"ti",
    "name":"Tigrinya"
},
    {"code":"bo",
    "name":"Tibetan Standard, Tibetan, Central"
},
    {"code":"tk",
    "name":"Turkmen"
},
    {"code":"tl",
    "name":"Tagalog"
},
    {"code":"tn",
    "name":"Tswana"
},
    {"code":"to",
    "name":"Tonga (Tonga Islands)"
},
    {"code":"tr",
    "name":"Turkish"
},
    {"code":"ts",
    "name":"Tsonga"
},
    {"code":"tt",
    "name":"Tatar"
},
    {"code":"tw",
    "name":"Twi"
},
    {"code":"ty",
    "name":"Tahitian"
},
    {"code":"ug",
    "name":"Uighur, Uyghur"
},
    {"code":"uk",
    "name":"Ukrainian"
},
    {"code":"ur",
    "name":"Urdu"
},
    {"code":"uz",
    "name":"Uzbek"
},
    {"code":"ve",
    "name":"Venda"
},
    {"code":"vi",        
    "name":"Vietnamese"
},
    {"code":"vo",
    "name":"Volapük"
},
    {"code":"wa",
    "name":"Walloon"
},
    {"code":"cy",
    "name":"Welsh"
},
    {"code":"wo",
    "name":"Wolof"
},
    {"code":"fy",
    "name":"Western Frisian"
},
    {"code":"xh",
    "name":"Xhosa"
},
    {"code":"yi",
    "name":"Yiddish"
},
    {"code":"yo",
    "name":"Yoruba"
},
    {"code":"za",
    "name":"Zhuang, Chuang"
}
]

  await Factory.model('App/Models/Language').createMany(isoLangs.length,isoLangs)

  let genres = [
    "Action",
    "Adventure",
    "Animated",
    "Biography",
    "Comedy",
    "Crime",
    "Dance",
    "Disaster",
    "Documentary",
    "Drama",
    "Erotic",
    "Family",
    "Fantasy",
    "Found Footage",
    "Historical",
    "Horror",
    "Independent",
    "Legal",
    "Live Action",
    "Martial Arts",
    "Musical",
    "Mystery",
    "Noir",
    "Performance",
    "Political",
    "Romance",
    "Satire",
    "Science Fiction",
    "Short",
    "Silent",
    "Slasher",
    "Sports",
    "Spy",
    "Superhero",
    "Supernatural",
    "Suspense",
    "Teen",
    "Thriller",
    "War",
    "Western"
]
  await Factory.model('App/Models/Genre').createMany(genres.length,genres)

  let countries = [
    {code:"BD", name : "Bangladesh"},
    {code:"BE", name : "Belgium"},
    {code:"BF", name : "Burkina Faso"},
    {code:"BG", name : "Bulgaria"},
    {code:"BA", name : "Bosnia and Herzegovina"},
    {code:"BB", name : "Barbados"},
    {code:"WF", name : "Wallis and Futuna"},
    {code:"BL", name : "Saint Barthelemy"},
    {code:"BM", name : "Bermuda"},
    {code:"BN", name : "Brunei"},
    {code:"BO", name : "Bolivia"},
    {code:"BH", name : "Bahrain"},
    {code:"BI", name : "Burundi"},
    {code:"BJ", name : "Benin"},
    {code:"BT", name : "Bhutan"},
    {code:"JM", name : "Jamaica"},
    {code:"BV", name : "Bouvet Island"},
    {code:"BW", name : "Botswana"},
    {code:"WS", name : "Samoa"},
    {code:"BQ", name : "Bonaire Saint Eustatius and Saba"},
    {code:"BR", name : "Brazil"},
    {code:"BS", name : "Bahamas"},
    {code:"JE", name : "Jersey"},
    {code:"BY", name : "Belarus"},
    {code:"BZ", name : "Belize"},
    {code:"RU", name : "Russia"},
    {code:"RW", name : "Rwanda"},
    {code:"RS", name : "Serbia"},
    {code:"TL", name : "East Timor"},
    {code:"RE", name : "Reunion"},
    {code:"TM", name : "Turkmenistan"},
    {code:"TJ", name : "Tajikistan"},
    {code:"RO", name : "Romania"},
    {code:"TK", name : "Tokelau"},
    {code:"GW", name : "Guinea-Bissau"},
    {code:"GU", name : "Guam"},
    {code:"GT", name : "Guatemala"},
    {code:"GS", name : "South Georgia and the South Sandwich Islands"},
    {code:"GR", name : "Greece"},
    {code:"GQ", name : "Equatorial Guinea"},
    {code:"GP", name : "Guadeloupe"},
    {code:"JP", name : "Japan"},
    {code:"GY", name : "Guyana"},
    {code:"GG", name : "Guernsey"},
    {code:"GF", name : "French Guiana"},
    {code:"GE", name : "Georgia"},
    {code:"GD", name : "Grenada"},
    {code:"GB", name : "United Kingdom"},
    {code:"GA", name : "Gabon"},
    {code:"SV", name : "El Salvador"},
    {code:"GN", name : "Guinea"},
    {code:"GM", name : "Gambia"},
    {code:"GL", name : "Greenland"},
    {code:"GI", name : "Gibraltar"},
    {code:"GH", name : "Ghana"},
    {code:"OM", name : "Oman"},
    {code:"TN", name : "Tunisia"},
    {code:"JO", name : "Jordan"},
    {code:"HR", name : "Croatia"},
    {code:"HT", name : "Haiti"},
    {code:"HU", name : "Hungary"},
    {code:"HK", name : "Hong Kong"},
    {code:"HN", name : "Honduras"},
    {code:"HM", name : "Heard Island and McDonald Islands"},
    {code:"VE", name : "Venezuela"},
    {code:"PR", name : "Puerto Rico"},
    {code:"PS", name : "Palestinian Territory"},
    {code:"PW", name : "Palau"},
    {code:"PT", name : "Portugal"},
    {code:"SJ", name : "Svalbard and Jan Mayen"},
    {code:"PY", name : "Paraguay"},
    {code:"IQ", name : "Iraq"},
    {code:"PA", name : "Panama"},
    {code:"PF", name : "French Polynesia"},
    {code:"PG", name : "Papua New Guinea"},
    {code:"PE", name : "Peru"},
    {code:"PK", name : "Pakistan"},
    {code:"PH", name : "Philippines"},
    {code:"PN", name : "Pitcairn"},
    {code:"PL", name : "Poland"},
    {code:"PM", name : "Saint Pierre and Miquelon"},
    {code:"ZM", name : "Zambia"},
    {code:"EH", name : "Western Sahara"},
    {code:"EE", name : "Estonia"},
    {code:"EG", name : "Egypt"},
    {code:"ZA", name : "South Africa"},
    {code:"EC", name : "Ecuador"},
    {code:"IT", name : "Italy"},
    {code:"VN", name : "Vietnam"},
    {code:"SB", name : "Solomon Islands"},
    {code:"ET", name : "Ethiopia"},
    {code:"SO", name : "Somalia"},
    {code:"ZW", name : "Zimbabwe"},
    {code:"SA", name : "Saudi Arabia"},
    {code:"ES", name : "Spain"},
    {code:"ER", name : "Eritrea"},
    {code:"ME", name : "Montenegro"},
    {code:"MD", name : "Moldova"},
    {code:"MG", name : "Madagascar"},
    {code:"MF", name : "Saint Martin"},
    {code:"MA", name : "Morocco"},
    {code:"MC", name : "Monaco"},
    {code:"UZ", name : "Uzbekistan"},
    {code:"MM", name : "Myanmar"},
    {code:"ML", name : "Mali"},
    {code:"MO", name : "Macao"},
    {code:"MN", name : "Mongolia"},
    {code:"MH", name : "Marshall Islands"},
    {code:"MK", name : "Macedonia"},
    {code:"MU", name : "Mauritius"},
    {code:"MT", name : "Malta"},
    {code:"MW", name : "Malawi"},
    {code:"MV", name : "Maldives"},
    {code:"MQ", name : "Martinique"},
    {code:"MP", name : "Northern Mariana Islands"},
    {code:"MS", name : "Montserrat"},
    {code:"MR", name : "Mauritania"},
    {code:"IM", name : "Isle of Man"},
    {code:"UG", name : "Uganda"},
    {code:"TZ", name : "Tanzania"},
    {code:"MY", name : "Malaysia"},
    {code:"MX", name : "Mexico"},
    {code:"IL", name : "Israel"},
    {code:"FR", name : "France"},
    {code:"IO", name : "British Indian Ocean Territory"},
    {code:"SH", name : "Saint Helena"},
    {code:"FI", name : "Finland"},
    {code:"FJ", name : "Fiji"},
    {code:"FK", name : "Falkland Islands"},
    {code:"FM", name : "Micronesia"},
    {code:"FO", name : "Faroe Islands"},
    {code:"NI", name : "Nicaragua"},
    {code:"NL", name : "Netherlands"},
    {code:"NO", name : "Norway"},
    {code:"NA", name : "Namibia"},
    {code:"VU", name : "Vanuatu"},
    {code:"NC", name : "New Caledonia"},
    {code:"NE", name : "Niger"},
    {code:"NF", name : "Norfolk Island"},
    {code:"NG", name : "Nigeria"},
    {code:"NZ", name : "New Zealand"},
    {code:"NP", name : "Nepal"},
    {code:"NR", name : "Nauru"},
    {code:"NU", name : "Niue"},
    {code:"CK", name : "Cook Islands"},
    {code:"XK", name : "Kosovo"},
    {code:"CI", name : "Ivory Coast"},
    {code:"CH", name : "Switzerland"},
    {code:"CO", name : "Colombia"},
    {code:"CN", name : "China"},
    {code:"CM", name : "Cameroon"},
    {code:"CL", name : "Chile"},
    {code:"CC", name : "Cocos Islands"},
    {code:"CA", name : "Canada"},
    {code:"CG", name : "Republic of the Congo"},
    {code:"CF", name : "Central African Republic"},
    {code:"CD", name : "Democratic Republic of the Congo"},
    {code:"CZ", name : "Czech Republic"},
    {code:"CY", name : "Cyprus"},
    {code:"CX", name : "Christmas Island"},
    {code:"CR", name : "Costa Rica"},
    {code:"CW", name : "Curacao"},
    {code:"CV", name : "Cape Verde"},
    {code:"CU", name : "Cuba"},
    {code:"SZ", name : "Swaziland"},
    {code:"SY", name : "Syria"},
    {code:"SX", name : "Sint Maarten"},
    {code:"KG", name : "Kyrgyzstan"},
    {code:"KE", name : "Kenya"},
    {code:"SS", name : "South Sudan"},
    {code:"SR", name : "Suriname"},
    {code:"KI", name : "Kiribati"},
    {code:"KH", name : "Cambodia"},
    {code:"KN", name : "Saint Kitts and Nevis"},
    {code:"KM", name : "Comoros"},
    {code:"ST", name : "Sao Tome and Principe"},
    {code:"SK", name : "Slovakia"},
    {code:"KR", name : "South Korea"},
    {code:"SI", name : "Slovenia"},
    {code:"KP", name : "North Korea"},
    {code:"KW", name : "Kuwait"},
    {code:"SN", name : "Senegal"},
    {code:"SM", name : "San Marino"},
    {code:"SL", name : "Sierra Leone"},
    {code:"SC", name : "Seychelles"},
    {code:"KZ", name : "Kazakhstan"},
    {code:"KY", name : "Cayman Islands"},
    {code:"SG", name : "Singapore"},
    {code:"SE", name : "Sweden"},
    {code:"SD", name : "Sudan"},
    {code:"DO", name : "Dominican Republic"},
    {code:"DM", name : "Dominica"},
    {code:"DJ", name : "Djibouti"},
    {code:"DK", name : "Denmark"},
    {code:"VG", name : "British Virgin Islands"},
    {code:"DE", name : "Germany"},
    {code:"YE", name : "Yemen"},
    {code:"DZ", name : "Algeria"},
    {code:"US", name : "United States"},
    {code:"UY", name : "Uruguay"},
    {code:"YT", name : "Mayotte"},
    {code:"UM", name : "United States Minor Outlying Islands"},
    {code:"LB", name : "Lebanon"},
    {code:"LC", name : "Saint Lucia"},
    {code:"LA", name : "Laos"},
    {code:"TV", name : "Tuvalu"},
    {code:"TW", name : "Taiwan"},
    {code:"TT", name : "Trinidad and Tobago"},
    {code:"TR", name : "Turkey"},
    {code:"LK", name : "Sri Lanka"},
    {code:"LI", name : "Liechtenstein"},
    {code:"LV", name : "Latvia"},
    {code:"TO", name : "Tonga"},
    {code:"LT", name : "Lithuania"},
    {code:"LU", name : "Luxembourg"},
    {code:"LR", name : "Liberia"},
    {code:"LS", name : "Lesotho"},
    {code:"TH", name : "Thailand"},
    {code:"TF", name : "French Southern Territories"},
    {code:"TG", name : "Togo"},
    {code:"TD", name : "Chad"},
    {code:"TC", name : "Turks and Caicos Islands"},
    {code:"LY", name : "Libya"},
    {code:"VA", name : "Vatican"},
    {code:"VC", name : "Saint Vincent and the Grenadines"},
    {code:"AE", name : "United Arab Emirates"},
    {code:"AD", name : "Andorra"},
    {code:"AG", name : "Antigua and Barbuda"},
    {code:"AF", name : "Afghanistan"},
    {code:"AI", name : "Anguilla"},
    {code:"VI", name : "U.S. Virgin Islands"},
    {code:"IS", name : "Iceland"},
    {code:"IR", name : "Iran"},
    {code:"AM", name : "Armenia"},
    {code:"AL", name : "Albania"},
    {code:"AO", name : "Angola"},
    {code:"AQ", name : "Antarctica"},
    {code:"AS", name : "American Samoa"},
    {code:"AR", name : "Argentina"},
    {code:"AU", name : "Australia"},
    {code:"AT", name : "Austria"},
    {code:"AW", name : "Aruba"},
    {code:"IN", name : "India"},
    {code:"AX", name : "Aland Islands"},
    {code:"AZ", name : "Azerbaijan"},
    {code:"IE", name : "Ireland"},
    {code:"ID", name : "Indonesia"},
    {code:"UA", name : "Ukraine"},
    {code:"QA", name : "Qatar"},
    {code:"MZ", name : "Mozambique"}
    ]

  await Factory.model('App/Models/Country').createMany(countries.length,countries)

  let ratings = [
      {name:'NR',description:'Not Rated'},
      {name:'MPAA G',description:'General Audiences'},
      {name:'MPAA PG',description:'Some material may not be suitable for children. Parents urged to give "parental guidance". May contain some material parents might not like for their young children.'},
      {name:'MPAA PG13',description:'Some material may be inappropriate for children under 13. Parents are urged to be cautious. Some material may be inappropriate for pre-teenagers.'},
      {name:'MPAA R',description:'Under 17 requires accompanying parent or adult guardian. Contains some adult material. Parents are urged to learn more about the film before taking their young children with them.'},
      {name:'MPAA NC17',description:'No One 17 and Under Admitted. Clearly adult. Children are not admitted.'},
      {name:'TVPG TV-Y',description:'This program is designed to be appropriate for all children.'},
      {name:'TVPG TV-Y7',description:'This program is designed for children age 7 and above.'},
      {name:'TVPG TV-G',description:'This program is suitable for all ages.'},
      {name:'TVPG TV-PG',description:'This program contains material that parents may find unsuitable for younger children.'},
      {name:'TVPG TV-14',description:'This program contains some material that many parents would find unsuitable for children under 14 years of age.'},
      {name:'TVPG TV-MA',description:'This program is specifically designed to be viewed by adults and therefore may be unsuitable for children under 17.'},
      {name:'BBFC U',description:'Suitable for all.'},
      {name:'BBFC PG',description:'Parental guidance.'},
      {name:'BBFC 12A',description:'Cinema release suitable for 12 years and over.'},
      {name:'BBFC 12',description:'Video release suitable for 12 years and over.'},
      {name:'BBFC 15',description:'Suitable only for 15 years and over.'},
      {name:'BBFC 18',description:'Suitable only for adults.'},
      {name:'BBFC R18',description:'Adult works for licensed premises only.'},
  ]
  await Factory.model('App/Models/Rating').createMany(ratings.length,ratings)

  let rights = [
    {name:'PTV'},
    {name:'SVOD'},
    {name:'AVOD'},
    {name:'TVOD'},
    {name:'EST'},
    {name:'PPV'},
    {name:'DVD'},
    {name:'ANCILARY'},
    {name:'THEATRICAL'},
    {name:'Non-theatrical'}
    ]

  await Factory.model('App/Models/Right').createMany(rights.length,rights)

  }
  
}

module.exports = DbSeeder
