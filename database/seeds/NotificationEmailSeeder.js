'use strict'

/*
|--------------------------------------------------------------------------
| NotificationEmailSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class NotificationEmailSeeder {
  static  async run () {
    let email = [
      {
        title: 'LibraryNotification',
        subject:'New Title Added',
        content:'User ~user added Title (~title) in Library at ~createdAt.'
      },
      {
        title: 'IncomingNotification',
        subject:'Incoming Status Update',
        content:'User ~user updated the Title (~title) in incoming at ~updatedAt. It is now eligible to be added to Programming.'
      },
      {
        title: 'IncomingFailNotification',
        subject:'Incoming Status Failed',
        content:'User ~user updated the Title (~title) in incoming at ~updatedAt. Statuses (~statuses) Failed'
      },
      {
        title: 'OutgoingNotification',
        subject:'Title Moved To Outgoing',
        content:'User ~user moved Title (~title) to Outgoing at ~updatedAt.'
      },
  ]
    await Factory.model('App/Models/Email').createMany(email.length,email)
  }
}

module.exports = NotificationEmailSeeder
