'use strict'

/*
|--------------------------------------------------------------------------
| UpdateUserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')
class UpdateUserSeeder {
  static  async run () {

    let admin = await User.findBy('email','superadmin@test.com')
    admin.isNew = 0
    admin.save()
    let user = await User.findBy('email','jhondoe@email.com')
    user.isNew = 0
    user.save()
  }
}

module.exports = UpdateUserSeeder
