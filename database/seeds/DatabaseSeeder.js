'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const DbSeed = require('./DbSeeder')
const PlatformRightSeeder = require('./PlatformRightSeeder')
const TerritorySeeder = require('./TerritorySeeder')
const TerritoryCountrySeeder = require('./TerritoryCountrySeeder')

const AdBasedStreamingSeeder = require('./AdBasedStreamingSeeder')
const AddParaguayInLatinAmerciaSeeder = require('./AddParaguayInLatinAmerciaSeeder')
const ManagerCustomerPermissionSeeder = require('./ManagerCustomerPermissionSeeder')
const ManagerPermissionSeeder = require('./ManagerPermissionSeeder')
const NotificationEmailSeeder = require('./NotificationEmailSeeder')
const PitchTrackerPermissionSeeder = require('./PitchTrackerPermissionSeeder')
const UpdateUserSeeder = require('./UpdateUserSeeder')
const UserCustomerPermissionSeeder = require('./UserCustomerPermissionSeeder')
const UserPermissionSeeder = require('./UserPermissionSeeder')
const WordlWideSeeder = require('./WordlWideSeeder')

class DatabaseSeeder {
  async run () {
    // Seed Roles,Permissions,Languages,Genres,Countries,Ratings,Rights
    await DbSeed.run() 
    // Add Ad Based streaming Right
    await AdBasedStreamingSeeder.run() 
    //Seed Territory Names
    await TerritorySeeder.run() 
    //Seed Platforms and associated rights
    await PlatformRightSeeder.run()
    //Seed country associated with territories
    await TerritoryCountrySeeder.run()
    //Add Paraguay in LatinAmercia territory
    await AddParaguayInLatinAmerciaSeeder.run()    
    //Seed country associated with world wide territory
    await WordlWideSeeder.run()
    //Add permissions for role "client_manager"
    await ManagerCustomerPermissionSeeder.run()
    //Add permissions for role "manager"
    await ManagerPermissionSeeder.run()
    //Add permissions for role "client_user"
    await UserCustomerPermissionSeeder.run()
    //Add permissions for role "user"
    await UserPermissionSeeder.run()
    // Add Pitch tracker in resource and access permissions to roles
    await PitchTrackerPermissionSeeder.run()
    //Add Countries under worldwide territory
    //Sets Is new check for admin and test account to false
    await UpdateUserSeeder.run()
    //Seed Email Templates
    await NotificationEmailSeeder.run()


  }
}

module.exports = DatabaseSeeder
