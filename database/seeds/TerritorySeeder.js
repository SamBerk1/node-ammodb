'use strict'

/*
|--------------------------------------------------------------------------
| TerritorySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class TerritorySeeder {
  static  async run () {
    
    let TerritoryData = [
      {territoryName : "World Wide"},
      {territoryName : "North America"},
      {territoryName : "Latin America"},
      {territoryName : "The Caribbean"},
      {territoryName : "Brazil"},
      {territoryName : "Africa"},
      {territoryName : "MENA"},
      {territoryName : "Iberia"},
      {territoryName : "Italy"},
      {territoryName : "English speaking Europe"},
      {territoryName : "French-speaking Europe"},
      {territoryName : "German-speaking Europe"},
      {territoryName : "Benelux"},
      {territoryName : "CEE"},
      {territoryName : "CIS"},
      {territoryName : "Scandinavia"},
      {territoryName : "Asia"},
      {territoryName : "Oceania"}
    ]

    await Factory.model("App/Models/Territory").createMany(TerritoryData.length,TerritoryData)
  }
}

module.exports = TerritorySeeder
