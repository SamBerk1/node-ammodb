'use strict'

/*
|--------------------------------------------------------------------------
| WordlWideSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Territory = use('App/Models/Territory')

class WordlWideSeeder {
  static  async run () {
    let word_wide = ['French Canada', 'English Canada', 'USA', 'Spanish USA', 'Mexico', 'Guatemala', 'El Salvador', 'Honduras', 'Nicaragua', 'Costa Rica', 'Panama', 'Venezuela', 'Colombia', 'Ecuador', 'Peru', 'Bolivia', 'Chile', 'Argentina', 'Uruguay', 'Cuba', 'Dominican Republic', 'Puerto Rico (US)', 'Guyana', 'Suriname', 'Belize', 'French Guiana (France)', 'Antigua and Barbuda', 'Bahamas', 'Barbados', 'Grenada', 'Haiti', 'Jamaica', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Vincent and the Grenadines', 'Trinidad and Tobago', 'Anguilla (UK)', 'Aruba (Netherlands)', 'Bermuda (UK)', 'Bonaire (Netherlands)', 'British Virgin Islands (UK)', 'Cayman Islands (UK)', 'Clipperton Island (France)', 'Curacao (Netherlands)', 'Guadeloupe (France)', 'Martinique (France)', 'Montserrat (UK)', 'Navassa Island (USA)', 'Saba (Netherlands)', 'Saint Barthelemy (France)', 'Saint Martin (France)', 'Saint Pierre and Miquelon (France)', 'Sint Eustatius (Netherlands)', 'Sint Maarten (Netherlands)', 'Turks and Caicos Islands (UK)', 'US Virgin Islands (USA)', 'Brazil', 'Angola', 'Benin', 'Botswana', 'Burkina Faso', 'Burundi', 'Cabo Verde', 'Cameroon', 'Central African Republic', 'Chad', 'Comoros', 'The Democratic Republic of the Congo', 'Republic of the Congo', 'Côte d\'Ivoire', 'Djibouti', 'Equatorial Guinea', 'Eritrea', 'Eswatini', 'Ethiopia', 'Gabon', 'Gambia', 'Ghana', 'Guinea',
    'Guinea-Bissau', 'Kenya', 'Lesotho', 'Liberia', 'Madagascar', 'Malawi', 'Mali',
    'Mauritania', 'Mauritius', 'Mozambique', 'Namibia', 'Niger', 'Nigeria', 'Rwanda', 'Sao Tome and Principe', 'Senegal', 'Seychelles', 'Sierra Leone', 'Somalia', 'South Africa', 'South Sudan', 'Tanzania', 'Togo', 'Uganda', 'Zambia', 'Zimbabwe', 'Morocco', 'Algeria', 'Tunisia', 'Libya', 'Egypt', 'Sudan', 'Israel', 'Lebanon', 'Jordan', 'Syria', 'Iraq', 'Kuwait', 'Qatar', 'UAE', 'Saudi Arabia', 'Oman', 'Yemen', 'Bahrain', 'Spain', 'Portugal', 'Andorra', 'Gibraltar', 'Italy', 'San Marino', 'Vatican City', 'Malta', 'England', 'Scotland', 'Wales', 'Northern Ireland', 'Ireland', 'France', 'French Switzerland', 'Monaco', 'Germany', 'Austria', 'German Switzerland', 'Liechtenstein', 'Belgium', 'The Netherlands', 'Luxembourg', 'Poland', 'Czech Republic', 'Slovakia', 'Hungary', 'Estonia', 'Latvia', 'Lithuania', 'Romania', 'Bulgaria', 'Greece', 'Turkey', 'Cyprus', 'Slovenia', 'Croatia', 'Bosnia', 'Montenegro', 'Serbia', 'Kosovo', 'Albania', 'Macedonia', 'Azerbaijan', 'Belarus', 'Kazakhstan', 'Kyrgyzstan', 'Armenia', 'Moldova', 'Russia', 'Tajikistan', 'Uzbekistan', 'Turkmenistan', 'Ukraine', 'Georgia', 'Iceland', 'Denmark', 'Greenland', 'Norway', 'Sweden', 'Finland', 'Afghanistan', 'Bangladesh', 'Bhutan', 'Cambodia', 'China', 'India', 'Indonesia',
    'Iran', 'Japan', 'Laos', 'Malaysia', 'Maldives', 'Mongolia', 'Myanmar', 'Nepal', 'North Korea', 'Pakistan', 'Philippines', 'Singapore', 'South Korea', 'Sri Lanka', 'Taiwan',
    'Thailand', 'Timor-Leste', 'Vietnam', 'Australia', 'Fiji', 'Kiribati', 'Marshall Islands', 'Micronesia', 'Nauru', 'New Zealand', 'Palau', 'Papua New Guinea', 'Samoa', 'Solomon Islands', 'American Samoa (USA)', 'Cook Islands (New Zealand)', 'French Polynesia (France)', 'Guam (USA)', 'New Caledonia (France)', 'Niue (New Zealand)', 'Norfolk Island (Australia)', 'Northern Mariana Islands (USA)', 'Pitcairn Islands (UK)', 'Tokelau (New Zealand)', 'Wake Island (USA)', 'Wallis and Futuna (France)']
  
  await Factory.model('App/Models/TerritoryCountry').createMany(word_wide.length,{territoryId:(await Territory.findBy('territoryName','World Wide')).id,countries:word_wide})
  }
}

module.exports = WordlWideSeeder
