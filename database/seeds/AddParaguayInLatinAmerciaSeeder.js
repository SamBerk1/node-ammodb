'use strict'

/*
|--------------------------------------------------------------------------
| AddParaguayInLatinAmerciaSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Territory = use('App/Models/Territory')

class AddParaguayInLatinAmerciaSeeder {
  static  async run () {
    let Latin_America = ['Paraguay']
    await Factory.model('App/Models/TerritoryCountry').createMany(Latin_America.length,{territoryId:(await Territory.findBy('territoryName','Latin America')).id,countries:Latin_America})
  }
}

module.exports = AddParaguayInLatinAmerciaSeeder
