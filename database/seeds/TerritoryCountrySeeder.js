'use strict'

/*
|--------------------------------------------------------------------------
| TerritoryCountrySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

const Territory = use('App/Models/Territory')

class TerritoryCountrySeeder {
  static async run () {
    let North_America =['French Canada', 'English Canada', 'USA', 'Spanish USA']

    let Latin_America = ['Mexico','Guatemala','El Salvador','Honduras','Nicaragua','Costa Rica','Panama','Venezuela','Colombia','Ecuador','Peru','Bolivia','Chile','Argentina','Uruguay','Cuba','Dominican Republic','Puerto Rico (US)']

    let The_Caribbean = ['Guyana', 'Suriname', 'Belize', 'French Guiana (France)', 'Antigua and Barbuda', 'Bahamas', 'Barbados', 'Grenada', 'Haiti', 'Jamaica', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Vincent and the Grenadines', 'Trinidad and Tobago', 'Anguilla (UK)', 'Aruba (Netherlands)', 'Bermuda (UK)', 'Bonaire (Netherlands)', 'British Virgin Islands (UK)', 'Cayman Islands (UK)', 'Clipperton Island (France)', 'Curacao (Netherlands)', 'Guadeloupe (France)', 'Martinique (France)', 'Montserrat (UK)', 'Navassa Island (USA)', 'Saba (Netherlands)', 'Saint Barthelemy (France)', 'Saint Martin (France)', 'Saint Pierre and Miquelon (France)', 'Sint Eustatius (Netherlands)', 'Sint Maarten (Netherlands)', 'Turks and Caicos Islands (UK)', 'US Virgin Islands (USA)']
    
    let Brazil = ['Brazil']

    let Africa = ['Angola', 'Benin', 'Botswana', 'Burkina Faso', 'Burundi', 'Cabo Verde', 'Cameroon', 'Central African Republic', 'Chad', 'Comoros', 'The Democratic Republic of the Congo', 'Republic of the Congo', 'Côte d\'Ivoire', 'Djibouti', 'Equatorial Guinea', 'Eritrea', 'Eswatini', 'Ethiopia', 'Gabon', 'Gambia', 'Ghana', 'Guinea',
    'Guinea-Bissau', 'Kenya', 'Lesotho', 'Liberia', 'Madagascar', 'Malawi', 'Mali',
    'Mauritania', 'Mauritius', 'Mozambique', 'Namibia', 'Niger', 'Nigeria', 'Rwanda', 'Sao Tome and Principe', 'Senegal', 'Seychelles', 'Sierra Leone', 'Somalia', 'South Africa', 'South Sudan', 'Tanzania', 'Togo', 'Uganda', 'Zambia', 'Zimbabwe']
  
    let MENA = ['Morocco', 'Algeria', 'Tunisia', 'Libya', 'Egypt', 'Sudan', 'Israel', 'Lebanon', 'Jordan', 'Syria', 'Iraq', 'Kuwait', 'Qatar', 'UAE', 'Saudi Arabia', 'Oman', 'Yemen', 'Bahrain']
  
    let Iberia =  ['Spain', 'Portugal', 'Andorra', 'Gibraltar']

    let Italy = ['Italy', 'San Marino', 'Vatican City', 'Malta']
 
    let English_speaking_Europe = ['England', 'Scotland', 'Wales', 'Northern Ireland', 'Ireland']
    
    let French_speaking_Europe = ['France', 'French Switzerland', 'Monaco']
    
    let German_speaking_Europe = ['Germany', 'Austria', 'German Switzerland', 'Liechtenstein']
    
    let Benelux = ['Belgium', 'The Netherlands', 'Luxembourg']

    let CEE = ['Poland', 'Czech Republic', 'Slovakia', 'Hungary', 'Estonia', 'Latvia', 'Lithuania', 'Romania', 'Bulgaria', 'Greece', 'Turkey', 'Cyprus', 'Slovenia', 'Croatia', 'Bosnia', 'Montenegro', 'Serbia', 'Kosovo', 'Albania', 'Macedonia']
 
    let CIS = ['Azerbaijan', 'Belarus', 'Kazakhstan', 'Kyrgyzstan', 'Armenia', 'Moldova', 'Russia', 'Tajikistan', 'Uzbekistan', 'Turkmenistan', 'Ukraine', 'Georgia']
    
    let Scandinavia = ['Iceland', 'Denmark', 'Greenland', 'Norway', 'Sweden', 'Finland']
    
    let Asia = ['Afghanistan', 'Bangladesh', 'Bhutan', 'Cambodia', 'China', 'India', 'Indonesia',
    'Iran', 'Japan', 'Laos', 'Malaysia', 'Maldives', 'Mongolia', 'Myanmar', 'Nepal', 'North Korea', 'Pakistan', 'Philippines', 'Singapore', 'South Korea', 'Sri Lanka', 'Taiwan',
    'Thailand', 'Timor-Leste', 'Vietnam']
    
    let Oceania = ['Australia', 'Fiji', 'Kiribati', 'Marshall Islands', 'Micronesia', 'Nauru', 'New Zealand', 'Palau', 'Papua New Guinea', 'Samoa', 'Solomon Islands', 'American Samoa (USA)', 'Cook Islands (New Zealand)', 'French Polynesia (France)', 'Guam (USA)', 'New Caledonia (France)', 'Niue (New Zealand)', 'Norfolk Island (Australia)', 'Northern Mariana Islands (USA)', 'Pitcairn Islands (UK)', 'Tokelau (New Zealand)', 'Wake Island (USA)', 'Wallis and Futuna (France)']

    
    await Factory.model('App/Models/TerritoryCountry').createMany(North_America.length,{territoryId:(await Territory.findBy('territoryName','North America')).id,countries:North_America})

    await Factory.model('App/Models/TerritoryCountry').createMany(Latin_America.length,{territoryId:(await Territory.findBy('territoryName','Latin America')).id,countries:Latin_America})
    
    await Factory.model('App/Models/TerritoryCountry').createMany(The_Caribbean.length,{territoryId:(await Territory.findBy('territoryName','The Caribbean')).id,countries:The_Caribbean})

    await Factory.model('App/Models/TerritoryCountry').createMany(Brazil.length,{territoryId:(await Territory.findBy('territoryName','Brazil')).id,countries:Brazil})

    await Factory.model('App/Models/TerritoryCountry').createMany(Africa.length,{territoryId:(await Territory.findBy('territoryName','Africa')).id,countries:Africa})

    await Factory.model('App/Models/TerritoryCountry').createMany(MENA.length,{territoryId:(await Territory.findBy('territoryName','MENA')).id,countries:MENA})

    await Factory.model('App/Models/TerritoryCountry').createMany(Iberia.length,{territoryId:(await Territory.findBy('territoryName','Iberia')).id,countries:Iberia})

    await Factory.model('App/Models/TerritoryCountry').createMany(Italy.length,{territoryId:(await Territory.findBy('territoryName','Italy')).id,countries:Italy})

    await Factory.model('App/Models/TerritoryCountry').createMany(English_speaking_Europe.length,{territoryId:(await Territory.findBy('territoryName','English speaking Europe')).id,countries:English_speaking_Europe})

    await Factory.model('App/Models/TerritoryCountry').createMany(French_speaking_Europe.length,{territoryId:(await Territory.findBy('territoryName','French-speaking Europe')).id,countries:French_speaking_Europe})

    await Factory.model('App/Models/TerritoryCountry').createMany(German_speaking_Europe.length,{territoryId:(await Territory.findBy('territoryName','German-speaking Europe')).id,countries:German_speaking_Europe})

    await Factory.model('App/Models/TerritoryCountry').createMany(Benelux.length,{territoryId:(await Territory.findBy('territoryName','Benelux')).id,countries:Benelux})

    await Factory.model('App/Models/TerritoryCountry').createMany(CEE.length,{territoryId:(await Territory.findBy('territoryName','CEE')).id,countries:CEE})

    await Factory.model('App/Models/TerritoryCountry').createMany(CIS.length,{territoryId:(await Territory.findBy('territoryName','CIS')).id,countries:CIS})

    await Factory.model('App/Models/TerritoryCountry').createMany(Scandinavia.length,{territoryId:(await Territory.findBy('territoryName','Scandinavia')).id,countries:Scandinavia})

    await Factory.model('App/Models/TerritoryCountry').createMany(Asia.length,{territoryId:(await Territory.findBy('territoryName','Asia')).id,countries:Asia})

    await Factory.model('App/Models/TerritoryCountry').createMany(Oceania.length,{territoryId:(await Territory.findBy('territoryName','Oceania')).id,countries:Oceania})

  }
}


module.exports = TerritoryCountrySeeder
