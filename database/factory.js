'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

const Factory = use('Factory')

Factory.blueprint('App/Models/User', async (faker,index,data) => {
  return {
    role_id:data.role_id,
    fullname:faker.name(),
    email: data.email,
    password: data.password
  }
})

Factory.blueprint('App/Models/Email', async (faker,index,data) => {
  return {
    title:data[index].title,
    subject:data[index].subject,
    content:data[index].content,
  }
})

Factory.blueprint('App/Models/Role', async (faker,index,data) => {
  return{
    name:data[index].role,
    slug:data[index].slug
  }
})

Factory.blueprint('Cerberus/Models/Permission', async (faker,index,data) => {
  return{
      role_id: data[index].role_id,
      resource_id: data[index].resource_id,
      create: data[index].create,
      read: data[index].read,
      update: data[index].update,
      delete: data[index].delete
  }
})

Factory.blueprint('Cerberus/Models/Resource', async (faker,index,data) => {
  return{
    name:data[index],
    slug:data[index]
  }
})

Factory.blueprint('App/Models/Language',async (faker,index,data) => {
  return{
    code:data[index].code,
    name:data[index].name
  }
})

Factory.blueprint('App/Models/Country',async (faker,index,data) => {
  return{
    code:data[index].code,
    name:data[index].name
  }
})

Factory.blueprint('App/Models/Genre', async (faker,index,data) => {
  return{
    title:data[index]
  }
})

Factory.blueprint('App/Models/Rating',async (faker,index,data) => {
  return{
    name:data[index].name,
    description:data[index].description
  }
})

Factory.blueprint('App/Models/Right', async (faker,index,data) => {
  return{
    name:data[index].name
  }
})

Factory.blueprint('App/Models/Platform', async (faker,index,data) => {
  return{
    name:data[index]
  }
})

Factory.blueprint('App/Models/PlatfromsRight', async (faker,index,data) => {
  return{
    platformId:data[index].platformId,
    rightId:data[index].rightId,
    territoryId:data[index].territoryId
  }
})

Factory.blueprint('App/Models/Territory', async (faker,index,data) => {
  return{
    territoryName:data[index].territoryName,
  }
})

Factory.blueprint('App/Models/TerritoryCountry', async (faker,index,data) => {
  return{
    territoryId:data.territoryId,
    country: data.countries[index]
  }
})

Factory.blueprint('App/Models/Studio', async (faker,index,data) => {
  return{
    title:data.title
  }
})

Factory.blueprint('App/Models/StudioCustomer', async (faker,index,data) => {
  return{
    userId:data.userId,
    studioId: data.studioId
  }
})