'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')
const glob = require( 'glob' )
const path = require( 'path' )

Route.get('/', async ({response,view,auth})=>{
    try {
        await auth.check()
        let user = (await auth.getUser()).toJSON()
        if (user.role.slug != 'client_user') {
            return response.redirect('/library')
        } else {
            return response.redirect('/incoming')
        }
    } catch (error) {
        return view.render('Auth.signin')
    }
})

Route.get('/signin', async ({view,auth,response})=>{
    try {
        await auth.check()
        let user = (await auth.getUser()).toJSON()
        if (user.role.slug != 'client_user') {
            return response.redirect('/library')
        } else {
            return response.redirect('/incoming')
        }
    } catch (error) {
        return view.render('Auth.signin')
    }
})

Route.get('/newaccount',async ({view}) => {
    return view.render('partials.changeNewPassword')
}).middleware(['auth'])

Route.on('/financial').render('financial.financial').middleware(['auth','guard:financial.read'])

Route.on('/crm/customer').render('crm.customer.customer').middleware(['auth','guard:customers.read'])

Route.on('/settings/activityLog').render('settings.activityLog.activityLog').middleware(['auth','guard:roles.read'])

Route.on('/others').render('others.other').middleware(['auth','guard:others.read'])

//Requiring all Routes files from ../app/Routes directory
glob.sync( './app/Routes/**/*.js' ).forEach( function( file ) {
    require( path.resolve( file ) )(Route)
});

Route.on('/page-not-found').render('pagenotfound.pagenotfound')

Route.any('*', ({response}) => {   
    response.redirect('/page-not-found')
})