# AmmoDB
This project is using AdonisJS 4.0
Install Adonis Cli required to run Migrations and Seed commands.
```bash

npm i -g @adonisjs/cli

```

## setup

  

Install dependencies
Run Following Command in root directory
```bash

npm install

```
## .env File
Create **.env** file in root directory.

**Sample**
```

HOST=127.0.0.1
PORT=3333
NODE_ENV=development
APP_URL=http://${HOST}:${PORT}

# For Dev
BASE_URL=http://ammo-dbdev.com
# For Prod
BASE_URL=https://ammo-db.com

CACHE_VIEWS=false

APP_KEY=2wsjLSopTjD6WQEztTYIZgCFou8wpLJn

DB_CONNECTION=mysql 
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER= {{Databse Username}
DB_PASSWORD= {{Database Password}
DB_DATABASE= {{Database Name}

SMTP_PORT=465
SECURE=true
SMTP_HOST= {{Smtp Host}
MAIL_USERNAME= {{email username}}
MAIL_PASSWORD= {{email password}

SESSION_DRIVER=cookie

```
  

### Database
Edit your .env file to include the correct DB credentials
Make Sure you have created a database named  provided in  **.env** file.
  

### Migrations
Run Following Command in root directory to create database tables.

  

```js

adonis  migration:run

```

  

### Database Seed

  

Run Following Command in root directory to seed data.

  

```js

adonis  seed

```

  

### Run Server (nodemon suggested)

  

```bash

adonis serve

```

  

or

  

```bash

nodemon server.js

```