$(document).ready(function () {
    const orderbyColumns = [{text:'Title',name:'title'},{text:'Series Name',name:'title'},{text:'Due Date',name:'dueDate'},{text:'Captions',name:'captions'},{text:'Subs',name:'subtitles'},{text:'Art Work',name:'artwork'},{text:'Dubbing',name:'dubbing'},{text:'Metadata',name:'metadata'},{text:'Video Status',name:'videoStatus'}]

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    $('#projectManagementTable').DataTable({
        "bInfo": false, //Dont display info e.g. "Showing 1 to 4 of 4 entries"
        "paging": false,//Dont want paging                
        "bPaginate": false,//Dont want paging   ,
        "searching":false,
        "sort":false,
    });

    function filterQueryFunc(){
        let paramsObj = Object.fromEntries(new URLSearchParams(window.location.search))
            
        paramsObj.search && delete paramsObj.search
        paramsObj.searchcolumn && delete paramsObj.searchcolumn
        paramsObj.orderby && delete paramsObj.orderby
        paramsObj.order && delete paramsObj.order
        paramsObj.page && delete paramsObj.page
        paramsObj.perpage && delete paramsObj.perpage

        let filterQuery = []

        for (let [key, value] of Object.entries(paramsObj)) {
            filterQuery.push(`${key}=${value}`)
        }
        return filterQuery
    }

    $('#pagLimit').on('change',function (e) {  
        if(e.target.value!=''){
            let url = window.location.href.includes('movie') ? 'project-management/movie' : 'project-management/season'
            let search = getParameterByName('search',window.location.href)
            let searchcolumn = getParameterByName('searchcolumn',window.location.href)
            let orderby = getParameterByName('orderby',window.location.href)
            let order = getParameterByName('order',window.location.href)

            let filterQuery = filterQueryFunc()


            if (search!=null && searchcolumn!=null && orderby==null && order==null) {
                window.location.href = `${url}?${filterQuery.join('&')}&search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${parseInt(e.target.value)}`
            }
            else if(search==null && searchcolumn==null && orderby!=null && order!=null) {
                window.location.href = `${url}?${filterQuery.join('&')}&page=1&perpage=${parseInt(e.target.value)}&orderby=${orderby}&order=${order}`
            }
            else if(search!=null && searchcolumn!=null && orderby!=null && order!=null){
                window.location.href = `${url}?${filterQuery.join('&')}&search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${parseInt(e.target.value)}&orderby=${orderby}&order=${order}`
            }
            else {
                window.location.href = `${url}?${filterQuery.join('&')}&page=1&perpage=${parseInt(e.target.value)}`
            }
        }
    });

    $('#projectManagementTable thead tr th').click( function (e) { 
        let sortingCols = ['Title','Series Name','Captions','Due Date','Subs','Art Work','Dubbing','Metadata','Video Status']

        let url = window.location.href.includes('movie') ? 'project-management/movie' : 'project-management/season'

        if ($(this).text().toLowerCase() != 'action' && sortingCols.includes($(this).text())){
            let order = getParameterByName('order',window.location.href)
            if (order == null) {
                order = 'asc'
            } else if(order == 'asc'){
                order = 'desc'
            }else if(order == 'desc'){
                order = 'asc'
            }
            let orderby = (orderbyColumns.filter( col => col.text == $(this).text()))[0].name
            let search = getParameterByName('search',window.location.href)
            let searchcolumn = getParameterByName('searchcolumn',window.location.href)
            let filterQuery = filterQueryFunc()

            if (search!=null && searchcolumn!=null && orderby==null && order==null) {
                window.location.href = `${url}?${filterQuery.join('&')}&search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${$('#pagLimit').val()}`
            }
            else if(search==null && searchcolumn==null && orderby!=null && order!=null) {
                window.location.href = `${url}?${filterQuery.join('&')}&page=1&perpage=${$('#pagLimit').val()}&orderby=${orderby}&order=${order}`
            }
            else if(search!=null && searchcolumn!=null && orderby!=null && order!=null){
                window.location.href = `${url}?${filterQuery.join('&')}&search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${$('#pagLimit').val()}&orderby=${orderby}&order=${order}`
            }
            else {
                window.location.href = `${url}?${filterQuery.join('&')}&page=1&perpage=${$('#pagLimit').val()}`  
            }
        }
    });

    $('#projectManagementTable thead tr th').addClass('sorting')
    $('#projectManagementTable thead tr th').map(th => {
        let sortingCols = ['Title','Series Name','Captions','Due Date','Subs','Art Work','Dubbing','Metadata','Video Status']
        !sortingCols.includes($('#projectManagementTable thead tr th')[th].innerText) && $($('#projectManagementTable thead tr th')[th]).removeClass('sorting')
    })

    let orderby = getParameterByName('orderby',window.location.href)
    let order = getParameterByName('order',window.location.href)
    if(orderby != null && order != null){
        orderby = (orderbyColumns.filter( col => col.name == orderby))[0].text
        $('#projectManagementTable thead tr th').map( th => {
            if($('#projectManagementTable thead tr th')[th].innerText == orderby){
                $($('#projectManagementTable thead tr th')[th]).removeClass('sorting')
                $($('#projectManagementTable thead tr th')[th]).addClass(`sorting_${order}`)
            }
        })
    }

    $('input[name="search"').on('keyup', function (e) {
        let url = window.location.href.includes('movie') ? 'project-management/movie' : 'project-management/season'

        if (e.keyCode === 13) {
            if ($('input[name="search"').val().trim() == '') {
                $('input[name="search"').addClass("invalid-search");   
            }
            else{
                window.location.href = `${url}?search=${$('input[name="search"]').val().trim()}&searchcolumn=${$('select[name="searchColumn"]').val().trim()}&page=1&perpage=${$('#pagLimit').val()}`
            }
        }

        if (e.keyCode === 8) {
            if($('input[name="search"').val().trim() == ''){
                window.location.href = `${url}?search=${$('input[name="search"]').val().trim()}&searchcolumn=${$('select[name="searchColumn"]').val().trim()}&page=1&perpage=${$('#pagLimit').val()}`
            }
        }
    });

    $('#fileUpload').change(function() {
        var file = $('#fileUpload')[0].files[0].name;
        $('#fileName').text(file);
    });

    $('#fileUploadSeason').change(function() {
        var file = $('#fileUploadSeason')[0].files[0].name;
        $('#fileNameSeason').text(file);
    });
});