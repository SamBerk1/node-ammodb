$(document).ready(function() {

    $('.project_managment_platform_dropdown').select2({
        templateResult:function formatState (state) {
            
            if(state.id == 'null' || state.id == 'undefined' || state.id == undefined){
                return state.text
            }
            
            let data = $(state.element).attr("data-").split('~') // 0 : territory, 1 : country , 2 : platform , 3 : right
            var $state = (!data[1].includes('undefined') ? 
                $(`<div class="row col-md-12">
                        <div class="col-md-12"><strong>${data[0]} - ${data[1]}</strong></div>
                        <div class="row col-md-12">
                            <div class="col-md-6"><h7>${data[2]}</h7></div>
                            <div class="col-md-5"><h7>${data[3]}</h7></div>
                        </div>
                    </div>`)
                :
                $(`<div class="row col-md-12">
                    <div class="col-md-12"><strong>${data[0]}</strong></div>
                    <div class="col-md-6"><h7>${data[2]}</h7></div>
                    <div class="col-md-5"><h7>${data[3]}</h7></div>
                </div>`)
                )
            return $state;
        }
    }) 
});