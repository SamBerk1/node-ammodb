$(document).ready(function() {    
    createSlick();
    
    if ($("#rainfallExample").length > 0 && $("#rainfallExample2").length > 0) {
    
    var rainChartId = $("#rainfallExample")[0];
    var rainChart2Id = $("#rainfallExample2")[0];
    
    var myChart =  echarts.init(rainChartId);
    var myChart2 = echarts.init(rainChart2Id);
    
    // // specify chart configuration item and data
    option = {
      title: {
        text: "Example Chart 1"
      },
      tooltip: {},
      legend: {
        data: ["User", "User1"]
      },
      // color: ["#3398DB"],
      tooltip: {
        trigger: "axis",
        axisPointer: {
          type: "shadow" // 'line' | 'shadow'
        }
      },
      grid: {
        left: "3%",
        right: "4%",
        bottom: "3%",
        containLabel: true
      },
      xAxis: [
        {
          type: "category",
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
          axisTick: {
            alignWithLabel: true
          }
        }
      ],
      yAxis: [
        {
          type: "value"
        }
      ],
      series: [
        {
          name: "User",
          type: "bar",
          barWidth: "60%",
          data: [10, 52, 200, 334, 390, 330, 220]
        },
        {
          name: "User1",
          type: "bar",
          barWidth: "40%",
          data: [10, 52, 200, 334, 390, 330, 220]
        }
      ]
    };
    
    var option2 = {
      title: {
        text: "Example Chart 2"
      },
      tooltip: {},
      legend: {
        data: ["Sales"]
      },
      xAxis: {
        data: ["shirt", "cardign", "chiffon shirt", "pants", "heels", "socks"]
      },
      yAxis: {},
      series: [
        {
          name: "Sales",
          type: "bar",
          data: [5, 20, 36, 10, 10, 20]
        }
      ]
    };
    // use configuration item and data specified to show chart
    
    myChart.setOption(option);
    myChart2.setOption(option2);
    }
    });
    
    
    // slider js
    function createSlick(){  
    
        $(".slider").not('.slick-initialized').slick({
            centerMode: true,
            autoplay: true,
            dots: true,
        
              slidesToShow: 3,
            responsive: [{ 
                breakpoint: 768,
                settings: {
                    dots: false,
                    arrows: false,
                    infinite: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                } 
            }]
        });	
    
    }
    
//Will not throw error, even if called multiple times.
$(window).on( 'resize', createSlick )