$(document).on("click", ".open-editAjaxModal", function () {
  $('#incomingFormModalAjax').show()
  let modal = $('#incomingFormModalAjax')
  let clickedStatus = $(this).data('name')

  if (clickedStatus) {
      $(modal).find(`select[name|='${clickedStatus}']`).closest('div').addClass('highlightOptDiv')
      $(modal).on('hide.bs.modal', function () {
          $(modal).find(`select[name|='${clickedStatus}']`).closest('div').removeClass('highlightOptDiv')
      });
  }
  

  showModal({id:$(this).data('id'),ModalTitle:$(this).data('title')})
});


function showModal(data) {
  console.log(data)
  $.ajax({
      url:`incoming/details/${data.id}`,
      method:'GET',
      success:function (result) { 
          console.log(result)
          if (result.data != null) {
              $("[id=customAssetForm]").children().not(':first').remove();
              $('#incomingFormModalAjax').modal('show');
              $('#incomingFormModalAjax h5.modal-title').html(data.ModalTitle)    
              
              $('#incomingFormModalAjax form[name="statusForm"]').attr('action',`/incoming/${data.id}?_method=PUT`) 

              $('#incomingFormModalAjax form[name="statusForm"] select[name="videoStatus"]').val(result.data.videoStatus).trigger('change')
              $('#incomingFormModalAjax form[name="statusForm"] select[name="captions"]').val(result.data.captions).trigger('change')
              $('#incomingFormModalAjax form[name="statusForm"] select[name="subtitles"]').val(result.data.subtitles).trigger('change')
              $('#incomingFormModalAjax form[name="statusForm"] select[name="artwork"]').val(result.data.artwork).trigger('change')
              $('#incomingFormModalAjax form[name="statusForm"] select[name="dubbing"]').val(result.data.dubbing).trigger('change')
              $('#incomingFormModalAjax form[name="statusForm"] select[name="metadata"]').val(result.data.metadata).trigger('change')

              if(result.data.customAssetsIncoming){
                  result.data.customAssetsIncoming.map( x => {
                      $("[id=customAssetForm]").append(
                          `<div class="row">
                              <div class="col-md-5 mb-3">
                                  <input class="form-control" type="text" value='${x.name}' placeholder="Custom Asset Name" name="customAsset[name]">               
                              </div>
                              <div class="col-md-6 mb-3">
                                  <div class="row">
                                      <div class="col-md-10">
                                          <input class="form-control" type="text" value='${x.status}' placeholder="Custom Asset Status" name="customAsset[status]"> 
                                      </div>    
                                      <div class="col-md-2">
                                          <button id="customAssetRemove" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                                      </div>  
                                  </div> 
                              </div> 
                          </div>`
                      )
                  })
              }
          }
      }
  })   
}