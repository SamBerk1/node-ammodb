$(function () {
    // 6 create an instance when the DOM is ready
    $('.jstree#outgoingTree').jstree({
        "plugins": ["contextmenu"],
        "contextmenu": {
            "items": function ($node) {
                if ($node.data.jstree.contextMenu) {
                    let contextMenuObj = {
                        // data-toggle="modal" data-target="#outgoingEditModal"
                        "Edit": {
                            "separator_before": true,
                            "separator_after": true,
                            "label": "Edit",
                            "action": function (obj) {
                                if ($node.data.jstree.href) {
                                    showModal($node.data.jstree)
                                }
                            }
                        },
                        "Detail": {
                            "separator_before": true,
                            "separator_after": true,
                            "label": "Detail",
                            "action": function (obj) {
                                if ($node.data.jstree.href) {
                                    window.location.href = $node.data.jstree.href
                                }
                            }
                        },
                        "Territories": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": "Territories",
                            "action": function (obj) {
                                $('#outgoingTerritoryModal h5.modal-title').text(`Edit Territories for Ttitle "${$node.data.jstree.ModalTitle}"`)
                                $('#outgoingTerritoryModal div form').attr('action', $node.data.jstree.type.toLowerCase() == 'season' ? `/outgoing/${$node.data.jstree.id}/territories/season?_method=PUT` : `/outgoing/${$node.data.jstree.id}/territories/movie?_method=PUT`)
                                $.ajax({
                                    type: "get",
                                    url: $node.data.jstree.type.toLowerCase() == 'season' ? `outgoing/season/${$node.data.jstree.id}/territories` : `outgoing/${$node.data.jstree.id}/territories`,
                                    success: function (response) {
                                        $('.selectTerritoryCountries').select2({
                                            tags: true,
                                            tokenSeparators: [','],
                                            data: (
                                                response.data != null && $.map((response.data), function (item) {
                                                        return {
                                                            id:item.territory,
                                                            text:item.territory,
                                                            selected: true
                                                        }
                                                    })
                                                )
                                        })
                                    }
                                });
                                $('#outgoingTerritoryModal').modal({
                                    show: true
                                }); 
                            }
                        },
                    }

                    if ($node.data.jstree.isStudio == 0) {
                        contextMenuObj['edit'] = {
                            "separator_before": true,
                            "separator_after": true,
                            "label": "Undo",
                            "action": function (obj) {
                                if ($node.data.jstree.href) {
                                    window.location.href = $node.data.jstree.undoAction
                                }
                            }
                        }
                    }

                    return contextMenuObj
                }
            }
        }
    }).on('select_node.jstree', function (e, data) {
         setTimeout(function() {
             data.instance.show_contextmenu(data.node)
         }, 100);
     });

    // 8 interact with the tree - either way is OK
    // $('.jstree#outgoingTree button').on('click', function () {
    //     $('.jstree#outgoingTree').jstree(true).select_node('child_node_1');
    //     $('.jstree#outgoingTree').jstree('select_node', 'child_node_1');
    //     $.jstree.reference('.jstree#outgoingTree').select_node('child_node_1');
    // });

    $('#outgoingTerritoryModal').on('hidden.bs.modal' , function () {
        $('.selectTerritoryCountries').empty()
     })

    $("[id=customAssetForm]").on('click','#customAssetBtn',function(e){
    e.preventDefault();
    let customAssetName = ($(this).parent().parent().parent().parent().parent()).find('input')[0]
    let customAssetStatus = ($(this).parent().parent().parent().parent().parent()).find('select')[0]
    if(customAssetName.value!='' && customAssetStatus.value!=''){
        $(this).parent().parent().parent().parent().parent().append(`
        <div class="row">
        <div class="col-md-5 mb-3">
            <input class="form-control" type="text" value='${customAssetName.value}' placeholder="Custom Asset Name" name="customAsset[name]">               
        </div>
        <div class="col-md-6 mb-3">
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control" type="text" value='${customAssetStatus.value}' placeholder="Custom Asset Status" name="customAsset[status]"> 
                </div>    
                <div class="col-md-2">
                    <button id="customAssetRemove" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                </div>  
            </div> 
        </div> 
        </div>
        `)

        customAssetName.value = ''
        customAssetStatus.selectedIndex = 0
    }
    });

    $("[id=customAssetForm]").on('click','#customAssetRemove',function(e){
    e.preventDefault();
    $(this).parent().parent().parent().parent().remove()
    });

});



function showModal(data) {
    console.log(data)
    let detailsAction = data.type.toLowerCase() == 'movie' ? `/project-management/details/${data.id}` : `/project-management/details/season/${data.id}`
    $.ajax({
        url:detailsAction,
        method:'GET',
        success:function (result) { 
            console.log(result)
            if (result.data != null) {
                data.type.toLowerCase() == 'movie' ? $("[id=customAssetForm]").parent().parent().show() : $("[id=customAssetForm]").parent().parent().hide()
                $("[id=customAssetForm]").children().not(':first').remove();
                $('#outgoingEditModal').modal('show');
                $('#outgoingEditModal h5.modal-title').text(`Edit "${data.ModalTitle}"`)    
                
                let action = data.type.toLowerCase() == "movie" ? `/project-management/${data.id}?_method=PUT` : `/project-management/season/${data.id}?_method=PUT`
                $('#outgoingEditModal form[name="statusForm"]').attr('action',action) 

                $('#outgoingEditModal form[name="statusForm"] select[name="platformId"]').val(result.data.platformId).trigger('change')
                
                $('#outgoingEditModal form[name="statusForm"] select[name="videoStatus"]').val(result.data.videoStatus).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="captions"]').val(result.data.captions).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="subtitles"]').val(result.data.subtitles).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="artwork"]').val(result.data.artwork).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="dubbing"]').val(result.data.dubbing).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="metadata"]').val(result.data.metadata).trigger('change')

                if(result.data.customAssetsOutgoing){
                    result.data.customAssetsOutgoing.map( x => {
                        $("[id=customAssetForm]").append(
                            `<div class="row">
                                <div class="col-md-5 mb-3">
                                    <input class="form-control" type="text" value='${x.name}' placeholder="Custom Asset Name" name="customAsset[name]">               
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input class="form-control" type="text" value='${x.status}' placeholder="Custom Asset Status" name="customAsset[status]"> 
                                        </div>    
                                        <div class="col-md-2">
                                            <button id="customAssetRemove" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                                        </div>  
                                    </div> 
                                </div> 
                            </div>`
                        )
                    })
                }


                let actionSchedule = data.type.toLowerCase() == "movie" ? `/project-management/${data.id}/schedule` : `/project-management/season/${data.id}/schedule`
                $('#outgoingEditModal form[name="scheduleForm"]').attr('action',actionSchedule) 

                let dueDate = result.data.dueDate != null ? new Date(result.data.dueDate) : ''
                $('#outgoingEditModal form[name="scheduleForm"] input[name="dueDate"]').val(dueDate == '' ? '' : dueDate.getFullYear() + "-" + (dueDate.getMonth()+1) + "-" + dueDate.getDate())    

                let launchDate = result.data.launchDate != null ? new Date(result.data.launchDate) : ''
                $('#outgoingEditModal form[name="scheduleForm"] input[name="launchDate"]').val(launchDate == '' ? '' : launchDate.getFullYear() + "-" + (launchDate.getMonth()+1) + "-" + launchDate.getDate())    

                let deliveryDate = result.data.deliveryDate != null ? new Date(result.data.deliveryDate) : ''
                $('#outgoingEditModal form[name="scheduleForm"] input[name="deliveryDate"]').val(deliveryDate == '' ? '' : deliveryDate.getFullYear() + "-" + (deliveryDate.getMonth()+1) + "-" + deliveryDate.getDate())    

                let sunSetDate = result.data.sunSetDate != null ? new Date(result.data.sunSetDate) : ''
                $('#outgoingEditModal form[name="scheduleForm"] input[name="sunSetDate"]').val(sunSetDate == '' ? '' : sunSetDate.getFullYear() + "-" + (sunSetDate.getMonth()+1) + "-" + sunSetDate.getDate())    
            
                let actionTerritories = data.type.toLowerCase() == "movie" ? `outgoing/${data.id}/territories/movie?_method=PUT` : `outgoing/${data.id}/territories/season?_method=PUT`
                $('#outgoingEditModal form[name="territoriesForm"]').attr('action',actionTerritories) 

                if(result.data.territories){
                    let selectTerritories = result.data.territories.map( x => {
                        return {"id":x.territory,"text":x.territory,"selected":true}
                    })
                    $('#outgoingEditModal form[name="territoriesForm"] select[name="territories[]"]').empty()
                    $('#outgoingEditModal form[name="territoriesForm"] select[name="territories[]"]').select2({
                        data: selectTerritories,
                        tags: true,
                        tokenSeparators: [',']
                    })
                }
            
            }
        }
    })   
}



