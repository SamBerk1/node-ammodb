$(document).ready(function () {
var synopsis_text_max = 400;

$('#synopsis').val() && $('#synopsis_charCount').html(synopsis_text_max - $('#synopsis').val().length + ' remaining');

$('#synopsis').keyup(function() {
  var text_length = $('#synopsis').val().length;
  var text_remaining = synopsis_text_max - text_length;
  
  $('#synopsis_charCount').html(text_remaining + ' remaining');
});


var alternate_synopsis_text_max = 400;
$('#alterSynopsis').val() && $('#alternate_synopsis_charCount').html(alternate_synopsis_text_max - $('#alterSynopsis').val().length+ ' remaining');

$('#alterSynopsis').keyup(function() {
  var text_length = $('#alterSynopsis').val().length;
  var text_remaining = alternate_synopsis_text_max - text_length;
  
  $('#alternate_synopsis_charCount').html(text_remaining + ' remaining');
});

});