$(document).ready(function () {
    const orderbyColumns = [{ text: 'GUID', name: 'guId' }, { text: 'Movie / Series', name: 'is' }, { text: 'Title', name: 'title' }, { text: 'Studio', name: 'studio_title' }, { text: 'Genre', name: 'genres_title' }, { text: 'Format', name: 'format_format' }, { text: 'Cast', name: 'cast_actorName' }, { text: 'Year', name: 'year' }, { text: 'Created At', name: 'created_at' }]

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function filterQueryFunc() {
        let paramsObj = Object.fromEntries(new URLSearchParams(window.location.search))

        paramsObj.search && delete paramsObj.search
        paramsObj.searchcolumn && delete paramsObj.searchcolumn
        paramsObj.orderby && delete paramsObj.orderby
        paramsObj.order && delete paramsObj.order
        paramsObj.page && delete paramsObj.page
        paramsObj.perpage && delete paramsObj.perpage

        let filterQuery = []

        for (let [key, value] of Object.entries(paramsObj)) {
            filterQuery.push(`${key}=${value}`)
        }
        return filterQuery
    }

    $('#libraryTable').DataTable({
        "bInfo": false, //Dont display info e.g. "Showing 1 to 4 of 4 entries"
        "paging": false,//Dont want paging                
        "bPaginate": false,//Dont want paging   ,
        "searching": false,
        "sort": false,
        columnDefs: [
            { "width": "20%", "targets": [1] },
            { "width": "10%", "targets": [2] },
        ]
    });

    $('#pagLimit').on('change', function (e) {
        if (e.target.value != '') {
            let search = getParameterByName('search', window.location.href)
            let searchcolumn = getParameterByName('searchcolumn', window.location.href)
            let orderby = getParameterByName('orderby', window.location.href)
            let order = getParameterByName('order', window.location.href)
            let filterQuery = filterQueryFunc()

            if (search != null && searchcolumn != null && orderby == null && order == null) {
                window.location.href = `library?${filterQuery.join('&')}&search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${parseInt(e.target.value)}`
            }
            else if (search == null && searchcolumn == null && orderby != null && order != null) {
                window.location.href = `library?${filterQuery.join('&')}&page=1&perpage=${parseInt(e.target.value)}&orderby=${orderby}&order=${order}`
            }
            else if (search != null && searchcolumn != null && orderby != null && order != null) {
                window.location.href = `library?${filterQuery.join('&')}&search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${parseInt(e.target.value)}&orderby=${orderby}&order=${order}`
            }
            else {
                window.location.href = `library?${filterQuery.join('&')}&page=1&perpage=${parseInt(e.target.value)}`
            }
        }
    });

    $('#libraryTable thead tr th').click(function (e) {
        let sortingCols = ['GUID', 'Title', 'Movie / Series', 'Year', 'Created At']
        if ($(this).text().toLowerCase() != 'action' && sortingCols.includes($(this).text())) {
            let order = getParameterByName('order', window.location.href)
            if (order == null) {
                order = 'asc'
            } else if (order == 'asc') {
                order = 'desc'
            } else if (order == 'desc') {
                order = 'asc'
            }
            let orderby = (orderbyColumns.filter(col => col.text == $(this).text()))[0].name
            let search = getParameterByName('search', window.location.href)
            let searchcolumn = getParameterByName('searchcolumn', window.location.href)

            let filterQuery = filterQueryFunc()

            if (search != null && searchcolumn != null && orderby == null && order == null) {
                window.location.href = `library?${filterQuery.join('&')}&search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${$('#pagLimit').val()}`
            }
            else if (search == null && searchcolumn == null && orderby != null && order != null) {
                window.location.href = `library?${filterQuery.join('&')}&page=1&perpage=${$('#pagLimit').val()}&orderby=${orderby}&order=${order}`
            }
            else if (search != null && searchcolumn != null && orderby != null && order != null) {
                window.location.href = `library?${filterQuery.join('&')}&search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${$('#pagLimit').val()}&orderby=${orderby}&order=${order}`
            }
            else {
                window.location.href = `library?${filterQuery.join('&')}&page=1&perpage=${$('#pagLimit').val()}`
            }
        }
    });

    $('#libraryTable thead tr th').addClass('sorting')
    $('#libraryTable thead tr th').map(th => {
        let sortingCols = ['GUID', 'Title', 'Movie / Series', 'Year', 'Created At']
        !sortingCols.includes($('#libraryTable thead tr th')[th].innerText) && $($('#libraryTable thead tr th')[th]).removeClass('sorting')
    })

    let orderby = getParameterByName('orderby', window.location.href)
    let order = getParameterByName('order', window.location.href)
    if (orderby != null && order != null) {
        orderby = (orderbyColumns.filter(col => col.name == orderby))[0].text
        $('#libraryTable thead tr th').map(th => {
            if ($('#libraryTable thead tr th')[th].innerText == orderby) {
                $($('#libraryTable thead tr th')[th]).removeClass('sorting')
                $($('#libraryTable thead tr th')[th]).addClass(`sorting_${order}`)
            }
        })
    }

    $('input[name="search"').on('keyup', function (e) {
        if (e.keyCode === 13) {
            if ($('input[name="search"').val().trim() == '') {
                $('input[name="search"').addClass("invalid-search");
            }
            else {
                window.location.href = `library?search=${$('input[name="search"]').val().trim()}&searchcolumn=${$('select[name="searchColumn"]').val().trim()}&page=1&perpage=${$('#pagLimit').val()}`
            }
        }

        if (e.keyCode === 8) {
            if ($('input[name="search"').val().trim() == '') {
                window.location.href = `library?search=${$('input[name="search"]').val().trim()}&searchcolumn=${$('select[name="searchColumn"]').val().trim()}&page=1&perpage=${$('#pagLimit').val()}`
            }
        }
    });
});