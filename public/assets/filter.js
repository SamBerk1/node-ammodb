$(document).ready(function () {
  
  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }


  $('#studioFilter').select2({
    placeholder: 'Studio',
    ajax: {
      url: '/filter/studio',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.title }
        })
        return {
          results: formatedData
        };
      }
    }
  });

  $.ajax({
    type: "get",
    url: '/filter/platform',
    success: function (response) {
      $('#platformFilter').select2({
        data: (
          response.data != null && $.map((response.data), function (item) {
            return {
              id: item.name,
              text: item.name,
            }
          })
        )
      })
      if (getParameterByName('platform', window.location.href) != null) {
        $("#platformFilter").val(getParameterByName('platform', window.location.href)).change()
      }
    }
  });


  $('#formatFilter').select2({
    placeholder: 'Format',
    ajax: {
      url: '/filter/format',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.format }
        })
        return {
          results: formatedData
        };
      }
    }
  });


  $('#genreFilter').select2({
    placeholder: 'Genre',
    ajax: {
      url: '/filter/genre',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.title }
        })
        return {
          results: formatedData
        };
      }
    }
  });

  $('#castFilter').select2({
    placeholder: 'Cast',
    ajax: {
      url: '/filter/cast',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.actorName }
        })
        return {
          results: formatedData
        };
      }
    }
  });

  $('#directorFilter').select2({
    placeholder: 'Director',
    ajax: {
      url: '/filter/director',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.name }
        })
        return {
          results: formatedData
        };
      }
    }
  });

  $('#producerFilter').select2({
    placeholder: 'Producer',
    ajax: {
      url: '/filter/producer',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.name }
        })
        return {
          results: formatedData
        };
      }
    }
  });

  $('#languageFilter').select2({
    placeholder: 'Language',
    ajax: {
      url: '/filter/language',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.name + ' (' + x.code + ')' }
        })
        return {
          results: formatedData
        };
      }
    }
  });

  $('#rightsFilter').select2({
    placeholder: 'Rights',
    ajax: {
      url: '/filter/rights',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.name }
        })
        return {
          results: formatedData
        };
      }
    }
  });

  $('#territoryFilter').select2({
    placeholder: 'Territory',
    ajax: {
      url: '/filter/territory',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.territoryName }
        })
        return {
          results: formatedData
        };
      }
    }
  });

  $('#countryFilter').select2({
    placeholder: 'Country',
    ajax: {
      url: '/filter/country',
      delay: 250,
      data: function (params) {
        var query = {
          search: params.term,
        }
        return query;
      },
      processResults: function (data) {
        let formatedData = data.data.map(x => {
          return { id: x.id, text: x.country }
        })
        return {
          results: formatedData
        };
      }
    }
  });




  $('#btnFilter').on('click', function () {
    let url = window.location.href.split('?')[0]

    let page = getParameterByName('page', window.location.href)
    let per_page = getParameterByName('perpage', window.location.href)
    page = page ? `&page=1` : ''
    per_page = per_page ? `&perpage=${per_page}` : ''

    let titleFilter = ($('#titleFilter').val() == '' || $('#titleFilter').val() == undefined) ? '' : `&title=${$('#titleFilter').val()}`
    let platformFilter = ($('#platformFilter').find(":selected").val() == '' || $('#platformFilter').find(":selected").val() == undefined) ? '' : `&platform=${$('#platformFilter').find(":selected").val()}`
    let typeFilter = ($('#isFilter').find(":selected").val() == '' || $('#isFilter').find(":selected").val() == undefined) ? '' : `&type=${$('#isFilter').find(":selected").val()}`
    let statusFilter = ($('#statusFilter').find(":selected").val() == '' || $('#statusFilter').find(":selected").val() == undefined) ? '' : `&status=${$('#statusFilter').find(":selected").val()}`

    let studioFilter = $('#studioFilter').length != 0 ? ($('#studioFilter').val().length == 0 ? '' : `&studio=` + $('#studioFilter').val()) : ''
    let formatFilter = $('#formatFilter').length != 0 ? ($('#formatFilter').val().length == 0 ? '' : `&format=` + $('#formatFilter').val()) : ''
    let genreFilter = $('#genreFilter').length != 0 ? ($('#genreFilter').val().length == 0 ? '' : `&genre=` + $('#genreFilter').val()) : ''
    let castFilter = $('#castFilter').length != 0 ? ($('#castFilter').val().length == 0 ? '' : `&cast=` + $('#castFilter').val()) : ''
    let directorFilter = $('#directorFilter').length != 0 ? ($('#directorFilter').val().length == 0 ? '' : `&director=` + $('#directorFilter').val()) : ''
    let producerFilter = $('#producerFilter').length != 0 ? ($('#producerFilter').val().length == 0 ? '' : `&producer=` + $('#producerFilter').val()) : ''
    let languageFilter = $('#languageFilter').length != 0 ? ($('#languageFilter').val().length == 0 ? '' : `&language=` + $('#languageFilter').val()) : ''
    let rightsFilter = $('#rightsFilter').length != 0 ? ($('#rightsFilter').val().length == 0 ? '' : `&rights=` + $('#rightsFilter').val()) : ''
    let territoryFilter = $('#territoryFilter').length != 0 ? ($('#territoryFilter').val().length == 0 ? '' : `&territory=` + $('#territoryFilter').val()) : ''
    let countryFilter = $('#countryFilter').length != 0 ? ($('#countryFilter').val().length == 0 ? '' : `&country=` + $('#countryFilter').val()) : ''

    let titleRights = `&titleRights=${rightsFilter.split("=")[1]}-${territoryFilter.split("=")[1]}-${countryFilter.split("=")[1]}`

    let videoStatusFilter = ($('#videoStatusFilter').find(":selected").val() == '' || $('#videoStatusFilter').find(":selected").val() == undefined) ? '' : `&videoStatus=${$('#videoStatusFilter').find(":selected").val()}`
    let captionsFilter = ($('#captionsFilter').find(":selected").val() == '' || $('#captionsFilter').find(":selected").val() == undefined) ? '' : `&captions=${$('#captionsFilter').find(":selected").val()}`
    let subtitlesFilter = ($('#subtitlesFilter').find(":selected").val() == '' || $('#subtitlesFilter').find(":selected").val() == undefined) ? '' : `&subtitles=${$('#subtitlesFilter').find(":selected").val()}`
    let artworkFilter = ($('#artworkFilter').find(":selected").val() == '' || $('#artworkFilter').find(":selected").val() == undefined) ? '' : `&artwork=${$('#artworkFilter').find(":selected").val()}`
    let dubbingFilter = ($('#dubbingFilter').find(":selected").val() == '' || $('#dubbingFilter').find(":selected").val() == undefined) ? '' : `&dubbing=${$('#dubbingFilter').find(":selected").val()}`
    let metadataFilter = ($('#metadataFilter').find(":selected").val() == '' || $('#metadataFilter').find(":selected").val() == undefined) ? '' : `&metadata=${$('#metadataFilter').find(":selected").val()}`

    let filters = {

      title: ($('#titleFilter').val() == '' || $('#titleFilter').val() == undefined) ? '' : `${$('#titleFilter').val()}`,
      platform: ($('#platformFilter').find(":selected").val() == '' || $('#platformFilter').find(":selected").val() == undefined) ? '' : `${$('#platformFilter').find(":selected").val()}`,
      type: ($('#isFilter').find(":selected").val() == '' || $('#isFilter').find(":selected").val() == undefined) ? '' : `${$('#isFilter').find(":selected").val()}`,
      status: ($('#statusFilter').find(":selected").val() == '' || $('#statusFilter').find(":selected").val() == undefined) ? '' : `${$('#statusFilter').find(":selected").val()}`,

      studio: $('#studioFilter').length != 0 ? $('#studioFilter').select2('data').map(x => x.text).join() : '',
      format: $('#formatFilter').length != 0 ? $('#formatFilter').select2('data').map(x => x.text).join() : '',
      genre: $('#genreFilter').length != 0 ? $('#genreFilter').select2('data').map(x => x.text).join() : '',
      cast: $('#castFilter').length != 0 ? $('#castFilter').select2('data').map(x => x.text).join() : '',
      director: $('#directorFilter').length != 0 ? $('#directorFilter').select2('data').map(x => x.text).join() : '',
      producer: $('#producerFilter').length != 0 ? $('#producerFilter').select2('data').map(x => x.text).join() : '',
      language: $('#languageFilter').length != 0 ? $('#languageFilter').select2('data').map(x => x.text).join() : '',
      rights: $('#rightsFilter').length != 0 ? $('#rightsFilter').select2('data').map(x => x.text).join() : '',
      territory: $('#territoryFilter').length != 0 ? $('#territoryFilter').select2('data').map(x => x.text).join() : '',
      country: $('#countryFilter').length != 0 ? $('#countryFilter').select2('data').map(x => x.text).join() : '',

      videoStatus: ($('#videoStatusFilter').find(":selected").val() == '' || $('#videoStatusFilter').find(":selected").val() == undefined) ? '' : `${$('#videoStatusFilter').find(":selected").text()}`,
      captions: ($('#captionsFilter').find(":selected").val() == '' || $('#captionsFilter').find(":selected").val() == undefined) ? '' : `${$('#captionsFilter').find(":selected").text()}`,
      subtitles: ($('#subtitlesFilter').find(":selected").val() == '' || $('#subtitlesFilter').find(":selected").val() == undefined) ? '' : `${$('#subtitlesFilter').find(":selected").text()}`,
      artwork: ($('#artworkFilter').find(":selected").val() == '' || $('#artworkFilter').find(":selected").val() == undefined) ? '' : `${$('#artworkFilter').find(":selected").text()}`,
      dubbing: ($('#dubbingFilter').find(":selected").val() == '' || $('#dubbingFilter').find(":selected").val() == undefined) ? '' : `${$('#dubbingFilter').find(":selected").text()}`,
      metadata: ($('#metadataFilter').find(":selected").val() == '' || $('#metadataFilter').find(":selected").val() == undefined) ? '' : `${$('#metadataFilter').find(":selected").text()}`,
    }
   
    
    if (titleFilter != '' || studioFilter != '' || statusFilter != '' || platformFilter != '' || typeFilter != ''
      || formatFilter != '' || genreFilter != '' || castFilter != '' || directorFilter != ''
      || producerFilter != '' || languageFilter != '' || rightsFilter != '' || territoryFilter != '' || countryFilter != ''
      || videoStatusFilter != '' || captionsFilter != '' || subtitlesFilter != '' || artworkFilter != '' || dubbingFilter != '' || metadataFilter) {
      let query = url + '?filter=true' + titleFilter + studioFilter
        + statusFilter + platformFilter + typeFilter + formatFilter + genreFilter
        + castFilter + directorFilter + producerFilter + languageFilter + rightsFilter + territoryFilter + countryFilter + titleRights
        + videoStatusFilter + captionsFilter + subtitlesFilter + artworkFilter + dubbingFilter + metadataFilter + page + per_page
      window.location.href = query
    }
    
  });

  $('#exportData').on('click', function () {
    let url = window.location.href.split('?')[0]
    let titleFilter = ($('#titleFilter').val() == '' || $('#titleFilter').val() == undefined) ? '' : `&title=${$('#titleFilter').val()}`
    let platformFilter = ($('#platformFilter').find(":selected").val() == '' || $('#platformFilter').find(":selected").val() == undefined) ? '' : `&platform=${$('#platformFilter').find(":selected").val()}`
    let typeFilter = ($('#isFilter').find(":selected").val() == '' || $('#isFilter').find(":selected").val() == undefined) ? '' : `&type=${$('#isFilter').find(":selected").val()}`
    let statusFilter = ($('#statusFilter').find(":selected").val() == '' || $('#statusFilter').find(":selected").val() == undefined) ? '' : `&status=${$('#statusFilter').find(":selected").val()}`

    let formatFilter = getParameterByName('format', window.location.href) != null ? `&format=` + getParameterByName('format', window.location.href) : ''
    let studioFilter = getParameterByName('studio', window.location.href) != null ? `&studio=` + getParameterByName('studio', window.location.href) : ''
    let genreFilter = getParameterByName('genre', window.location.href) != null ? `&genre=` + getParameterByName('genre', window.location.href) : ''
    let castFilter = getParameterByName('cast', window.location.href) != null ? `&cast=` + getParameterByName('cast', window.location.href) : ''
    let directorFilter = getParameterByName('director', window.location.href) != null ? `&director=` + getParameterByName('director', window.location.href) : ''
    let producerFilter = getParameterByName('producer', window.location.href) != null ? `&producer=` + getParameterByName('producer', window.location.href) : ''
    let languageFilter = getParameterByName('language', window.location.href) != null ? `&language=` + getParameterByName('language', window.location.href) : ''
    let rightsFilter = getParameterByName('rights', window.location.href) != null ? `&rights=` + getParameterByName('rights', window.location.href) : ''
    let territoryFilter = getParameterByName('territory', window.location.href) != null ? `&territory=` + getParameterByName('territory', window.location.href) : ''
    let countryFilter = getParameterByName('country', window.location.href) != null ? `&country=` + getParameterByName('country', window.location.href) : ''

    let titleRights = `&titleRights=${rightsFilter.split("=")[1]}-${territoryFilter.split("=")[1]}-${countryFilter.split("=")[1]}`

    let videoStatusFilter = ($('#videoStatusFilter').find(":selected").val() == '' || $('#videoStatusFilter').find(":selected").val() == undefined) ? '' : `&videoStatus=${$('#videoStatusFilter').find(":selected").val()}`
    let captionsFilter = ($('#captionsFilter').find(":selected").val() == '' || $('#captionsFilter').find(":selected").val() == undefined) ? '' : `&captions=${$('#captionsFilter').find(":selected").val()}`
    let subtitlesFilter = ($('#subtitlesFilter').find(":selected").val() == '' || $('#subtitlesFilter').find(":selected").val() == undefined) ? '' : `&subtitles=${$('#subtitlesFilter').find(":selected").val()}`
    let artworkFilter = ($('#artworkFilter').find(":selected").val() == '' || $('#artworkFilter').find(":selected").val() == undefined) ? '' : `&artwork=${$('#artworkFilter').find(":selected").val()}`
    let dubbingFilter = ($('#dubbingFilter').find(":selected").val() == '' || $('#dubbingFilter').find(":selected").val() == undefined) ? '' : `&dubbing=${$('#dubbingFilter').find(":selected").val()}`
    let metadataFilter = ($('#metadataFilter').find(":selected").val() == '' || $('#metadataFilter').find(":selected").val() == undefined) ? '' : `&metadata=${$('#metadataFilter').find(":selected").val()}`


    if (titleFilter != '' || studioFilter != '' || statusFilter != '' || platformFilter != '' || typeFilter != ''
      || formatFilter != '' || genreFilter != '' || castFilter != '' || directorFilter != ''
      || producerFilter != '' || languageFilter != '' || rightsFilter != '' || territoryFilter != '' || countryFilter != ''
      || videoStatusFilter != '' || captionsFilter != '' || subtitlesFilter != '' || artworkFilter != '' || dubbingFilter != '' || metadataFilter) {
      let query = url + '/export?filter=true' + titleFilter + studioFilter + statusFilter + platformFilter + typeFilter + formatFilter + genreFilter + castFilter + directorFilter + producerFilter + languageFilter + rightsFilter + territoryFilter + countryFilter + titleRights
        + videoStatusFilter + captionsFilter + subtitlesFilter + artworkFilter + dubbingFilter + metadataFilter
      console.log(query, videoStatusFilter)
      window.location.href = query
    }
    else {
      window.location.href = url + '/export'
    }

  });

  $('#btnClearFilter').on('click', function () {
    let page = getParameterByName('page', window.location.href)
    let per_page = getParameterByName('perpage', window.location.href)
    page = page ? `?page=1` : ''
    per_page = per_page ? `&perpage=${per_page}` : ''
    window.location.href = window.location.href.split('?')[0] + page + per_page
  });


  if (getParameterByName('title', window.location.href) != null) {
    $("#titleFilter").val(getParameterByName('title', window.location.href))
  }

  if (getParameterByName('type', window.location.href) != null) {
    $("#isFilter").val(getParameterByName('type', window.location.href)).change()
  }

  if (getParameterByName('status', window.location.href) != null) {
    $("#statusFilter").val(getParameterByName('status', window.location.href)).change()
  }


  if (getParameterByName('videoStatus', window.location.href) != null) {
    $("#videoStatusFilter").val(getParameterByName('videoStatus', window.location.href)).change()
  }
  if (getParameterByName('captions', window.location.href) != null) {
    $("#captionsFilter").val(getParameterByName('captions', window.location.href)).change()
  }
  if (getParameterByName('subtitles', window.location.href) != null) {
    $("#subtitlesFilter").val(getParameterByName('subtitles', window.location.href)).change()
  }
  if (getParameterByName('artwork', window.location.href) != null) {
    $("#artworkFilter").val(getParameterByName('artwork', window.location.href)).change()
  }
  if (getParameterByName('dubbing', window.location.href) != null) {
    $("#dubbingFilter").val(getParameterByName('dubbing', window.location.href)).change()
  }
  if (getParameterByName('metadata', window.location.href) != null) {
    $("#metadataFilter").val(getParameterByName('metadata', window.location.href)).change()
  }

  if (getParameterByName('filter', window.location.href) == 'true') {
    let filters = window.location.href.split('?')[1].split('&')
    let badges = 0;
    filters.map(x => {
      if (!x.includes('filter') && !x.includes('search') && !x.includes('searchColumn') && !x.includes('order') && !x.includes('orderby') && !x.includes('page') && !x.includes('perpage')) {
        badges++;
      }
    })
    $('#filterBadge').text(badges)
  }

  


  $('#btnExportModal').on('click', function () {
    let url = window.location.href.split('?')[0]

    let titleFilter = ($('#titleFilter').val() == '' || $('#titleFilter').val() == undefined) ? '' : `&title=${$('#titleFilter').val()}`
    let platformFilter = ($('#platformFilter').find(":selected").val() == '' || $('#platformFilter').find(":selected").val() == undefined) ? '' : `&platform=${$('#platformFilter').find(":selected").val()}`
    let typeFilter = ($('#isFilter').find(":selected").val() == '' || $('#isFilter').find(":selected").val() == undefined) ? '' : `&type=${$('#isFilter').find(":selected").val()}`
    let statusFilter = ($('#statusFilter').find(":selected").val() == '' || $('#statusFilter').find(":selected").val() == undefined) ? '' : `&status=${$('#statusFilter').find(":selected").val()}`

    let formatFilter = getParameterByName('format', window.location.href) != null ? `&format=` + getParameterByName('format', window.location.href) : ''
    let studioFilter = getParameterByName('studio', window.location.href) != null ? `&studio=` + getParameterByName('studio', window.location.href) : ''
    let genreFilter = getParameterByName('genre', window.location.href) != null ? `&genre=` + getParameterByName('genre', window.location.href) : ''
    let castFilter = getParameterByName('cast', window.location.href) != null ? `&cast=` + getParameterByName('cast', window.location.href) : ''
    let directorFilter = getParameterByName('director', window.location.href) != null ? `&director=` + getParameterByName('director', window.location.href) : ''
    let producerFilter = getParameterByName('producer', window.location.href) != null ? `&producer=` + getParameterByName('producer', window.location.href) : ''
    let languageFilter = getParameterByName('language', window.location.href) != null ? `&language=` + getParameterByName('language', window.location.href) : ''
    let rightsFilter = getParameterByName('rights', window.location.href) != null ? `&rights=` + getParameterByName('rights', window.location.href) : ''
    let territoryFilter = getParameterByName('territory', window.location.href) != null ? `&territory=` + getParameterByName('territory', window.location.href) : ''
    let countryFilter = getParameterByName('country', window.location.href) != null ? `&country=` + getParameterByName('country', window.location.href) : ''

    let titleRights = `&titleRights=${rightsFilter.split("=")[1]}-${territoryFilter.split("=")[1]}-${countryFilter.split("=")[1]}`

    let videoStatusFilter = ($('#videoStatusFilter').find(":selected").val() == '' || $('#videoStatusFilter').find(":selected").val() == undefined) ? '' : `&videoStatus=${$('#videoStatusFilter').find(":selected").val()}`
    let captionsFilter = ($('#captionsFilter').find(":selected").val() == '' || $('#captionsFilter').find(":selected").val() == undefined) ? '' : `&captions=${$('#captionsFilter').find(":selected").val()}`
    let subtitlesFilter = ($('#subtitlesFilter').find(":selected").val() == '' || $('#subtitlesFilter').find(":selected").val() == undefined) ? '' : `&subtitles=${$('#subtitlesFilter').find(":selected").val()}`
    let artworkFilter = ($('#artworkFilter').find(":selected").val() == '' || $('#artworkFilter').find(":selected").val() == undefined) ? '' : `&artwork=${$('#artworkFilter').find(":selected").val()}`
    let dubbingFilter = ($('#dubbingFilter').find(":selected").val() == '' || $('#dubbingFilter').find(":selected").val() == undefined) ? '' : `&dubbing=${$('#dubbingFilter').find(":selected").val()}`
    let metadataFilter = ($('#metadataFilter').find(":selected").val() == '' || $('#metadataFilter').find(":selected").val() == undefined) ? '' : `&metadata=${$('#metadataFilter').find(":selected").val()}`


    if (titleFilter != '' || studioFilter != '' || statusFilter != '' || platformFilter != '' || typeFilter != ''
      || formatFilter != '' || genreFilter != '' || castFilter != '' || directorFilter != ''
      || producerFilter != '' || languageFilter != '' || rightsFilter != '' || territoryFilter != '' || countryFilter != ''
      || videoStatusFilter != '' || captionsFilter != '' || subtitlesFilter != '' || artworkFilter != '' || dubbingFilter != '' || metadataFilter) {
      let query = url + '/export?filter=true' + titleFilter + studioFilter + statusFilter + platformFilter + typeFilter + formatFilter + genreFilter + castFilter + directorFilter + producerFilter + languageFilter + rightsFilter + territoryFilter + countryFilter + titleRights
        + videoStatusFilter + captionsFilter + subtitlesFilter + artworkFilter + dubbingFilter + metadataFilter
      $('#exportModal form').attr('action', query)
    }
    else {
      $('#exportModal form').attr('action', url + '/export')
    }

  });




  $('#btnSelectAll').on('click', function (e) {
    e.preventDefault()
    $('#checkBoxesExport input[type="checkbox"]').prop('checked', true);
  })

  $('#btnDelectAll').on('click', function (e) {
    e.preventDefault()
    $('#checkBoxesExport input[type="checkbox"]').prop('checked', false);
  })

});