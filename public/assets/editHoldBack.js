function formatDate(input) {
    let date = new Date(input)
    return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate()
}
$(document).on("click", ".open-editHoldBackModal", function () {
    var data = {holback:$(this).data('holdback'),action:$(this).data('action')}
    console.log(data)
    $('#editHoldBackModal h5.modal-title').text(`Edit HoldBacks For "${data.holback.library.title}"`)    
    $('#editHoldBackModal form').attr('action',data.action)    
    $('#editHoldBackModal form input[name="name"]').val(data.holback.name)    
    $('#editHoldBackModal form input[name="startDate"]').val(formatDate(data.holback.startDate))    
    $('#editHoldBackModal form input[name="endDate"]').val(formatDate(data.holback.endDate))    
    $('#editHoldBackModal form select[name="libraryId"]').val(data.holback.library.id).trigger('change');    
});