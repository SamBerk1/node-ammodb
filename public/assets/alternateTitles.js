$(document).ready(function () {
    $("#alternateTitlesBtn").click(function (e) { 
        e.preventDefault();
        if($("#alterName").val().trim()!='' && $("#alterLanguage").val()!='null' && $("#alterSynopsis").val().trim()!=''){
          $("#alternateTitlesTable").append(`
          <tr>
            <td>${$("#alterName").val()}</td>
            <td><textarea maxlength="400" readonly class="form-control" placeholder="Alter Synopsis" rows="3">${$("#alterSynopsis").val()}</textarea></td>
            <td>${$("#alterLanguage").val()}</td>
            <td></td>
            <td>
              <button id='btnTitles' class="btn btn-danger" style="padding: .2rem .4rem;"> <i class="ti-trash icon-btn"></i> </button>
                  <input type="text" name="alternateNames[]" id="" value="${$("#alterName").val()+'/~/'+$("#alterLanguage").val()+'/~/'+$("#alterSynopsis").val()}" hidden>
            </td>
        
          </tr>
          `);
          $("#alterName").val('')
          $("#customers_select").val('')
          $("#alterSynopsis").val('')
          $('#alternate_synopsis_charCount').text('400 remaining')
        }
      });
      
      $("#alternateTitlesTable").on('click','#btnTitles',function(){
        $(this).parent().parent().remove();   
      });
});