$(document).ready(function() {

    $('#pagLimit').on('change',function (e) {
        if(e.target.value!=''){
            window.location.href = `/incoming?page=1&perpage=${parseInt(e.target.value)}`
        }
    });

    if (window.location.href.includes('/incoming?id=')) {
        let elm = $(`#incoming_${window.location.href.split('/incoming?id=')[1]}`).closest('tr').find('i.btn-expandDetail');
        $(elm).trigger("click");
    }

    $('#incomingTable').DataTable({
        "bInfo": false, //Dont display info e.g. "Showing 1 to 4 of 4 entries"
        "paging": false,//Dont want paging
        "bPaginate": false,//Dont want paging   ,
        "searching":false,
        "sort":false,
        columnDefs: [
            { "width": "25%", "targets": [1] },
          ]
    });


    function format(data) {
        let start = `<table cellpadding="5" cellspacing=0 style="padding-left:50px;">
            <tbody>
            <tr style="background-color: #212121">
                <td><strong>GUID</strong></td>
                <td><strong>Season Name</strong></td>
                <td><strong>Season Number</strong></td>
                <td><strong>Total Episodes</strong></td>
                <th><strong>Captions</th>
                <th><strong>Subs</strong></th>
                <th><strong>Art Work</strong></th>
                <th><strong>Dubbing</strong></th>
                <th><strong>Metadata</strong></th>
                <th><strong>VideoStatus</strong></th>
                <td><strong>Action</strong></td>
            </tr>`

          let body =  data.incomingseasons.map(element => {
                   return `<tr style="background-color: #282828">
                    <td>${element.season.guId}</td>
                    <td>${element.season.name}</td>
                    <td>${element.season.number}</td>
                    <td>${element.__meta__.incomingEpisodes_count}</td>
                    <td>`+
                        // (element.captions.map( status => {
                            // if(status.selected){
                                `${element.captions == null ? '' : `<img data-toggle="tooltip" data-placement="top" title="${element.captions.name}" style="width: 48px; height: 48px;" src="${element.captions.icon}" alt="${element.captions.name}">`}`
                            // }
                        // })).join('')
                        +
                    '</td><td>'+
                        // (element.subtitles.map( status => {
                            // if(status.selected){
                                `${element.subtitles == null ? '' : `<img data-toggle="tooltip" data-placement="top" title="${element.subtitles.name}" style="width: 48px; height: 48px;" src="${element.subtitles.icon}" alt="${element.subtitles.name}">`}`
                            // }
                        // })).join('')
                        +
                    `</td><td>`+
                        // (element.artwork.map( status => {
                            // if(status.selected){
                                `${element.artwork == null ? '' : `<img data-toggle="tooltip" data-placement="top" title="${element.artwork.name}" style="width: 48px; height: 48px;" src="${element.artwork.icon}" alt="${element.artwork.name}">`}`
                            // }
                        // })).join('')
                        +
                    `</td><td>`+
                        // (element.dubbing.map( status => {
                            // if(status.selected){
                                `${element.dubbing == null ? '' : `<img data-toggle="tooltip" data-placement="top" title="${element.dubbing.name}" style="width: 48px; height: 48px;" src="${element.dubbing.icon}" alt="${element.dubbing.name}">`}`
                            // }
                        // })).join('')
                        +
                    `</td><td>`+
                        // (element.metadata.map( status => {
                            // if(status.selected){
                                `${element.metadata == null ? '' : `<img data-toggle="tooltip" data-placement="top" title="${element.metadata.name}" style="width: 48px; height: 48px;" src="${element.metadata.icon}" alt="${element.metadata.name}">`}`
                            // }
                        // })).join('')
                        +
                    `</td><td>`+
                        // (element.videoStatus.map( status => {
                            // if(status.selected){
                                `${element.videoStatus == null ? '' : `<img data-toggle="tooltip" data-placement="top" title="${element.videoStatus.name}" style="width: 48px; height: 48px;" src="${element.videoStatus.icon}" alt="${element.videoStatus.name}">`}`
                            // }
                        // })).join('')
                        +
                    `</td>
                    <td>
                        <div class="dropdown">
                            <a class="btn btn-transparent border p-2 default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class=" icon-options"></i>
                            </a>
                            <div class="dropdown-menu ">
                                <a class="dropdown-item" href="/incoming/${data.id}/season/${element.id}/episodes"><i class="ti-video-clapper icon-btn text-success pr-2"></i>Episodes</a>
                                <a class="dropdown-item open-addToPm" data-title="${data.title} Season: ${element.season.name}" data-season="${element.id}" data-action="/incoming/${data.id}/season/${element.id}/addtoproject_management" data-toggle="modal" data-target="#addToPmModal"> <i class="ti-plus icon-btn text-info pr-2"></i> Programming</a>
                            </div>
                        </div>
                    </td>
                </tr>`
                })

        let end = `</tbody>
                <tfoot>
                    <tr style="background-color: #212121"><td colspan="12"></td></tr>
                </tfoot>
            </table>`

                return start + body.join('') + end


    }

    $('#incomingTable tbody').on('click', 'td .btn-expandDetail', function () {
        var tr = $(this).closest('tr');
        var row = $('#incomingTable').DataTable().row( tr );
        let data = {incomingseasons:$(tr).data("incomingseasons"),title:$(tr).data("title"),id:$(tr).data("id")}

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            $(this).toggleClass('fa-minus-circle text-danger')
            $(this).toggleClass('fa-plus-circle text-success')
        }
        else {
            // Open this row
            row.child(format(data)).show();
            tr.addClass('shown');
            $(this).toggleClass('fa-minus-circle text-danger')
            $(this).toggleClass('fa-plus-circle text-success')
        }
} );

} );

$(document).on('click','.open-addToPm',function () {
    $('#addToPmModal h5.title').html(`"${$(this).data('title')}"${$(this).data('subtitle') ? "<br>"+$(this).data('subtitle') : ''}`)
    $('#addToPmModal form').attr('action',$(this).data('action'))
    $('#addToPmModal form #incomingTitleSearch').val($(this).data('id'))
    $('#addToPmModal form #incomingSeasonSearch').val($(this).data('season'))
    $('#addToPmModal form #titleStatusDiv').html('')
    $('#addToPmModal form #applicablePlatforms').val('').change()
 })


 let preventEvent = true

 $('#addToPmModal').on('show.bs.modal', function() {
     $('#confirmMsg').html('')
     $('#confirmMsg').removeClass('mt-2 ml-2 mr-2')
     $('#btnSchedule').show()
     $('#btnConfirmSchedule').hide()
     preventEvent = true
 })

 $('#applicablePlatforms').on('change',function () {
    $('#confirmMsg').html('')
    $('#confirmMsg').removeClass('mt-2 ml-2 mr-2')
    $('#btnSchedule').show()
    $('#btnConfirmSchedule').hide()
    $('#addToPmModal form #titleStatusDiv').html('')
    preventEvent = true
 });

 $("#addToPmForm").on('submit',function (event) {
     preventEvent && event.preventDefault()
     let data = $(this).attr('action')
     $.ajax({
         type: "post",
         url: data.includes('season') ? '/project-management/season/check-duplicates' : '/project-management/movie/check-duplicates',
         data:{incomingId:$('#incomingTitleSearch').val(),incomingSeasonId:$('#incomingSeasonSearch').val(),platforms:$('#applicablePlatforms').val()},
         success: function (response) {
             if(response.flag && response.duplicate.length > 0){
                 let error = response.duplicate.map( x => {
                    return $('#applicablePlatforms option[value="'+x+'"]').text()
                 }).join()
                 preventEvent && $('#confirmMsg').html(`<p>These Platform have duplicate entries Confirm to Proceed.</p><p>${error}</p>`)
                 preventEvent && $('#confirmMsg').addClass('mt-2 ml-2 mr-2')
                 preventEvent && $('#btnSchedule').hide()
                 preventEvent && $('#btnConfirmSchedule').show()
                 if(response.titleStatus && preventEvent){
                    response.titleStatus.length > 0 && $('#addToPmModal form #titleStatusDiv').append(`<br><h6>Select The Programming Entry to add seasons</h6>`)
                    response.titleStatus.map( item => {
                        $('#addToPmModal form #titleStatusDiv').append(`<label class="control control-solid control-solid-info control--checkbox"><strong>${item.alternateName.alternateName}</strong> | Platform: ${item.platform.name} | CreatedAt: ${item.created_at}<input type="checkbox" name="titleStatus[]" value="${item.id}" ><span class="control__indicator"></span></label>`)
                    })
                 }
                preventEvent = false
             }
             else {
                 preventEvent && (
                     preventEvent = false,
                     $('#btnConfirmSchedule').trigger('click')
                 )

             }
         }
     });
 })
