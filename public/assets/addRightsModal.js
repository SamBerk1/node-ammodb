$(document).ready(function () {
    $('#territoryData').on('change',function (e) {
        let territory = $(this).find(':selected').val()
        let text = $(this).find(':selected').text()

        if ($(this).find(':selected').text().toLowerCase() == 'world wide') {
            $('#territoryData option').each((index, data) => {
                if (data.value != '' && data.text.toLowerCase() != 'world wide') {
                    $(`#territoryListTab #territory${territory}`).remove()
                    $(`#countryListTab #territoryCountry${territory}`).remove()
                    addTerritory(data.value,data.text,data)                    
                }
            })
        }
        else{
            if (territory!='') {
                $(`#territoryListTab #territory${territory}`).remove()
                $(`#countryListTab #territoryCountry${territory}`).remove()
            }
            addTerritory(territory,text)
        }

        
    })  

    function addTerritory (territory,text) { 
        let data = $(`#territoryData option[value="${territory}"]`).attr('data-country')
        
        if ($(`#territoryListTab #territory${territory}`).length == 0 && territory!='') {
            $('#territoryListTab').append(`<a title="Double Click To Remove" class="list-group-item list-group-item-action" data-id="${territory}" id="territory${territory}" data-toggle="list" href="#territoryCountry${territory}" role="tab">${text} <i class="fa fa-close pr-4"></i></a>`)

            $('#countryListTab').append(`
                <div class="tab-pane fade" id="territoryCountry${territory}" role="tabpanel" aria-labelledby="list-profile-list">`
                +
                `<select name='countries' id="territorySelectCountry${territory}" class="js-example-basic-multiple custom-select d-block my-3" style="width: 100%;" multiple="multiple">`
                +
                JSON.parse(data).map(item => {
                    return `<option selected value='{"id":"${item.id}","territoryId":"${item.territoryId}"}'>${item.country}</option>`
                }).join()
                +
                `</select>`
                +
                `</div>
            `)

            $(`#territorySelectCountry${territory}`).select2()
            $("#territoryData").val('').change()
        }

        $('#territoryListTab a').dblclick(function (e) {
            $(`#territoryListTab #territory${$(this).data().id}`).remove()
            $(`#countryListTab #territoryCountry${$(this).data().id}`).remove()
            e.preventDefault()
        })

        return
    }
})
    