$(document).ready(function () {  
    $("#screenerDynamicDiv").hide();
    
    $("#screenerBtn").click(function (e) { 
      e.preventDefault();
      $("#showPassword").popover("dispose")
      $("#screenerSelectDiv").hide();
      $("input[name ='screener[url]']").val('');
      $("input[name ='screener[name]']").val('');
      $("input[name ='screener[pw]']").val('');
      $("#screenerDynamicDiv").show();
    });
    
    $("#screenerCancel").click(function (e) { 
      e.preventDefault();
      $("#screenerSelectDiv").show();
      $("input[name ='screener[url]']").val('');
      $("input[name ='screener[name]']").val('');
      $("input[name ='screener[pw]']").val('');
      $("#screenerDynamicDiv").hide();
    });
    
    
    $("#trailerDynamicDiv").hide();
    
    $("#trailerBtn").click(function (e) { 
      e.preventDefault();
      $("#trailerSelectDiv").hide();
      $("input[name ='trailer[url]']").val('');
      $("input[name ='trailer[name]']").val('');
      $("#trailerDynamicDiv").show();
    });
    
    $("#trailerCancel").click(function (e) { 
      e.preventDefault();
      $("#trailerSelectDiv").show();
      $("input[name ='trailer[url]']").val('');
      $("input[name ='trailer[name]']").val('');
      $("#trailerDynamicDiv").hide();
    });
    
    function getScreenerInfo(){
      if ($("select[name ='screenerId']").val() && $("select[name ='screenerId']").val() != 'null') {
        var screener = $("select[name ='screenerId']").find('option:selected').attr('data');
        screener = screener.split("~")
        console.log(screener)
        return {password:screener[0],link:screener[1]}
      }
      else{
        return { password:null , link:null}
      }
      
    }
    
    $("select[name ='screenerId']").change(function (e) { 
      $("#showPassword").popover("dispose")
      $("#showPassword").popover({title: "Screener Info",content: `Link:<br><a style="color:black;" target="_blank" href=${getScreenerInfo().link=='null' ? '' : getScreenerInfo().link} >${getScreenerInfo().link=='null' ? '' : getScreenerInfo().link}</a> <br> Password: ${getScreenerInfo().password=='null' ? '' : getScreenerInfo().password}`,html: true});
    });
    
    $("#showPassword").click(function (e) { 
      e.preventDefault();
      $("#showPassword").popover("enable")  
    });
    
    
    $("#showPassword").popover({title: "Screener Info",content: `Link:<br><a style="color:black;" target="_blank" href=${getScreenerInfo().link=='null' ? '' : getScreenerInfo().link} >${getScreenerInfo().link=='null' ? '' : getScreenerInfo().link}</a> <br> Password: ${getScreenerInfo().password=='null' ? '' : getScreenerInfo().password}`,html: true});
});