$(document).ready(function () {
  
    $('#confirmationModal').on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget) // Button that triggered the modal
        const incomingid = button.data('incomingid')
        const platformid = button.data('platformid')

        const modal = $(this)
        modal.find(".modal-footer input[name='incomingTitle']")[0].value = incomingid
        modal.find(".modal-footer input[name='platforms[]']")[0].value = platformid
      })



      $('#deleteConfirmationModal').on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget) // Button that triggered the modal
        const action = button.data('action')

        const modal = $(this)
        modal.find(".modal-footer form")[0].setAttribute("action",action)
      })

      
});