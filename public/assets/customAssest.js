$(document).ready(function () {

    $("[id=customAssetForm]").on('click','#customAssetBtn',function(e){
        e.preventDefault();
        let customAssetName = ($(this).parent().parent().parent().parent().parent()).find('input')[0]
        let customAssetStatus = ($(this).parent().parent().parent().parent().parent()).find('select')[0]
        if(customAssetName.value!='' && customAssetStatus.value!=''){
          $(this).parent().parent().parent().parent().parent().append(`
          <div class="row">
            <div class="col-md-5 mb-3">
                <input class="form-control" type="text" value='${customAssetName.value}' placeholder="Custom Asset Name" name="customAsset[name]">               
            </div>
            <div class="col-md-6 mb-3">
              <div class="row">
                  <div class="col-md-10">
                      <input class="form-control" type="text" value='${customAssetStatus.value}' placeholder="Custom Asset Status" name="customAsset[status]"> 
                  </div>    
                  <div class="col-md-2">
                      <button id="customAssetRemove" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                  </div>  
              </div> 
            </div> 
          </div>
          `)
  
          customAssetName.value = ''
          customAssetStatus.selectedIndex = 0
        }
      });
  
      $("[id=customAssetForm]").on('click','#customAssetRemove',function(e){
        e.preventDefault();
        $(this).parent().parent().parent().parent().remove()
      });

});