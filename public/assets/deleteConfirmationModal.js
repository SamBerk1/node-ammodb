$(document).on("click", ".open-deleteConfirmModal", function () {
    $('#deleteConfirmationModal h5.modal-title').text($(this).data('title'))
    $('#deleteConfirmationModal div.modal-body').text($(this).data('message'))
});