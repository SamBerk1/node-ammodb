$(document).ready( function () {  
    $('#userSearch').select2({
        tags: true,
        placeholder : 'Users',
        ajax: {
          url: '/users/search',
          delay: 250,
          data: function (params) {
            var query = {
              search: params.term,
            }      
            return query;
          },
          processResults: function (data) {
              let formatedData = data.users.map( x=> {
                  return {id:x.email,text:`${x.fullname} (${x.email})`}
              })
            return {
              results: formatedData
            };
          }
        }
    });

    
});