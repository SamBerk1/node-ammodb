/**
 * Created by mosaddek on 1/24/18.
 */

$(document).ready(function() {
    
    $('.bs4-table thead th').each( function () {
        var title = $(this).text();
        if (!(title == 'Action' || title == 'action') && !(title == 'Id' || title == 'id') && !(title == 'Notes' || title == 'notes') && !(title == 'captions' || title == 'Captions') && !(title == 'art work' || title == 'Art Work') && !(title == 'subs' || title == 'Subs') && !(title == 'dubbing' || title == 'Dubbing') && !(title == 'metadata' || title == 'Metadata') && !(title.replace(' ','') == 'videostatus' || title.replace(' ','') == 'VideoStatus')) {
            $(this).append( '<input class="form-control" type="text" placeholder="Search"/>' );
        }
    } );
 
    // DataTable
    var table = $('.bs4-table').DataTable()
    
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
