$(document).ready(function () {
    $('select').select2({
      createTag: function (params) {
        var term = $.trim(params.term);
    
        if (term === '') {
          return null;
        }
    
        return {
          id: term,
          text: term,
          newTag: true // add additional parameters
        }
      }
    });

    $("select").on("select2:select", function (evt) {
      var element = evt.params.data.element;
      var $element = $(element);
    
      $element.detach();
      $(this).append($element);
      $(this).trigger("change");
    });

    $('.js-example-basic-single').select2();
    $(".js-example-tags").select2({
    tags: true,
    });


    $('.js-example-basic-multiple').select2();
    $(".js-example-tokenizer").select2({
    tags: true,
    tokenSeparators: [',']
    });    
});