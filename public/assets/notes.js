$(document).ready(function () {

    $("[id=notesForm]").on('click','#notesBtn',function(e){
        e.preventDefault();
        let noteTitle = ($(this).parent().parent().parent()).find('input')[0]
        let noteText = ($(this).parent().parent().parent()).find('textarea')[0]
        if(noteTitle.value!='' && noteText.value!=''){
          $(this).parent().parent().parent().parent().append(`
          <div class="horiznontal">
            <div class="mb-2">
                <label for="">Title:</label>
                <input class="form-control" type="text" value='${noteTitle.value}' placeholder="Title for note" name="notes[title]">               
            </div>
            <div class="mb-2">
                    <label for="">Note:</label>
                    <textarea class="form-control" name="notes[text]" cols="" rows="3">${noteText.value}</textarea>
            </div>      
            <div class="row justify-content-end">
                <button id="noteRemove" class="btn btn-danger"><i class="fa fa-minus"></i></button>
            </div>    
         </div>
          `)
          
          noteTitle.value = ''
          noteText.value = ''
        }
      });

      $("#notesForm .horiznontal").on('click','#noteRemove',function (e) {
        e.preventDefault();
        $(this).parent().parent().remove()
      });
});