$(document).on("click", ".open-addPitchModal", function () {
    var data = {title : $(this).data('title'),action:$(this).data('action')};
    $('#addPitchModal h5.modal-title').text(`Pitch "${data.title}"`)    
    $('#addPitchModal form').attr('action',data.action)    
});

$(document).on("click", ".open-editPitchModal", function () {
    var data = {title : $(this).data('title'),action:$(this).data('action'),pitchDate:$(this).data('date'),status:$(this).data('status'),platform:$(this).data('platform'),libraryId:$(this).data('libraryid')};
    $('#editPitchModal h5.modal-title').text(`Edit Pitch "${data.title}"`)    
    $('#editPitchModal form').attr('action',data.action)    
    $('#editPitchModal form input[name="pitchDate"]').val(data.pitchDate)    
    $('#editPitchModal form select[name="status"]').val(data.status).trigger('change')
    $('#editPitchModal form input[name="pitchDate"]').datepicker('setDate', data.pitchDate)
    $('#editPitchModal form input[name="platform"]').val(data.platform)    
    $('#editPitchModal form select[name="libraryId"]').val(data.libraryId).trigger('change') 
});