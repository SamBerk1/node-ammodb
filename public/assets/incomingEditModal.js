$(document).ready(function () {  

    $("a.statusLinks").on("click", function(e){
        e.preventDefault()
        let modalId = $(this).closest('a').attr('data-target')
        let clickedStatus = $(this).attr('name')
        $(modalId).find(`select[name|='${clickedStatus}']`).closest('div').addClass('highlightOptDiv')
        
        $(modalId).on('hide.bs.modal', function () {
          $(modalId).find(`select[name|='${clickedStatus}']`).closest('div').removeClass('highlightOptDiv')
        });
  
      });

});