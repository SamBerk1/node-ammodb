$(document).ready(function () {
    const sortingCols = ['Title','Platform','Formated Time','Time','Adbreak Format','Keyword','Name','Code','Award Name','Award Type','Format','Territory Name','Start Date','End Date','Rating Name']
    const orderbyColumns = [{text:'Platform',title:'platform'},{text:'Formated Time',title:'timeFormated'},{text:'Time',title:'time'},{text:'Adbreak Format',title:'adbreakFormat'},{text:'Title',title:'title'},{text:'Keyword',title:'keyword'},{text:'Name',title:'name'},{text:'Code',title:'code'},{text:'Award Name',title:'awardName'},{text:'Award Name',title:'awardName'},{text:'Format',title:'format'},{text:'Territory Name',title:'territoryName'},{text:'Start Date',title:'startDate'},{text:'End Date',title:'endDate'},{text:'Rating Name',title:'name'}]
    const url = window.location.href.split('?')[0]

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
   
    $('table').DataTable({             
        "bInfo": false, //Dont display info e.g. "Showing 1 to 4 of 4 entries"
        "paging": false,//Dont want paging                
        "bPaginate": false,//Dont want paging   ,
        "searching":false,
        "sort":false
    });

    $('#pagLimit').on('change',function (e) {  
        if(e.target.value!=''){
            let search = getParameterByName('search',window.location.href)
            let searchcolumn = getParameterByName('searchcolumn',window.location.href)
            let orderby = getParameterByName('orderby',window.location.href)
            let order = getParameterByName('order',window.location.href)

            if (search!=null && searchcolumn!=null && orderby==null && order==null) {
                window.location.href = `${url}?search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${parseInt(e.target.value)}`
            }
            else if(search==null && searchcolumn==null && orderby!=null && order!=null) {
                window.location.href = `${url}?page=1&perpage=${parseInt(e.target.value)}&orderby=${orderby}&order=${order}`
            }
            else if(search!=null && searchcolumn!=null && orderby!=null && order!=null){
                window.location.href = `${url}?search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${parseInt(e.target.value)}&orderby=${orderby}&order=${order}`
            }
            else {
                window.location.href = `${url}?page=1&perpage=${parseInt(e.target.value)}`
            }
        }
    });

    $('table thead tr th').click( function (e) { 
        if ($(this).text().toLowerCase() != 'action' && sortingCols.includes($(this).text())){
            let order = getParameterByName('order',window.location.href)
            if (order == null) {
                order = 'asc'
            } else if(order == 'asc'){
                order = 'desc'
            }else if(order == 'desc'){
                order = 'asc'
            }

            let orderby = (orderbyColumns.filter( col => col.text == $(this).text()))[0].title
            let search = getParameterByName('search',window.location.href)
            let searchcolumn = getParameterByName('searchcolumn',window.location.href)

            if (search!=null && searchcolumn!=null && orderby==null && order==null) {
                window.location.href = `${url}?search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${$('#pagLimit').val()}`
            }
            else if(search==null && searchcolumn==null && orderby!=null && order!=null) {
                window.location.href = `${url}?page=1&perpage=${$('#pagLimit').val()}&orderby=${orderby}&order=${order}`
            }
            else if(search!=null && searchcolumn!=null && orderby!=null && order!=null){
                window.location.href = `${url}?search=${search}&searchcolumn=${searchcolumn}&page=1&perpage=${$('#pagLimit').val()}&orderby=${orderby}&order=${order}`
            }
            else {
                window.location.href = `${url}?page=1&perpage=${$('#pagLimit').val()}`  
            }
        }
    });

    $('table thead tr th').addClass('sorting')
    $('table thead tr th').map(th => {
        !sortingCols.includes($('table thead tr th')[th].innerText) && $($('table thead tr th')[th]).removeClass('sorting')
    })

    let orderby = getParameterByName('orderby',window.location.href)
    let order = getParameterByName('order',window.location.href)
    if(orderby != null && order != null){
        orderby = orderbyColumns.find( col => col.title.toLowerCase().trim() == orderby.toLowerCase().trim()).text
        $('table thead tr th').map( th => {
            if($('table thead tr th')[th].innerText == orderby){
                $($('table thead tr th')[th]).removeClass('sorting')
                $($('table thead tr th')[th]).addClass(`sorting_${order}`)
            }
        })
    }

    $('input[name="search"').on('keyup', function (e) {
        if (e.keyCode === 13) {
            if ($('input[name="search"').val().trim() == '') {
                $('input[name="search"').addClass("invalid-search");   
            }
            else{
                window.location.href = `${url}?page=1&perpage=${$('#pagLimit').val()}&search=${$('input[name="search"]').val().trim()}&searchcolumn=${$('select[name="searchColumn"]').val().trim()}`
            }
        }

        if (e.keyCode === 8) {
            if($('input[name="search"').val().trim() == ''){
                window.location.href = url
            }
        }
    });
});