$(document).ready(function() {
    let preventEvent = true

    $('#incomingPopup').on('show.bs.modal', function() {
        $('#confirmMsg').html('')
        $('#confirmMsg').removeClass('mt-5 ml-5 mr-5')
        $('#btnSchedule').show()
        $('#btnConfirmSchedule').hide()
        preventEvent = true
    })

    $('#applicablePlatforms').on('change',function () {  
        $('#confirmMsg').html('')
        $('#confirmMsg').removeClass('mt-2 ml-2 mr-2')
        $('#btnSchedule').show()
        $('#btnConfirmSchedule').hide()
        preventEvent = true
     });

    $("#pullIncomingForm").on('submit',function (event) { 
        console.log(preventEvent)
        preventEvent && event.preventDefault()
        $.ajax({
            type: "post",
            url: '/project-management/check-duplicates',
            data:{incomingId:$('#incomingTitleSearch').val(),platforms:$('#applicablePlatforms').val()},
            success: function (response) {
                if(response.flag && response.duplicate.length > 0){
                    let error = response.duplicate.map( x => {
                       return $('#applicablePlatforms option[value="'+x+'"]').text()
                    }).join()
                    preventEvent && $('#confirmMsg').html(`<p>These Platform have duplicate entries Confirm to Proceed.</p><p>${error}</p>`)
                    preventEvent && $('#confirmMsg').addClass('mt-5 ml-5 mr-5')
                    preventEvent && $('#btnSchedule').hide()
                    preventEvent && $('#btnConfirmSchedule').show()
                    preventEvent = false
                }
                else {
                    preventEvent && (
                        preventEvent = false,
                        $('#btnConfirmSchedule').trigger('click')
                    )

                }
            }
        });
    })

    function setPlatformOptions(selectData){
        $('#applicablePlatforms').empty()
        if (selectData.length) {
            $('#applicablePlatforms').select2({
                data: ($.map(selectData.applicablePlatforms, function (item) {
                        return {
                                
                                id:item.id,
                                item:item,
                                text:item.name
                            }
                })).reverse()
            }) 
        } else {
            $('#applicablePlatforms').empty()
        }
 
        return true
    }


    $("#incomingTitleSearch").select2({
        minimumInputLength: 2,
        templateSelection: function (data) {
            setPlatformOptions(data)
            return data.text;
        },
        ajax: {
            url: "/incoming/search",
            dataType: "json",
            type: "GET",
            data: function (params) {
    
                var queryParameters = {
                    term: params.term
                }
                return queryParameters;
            },
            cache:true,
            processResults: function (result) {
                return {
                    results: $.map(result.data.titles, function (item) {
                        return {
                            // text: item.alternateName,
                            // id: item.incoming.id,
                            // applicablePlatforms:result.data.applicablePlatforms,
                            // applicablePlatforms:item.incoming.applicablePlatforms,
                            // length:item.incoming.length

                            text: item.alternateName,
                            id: item.incoming.id,
                            applicablePlatforms:result.data.platforms,
                            length:true
                        }
                    })
                };
            }
        }
    });

    
});