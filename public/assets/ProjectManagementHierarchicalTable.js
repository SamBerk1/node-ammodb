function format(data) { 
    let start = `<table cellpadding="5" cellspacing=0 style="padding-left:50px;">
        <tbody>
            <tr style="background-color: #212121">
                <td><strong>GUID</strong></td>
                <td><strong>Season Name</strong></td>
                <td><Strong>Season No.</Strong></td>
                <td><strong>Action</strong></td>
            </tr>`

      let body =  data.seasons.map(element => {
              return `<tr style="background-color: #282828">
                <td>${element.season.guId}</td>
                <td>${element.season.name}</td>
                <td>${element.season.number}</td>
                <td>
                    <div class="dropdown ">
                        <a class="btn btn-transparent border p-2 default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class=" icon-options"></i>
                        </a>
                        <div class="dropdown-menu ">
                            <a class="dropdown-item open-editAjaxModal" data-id="${element.id}" data-title='Edit Status Season:"${element.season.name}" of <br> Series ${data.alternateName}' data-type="series"><i class="icon-note icon-btn text-warning pr-2"></i>Edit</a>
                            <a class="dropdown-item open-deleteConfirmModal" data-title="Are you Sure!" data-message='To Remove "${element.season.name}" of Series "${data.alternateName}" from Programming.' data-toggle='modal' data-action="/project-management/season/${element.id}?_method=DELETE" data-target='#deleteConfirmationModal'><i class="icon-action-undo icon-btn text-danger pr-2"></i> Incoming / Undo</a>
                        </div>
                    </div>
                </td>
            </tr>` 
        })
        
    let end = `</tbody>
                <tfoot>
                    <tr style="background-color: #212121"><td colspan="12"></td></tr>
                </tfoot>
            </table>`

            return start + body.join("") + end


}

$('#projectManagementTable tbody').on('click', 'td .btn-expandDetail', function () {
var tr = $(this).closest('tr');
var row = $('#projectManagementTable').DataTable().row( tr );
let data = {seasons:$(tr).data("seasons"),platform:$(tr).data("platform"),alternateName:$(tr).data("alternatename")}

if ( row.child.isShown() ) {
    // This row is already open - close it
    row.child.hide();
    tr.removeClass('shown');
    $(this).toggleClass('fa-minus-circle text-danger')
    $(this).toggleClass('fa-plus-circle text-success')
}
else {
    // Open this row
    row.child(format(data)).show();
    tr.addClass('shown');
    $(this).toggleClass('fa-minus-circle text-danger')
    $(this).toggleClass('fa-plus-circle text-success')
}
} );


$("[id=customAssetForm]").on('click','#customAssetBtn',function(e){
    e.preventDefault();
    let customAssetName = ($(this).parent().parent().parent().parent().parent()).find('input')[0]
    let customAssetStatus = ($(this).parent().parent().parent().parent().parent()).find('select')[0]
    if(customAssetName.value!='' && customAssetStatus.value!=''){
        $(this).parent().parent().parent().parent().parent().append(`
        <div class="row">
        <div class="col-md-5 mb-3">
            <input class="form-control" type="text" value='${customAssetName.value}' placeholder="Custom Asset Name" name="customAsset[name]">               
        </div>
        <div class="col-md-6 mb-3">
            <div class="row">
                <div class="col-md-10">
                    <input class="form-control" type="text" value='${customAssetStatus.value}' placeholder="Custom Asset Status" name="customAsset[status]"> 
                </div>    
                <div class="col-md-2">
                    <button id="customAssetRemove" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                </div>  
            </div> 
        </div> 
        </div>
        `)

        customAssetName.value = ''
        customAssetStatus.selectedIndex = 0
    }
    });

    $("[id=customAssetForm]").on('click','#customAssetRemove',function(e){
    e.preventDefault();
    $(this).parent().parent().parent().parent().remove()
});


function showModal(data) {
    console.log(data)
    let detailsAction = data.type.toLowerCase() == 'movie' ? `/project-management/details/${data.id}` : `/project-management/details/season/${data.id}`
    $.ajax({
        url:detailsAction,
        method:'GET',
        success:function (result) { 
            console.log(result)
            if (result.data != null) {
                data.type.toLowerCase() == 'movie' ? $("[id=customAssetForm]").parent().parent().show() : $("[id=customAssetForm]").parent().parent().hide()
                $("[id=customAssetForm]").children().not(':first').remove();
                $('#outgoingEditModal').modal('show');
                $('#outgoingEditModal h5.modal-title').html(data.ModalTitle)    
                
                let action = data.type.toLowerCase() == "movie" ? `/project-management/${data.id}?_method=PUT` : `/project-management/season/${data.id}?_method=PUT`
                $('#outgoingEditModal form[name="statusForm"]').attr('action',action) 

                $('#outgoingEditModal form[name="statusForm"] select[name="platformId"]').val(result.data.platformId).trigger('change')
                
                $('#outgoingEditModal form[name="statusForm"] select[name="videoStatus"]').val(result.data.videoStatus).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="captions"]').val(result.data.captions).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="subtitles"]').val(result.data.subtitles).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="artwork"]').val(result.data.artwork).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="dubbing"]').val(result.data.dubbing).trigger('change')
                $('#outgoingEditModal form[name="statusForm"] select[name="metadata"]').val(result.data.metadata).trigger('change')

                if(result.data.customAssetsOutgoing){
                    result.data.customAssetsOutgoing.map( x => {
                        $("[id=customAssetForm]").append(
                            `<div class="row">
                                <div class="col-md-5 mb-3">
                                    <input class="form-control" type="text" value='${x.name}' placeholder="Custom Asset Name" name="customAsset[name]">               
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input class="form-control" type="text" value='${x.status}' placeholder="Custom Asset Status" name="customAsset[status]"> 
                                        </div>    
                                        <div class="col-md-2">
                                            <button id="customAssetRemove" class="btn btn-danger"><i class="fa fa-minus"></i></button>
                                        </div>  
                                    </div> 
                                </div> 
                            </div>`
                        )
                    })
                }


                let actionSchedule = data.type.toLowerCase() == "movie" ? `/project-management/${data.id}/schedule` : `/project-management/season/${data.id}/schedule`
                $('#outgoingEditModal form[name="scheduleForm"]').attr('action',actionSchedule) 

                let dueDate = result.data.dueDate != null ? new Date(result.data.dueDate) : ''
                $('#outgoingEditModal form[name="scheduleForm"] input[name="dueDate"]').val(dueDate == '' ? '' : dueDate.getFullYear() + "-" + (dueDate.getMonth()+1) + "-" + dueDate.getDate())    

                let launchDate = result.data.launchDate != null ? new Date(result.data.launchDate) : ''
                $('#outgoingEditModal form[name="scheduleForm"] input[name="launchDate"]').val(launchDate == '' ? '' : launchDate.getFullYear() + "-" + (launchDate.getMonth()+1) + "-" + launchDate.getDate())    

                let deliveryDate = result.data.deliveryDate != null ? new Date(result.data.deliveryDate) : ''
                $('#outgoingEditModal form[name="scheduleForm"] input[name="deliveryDate"]').val(deliveryDate == '' ? '' : deliveryDate.getFullYear() + "-" + (deliveryDate.getMonth()+1) + "-" + deliveryDate.getDate())    

                let sunSetDate = result.data.sunSetDate != null ? new Date(result.data.sunSetDate) : ''
                $('#outgoingEditModal form[name="scheduleForm"] input[name="sunSetDate"]').val(sunSetDate == '' ? '' : sunSetDate.getFullYear() + "-" + (sunSetDate.getMonth()+1) + "-" + sunSetDate.getDate())    
                
                $('#outgoingEditModal form[name="territoriesForm"] select[name="territories"]').val(result.data.videoStatus).trigger('change')

                let actionTerritories = data.type.toLowerCase() == "movie" ? `outgoing/${data.id}/territories/movie?_method=PUT` : `outgoing/${data.id}/territories/season?_method=PUT`
                $('#outgoingEditModal form[name="territoriesForm"]').attr('action',actionTerritories) 

                if(result.data.territories){
                    let selectTerritories = result.data.territories.map( x => {
                        return {"id":x.territory,"text":x.territory,"selected":true}
                    })
                    
                    $('#outgoingEditModal form[name="territoriesForm"] select[name="territories[]"]').select2({
                        data: selectTerritories,
                        tags: true,
                        tokenSeparators: [',']
                    })
                }

            }
        }
    })   
}

$(document).on("click", ".open-editAjaxModal", function () {
    $('#outgoingEditModal').show()
    let modal = $('#outgoingEditModal')
    let clickedStatus = $(this).data('name')

    if (clickedStatus) {
        $(modal).find(`select[name|='${clickedStatus}']`).closest('div').addClass('highlightOptDiv')
        $(modal).on('hide.bs.modal', function () {
            $(modal).find(`select[name|='${clickedStatus}']`).closest('div').removeClass('highlightOptDiv')
        });
    }
    

    showModal({type:$(this).data('type'),id:$(this).data('id'),ModalTitle:$(this).data('title')})
});