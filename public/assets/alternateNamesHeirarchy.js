function format(data) { 
        let start = `<table cellpadding="5" cellspacing=0 style="padding-left:50px;">
            <tbody>
                <tr style="background-color: #212121">
                    <td><strong>Alternate Title</strong></td>
                    <td><strong>GUID</strong></td>
                    <td><Strong>Alter Synopsis</Strong></td>
                    <td><strong>Language</strong></td>
                    <td><strong>Action</strong></td>
                </tr>`

          let body =  data.alternateName.map(element => {
                   return `<tr style="background-color: #282828">
                    <td>${element.alternateName}</td>
                    <td>${element.guId}</td>
                    <td><textarea class="form-control" readonly cols="20" rows="3">${element.alterSynopsis}</textarea></td>
                    <td>${element.language.name}</td>
                    <td>
                        <div class="dropdown ">
                            <a class="btn btn-transparent border p-2 default-color dropdown-hover p-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class=" icon-options"></i>
                            </a>
                            <div class="dropdown-menu ">
                            <a class="dropdown-item open-keywordModal" data-toggle="modal" data-target="#keywordModal" data-type="alternatename" data-title="${element.alternateName}" data-alternatetitleid="${element.id}" data-libraryid="${data.id}"><i class="fa fa-key icon-btn text-success pr-2"></i>Keywords</a>`
                            
                            +
                            
                            (
                                data.is == 'series' ?
                                `<a class="dropdown-item" href="/library/${data.id}/alternatetitle/${element.id}/episodes"><i class="ti-video-clapper icon-btn text-success pr-2"></i>Episodes</a>
                                <a class="dropdown-item" href="/library/${data.id}/alternatetitle/${element.id}/seasons"><i class="ti-file icon-btn text-success pr-2"></i>Seasons</a>
                                <a class="dropdown-item" href="/library/${data.id}/alternatetitle/${element.id}/episodes/export"><i class="ti-export icon-btn text-success pr-2"></i>Export Episodes</a>`
                                : ''  
                            )
                                
                            +
                            (
                                element.isParent == 0 ? `
                                    <a class="dropdown-item" data-toggle="modal" data-target="#alternateTitleModal" data-data = '${JSON.stringify(element)}' data-title="${element.alternateName}" data-action="/library/alternatename/${element.id}?_method=PUT" data-whatever="@mdo"><i class="ti-pencil-alt icon-btn text-warning pr-2"></i>Edit</a>
                                    <a class="dropdown-item open-deleteConfirmModal" data-title="Are you Sure!" data-message='To Delete Alternate title "${element.alternateName}".' data-toggle='modal' data-action="/library/alternatename/${element.id}?_method=DELETE" data-target='#deleteConfirmationModal'> <i class="ti-trash icon-btn text-danger pr-2"></i>Delete</a>`
                                    : ''                                          
                            )
                            +
                                
                            `</div>
                        </div>
                    </td>
                </tr>`
                })
            
        let end = `</tbody>
                    <tfoot>
                        <tr style="background-color: #212121"><td colspan="12"></td></tr>
                    </tfoot>
                </table>`

                return start + body.join() + end


 }

 $('input[name="movie"]').change( function (e) {  
    console.log(e.target.value)
    $('#importModal div.modal-footer a').toggle()
 });

$('#libraryTable tbody').on('click', 'td .btn-expandDetail', function () {
    var tr = $(this).closest('tr');
    var row = $('#libraryTable').DataTable().row( tr );
    let data = {alternateName:$(tr).data("alternatenames"),is:$(tr).data("is"),title:$(tr).data("title"),id:$(tr).data("id")}
    console.log(data)
    

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
        $(this).toggleClass('fa-minus-circle text-danger')
        $(this).toggleClass('fa-plus-circle text-success')
    }
    else {
        // Open this row
        row.child(format(data)).show();
        tr.addClass('shown');
        $(this).toggleClass('fa-minus-circle text-danger')
        $(this).toggleClass('fa-plus-circle text-success')
    }
} );

$('#fileUpload').change(function() {
    var file = $('#fileUpload')[0].files[0].name;
    $('#fileName').text(file);
});

$('#fileUpload_update').change(function() {
    var file = $('#fileUpload_update')[0].files[0].name;
    $('#fileName_update').text(file);
});