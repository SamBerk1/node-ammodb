$(document).ready( function () {  

    $('#alternateTitleModal').on('show.bs.modal', function (event) {
        const button = $(event.relatedTarget) // Button that triggered the modal
        const data = JSON.parse(JSON.stringify(button.data('data')))
        const action = button.data('action')
        const title = button.data('title')

        const modal = $(this)
        modal.find(".modal-body input[name='alternateName']")[0].value = data.alternateName
        modal.find(".modal-body textarea[name='alterSynopsis']")[0].value = data.alterSynopsis
        modal.find(".modal-body form")[0].setAttribute('action',action)
        modal.find(".modal-content h5")[0].innerText = `Edit alternate title "${data.alternateName}"`   

        if (data.languageId != '' || data.languageId != 'null' || data.languageId != null || data.languageId != 'undefined' || data.languageId != undefined) {
            $(modal.find(".modal-body select[name='languageId']")[0]).val(data.languageId).trigger('change')
        }
    })

    var alternate_synopsis_text_max = 400;
    $('#alterSynopsis').val() && $('#alternate_synopsis_charCount').html(alternate_synopsis_text_max - $('#alterSynopsis').val().length+ ' remaining');

    $('#alterSynopsis').keyup(function() {
    var text_length = $('#alterSynopsis').val().length;
    var text_remaining = alternate_synopsis_text_max - text_length;
    
    $('#alternate_synopsis_charCount').html(text_remaining + ' remaining');
});
})