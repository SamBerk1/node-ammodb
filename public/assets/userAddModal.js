$(document).ready( function () {  
    
    $('#confirmPasswordView').on('click',function () {  
        if ($('#userAddModal input[name="password"]').attr('type') === "password") {
            $('#userAddModal input[name="password"]').attr('type','text')
            $(this).toggleClass('text-info')
        } else {
            $('#userAddModal input[name="password"]').attr('type','password')
            $(this).removeClass('text-info')
        }
    });

    $('#viewPasswordChange').on('click',function () {  
        if ($('#passwordChangeModal input[name="password"]').attr('type') === "password") {
            $('#passwordChangeModal input[name="password"]').attr('type','text')
            $(this).toggleClass('text-info')
        } else {
            $('#passwordChangeModal input[name="password"]').attr('type','password')
            $(this).removeClass('text-info')
        }
    });

    $('#userAddModal').on('show.bs.modal',function(){
        $('#confirmPasswordView').removeClass('text-info')
        $('#userAddModal input[name="password"]').attr('type','password')
        $('#userAddModal select[name="role_id"]').on('change',function(){
            if ($(this). children("option:selected").attr("data-is") == 1) {
                $('#studioDiv').show()
                $('#studioDiv select[name="studioId"]').attr('disabled',false)
            } else {
                $('#studioDiv').hide()
                $('#studioDiv select[name="studioId"]').attr('disabled',true)  
            }
        })
    })

});

$(document).on("click", ".open-changePasswordModal", function () {
    $('#passwordChangeModal h5.modal-title').text($(this).data('title'))
    $('#passwordChangeModal form').attr('action',`settings/users/${$(this).data('id')}/changepassword`)
});