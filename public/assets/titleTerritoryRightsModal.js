$(document).on("click", ".open-territoryCountryModal", function () {
    var data = {libraryId : $(this).data('libraryid'),territoryId : $(this).data('territoryid'),rightId : $(this).data('rightid')};
    $('#territoryCountryModal h5.modal-title').text(`Edit Countries for Territory "${$(this).data('territory')}"`)
    let startdate = new Date($(this).data('startdate'))
    let enddate = new Date($(this).data('enddate'))
    $('#territoryCountryModal h5.modal-title').append(`<h6>Start Date:${startdate.getFullYear()}-${startdate.getMonth()+1}-${startdate.getDate()}</h6>`)
    $('#territoryCountryModal h5.modal-title').append(`<h6>End Date:${enddate.getFullYear()}-${enddate.getMonth()+1}-${enddate.getDate()}</h6>`)

    $('#territoryCountryModal form input[name="startDate"]').val(`${startdate.getFullYear()}-${startdate.getMonth()+1}-${startdate.getDate()}`)
    $('#territoryCountryModal form input[name="endDate"]').val(`${enddate.getFullYear()}-${enddate.getMonth()+1}-${enddate.getDate()}`)

    $($('#territoryCountryModal form div')[0])
    .prepend(`<input type="hidden" name="libraryId" value="${data.libraryId}">
            <input type="hidden" name="rightId" value="${data.rightId}">
            <input type="hidden" name="territoryId" value="${data.territoryId}">
        `)
    $($('#territoryCountryModal form')[0])
    .prepend(`<input type="hidden" name="libraryId" value="${data.libraryId}">
            <input type="hidden" name="rightId" value="${data.rightId}">
            <input type="hidden" name="territoryId" value="${data.territoryId}">
        `)

    $('#territoryCountryModal div.delete-action button.btn-danger').one('click',(e)=>{
        e.preventDefault()
        $('#territoryCountryModal div.delete-message').text(`Are you Sure! To Delete Territory "${$(this).data('territory')}" and its associated Countries.`)
    $('#territoryCountryModal form.delete-form').show()
        
    })

    $.ajax({
        type: "post",
        url: window.location.href.includes('/platform/rights') ?"others/platform/rights/getCountries" : "library/rights/getCountries",
        data: data,
        success: function (response) {
            $('#selectTerritoryCountries').select2({
                data: (
                    response.data != null && $.map((response.data), function (item) {
                            return {
                                id:item.id,
                                text:item.country,
                                selected: item.selected && true
                            }
                        })
                    )
            })
        }
    });
    
});

$(document).ready(function () {

    $('#rightsTable').DataTable({
        sorting:false,
        sort:false
    })

    $('#territoryCountryModal form.delete-form').hide()
     $('#territoryCountryModal').on('hidden.bs.modal' , function () {
        $('#selectTerritoryCountries').empty()
        $('#territoryCountryModal input[type="hidden"]').remove()
        $('#territoryCountryModal div.delete-message').text('')
        $('#territoryCountryModal form.delete-form').hide()
     })

    function formatTerritoryData(element){
        if (element.territoryName.toLowerCase() != 'world wide') {
            return element.countries.filter(item => item.selected).map((item,index) => {
                if (index == element.countries.filter(item => item.selected).length-1) {
                    return ` ${item.country}`
                }
                else{
                    return ` ${item.country},`
                }
            }).join('') 
        }else{
            return `World Wide - `+ element.countries.filter(item => !item.selected).map((item,index) => {
                if (index == element.countries.filter(item => item.selected).length-1) {
                    return ` ${item.country}`
                }
                else{
                    return ` ${item.country},`
                }
            }).join('')
        }
     }


     function format(data) { 

        let start = `<table cellpadding="5" cellspacing=0 style="padding-left:50px;" class="">
            <tbody>
                <tr class="table-secondary">
                    <td><strong>Territory</strong></td>
                    <td><strong>Countries</strong></td>
                    <td><strong>StartDate</strong></td>
                    <td><strong>EndDate</strong></td>
                    <td><strong>Action</strong></td>
                </tr>`

          let body =  data.territories.map(element => {
                   return `<tr style="background-color: #ffffff">
                    <td>${element.territoryName}</td>`
                    +
                    (
                        `<td class="textOveflow">`
                        +           
                        formatTerritoryData(element)
                        +
                        `</td>`
                    )
                    +
                    `
                    <td>${new Date(element.dates.startDate).getFullYear()}-${new Date(element.dates.startDate).getMonth()+1}-${new Date(element.dates.startDate).getDate()}</td>
                    <td>${new Date(element.dates.endDate).getFullYear()}-${new Date(element.dates.endDate).getMonth()+1}-${new Date(element.dates.endDate).getDate()}</td>
                    <td>
                    <button data-toggle="modal" data-target="#territoryCountryModal" data-startdate="${Date.parse(element.dates.startDate)}" data-enddate="${Date.parse(element.dates.endDate)}" data-territory="${element.territoryName}" data-libraryid="${data.libraryId}" data-rightid="${data.rightId}" data-territoryid="${element.id}" class="btn btn-warning open-territoryCountryModal"><i class='ti-pencil-alt icon-btn'></i></button>
                    </td>`
                    +
                    `</tr>`
                })
            
        let end = `</tbody>
                    <tfoot>
                        <tr class="table-secondary"><td colspan="12"></td></tr>
                    </tfoot>
                </table>`

                return start + body.join('') + end


 }

$('#rightsTable tbody').on('click', 'td .btn-expandDetail',async function () {
    var tr = $(this).closest('tr');
    var row = $('#rightsTable').DataTable().row( tr );
    let data = {libraryId:$(tr).data("libraryid"),territories:$(tr).data("territories"),rightId:$(tr).data("rightid")}
    let url;

    url = window.location.href.includes('/platform/rights') ?"others/platform/rights/getCountries" : "library/rights/getCountries"

    await Promise.all(
        data.territories.map(async element => {
           let response = await fetch(url, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, *cors, same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({libraryId:data.libraryId,territoryId:element.id,rightId:data.rightId}) // body data type must match "Content-Type" header
            });
            response = (await response.json())
            element.countries = response.success && response.data
            element.dates = response.success && response.dates
        })
    )

    if ( row.child.isShown() ) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
        $(this).toggleClass('fa-minus-circle text-danger')
        $(this).toggleClass('fa-plus-circle text-success')
    }
    else {        
        row.child(format(data)).show();
        tr.addClass('shown');
        $(this).toggleClass('fa-minus-circle text-danger')
        $(this).toggleClass('fa-plus-circle text-success')
    }
} );



});