$(document).on("click", ".open-keywordModal", function () {
    $('#selectKeywords').select2({
        data:null,   
        tags: true,
        tokenSeparators: [',']
    })
    
    var data = {
        title:$(this).data('title'),
        type:$(this).data('type'),
        libraryId : $(this).data('libraryid'),
        alternateTitleId : $(this).data('alternatetitleid'),
        seasonId : $(this).data('seasonid'),
        episodeId : $(this).data('episodeid')
    };
    $('#keywordModal h5.modal-title').text(`Key Words For "${data.title}"`)
    let url;

    if (data.type.toLowerCase() == 'library') {
        url = `/library/keywords/${data.libraryId}`
    }
    else if(data.type.toLowerCase() == 'alternatename'){
        url = `/library/keywords/${data.libraryId}/${data.alternateTitleId}`
    }
    else if(data.type.toLowerCase() == 'season'){
        url = `/library/keywords/${data.libraryId}/${data.alternateTitleId}/${data.seasonId}`
    }
    else if(data.type.toLowerCase() == 'episode'){
        url = `/library/keywords/${data.libraryId}/${data.alternateTitleId}/${data.seasonId}/${data.episodeId}`
    }
    else{
        url = `/library/keywords/${data.libraryId}`
    }

    $('#keywordModal form').attr('action',url+'?_method=PUT')

    $.ajax({
        type: "get",
        url: url,
        data: data,
        success: function (response) {
            $('#selectKeywords').select2({
                data: (
                    response.data != null && $.map((response.data), function (item) {
                            return {
                                id:item.keyword,
                                text:item.keyword,
                                selected:true
                            }
                        })
                ),   
                tags: true,
                tokenSeparators: [',']
            })
        }
    });
});

$(document).ready( function () {  
    $('#keywordModal').on('hide.bs.modal', function () { 
        $('#selectKeywords').empty()
        $('#keywordModal form').attr('action','')
    })   
});