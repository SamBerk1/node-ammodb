$(document).ready(function () {  


    if ($('.input-images-1').length > 0) {
        $('.input-images-1').imageUploader({
            imagesInputName: 'photos'
        });
    
        $('input[name="photos[]"]').attr('required',true)
        $('input[name="photos[]"]').attr('accept','image/*')
    }

    if ($('.open-viewModal').length > 0) {

        $('.open-viewModal').on('click',function () {  
            $('#viewModal img').attr('src',$(this).data('src'))
            $('#viewModal a').attr('href',$(this).data('src'))
        });
    }
});