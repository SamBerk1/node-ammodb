const TitleStatus = use("App/Models/TitleStatus");
const TitleStatusesSeason = use('App/Models/TitleStatusesSeason');
const StudioCustomer = use('App/Models/StudioCustomer');
const OutgoingMovieFilter = use('App/ModelFilters/OutgoingMovieFilter')
const OutgoingSeasonFilter = use('App/ModelFilters/OutgoingSeasonFilter')


class OutgoingPolicy{

    async getOutgoingSeasons(user,query){
        let data = null;
        
        if (user.role.isStudio == 0) {
            data = await TitleStatusesSeason.query().with('alternateName',(builder) => builder.select('id','alternateName','guId','isParent','libraryId').with('library',(builder) => builder.select('id','title','is','studioId').with('studio'))).with('season').with('series').where("outgoing",1).with('territories').with('platform').filter(query,OutgoingSeasonFilter).fetch();
            return data.toJSON()
        }
        else if (user.role.isStudio == 1) {
            let userStudio = await StudioCustomer.findBy('userId',user.id)
            data = await TitleStatusesSeason.query().where('studioId', userStudio.studioId).with('alternateName',(builder) => builder.select('id','alternateName','isParent','libraryId').with('library',(builder) => builder.select('id','title','is','studioId').with('studio'))).with('season').with('series').where("outgoing",1).with('territories').with('platform').filter(query,OutgoingSeasonFilter).fetch();
            return data.toJSON()
        }
        else{
            return []
        }
    }


    async getOutgoingMovies(user,query){
        let data = null
        if (user.role.isStudio == 0) {
            data = await TitleStatus.query().with('alternateName',(builder) => builder.select('id','alternateName','guId')).with('library',(builder) => builder.select('id','title','is','studioId').with('studio',(builder) => builder.select('id','title'))).where("outgoing",1).with('territories').with('platform',(builder) => builder.select('id','name')).filter(query,OutgoingMovieFilter).fetch();               
            return data.toJSON()
        }
        else if (user.role.isStudio == 1) {
            let userStudio = await StudioCustomer.findBy('userId',user.id)
            data = await TitleStatus.query().where('studioId', userStudio.studioId).with('alternateName',(builder) => builder.select('id','alternateName')).with('library',(builder) => builder.select('id','title','is','studioId').with('studio',(builder) => builder.select('id','title'))).where("outgoing",1).with('territories').with('platform',(builder) => builder.select('id','name')).filter(query,OutgoingMovieFilter).fetch();
            return data.toJSON()
        }
        else{
            return []
        }
    }
}

const outgoingPolicy = new OutgoingPolicy()

module.exports = outgoingPolicy;
