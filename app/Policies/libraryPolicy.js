const Library = use("App/Models/Library");
const StudioCustomer = use('App/Models/StudioCustomer');
const TitlesCountry = use('App/Models/TitlesCountry');

class LibraryPolicy{

    async getLibrary(user,query){
        let libraries = null;
        if (user.role.isStudio == 0) {          
              if(!query.page){
                try {
                libraries = await Library.query().with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).orderBy('created_at','desc').filter(query).paginate(1,25)
                  
                } catch (error) {
                  console.log(error)
                }
              } 
              else if(query.page && !query.search && !query.orderby){
                libraries = await Library.query().with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
              }
              else if(query.page && query.search && !query.orderby){
                    
                libraries = (['year','guId'].includes(query.searchcolumn))
              
                ? await Library.query().where(query.searchcolumn,'like',`%${query.search}%`).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
              
                : await Library.query().with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).whereHas(query.searchcolumn.split('_')[0],(builder) => builder.where(query.searchcolumn.split('_')[1],'like',`%${query.search}%`)).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
              
              }
              else if(query.page && !query.search && query.orderby){
                libraries = await Library.query().with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).orderBy(query.orderby,query.order).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))      
              }
              else if(query.page && query.search && query.orderby){
                libraries = (['year','guId'].includes(query.searchcolumn))
              
                ? await Library.query().where(query.searchcolumn,'like',`%${query.search}%`).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).orderBy(query.orderby,query.order).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
              
                : await Library.query().with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).whereHas(query.searchcolumn.split('_')[0],(builder) => builder.where(query.searchcolumn.split('_')[1],'like',`%${query.search}%`)).orderBy(query.orderby,query.order).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
              }
              return libraries.toJSON()
        }
        else if (user.role.isStudio == 1) {

            let userStudio = await StudioCustomer.findBy('userId',user.id)

            if(!query.page){
              libraries = await Library.query().where('studioId', userStudio.studioId).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).orderBy('created_at','desc').filter(query).paginate(1,25)
            } 
            else if(query.page && !query.search && !query.orderby){
              libraries = await Library.query().where('studioId', userStudio.studioId).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
            }
            else if(query.page && query.search && !query.orderby){
                  
              libraries = (['year','guId'].includes(query.searchcolumn))
            
              ? await Library.query().where('studioId', userStudio.studioId).where(query.searchcolumn,'like',`%${query.search}%`).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
            
              : await Library.query().where('studioId', userStudio.studioId).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).whereHas(query.searchcolumn.split('_')[0],(builder) => builder.where(query.searchcolumn.split('_')[1],'like',`%${query.search}%`)).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
            
            }
            else if(query.page && !query.search && query.orderby){
              libraries = await Library.query().where('studioId', userStudio.studioId).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).orderBy(query.orderby,query.order).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))      
            }
            else if(query.page && query.search && query.orderby){
              libraries = (['year','guId'].includes(query.searchcolumn))
            
              ? await Library.query().where('studioId', userStudio.studioId).where(query.searchcolumn,'like',`%${query.search}%`).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).orderBy(query.orderby,query.order).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
            
              : await Library.query().where('studioId', userStudio.studioId).with('studio').with('image').with('genres').with('producers').with('cast').with('ratings').with('format').with('alternateNames',(builder) => builder.with('language')).whereHas(query.searchcolumn.split('_')[0],(builder) => builder.where(query.searchcolumn.split('_')[1],'like',`%${query.search}%`)).orderBy(query.orderby,query.order).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
            }
            return libraries.toJSON()
        }
        else{
            return []
        }
    }

    async exportLibrary(user,filter,columns){
      let libraries = null;
      if (user.role.isStudio == 0) { 
        let query = Library.query().with('originalLanguage').filter(filter)

        if (columns.export_trailer) {
          query.with('trailer')
        }
        if (columns.export_ratings) {
          query.with('ratings')
        }
        
        if (columns.export_images) {
          query.with('images',(builder) => builder.select('id','name','libraryId','compressed'))
        }
        
        if (columns.export_screener) {
          query.with('screener')
        }
        
        if (columns.export_studio) {
          query.with('studio')
        }
        if (columns.export_format) {
          query.with('format')
        }
        if (columns.export_cast) {
          query.with('cast')
        }
        if (columns.export_alternateNames) {
          query.with('alternateNames',(builder) => builder.with('language'))
        }
        if (columns.export_directors) {
          query.with('directors')
        }
        if (columns.export_producers) {
          query.with('producers')
        }
        if (columns.export_genres) {
          query.with('genres')
        }
        if (columns.export_rights) {
          query.with('rightsName',(builder) => builder.distinct())
        }
        if (columns.export_territories) {
          query.with('territories',(builder) => builder.distinct())
        }
        if (columns.export_languages) {
          query.with('languages',(builder) => builder.distinct())
        }
        if (columns.export_holdbacks) {
          query.with('holdbacks')
        }
        if (columns.export_pitchTracker) {
          query.with('pitchTracker')
        }
        if (columns.export_keywords) {
          query.with('keyword')
        }
        
        

        libraries = await query.fetch()
        return libraries.toJSON()
      }
      else if (user.role.isStudio == 1) {
        let userStudio = await StudioCustomer.findBy('userId',user.id)
        let query = Library.query().where('studioId', userStudio.studioId).with('originalLanguage').filter(filter)

        if (columns.export_trailer) {
          query.with('trailer')
        }
        // Fetching ratings if requested in req params
        if (columns.export_ratings) {
          query.with('ratings')
        }

        if (columns.export_images) {
          query.with('images',(builder) => builder.select('id','name','libraryId','compressed'))
        }

        if (columns.export_screener) {
          query.with('screener')
        }
        
        if (columns.export_studio) {
          query.with('studio')
        }
        if (columns.export_format) {
          query.with('format')
        }
        if (columns.export_cast) {
          query.with('cast')
        }
        if (columns.export_alternateNames) {
          query.with('alternateNames',(builder) => builder.with('language'))
        }
        if (columns.export_directors) {
          query.with('directors')
        }
        if (columns.export_producers) {
          query.with('producers')
        }
        if (columns.export_genres) {
          query.with('genres')
        }
        if (columns.export_rights) {
          query.with('rightsName',(builder) => builder.distinct())
        }
        if (columns.export_territories) {
          query.with('territories',(builder) => builder.distinct())
        }
        if (columns.export_languages) {
          query.with('languages',(builder) => builder.distinct())
        }
        if (columns.export_holdbacks) {
          query.with('holdbacks')
        }
        if (columns.export_pitchTracker) {
          query.with('pitchTracker')
        }
        if (columns.export_keywords) {
          query.with('keyword')
        }
        
        libraries = await query.fetch()
        return libraries.toJSON()
      }
      else{
        return []
      }

    }
}

const libraryPolicy = new LibraryPolicy()

module.exports = libraryPolicy;
