const Incoming = use("App/Models/Incoming");
const StudioCustomer = use('App/Models/StudioCustomer');

class IncomingPolicy{

    async getIncoming(user,query){
        let data = null;
        if (user.role.isStudio == 0) {

            if(!query.page){
                data = await Incoming.query().with('alternateName') 
                .with('library',(builder) => builder.select('id','studioId','is').with('studio')).with('notes')
                .with('incomingSeasons',(builder) => {
                builder.with('season').withCount('incomingEpisodes')
                }).filter(query).paginate(parseInt(1),parseInt(10))
              } 
              else if(query.page && !query.search && !query.orderby){

                data = await Incoming.query().with('alternateName') 
                .with('library',(builder) => builder.select('id','studioId','is').with('studio')).with('notes')
                .with('incomingSeasons',(builder) => {
                builder.with('season').withCount('incomingEpisodes')
                }).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))

              }
              else if(query.page && query.search && !query.orderby){

                switch (query.searchcolumn) {
                  case 'title':
                    data = await Incoming.query().with('alternateName') 
                    .whereHas('alternateName',(builder) => builder.select('id','alternateName').where('alternateName','like',`%${query.search}%`))
                    .with('library.studio')
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')
                    }).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  case 'guId':
                    data = await Incoming.query().with('alternateName') 
                    .whereHas('alternateName',(builder) => builder.select('id','guId').where('guId','like',`%${query.search}%`))
                    .with('library.studio')
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')
                    }).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  case 'studio':
                    data = await Incoming.query().with('alternateName') 
                    .with('library',(builder) => builder.select('id','studioId','is','title','guId').with('studio'))
                    .whereHas('library.studio',(builder) => builder.where('title','like',`%${query.search}%` ))
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')
                    }).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  default:
                    break;
                }
                
              }
              else if(query.page && !query.search && query.orderby){

                data = await Incoming.query().with('alternateName')
                .select('incomings.*')
                .join('libraries', 'incomings.libraryId', '=', 'libraries.id') 
                .join('alternate_title_languages', 'incomings.alternateTitleId', '=', 'alternate_title_languages.id') 
                .with('library',(builder) => builder.select('id','studioId','is').with('studio')).with('notes')
                .with('incomingSeasons',(builder) => {
                builder.with('season').withCount('incomingEpisodes')})
                .orderBy(query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby,query.order)
                .filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
              }
              else if(query.page && query.search && query.orderby){
                switch (query.searchcolumn) {
                  case 'title':
                    data = await Incoming.query().with('alternateName') 
                    .select('incomings.*')
                    .join('libraries', 'incomings.libraryId', '=', 'libraries.id') 
                    .join('alternate_title_languages', 'incomings.alternateTitleId', '=', 'alternate_title_languages.id')
                    .whereHas('alternateName',(builder) => builder.select('id','alternateName').where('alternateName','like',`%${query.search}%`))
                    .with('library.studio')
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')})
                    .orderBy(query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby,query.order)
                    .filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                case 'guId':
                    data = await Incoming.query().with('alternateName') 
                    .select('incomings.*')
                    .join('libraries', 'incomings.libraryId', '=', 'libraries.id') 
                    .join('alternate_title_languages', 'incomings.alternateTitleId', '=', 'alternate_title_languages.id')
                    .whereHas('alternateName',(builder) => builder.select('id','guId').where('guId','like',`%${query.search}%`))
                    .with('library.studio')
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')})
                    .orderBy(query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby,query.order)
                    .filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                case 'studio':
                    data = await Incoming.query().with('alternateName') 
                    .select('incomings.*')
                    .join('libraries', 'incomings.libraryId', '=', 'libraries.id') 
                    .join('alternate_title_languages', 'incomings.alternateTitleId', '=', 'alternate_title_languages.id')
                    .with('library',(builder) => builder.select('id','studioId','is','title','guId').with('studio'))
                    .whereHas('library.studio',(builder) => builder.where('title','like',`%${query.search}%` ))
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')})
                    .orderBy(query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby,query.order)
                    .filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  default:
                    break;
                }
              }
              return data.toJSON()
            
        }
        else if (user.role.isStudio == 1) {
            let userStudio = await StudioCustomer.findBy('userId',user.id)


            if(!query.page){
                data = await Incoming.query()
                .where('studioId', userStudio.studioId) 
                .with('alternateName') 
                .with('library',(builder) => builder.select('id','studioId','is').with('studio')).with('notes')
                .with('incomingSeasons',(builder) => {
                builder.with('season').withCount('incomingEpisodes')
                }).filter(query).paginate(parseInt(1),parseInt(10))
              } 
              else if(query.page && !query.search && !query.orderby){

                data = await Incoming.query()
                .where('studioId', userStudio.studioId) 
                .with('alternateName') 
                .with('library',(builder) => builder.select('id','studioId','is').with('studio')).with('notes')
                .with('incomingSeasons',(builder) => {
                builder.with('season').withCount('incomingEpisodes')
                }).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))

              }
              else if(query.page && query.search && !query.orderby){
                    
                switch (query.searchcolumn) {
                  case 'title':
                    data = await Incoming.query()
                    .where('studioId', userStudio.studioId) 
                    .with('alternateName') 
                    .whereHas('alternateName',(builder) => builder.select('id','alternateName').where('alternateName','like',`%${query.search}%`))
                    .with('library.studio')
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')
                    }).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  case 'guId':
                    data = await Incoming.query()
                    .where('studioId', userStudio.studioId) 
                    .with('alternateName') 
                    .whereHas('alternateName',(builder) => builder.select('id','guId').where('guId','like',`%${query.search}%`))
                    .with('library.studio')
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')
                    }).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  case 'studio':
                    data = await Incoming.query()
                    .where('studioId', userStudio.studioId) 
                    .with('alternateName') 
                    .with('library',(builder) => builder.select('id','studioId','is','title','guId').with('studio'))
                    .whereHas('library.studio',(builder) => builder.where('title','like',`%${query.search}%` ))
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')
                    }).filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  default:
                    break;
                }
              }
              else if(query.page && !query.search && query.orderby){

                data = await Incoming.query()
                .where('incomings.studioId', userStudio.studioId) 
                .with('alternateName')
                .select('incomings.*')
                .join('libraries', 'incomings.libraryId', '=', 'libraries.id') 
                .join('alternate_title_languages', 'incomings.alternateTitleId', '=', 'alternate_title_languages.id')
                .with('library',(builder) => builder.select('id','studioId','is').with('studio')).with('notes')
                .with('incomingSeasons',(builder) => {
                builder.with('season').withCount('incomingEpisodes')})
                .orderBy(query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby,query.order)
                .filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
              }
              else if(query.page && query.search && query.orderby){

                switch (query.searchcolumn) {
                  case 'title':
                    data = await Incoming.query()
                    .where('incomings.studioId', userStudio.studioId) 
                    .with('alternateName') 
                    .select('incomings.*')
                    .join('libraries', 'incomings.libraryId', '=', 'libraries.id') 
                    .join('alternate_title_languages', 'incomings.alternateTitleId', '=', 'alternate_title_languages.id')
                    .whereHas('alternateName',(builder) => builder.select('id','alternateName').where('alternateName','like',`%${query.search}%`))
                    .with('library.studio')
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')})
                    .orderBy(query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby,query.order)
                    .filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  case 'guId':
                    data = await Incoming.query()
                    .where('incomings.studioId', userStudio.studioId) 
                    .with('alternateName') 
                    .select('incomings.*')
                    .join('libraries', 'incomings.libraryId', '=', 'libraries.id') 
                    .join('alternate_title_languages', 'incomings.alternateTitleId', '=', 'alternate_title_languages.id')
                    .whereHas('alternateName',(builder) => builder.select('id','guId').where('guId','like',`%${query.search}%`))
                    .with('library.studio')
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')})
                    .orderBy(query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby,query.order)
                    .filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  case 'studio':
                    data = await Incoming.query()
                    .where('incomings.studioId', userStudio.studioId) 
                    .with('alternateName') 
                    .select('incomings.*')
                    .join('libraries', 'incomings.libraryId', '=', 'libraries.id') 
                    .with('library',(builder) => builder.select('id','studioId','is','title','guId').with('studio'))
                    .whereHas('library.studio',(builder) => builder.where('title','like',`%${query.search}%` ))
                    .with('notes')
                    .with('incomingSeasons',(builder) => {
                    builder.with('season').withCount('incomingEpisodes')})
                    .orderBy(query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby,query.order)
                    .filter(query).paginate(parseInt(query.page),parseInt(query.perpage))
                    break;
                  default:
                    break;
                }
              }
              return data.toJSON()
        }
        else{
            return []
        }
    }

    async exportIncoming(user,query){
      let data = null;
      if (user.role.isStudio == 0) {

        data = await Incoming.query().with('alternateName') 
        .with('library',(builder) => builder.select('id','title','studioId','is').with('studio'))
        .with('incomingSeasons',(builder) => {
        builder.with('season').withCount('incomingEpisodes')
        }).with('incomingNotes',(builder) => builder.with('note',(builder) => builder.select('id', 'text')).with('user',(builder) => builder.select('id','fullname' )))
        .filter(query).fetch()

        return data.toJSON()

      }
      else if (user.role.isStudio == 1) {
        let userStudio = await StudioCustomer.findBy('userId',user.id)

        data = await Incoming.query()
        .where('studioId', userStudio.studioId) 
        .with('alternateName') 
        .with('library',(builder) => builder.select('id','title','studioId','is').with('studio'))
        .with('incomingSeasons',(builder) => {
        builder.with('season').withCount('incomingEpisodes')
        }).filter(query).fetch()

        return data.toJSON()

      }
      else{
        return []
      }
    }
}

const incomingPolicy = new IncomingPolicy()

module.exports = incomingPolicy;
