const { validateAll } = use('Validator')

module.exports.movie = (data) => {
        // Validation Rules
        const rules = {
            title:'required',
            language:'required|not_equals:null',
            genres: 'required',
            is: 'required|not_equals:null',
            studioId : 'required',
            releaseDate: 'required',
            directors : 'required',
            producers : 'required',
            ratingId : 'required',
            formatId : 'required',
            synopsis : 'required',
            runtime : 'required',
            guId : 'required',
            imdb : 'required',
            countries : 'required',
            year : 'required'
          }
      
          // Error Messages
          const messages = {
            'title.required' : 'Title is required.',
            'language.required' : 'Language is Required.',
            'language.not_equals' : 'Language is Required.',
            'genres.required' : 'Genres are required.',
            'is.required' : 'Type of Title is Required',
            'is.not_equals' : 'Type of Title is Required',
            'studioId.required' : 'Stduio Is Required.',
            'releaseDate.required' : 'Release Date is required.',
            'directors.required' : 'Directors are required.',
            'producers.required' : 'Procuders are required.',
            'ratingId.required' : 'Rating is Required.',
            'formatId.required' : 'Format is Required.',
            'synopsis.required' : 'Synopsis is Required.',
            'runtime.required' : 'Runtime is Required.',
            'guId.required' : 'GUID is Required.',
            'imdb.required' :'IMDB Url is Required.',
            'countries.required' : 'Countires are Required.',
            'year.required' : 'Year is Required.'
            
        }
      
        let validation = validateAll(data, rules, messages)
        return validation
}