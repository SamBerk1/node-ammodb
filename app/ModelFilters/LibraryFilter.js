'use strict'

const ModelFilter = use('ModelFilter')

class LibraryFilter extends ModelFilter {

    title(query){
        return this.related('alternateNames', 'alternateName','LIKE',`%${query}%`)
    }

    type(query){
        return this.where('is','LIKE',`%${query}%`)
    }

    studio(query){
        return this.related('studio', (builder) => builder.whereIn('studios.id',query.split(',')))
    }

    genre(query){
        return this.whereHas('genres', (builder) => builder.whereIn('genres.id',query.split(',')))
    }

    cast(query){
        return this.whereHas('cast', (builder) => builder.whereIn('casts.id',query.split(',')))
    }

    director(query){
        return this.whereHas('directors', (builder) => builder.whereIn('directors.id',query.split(',')))
    }

    producer(query){
        return this.whereHas('producers', (builder) => builder.whereIn('producers.id',query.split(',')))
    }

    language(query){
        return this.whereHas('alternateNames',(builder) => builder.whereIn('alternate_title_languages.languageId',query.split(',') ))
    }

    // rights(query){
    //     return this.whereHas('rights',(builder) => builder.whereIn('titles_territories_rights.rightId',query.split(',') ))
    // }

    // territory(query){
    //     return this.whereHas('rights',(builder) => builder.whereIn('titles_territories_rights.territoryId',query.split(',') ))
    // }
    
    format(query){
        this.related('format', (builder) => builder.whereIn('formats.id',query.split(',')))
    }

    // country(query){
    //     return this.whereHas('territoryCountries',(builder) => builder.whereIn('territory_countries.id',query.split(',') ))
    // }

    titleRights(query){
        // console.log("Query in titleRights filter / model filter of library: ")
        // console.log(query)
        // let data = query.split("-")
        // let rights = data[0]!="undefined" ? data[0].split(',') : []
        // let territories = data[1]!="undefined" ? data[1].split(',') : []
        // let countries = data[2]!="undefined" ? data[2].split(',') : []

        // return this.orWhereHas('rights',(builder) => builder.where(function (){
        //     if (rights.length > 0) {
        //         this.whereIn('titles_territories_rights.rightId', rights)
        //     }
        //     if (territories.length > 0) {
        //         this.whereIn('titles_territories_rights.territoryId', territories)
        //     }
        //     if (countries.length > 0) {
        //         this.whereIn('titles_territories_rights.countryId', countries)
        //     }
        // }))
    }

    status(query){
        let status = (query.toLowerCase() == 'active') ? true : false
        if (status) {
            return this.whereHas('rights',(builder) => builder.where('endDate','>',new Date()))
        } else {
            return this.whereHas('rights',(builder) => builder.where('endDate','<',new Date()))
        }
    }
}

module.exports = LibraryFilter
