'use strict'

const ModelFilter = use('ModelFilter')

class OutgoingSeasonFilter extends ModelFilter {
    platform(query){
        this.related('platform', 'name', 'LIKE', `%${query}%`)
    }   

    title(query){
        this.related('series', 'title', 'LIKE', `%${query}%`)
    }

    guid(query){
        this.related('alternateName', 'guId', 'LIKE', `%${query}%`)
    }

    type(query){
        this.related('series', 'is', 'LIKE', `%${query}%`)
    }

    studio(query){
        this.related('studio', (builder) => builder.whereIn('studios.id',query.split(',')))
    }
    
    studioname(query){
        this.related('studio','title','LIKE',`%${query}%`)
    }
}

module.exports = OutgoingSeasonFilter
