'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */



class ActivityLog {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Function} next
   */
  async handle ({ request , response ,auth }, next) {
    
    await next()
    if (!request.url().includes('activity_log')) {
      const ActivityLog = use("App/Models/ActivityLog")
      let userId = (await auth.getUser()).id
      switch (request.method()) {
        case 'POST' || 'post':
          await ActivityLog.create({ action : 'create' , resource: request.url() , url: request.url() ,userId: userId })
          break;
        case 'PUT' || 'put':
          await ActivityLog.create({ action : 'update' , resource: request.url() , url: request.url() ,userId: userId })
          break;
        case 'DELETE' || 'delete':
          await ActivityLog.create({ action : 'delete' , resource: request.url() , url: request.url() ,userId: userId })
          break;
        default:
          break;
      }
    }
  }
}

module.exports = ActivityLog
