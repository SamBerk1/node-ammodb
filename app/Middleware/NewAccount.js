'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class NewAccount {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Function} next
   */
  async handle ({ request,response,view,auth }, next) {
    // call next to advance the request

      if (request.url().includes('/login') || request.url() == '/updatepassword' || (request.url() == '/') || request.url() == '/logout' || request.url() == '/newaccount') {
        await next()
        return
      }else{
        try {
          await auth.check()
          let user = await auth.getUser()
          if (user.toJSON().isNew == 1) {
            return response.redirect('/newaccount')
          }

          } catch (error) {
            return response.redirect('back')
        }
      }
      await next()
    }
}

module.exports = NewAccount
