'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const config = use('Adonis/Src/Config').get('auth.staticAuth')
const ExportToken = use('App/Models/ExportToken')
const validConfig = config && config.protectedUrls.length

class StaticAuth {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Function} next
   */
  async handle ({ request, response, auth}, next) {

    // call next to advance the request
    // if there is no valid config... skip this middleware
    if(!validConfig) return await next();

    // check if currently visited url is matching protectedUrls
    if(request.url().match(config.protectedUrls) == null) return await next()

    if(request.url().match(config.protectedUrls) != null){
      try {
        await auth.check()
        return await next()
      } catch (error) {
        if (!request.get().export || !request.get().token) return response.status(403).json({msg:'Access Denied'});
        let token = await ExportToken.query().where('batch', request.get().export).andWhere('token',request.get().token).first()
        if (token != null) {
          await next()
        } else {
          return response.status(403).json({msg:'Access Denied'})
        }
      }
    }
  }
}

module.exports = StaticAuth
