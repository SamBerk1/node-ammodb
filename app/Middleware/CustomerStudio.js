'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const StudioCustomer = use("App/Models/StudioCustomer")
class CustomerStudio {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {Function} next
   */
  async handle ({ view , auth}, next) {

    try {
        await auth.check()
        let user = await auth.getUser()
        await user.load('role')
        if (user.toJSON().role.isStudio == 1) {
          let studioCustomer = await StudioCustomer.query().with('studio').where('userId',user.id ).first()
          view.share({
            studioCustomer : studioCustomer.toJSON().studio,
            userRole : user.toJSON().role
          })
        } else {
          view.share({
            userRole : user.toJSON().role
          })
        }
    } catch (error) {
    }
    

    await next()
  }
}

module.exports = CustomerStudio
