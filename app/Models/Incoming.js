'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const IncomingFilter = use('App/ModelFilters/IncomingFilter')
const TitleStatus = use('App/Models/TitleStatus')

class Incoming extends Model {
    
    static boot () {
        super.boot()
        this.addTrait('@provider:Filterable', IncomingFilter)
        this.addHook('afterDelete', async modelInstance => {
            console.log("Incoming")
            try {
                await modelInstance.incomingNotes().delete()
                await modelInstance.customAssetsIncoming().delete()

                let titleStatus = await TitleStatus.query().where('incomingId', modelInstance.id).ids()
                titleStatus.map( async id => {
                    await(await TitleStatus.find(id)).delete()
                })
            } catch (error) {
                console.log(error.message)
            }
        })
    }
    studio(){
        return this.hasOne('App/Models/Studio','studioId','id')
    }
    library(){
        return this.hasOne('App/Models/Library','libraryId','id')
    }
    alternateName(){
        return this.hasOne('App/Models/AlternateTitleLanguage','alternateTitleId','id')
    }
    customer(){
        return this.belongsTo('App/Models/User','customerId','id')
    }
    notes(){
        return this.manyThrough('App/Models/IncomingNote','note','id','incomingId')
    }
    incomingNotes(){
        return this.hasMany('App/Models/IncomingNote','id','incomingId')
    }
    incomingEpisodes(){
        return this.hasMany('App/Models/IncomingEpisode','id','incomingId')
    }
    incomingSeasons(){
        return this.hasMany('App/Models/IncomingSeason','id','incomingId')
    }
    titleStatus(){
        return this.hasMany('App/Models/TitleStatus','id','incomingId')
    }
    customAssetsIncoming(){
        return this.hasMany('App/Models/CustomAssetsIncoming','id','incomingId')
    }
    
}

module.exports = Incoming
