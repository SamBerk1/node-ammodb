'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Season = use('App/Models/Season')
const Incoming = use('App/Models/Incoming')
class AlternateTitleLanguage extends Model {
    static boot () {
        super.boot()
        this.addHook('afterDelete', async modelInstance => {
            console.log('AlternateTitleLanguage')
            try {
                let incomings = await Incoming.query().where('alternateTitleId',modelInstance.id ).ids()
                incomings.map(async id => {
                    await (await Incoming.find(id)).delete()
                })

                let seasons = await Season.query().where('alternateTitleId',modelInstance.id ).ids()
                seasons.map(async id => {
                    await (await Season.find(id)).delete()
                })
            } catch (error) {
                console.log(error.message)
            }
        })
    }
    language(){
        return this.hasOne('App/Models/Language','languageId','id')
    }
    library(){
        return this.belongsTo('App/Models/Library','libraryId','id')
    }
    incoming(){
        return this.belongsTo('App/Models/Incoming','id','alternateTitleId')
    }
    studio(){
        return this.belongsTo('App/Models/Studio','studioId','id')
    }
    pitchs(){
        return this.hasMany('App/Models/AlternateTitlesPlatform','id','alternateTitleId')
    }
    season(){
        return this.hasMany('App/Models/Season','id','alternateTitleId')
    }
}

module.exports = AlternateTitleLanguage
