'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesTrailer extends Model {
    trailer(){
        return this.hasOne('App/Models/Trailer','trailerId','id')
    }
}

module.exports = TitlesTrailer
