'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const TitleStatusesSeasonFilter = use('App/ModelFilters/TitleStatusesSeasonFilter')

class TitleStatusesSeason extends Model {
    static boot () {
        super.boot()
        this.addTrait('@provider:Filterable', TitleStatusesSeasonFilter)
        this.addHook('afterDelete', async modelInstance => {
            console.log("TitleStatusesSeason")
            try {
                await modelInstance.territories().delete()
                await modelInstance.outgoingNotes().delete()
            } catch (error) {
                console.log(error.message)
            }
        })

        // this.addHook('beforeCreate', async modelInstance => {
        //     try {
        //         let title_status_season = await TitleStatusesSeason.query().where('incomingSeasonId', modelInstance.incomingSeasonId).where('platformId', modelInstance.platformId).first()
        //         if (title_status_season) {
        //             throw new Error('Duplicate Entry')
        //         }
        //     } catch (error) {
        //         throw new Error('Duplicate Entry')
        //     }
        // })

        // this.addHook('beforeSave', async modelInstance => {
        //     try {
        //         let title_status_season = await TitleStatusesSeason.query().where('incomingSeasonId', modelInstance.incomingSeasonId).where('platformId', modelInstance.platformId).first()
        //         if (title_status_season) {
        //             throw new Error('Duplicate Entry')
        //         }
        //     } catch (error) {
        //         throw new Error('Duplicate Entry')
        //     }
        // })
    }
    
    platformRights(){
        return this.hasOne('App/Models/PlatfromsRight','platform_rightsId','id')
    }
    series(){
        return this.hasOne('App/Models/Library','seriesId','id')
    }
    season(){
        return this.hasOne('App/Models/Season','seasonId','id')
    }
    alternateName(){
        return this.hasOne('App/Models/AlternateTitleLanguage','alternateTitleId','id')
    }
    incoming(){
        return this.belongsTo('App/Models/Incoming','incomingId','id')
    }
    incomingSeason(){
        return this.hasOne('App/Models/IncomingSeason','incomingSeasonId','id')
    }
    platform(){
        return this.hasOne('App/Models/Platform','platformId','id')
    }
    notes(){
        return this.manyThrough('App/Models/OutgoingSeasonNote','note','id','outgoingSeasonId')
    }
    outgoingNotes(){
        return this.hasMany('App/Models/OutgoingSeasonNote','id','outgoingSeasonId')
    }
    studio(){
        return this.hasOne('App/Models/Studio','studioId','id')
    }
    territories(){
        return this.hasMany('App/Models/OutgoingTerritory','id','outgoingSeasonId')
    }
}

module.exports = TitleStatusesSeason
