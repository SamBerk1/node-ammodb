'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Role extends Model {
    permissions(){
        return this.hasMany('Cerberus/Models/Permission','id','role_id')
    }
    
}

module.exports = Role
