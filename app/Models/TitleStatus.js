'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const TitleStatusFilter = use('App/ModelFilters/TitleStatusFilter')
const TitleStatusesSeason = use('App/Models/TitleStatusesSeason')

class TitleStatus extends Model {
    static boot () {
        super.boot()
        this.addTrait('@provider:Filterable', TitleStatusFilter)
        this.addHook('afterDelete', async modelInstance => {
            console.log('TitleStatus')
            try {
                await modelInstance.outgoingNotes().delete()
                await modelInstance.territories().delete()
                await modelInstance.customAssetsOutgoing().delete()
            } catch (error) {
                console.log(error.message)   
            }
        })

        // this.addHook('beforeCreate', async modelInstance => {
        //     try {
        //         let title_status = await TitleStatus.query().where('incomingId', modelInstance.incomingId).where('platformId', modelInstance.platformId).first()
        //         if (title_status) {
        //             throw new Error('Duplicate Entry')
        //         }
        //     } catch (error) {
        //         throw new Error('Duplicate Entry')
        //     }
        // })

        // this.addHook('beforeSave', async modelInstance => {
        //     try {
        //         let title_status = await TitleStatus.query().where('incomingId', modelInstance.incomingId).where('platformId', modelInstance.platformId).first()
        //         if (title_status) {
        //             throw new Error('Duplicate Entry')
        //         }
        //     } catch (error) {
        //         throw new Error('Duplicate Entry')
        //     }
        // })
    }

    platformRights(){
        return this.hasOne('App/Models/PlatfromsRight','platform_rightsId','id')
    }
    library(){
        return this.hasOne('App/Models/Library','libraryId','id')
    }
    alternateName(){
        return this.hasOne('App/Models/AlternateTitleLanguage','alternateTitleId','id')
    }
    incoming(){
        return this.belongsTo('App/Models/Incoming','incomingId','id')
    }
    notes(){
        return this.manyThrough('App/Models/OutgoingNote','note','id','outgoingId')
    }
    outgoingNotes(){
        return this.hasMany('App/Models/OutgoingNote','id','outgoingId')
    }
    platform(){
        return this.hasOne('App/Models/Platform','platformId','id')
    }
    studio(){
        return this.hasOne('App/Models/Studio','studioId','id')
    }
    territories(){
        return this.hasMany('App/Models/OutgoingTerritory','id','outgoingMovieId')
    }
    customAssetsOutgoing(){
        return this.hasMany('App/Models/CustomAssetsOutgoing','id','outgoingId')
    }
    seasons(){
        return this.hasMany('App/Models/TitleStatusesSeason','incomingId','incomingId')
    }
    
}

module.exports = TitleStatus
