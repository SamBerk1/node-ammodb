'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class IncomingNote extends Model {
    note(){
        return this.hasOne('App/Models/Note','noteId','id')
    }
    user(){
        return this.belongsTo('App/Models/User','userId','id')
    }
}

module.exports = IncomingNote
