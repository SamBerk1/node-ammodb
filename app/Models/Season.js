'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const IncomingSeason = use('App/Models/IncomingSeason')
const Episode = use('App/Models/Episode')

class Season extends Model {
    static boot () {
        super.boot()
        this.addHook('afterDelete', async modelInstance => {
            console.log('Season')
            try {
                let incomingSeason = await IncomingSeason.findBy('seasonId',modelInstance.id)
                await incomingSeason.delete()

                let episodes = await Episode.query().where('seasonId', modelInstance.id).ids()
                episodes.map( async id => {
                    await(await Episode.find(id)).delete()
                })
            } catch (error) {
                console.log(error.message)
            }
        })
    }
    incomingSeason(){
        return this.hasOne('App/Models/IncomingSeason','id','seasonId')
    }
}

module.exports = Season
