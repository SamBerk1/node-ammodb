'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Hash = use('Hash')

class User extends Model {
  static boot () {
    super.boot()

    this.addTrait('@provider:Cerberus/Traits/Role')
    this.addTrait('@provider:Cerberus/Traits/Permission')
    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async userInstance => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
 * A relationship on tokens is required for auth to
 * work. Since features like `refreshTokens` or
 * `rememberToken` will be saved inside the
 * tokens table.
 *
 * @method tokens
 *
 * @return {Object}
 */

  role () {
    return this.hasOne('Cerberus/Models/Role','role_id','id')
  }

  studio(){
    return this.hasOne('App/Models/StudioCustomer','id','userId')
  }

  /**
   * Hide password when user is fetched.
   */
  static get hidden () {
    return ['password']
  }

  
}

module.exports = User