'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TerritoryCountry extends Model {
    territory(){
        return this.hasOne("App/Models/Territory","territoryId","id")
    }
}

module.exports = TerritoryCountry
