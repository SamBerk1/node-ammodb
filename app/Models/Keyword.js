'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Keyword extends Model {
    platform(){
        return this.hasOne('App/Models/Platform','platformId','id')
    }
    library(){
        return this.hasOne('App/Models/Library','libraryId','id')
    }
}

module.exports = Keyword
