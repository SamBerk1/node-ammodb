'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesAward extends Model {
    award(){
        return this.hasOne('App/Models/Award','awardId','id')
    }
}

module.exports = TitlesAward
