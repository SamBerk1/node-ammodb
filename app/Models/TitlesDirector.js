'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesDirector extends Model {
    director(){
        return this.hasOne('App/Models/Director','directorId','id')
    }
}

module.exports = TitlesDirector
