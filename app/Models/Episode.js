'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Episode extends Model {
    static boot () {
        super.boot()
        this.addHook('afterDelete', async modelInstance => {
            await modelInstance.incomingEpisode().delete()
        })
    }
    season(){
        return this.belongsTo("App/Models/Season","seasonId","id")
    }
    incoming(){
        return this.belongsTo("App/Models/Incoming","alternateTitleId","alternateTitleId")
    }
    alternateName(){
        return this.belongsTo("App/Models/AlternateTitleLanguage","alternateTitleId","id")
    }
    studio(){
        return this.belongsTo("App/Models/Studio","studioId","id")
    }
    incomingEpisode(){
        return this.hasOne("App/Models/IncomingEpisode","id","episodeId")
    }
}

module.exports = Episode
