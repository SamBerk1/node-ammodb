'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const TitleStatusesSeason = use('App/Models/TitleStatusesSeason')
class IncomingSeason extends Model {
    static boot () {
        super.boot()
        this.addHook('afterDelete', async modelInstance => {
            console.log("IncomingSeason")
            try {
                await modelInstance.incomingEpisodes().delete()
            
                let titleStatusesSeason = await TitleStatusesSeason.query().where('incomingSeasonId', modelInstance.id).ids()
                titleStatusesSeason.map( async id => {
                    await(await TitleStatusesSeason.find(id)).delete()
                })
            } catch (error) {
                console.log(error.message)
            }
        })
    }
    season(){
        return this.hasOne('App/Models/Season','seasonId','id')
    }
    incomingEpisodes(){
        return this.hasMany('App/Models/IncomingEpisode','incomingId','incomingId')
    }
    incoming(){
        return this.belongsTo('App/Models/Incoming','incomingId','id')
    }
    alternateName(){
        return this.hasOne('App/Models/AlternateTitleLanguage','alternateTitleId','id')
    }
    TitleStatusSeason(){
        return this.hasMany('App/Models/TitleStatusesSeason','id','incomingSeasonId')
    }
}

module.exports = IncomingSeason
