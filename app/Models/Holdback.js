'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Holdback extends Model {
    platform(){
        return this.hasOne('App/Models/Platform','platformId','id')
    }
    library(){
        return this.hasOne('App/Models/Library','libraryId','id')
    }
    territory(){
        return this.hasOne('App/Models/Territory','territoryId','id')
    }
}

module.exports = Holdback
