'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AlternateTitlesPlatform extends Model {
    library(){
        return this.belongsTo('App/Models/Library','libraryId','id')
    }
}

module.exports = AlternateTitlesPlatform
