'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesProducer extends Model {
    producer(){
        return this.hasOne('App/Models/Producer','producerId','id')
    }
}

module.exports = TitlesProducer
