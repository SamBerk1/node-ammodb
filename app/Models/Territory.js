'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Territory extends Model {
    countries(){
        return this.hasMany("App/Models/TerritoryCountry","id",'territoryId')
    }
    rightsCountries(){
        return this.hasMany("App/Models/TitlesTerritoriesRight","id","countryId")
    }

    rightsTitles(){
        return this.hasMany("App/Models/TitlesTerritoriesRight","id","territoryId")
    }
}

module.exports = Territory
