'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesGenre extends Model {
    genre(){
        return this.hasOne('App/Models/Genre','genreId','id')
    }
}

module.exports = TitlesGenre
