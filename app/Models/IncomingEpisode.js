'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class IncomingEpisode extends Model {
    season(){
        return this.hasOne('App/Models/Season','seasonId','id')
    }
    episode(){
        return this.hasOne('App/Models/Episode','episodeId','id')
    }
}

module.exports = IncomingEpisode
