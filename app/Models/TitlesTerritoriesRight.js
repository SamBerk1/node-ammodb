'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const moment = require('moment')

class TitlesTerritoriesRight extends Model {

    static get computed () {
        return ['active']
    }
    
    getActive ({ endDate }) {
        let status = moment(endDate).isAfter(new Date())
        return status
    }

    territory(){
        return this.belongsTo('App/Models/Territory','territoryId','id')
    }
    rights(){
        return this.belongsTo('App/Models/Right','rightId','id')
    }
    library(){
        return this.belongsTo('App/Models/Library','libraryId','id')
    }
    country(){
        return this.hasOne('App/Models/TerritoryCountry','countryId','id')
    }
    territoryCountries(){
        return this.manyThrough('App/Models/TitlesTerritoriesRight','country','rightId','rightId')
    }
}

module.exports = TitlesTerritoriesRight