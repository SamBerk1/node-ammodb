'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesScreener extends Model {
    screener(){
        return this.hasOne('App/Models/Screener','screenerId','id')
    }
}

module.exports = TitlesScreener
