'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AdBreak extends Model {
    library(){
        return this.hasOne('App/Models/Library','libraryId','id')
    }
    platform(){
        return this.hasOne('App/Models/Platform','platformId','id')
    }
}

module.exports = AdBreak
