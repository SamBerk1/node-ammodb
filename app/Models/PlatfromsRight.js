'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PlatfromsRight extends Model {
    territory(){
        return this.belongsTo('App/Models/Territory','territoryId','id')
    }
    rights(){
        return this.belongsTo('App/Models/Right','rightId','id')
    }
    platform(){
        return this.belongsTo('App/Models/Platform','platformId','id')
    }
    country(){
        return this.belongsTo('App/Models/TerritoryCountry','countryId','id')
    }
}

module.exports = PlatfromsRight
