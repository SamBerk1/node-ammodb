'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesCountry extends Model {
    country(){
        return this.hasOne('App/Models/Country','countryId','id')
    }
}

module.exports = TitlesCountry
