'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Subscriber extends Model {
    emailTemplate(){
        return this.hasOne('App/Models/Email','emailId','id')
    }
}

module.exports = Subscriber
