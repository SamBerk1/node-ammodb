'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Delivery extends Model {
    library(){
        return this.hasOne('App/Models/Library','libraryId','id')
    }
}

module.exports = Delivery
