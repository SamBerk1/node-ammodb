'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StudioCustomer extends Model {
    studio(){
        return this.hasOne("App/Models/Studio","studioId","id")
    }
}

module.exports = StudioCustomer
