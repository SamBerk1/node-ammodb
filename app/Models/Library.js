'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const LibraryFilter = use('App/ModelFilters/LibraryFilter')
const AlternateTitleLanguage = use('App/Models/AlternateTitleLanguage')
const Image = use('App/Models/Image')
class Library extends Model {
    static boot () {
        super.boot()
        this.addTrait('@provider:Filterable', LibraryFilter)

        this.addHook('afterDelete', async library => {
            try {
                let alternateNames = await AlternateTitleLanguage.query().where('libraryId',library.id ).ids()
                alternateNames.map( async id => {
                await (await AlternateTitleLanguage.find(id)).delete()
                })
                await library.titleProducers().delete()
                await library.titleDirectors().delete()
                await library.titleCast().delete()
                await library.titleGenres().delete()
                await library.titleAwards().delete()
                await library.titleCountries().delete()
                await library.keyword().delete()
                await library.adbreaks().delete()
                await library.rights().delete()
                await library.holdbacks().delete()
                await library.images().delete()
                await library.pitchTracker().delete()
            } catch (error) {
                console.log(error.message)
            }
        })
    }
    studio(){
        return this.hasOne('App/Models/Studio','studioId','id')
    }
    adbreaks(){
        return this.hasOne('App/Models/AdBreak','id','libraryId')
    }
    series(){
        return this.hasOne('App/Models/Series','seriesId','id')
    }
    alternateNames(){
        return this.hasMany('App/Models/AlternateTitleLanguage','id','libraryId')
    }
    producers(){
        return this.manyThrough('App/Models/TitlesProducer','producer','id','libraryId')
    }
    titleProducers(){
        return this.hasMany('App/Models/TitlesProducer','id','libraryId')
    }
    directors(){
        return this.manyThrough('App/Models/TitlesDirector','director','id','libraryId')
    }
    titleDirectors(){
        return this.hasMany('App/Models/TitlesDirector','id','libraryId')
    }
    cast(){
        return this.manyThrough('App/Models/TitlesCast','cast','id','libraryId')
    }
    titleCast(){
        return this.hasMany('App/Models/TitlesCast','id','libraryId')
    }
    format(){
        return this.hasOne('App/Models/Format','formatId','id')
    }
    ratings(){
        return this.hasOne('App/Models/Rating','ratingId','id')
    }
    genres(){
        return this.manyThrough('App/Models/TitlesGenre','genre','id','libraryId')
    }
    titleGenres(){
        return this.hasMany('App/Models/TitlesGenre','id','libraryId')
    }
    awards(){
        return this.manyThrough('App/Models/TitlesAward','award','id','libraryId')
    }
    titleAwards(){
        return this.hasMany('App/Models/TitlesAward','id','libraryId')
    }
    trailers(){
        return this.hasMany('App/Models/Trailer','trailerId','id')
    }
    screeners(){
        return this.hasMany('App/Models/Screener','screenerId','id')
    }
    statuses(){
        return this.hasOne('App/Models/TitleStatus','id','libraryId')
    }
    countries(){
        return this.manyThrough('App/Models/TitlesCountry','country','id','libraryId')
    }
    titleCountries(){
        return this.hasMany('App/Models/TitlesCountry','id','libraryId')
    }
    keyword(){
        return this.hasMany('App/Models/Keyword','id','libraryId')
    }
    rights(){
        return this.hasMany('App/Models/TitlesTerritoriesRight','id','libraryId')
    }
    rightsName(){
        return this.manyThrough('App/Models/TitlesTerritoriesRight','rights','id','libraryId')
    }
    territories(){
        return this.manyThrough('App/Models/TitlesTerritoriesRight','territory','id','libraryId')
    }
    territoryCountries(){
        return this.manyThrough('App/Models/TitlesTerritoriesRight','country','id','libraryId')
    }
    languages(){
        return this.manyThrough('App/Models/AlternateTitleLanguage','language','id','libraryId')
    }
    trailer(){
        return this.hasOne('App/Models/Trailer','trailerId','id')
    }
    screener(){
        return this.hasOne('App/Models/Screener','screenerId','id')
    }
    holdbacks(){
        return this.hasMany('App/Models/Holdback','id','libraryId')
    }
    pitchTracker(){
        return this.hasMany('App/Models/AlternateTitlesPlatform','id','libraryId')
    }
    originalLanguage(){
        return this.hasOne('App/Models/Language','originalLanguageId','id')
    }
    incoming(){
        return this.hasMany('App/Models/Incoming','id','libraryId')
    }
    images(){
        return this.hasMany('App/Models/Image','id','libraryId')
    }
    image(){
        return this.hasOne('App/Models/Image','id','libraryId')
    }
}

module.exports = Library
