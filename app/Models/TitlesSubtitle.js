'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesSubtitle extends Model {
    subtitle(){
        return this.hasOne('App/Models/Subtitle','subtitleId','id')
    }
}

module.exports = TitlesSubtitle
