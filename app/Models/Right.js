'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Right extends Model {
    territory(){
        return this.manyThrough('App/Models/TitlesTerritoriesRight','territory','id','rightId')
    }
    countries(){
        return this.manyThrough('App/Models/TitlesTerritoriesRight','country','id','rightId')
    }
    rightsTitles(){
        return this.hasMany('App/Models/TitlesTerritoriesRight','id','rightId')
    }
    platformTerritories(){
        return this.manyThrough('App/Models/PlatfromsRight','territory','id','rightId')
    }
    dates(){
        return this.hasOne('App/Models/TitlesTerritoriesRight','id','rightId')
    }
}

module.exports = Right
