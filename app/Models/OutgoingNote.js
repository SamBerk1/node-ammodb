'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class OutgoingNote extends Model {
    note(){
        return this.hasOne('App/Models/Note','noteId','id')
    }
}

module.exports = OutgoingNote
