'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TitlesCast extends Model {
    cast(){
        return this.hasOne('App/Models/Cast','castId','id')
    }
}

module.exports = TitlesCast
