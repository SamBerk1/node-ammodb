module.exports = (Route) => {

    Route.get('/settings/activity_log','activityLogController/ActivityLogController.index').middleware(['auth','guard:permissions.update'])
    Route.delete('/settings/activity_log/:id', 'activityLogController/ActivityLogController.destroy').middleware(['auth','guard:permissions.update'])
}