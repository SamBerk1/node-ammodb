const Studio = use('App/Models/Studio')
const StudioCustomer = use('App/Models/StudioCustomer')
const Platform = use('App/Models/Platform')
const Format = use('App/Models/Format')
const Genre = use('App/Models/Genre')
const Cast = use('App/Models/Cast')
const Director = use("App/Models/Director");
const Producer = use("App/Models/Producer");
const Right = use("App/Models/Right");
const Languages = use("App/Models/Language");
const Territory = use('App/Models/Territory');
const TerritoryCountry = use('App/Models/TerritoryCountry');
module.exports = (Route) => {
    Route.get('/filter/studio',async ({ request, response, auth })=>{

        let user = await auth.getUser()

        if (user.toJSON().role.isStudio == 0) {
            let studios = await Studio.query().where('title', 'LIKE', `%${request.get().search ? request.get().search : ''}%`).select('id','title').fetch()
            return response.json({flag:true,data:studios.toJSON()})
            
        }
        else{
            let userStudio = await StudioCustomer.findBy('userId',user.id)
            let studio = await Studio.query().where('id', userStudio.studioId).select('id','title').fetch()

            return response.json({flag:true,data:studio.toJSON()})

        }
    }).middleware(['auth'])

    Route.get('/filter/platform',async ({ request, response })=>{
        let data = await Platform.query().select('id','name').fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/format',async ({ request, response })=>{
        let data = await Format.query().select('id','format').fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/genre',async ({ request, response })=>{
        let data = await Genre.query().select('id','title').where('title','LIKE',`%${request.get().search}%` ).fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/cast',async ({ request, response })=>{
        let data = await Cast.query().select('id','actorName').where('actorName','LIKE',`%${request.get().search}%` ).fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/director',async ({ request, response })=>{
        let data = await Director.query().select('id','name').where('name','LIKE',`%${request.get().search}%` ).fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/producer',async ({ request, response })=>{
        let data = await Producer.query().select('id','name').where('name','LIKE',`%${request.get().search}%` ).fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/language',async ({ request, response })=>{
        let data = await Languages.query().select('id','code','name').where('name','LIKE',`%${request.get().search}%` ).fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/rights',async ({ request, response })=>{
        let data = await Right.query().select('id','name').where('name','LIKE',`%${request.get().search}%` ).fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/territory',async ({ request, response })=>{
        let data = await Territory.query().select('id','territoryName').where('territoryName','LIKE',`%${request.get().search}%` ).fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])

    Route.get('/filter/country',async ({ request, response })=>{
        let data = await TerritoryCountry.query().select('id','country').where('country','LIKE',`%${request.get().search}%` ).fetch()
        return response.json({flag:true,data:data.toJSON()})
    }).middleware(['auth'])
}