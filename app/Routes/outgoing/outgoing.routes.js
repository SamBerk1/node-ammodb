module.exports = (Route) => {
    Route.get('/outgoing/movies','outgoing/outgoingController.indexMovie').middleware(['auth','guard:outgoing.read'])
    Route.get('/outgoing/movies/export','outgoing/outgoingController.indexMovie').middleware(['auth','guard:outgoing.read'])

    Route.get('/outgoing/seasons','outgoing/outgoingController.indexSeason').middleware(['auth','guard:outgoing.read'])
    Route.get('/outgoing/seasons/export','outgoing/outgoingController.indexSeason').middleware(['auth','guard:outgoing.read'])
    
    Route.get('/outgoing/:id','outgoing/outgoingController.details').middleware(['auth','guard:outgoing.read'])

    Route.get('/outgoing/season/:id','outgoing/outgoingController.detailsSeason').middleware(['auth','guard:outgoing.read'])

    Route.get('/outgoing/:id/territories','outgoing/outgoingController.getTerritories').middleware(['auth','guard:outgoing.read'])

    Route.get('/outgoing/season/:id/territories','outgoing/outgoingController.getSeasonTerritories').middleware(['auth','guard:outgoing.read'])

    Route.put('/outgoing/:id/territories/:is','outgoing/outgoingController.updateTerritories').middleware(['auth','guard:outgoing.read'])
    

}