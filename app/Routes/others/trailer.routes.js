module.exports = (Route) => {
    Route.get('/others/trailer','others/TrailerController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/trailer/add','others/TrailerController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/trailer/edit/:id','others/TrailerController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/trailer/:id','others/TrailerController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/trailer','others/TrailerController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/trailer/:id','others/TrailerController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/trailer/:id','others/TrailerController.remove').middleware(['auth','guard:others.delete'])

}







