module.exports = (Route) => {
    Route.get('/others/studio','others/StudioController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/studio/add','others/StudioController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/studio/edit/:id','others/StudioController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/studio/:id','others/StudioController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/studio','others/StudioController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/studio/:id','others/StudioController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/studio/:id','others/StudioController.remove').middleware(['auth','guard:others.delete'])

    Route.put('/studio/:id','others/StudioController.update').middleware(['auth','guard:library.create'])


}







