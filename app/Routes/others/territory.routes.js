module.exports = (Route) => {
    Route.get('/others/territory','others/TerritoryController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/territory/add','others/TerritoryController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/territory/edit/:id','others/TerritoryController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/territory/:id','others/TerritoryController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/territory','others/TerritoryController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/territory/:id','others/TerritoryController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/territory/:id','others/TerritoryController.remove').middleware(['auth','guard:others.delete'])


    Route.post('/others/territory/:id/country','others/TerritoryController.storeCountry').middleware(['auth','guard:others.create'])
    Route.put('/others/territory/:territoryId/country/:id','others/TerritoryController.updateCountry').middleware(['auth','guard:others.update'])
    Route.delete('/others/territory/:territoryId/country/:id','others/TerritoryController.removeCountry').middleware(['auth','guard:others.delete'])
    
    
}






