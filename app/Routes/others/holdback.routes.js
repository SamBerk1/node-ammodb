module.exports = (Route) => {
    Route.get('/others/holdback','others/HoldbackController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/holdback/:id','others/HoldbackController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/holdback','others/HoldbackController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/holdback/:id','others/HoldbackController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/holdback/:id','others/HoldbackController.remove').middleware(['auth','guard:others.delete'])

    Route.get('/library/:libraryId/holdback','others/HoldbackController.index').middleware(['auth','guard:library.read'])  
}