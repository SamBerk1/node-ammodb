module.exports = (Route) => {
    Route.get('/others/delivery','others/DeliveryController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/delivery/add','others/DeliveryController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/delivery/edit/:id','others/DeliveryController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/delivery/:id','others/DeliveryController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/delivery','others/DeliveryController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/delivery/:id','others/DeliveryController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/delivery/:id','others/DeliveryController.remove').middleware(['auth','guard:others.delete'])

}