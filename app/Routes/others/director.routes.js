module.exports = (Route) => {
    Route.get('/others/director','others/DirectorController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/director/add','others/DirectorController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/director/edit/:id','others/DirectorController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/director/:id','others/DirectorController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/director','others/DirectorController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/director/:id','others/DirectorController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/director/:id','others/DirectorController.remove').middleware(['auth','guard:others.delete'])

}