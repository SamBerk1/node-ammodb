module.exports = (Route) => {
    Route.get('/others/genre','others/GenreController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/genre/add','others/GenreController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/genre/edit/:id','others/GenreController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/genre/:id','others/GenreController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/genre','others/GenreController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/genre/:id','others/GenreController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/genre/:id','others/GenreController.remove').middleware(['auth','guard:others.delete'])

}