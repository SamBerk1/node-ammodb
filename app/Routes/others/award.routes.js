module.exports = (Route) => {
    Route.get('/others/award','others/AwardController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/award/add','others/AwardController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/award/edit/:id','others/AwardController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/award/:id','others/AwardController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/award','others/AwardController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/award/:id','others/AwardController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/award/:id','others/AwardController.remove').middleware(['auth','guard:others.delete'])

}