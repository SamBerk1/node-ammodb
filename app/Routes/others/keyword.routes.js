module.exports = (Route) => {
    Route.get('/others/keyword','others/KeywordController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/keyword/add','others/KeywordController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/keyword/edit/:id','others/KeywordController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/keyword/:id','others/KeywordController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/keyword','others/KeywordController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/keyword/:id','others/KeywordController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/keyword/:id','others/KeywordController.remove').middleware(['auth','guard:others.delete'])

}