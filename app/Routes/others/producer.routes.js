module.exports = (Route) => {
    Route.get('/others/producer','others/ProducerController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/producer/add','others/ProducerController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/producer/edit/:id','others/ProducerController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/producer/:id','others/ProducerController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/producer','others/ProducerController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/producer/:id','others/ProducerController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/producer/:id','others/ProducerController.remove').middleware(['auth','guard:others.delete'])

}