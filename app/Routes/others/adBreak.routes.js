module.exports = (Route) => {
    Route.get('/others/ad-break','others/AdBreakController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/ad-break/add','others/AdBreakController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/ad-break/edit/:id','others/AdBreakController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/ad-break/:id','others/AdBreakController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/ad-break','others/AdBreakController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/ad-break/:id','others/AdBreakController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/ad-break/:id','others/AdBreakController.remove').middleware(['auth','guard:others.delete'])
    

}