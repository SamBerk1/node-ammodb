module.exports = (Route) => {
    Route.get('/others/emails','others/EmailController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/emails/add','others/EmailController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/emails/edit/:id','others/EmailController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/emails/:id','others/EmailController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/emails','others/EmailController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/emails/:id','others/EmailController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/emails/:id','others/EmailController.remove').middleware(['auth','guard:others.delete'])
    
    Route.get('/others/emails/:emailId/subscribers','others/EmailController.subscribers').middleware(['auth','guard:others.read'])
    Route.get('/others/emails/:emailId/subscribers/add','others/SubscriberController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/emails/:emailId/subscribers/edit/:id','others/SubscriberController.edit').middleware(['auth','guard:others.update'])


}