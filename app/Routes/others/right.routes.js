module.exports = (Route) => {
    Route.get('/others/right','others/RightController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/right/add','others/RightController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/right/edit/:id','others/RightController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/right/:id','others/RightController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/right','others/RightController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/right/:id','others/RightController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/right/:id','others/RightController.remove').middleware(['auth','guard:others.delete'])

}