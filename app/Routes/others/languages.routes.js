module.exports = (Route) => {
    Route.get('/others/languages','others/LanguageController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/languages/add','others/LanguageController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/languages/edit/:id','others/LanguageController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/languages/:id','others/LanguageController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/languages','others/LanguageController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/languages/:id','others/LanguageController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/languages/:id','others/LanguageController.remove').middleware(['auth','guard:others.delete'])

}