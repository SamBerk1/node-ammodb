module.exports = (Route) => {
    Route.get('/others/subtitle','others/SubtitleController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/subtitle/add','others/SubtitleController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/subtitle/edit/:id','others/SubtitleController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/subtitle/:id','others/SubtitleController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/subtitle','others/SubtitleController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/subtitle/:id','others/SubtitleController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/subtitle/:id','others/SubtitleController.remove').middleware(['auth','guard:others.delete'])

}






