module.exports = (Route) => {
    Route.get('/others/series','others/SeriesController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/series/add','others/SeriesController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/series/edit/:id','others/SeriesController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/series/:id','others/SeriesController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/series','others/SeriesController.store').middleware(['auth','guard:others.create'])
    Route.post('/others/series/add/:is','others/SeriesController.selectAdd').middleware(['auth','guard:others.create'])
    Route.post('/others/series/edit/:is/:id','others/SeriesController.selectEdit').middleware(['auth','guard:others.create'])
    Route.put('/others/series/:id','others/SeriesController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/series/:id','others/SeriesController.remove').middleware(['auth','guard:others.delete'])

}