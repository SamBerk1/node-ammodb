module.exports = (Route) => {

    Route.post('/others/platform/rights/getCountries', 'others/PlatformController.getRightsCountries').middleware(['auth','guard:others.read'])
    Route.put('/others/platform/rights/update/countries','others/PlatformController.RightsManagementUpdate').middleware(['auth','guard:others.update'])

    Route.get('/others/platform','others/PlatformController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/platform/add','others/PlatformController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/platform/edit/:id','others/PlatformController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/platform/:id','others/PlatformController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/platform','others/PlatformController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/platform/:id','others/PlatformController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/platform/:id','others/PlatformController.remove').middleware(['auth','guard:others.delete'])


    Route.delete('/others/platform/rights/manage','others/PlatformController.RightsManagementRemoveTerritory').middleware(['auth','guard:others.delete'])
    Route.get('/others/platform/rights/manage/:id','others/PlatformController.RightsManagementIndex').middleware(['auth','guard:others.create'])
    Route.get('/others/platform/:platformId/rights/manage/:rightId/edit','others/PlatformController.RightsManagementEdit').middleware(['auth','guard:others.update'])
    Route.post('/others/platform/rights/manage/:id','others/PlatformController.RightsManagementStore').middleware(['auth','guard:others.create'])
    Route.put('/others/platform/rights/manage/:id','others/PlatformController.RightsManagementUpdate').middleware(['auth','guard:others.update'])
    Route.delete('/others/platform/rights/:rightId/manage/:platformId','others/PlatformController.RightsManagementRemoveRight').middleware(['auth','guard:others.delete'])
    
    
}