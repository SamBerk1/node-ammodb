module.exports = (Route) => {
    Route.get('/others/ratings','others/RatingController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/ratings/add','others/RatingController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/ratings/edit/:id','others/RatingController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/ratings/:id','others/RatingController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/ratings','others/RatingController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/ratings/:id','others/RatingController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/ratings/:id','others/RatingController.remove').middleware(['auth','guard:others.delete'])

}