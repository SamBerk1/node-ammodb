module.exports = (Route) => {
    Route.get('/others/subscribers','others/SubscriberController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/subscribers/add','others/SubscriberController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/subscribers/edit/:id','others/SubscriberController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/subscribers/:id','others/SubscriberController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/subscribers','others/SubscriberController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/subscribers/:id','others/SubscriberController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/subscribers/:id','others/SubscriberController.remove').middleware(['auth','guard:others.delete'])

}