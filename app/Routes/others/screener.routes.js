module.exports = (Route) => {
    Route.get('/others/screener','others/ScreenerController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/screener/add','others/ScreenerController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/screener/edit/:id','others/ScreenerController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/screener/:id','others/ScreenerController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/screener','others/ScreenerController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/screener/:id','others/ScreenerController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/screener/:id','others/ScreenerController.remove').middleware(['auth','guard:others.delete'])

}








