module.exports = (Route) => {
    Route.get('/others/format','others/FormatController.index').middleware(['auth','guard:others.read'])
    Route.get('/others/format/add','others/FormatController.add').middleware(['auth','guard:others.create'])
    Route.get('/others/format/edit/:id','others/FormatController.edit').middleware(['auth','guard:others.update'])
    Route.get('/others/format/:id','others/FormatController.details').middleware(['auth','guard:others.read'])
    Route.post('/others/format','others/FormatController.store').middleware(['auth','guard:others.create'])
    Route.put('/others/format/:id','others/FormatController.update').middleware(['auth','guard:others.update'])
    Route.delete('/others/format/:id','others/FormatController.remove').middleware(['auth','guard:others.delete'])

}