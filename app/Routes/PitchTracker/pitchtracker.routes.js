module.exports = (Route)=> {
    Route.get('/pitchtracker', 'PitchTracker/PitchTrackerController.index').middleware(['auth','guard:pitchtracker.read'])
    Route.post('/pitchtracker', 'PitchTracker/PitchTrackerController.store').middleware(['auth','guard:pitchtracker.create'])
    Route.put('/pitchtracker/:id', 'PitchTracker/PitchTrackerController.update').middleware(['auth','guard:pitchtracker.update'])
    Route.delete('/pitchtracker/:id', 'PitchTracker/PitchTrackerController.remove').middleware(['auth','guard:pitchtracker.delete'])

    Route.get('/library/:libraryId/pitchtracker', 'PitchTracker/PitchTrackerController.indexTitle').middleware(['auth','guard:pitchtracker.read'])

}