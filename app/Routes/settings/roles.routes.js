module.exports = (Route) => {    

    Route.get('settings/roles','Settings/RoleController.index').middleware(['auth','guard:roles.read'])
    Route.get('settings/roles/add','Settings/RoleController.add').middleware(['auth','guard:roles.create'])
    Route.get('settings/roles/edit/:id','Settings/RoleController.edit').middleware(['auth','guard:roles.update'])
    Route.get('settings/roles/:id','Settings/RoleController.details').middleware(['auth','guard:roles.read'])
    Route.post('settings/roles','Settings/RoleController.store').middleware(['auth','guard:roles.create'])
    Route.put('settings/roles/:id','Settings/RoleController.update').middleware(['auth','guard:roles.update'])
    Route.delete('settings/roles/:id','Settings/RoleController.remove').middleware(['auth','guard:roles.delete'])

}