module.exports = (Route) => {

    Route.get('settings/roles/:roleId/permissions','Settings/PermissionController.index').middleware(['auth','guard:permissions.read'])
    Route.put('settings/roles/:roleId/permissions/:id','Settings/PermissionController.update').middleware(['auth','guard:permissions.update'])
}

