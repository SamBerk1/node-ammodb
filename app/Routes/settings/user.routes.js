module.exports = (Route) => { 
    
    Route.get('settings/users','Settings/UserController.index').middleware(['auth','guard:users.read'])
    Route.get('users/search','Settings/UserController.searchUser').middleware(['auth','guard:users.read'])
    Route.get('settings/users/add','Settings/UserController.add').middleware(['auth','guard:users.create'])
    Route.get('settings/users/edit/:id','Settings/UserController.edit').middleware(['auth','guard:users.update'])
    Route.get('settings/users/:id','Settings/UserController.details').middleware(['auth','guard:users.read'])
    Route.post('settings/users','Settings/UserController.store').middleware(['auth','guard:users.create'])
    Route.put('settings/users/:id','Settings/UserController.update').middleware(['auth','guard:users.update'])
    Route.delete('settings/users/:id','Settings/UserController.remove').middleware(['auth','guard:users.delete'])
    Route.post('settings/users/:id/changepassword', 'Settings/UserController.updatePassword').middleware(['auth','guard:users.update'])
}