module.exports = (Route) => {
    
    Route.post('/project-management/import/update','ProjectManagement/ProjectManagementController.import').middleware(['auth','guard:projectManagement.read'])
    Route.post('/project-management/import/episodes/update','ProjectManagement/ProjectManagementController.importSeasons').middleware(['auth','guard:projectManagement.read'])
    
    Route.get('/project-management/movie','ProjectManagement/ProjectManagementController.index').middleware(['auth','guard:projectManagement.read'])
    Route.get('/project-management/movie/export','ProjectManagement/ProjectManagementController.exportData').middleware(['auth','guard:projectManagement.read'])

    Route.get('/project-management/season/:alternateTitleId?/:platformId?','ProjectManagement/ProjectManagementController.indexSeason').middleware(['auth','guard:projectManagement.read'])

    Route.get('/project-management/undo/movie/:id','ProjectManagement/ProjectManagementController.undoMovie').middleware(['auth','guard:projectManagement.update'])
    Route.get('/project-management/undo/season/:id','ProjectManagement/ProjectManagementController.undoSeason').middleware(['auth','guard:projectManagement.update'])
    
    Route.get('/project-management/details/season/:id','ProjectManagement/ProjectManagementController.getDetailsSeason').middleware(['auth','guard:projectManagement.read'])
    Route.get('/project-management/details/:id','ProjectManagement/ProjectManagementController.getDetails').middleware(['auth','guard:projectManagement.read'])
    
    Route.post('/project-management/:type/check-duplicates','ProjectManagement/ProjectManagementController.checkDuplicates').middleware(['auth','guard:projectManagement.update'])
    

    Route.get('/project-management/:id','ProjectManagement/ProjectManagementController.details').middleware(['auth','guard:projectManagement.read'])

    Route.post('/project-management/incoming_titles','ProjectManagement/ProjectManagementController.pullIncoming').middleware(['auth','guard:projectManagement.read'])
    Route.post('/project-management/season/incoming_season','ProjectManagement/ProjectManagementController.pullIncomingSeason').middleware(['auth','guard:projectManagement.read'])

    Route.put('/project-management/:id','ProjectManagement/ProjectManagementController.update').middleware(['auth','guard:projectManagement.update'])
    Route.put('/project-management/season/:id','ProjectManagement/ProjectManagementController.updateSeason').middleware(['auth','guard:projectManagement.update'])

    Route.delete('/project-management/:id','ProjectManagement/ProjectManagementController.remove').middleware(['auth','guard:projectManagement.delete'])
    Route.delete('/project-management/season/:id','ProjectManagement/ProjectManagementController.removeSeason').middleware(['auth','guard:projectManagement.delete'])


    Route.post('/project-management/:id/schedule', 'ProjectManagement/ProjectManagementController.schedule').middleware(['auth','guard:projectManagement.update'])
    Route.post('/project-management/season/:id/schedule', 'ProjectManagement/ProjectManagementController.scheduleSeason').middleware(['auth','guard:projectManagement.update'])

    Route.post('/project-management/:id/notes', 'ProjectManagement/ProjectManagementController.addNotes').middleware(['auth','guard:projectManagement.update'])
    Route.post('/project-management/season/:id/notes', 'ProjectManagement/ProjectManagementController.addSeasonNotes').middleware(['auth','guard:projectManagement.update'])

    Route.delete('/project-management/:id/notes/:noteId', 'ProjectManagement/ProjectManagementController.deleteNotes').middleware(['auth','guard:projectManagement.update'])
    Route.delete('/project-management/season/:id/notes/:noteId', 'ProjectManagement/ProjectManagementController.deleteSeasonNotes').middleware(['auth','guard:projectManagement.update'])
}