module.exports = (Route) => {
    Route.post('/incoming/imoport/update','incoming/IncomingController.import').middleware(['auth','guard:incoming.update'])
    Route.post('/incoming/imoport/episodes/update','incoming/IncomingController.importEpisodes').middleware(['auth','guard:incoming.update'])

    Route.get('/incoming','incoming/IncomingController.index').middleware(['auth','guard:incoming.read'])
    Route.get('/incoming/export','incoming/IncomingController.index').middleware(['auth','guard:incoming.read'])

    Route.get('/incoming/details/:id','incoming/IncomingController.incomingDetail').middleware(['auth','guard:incoming.read'])

    Route.get('/incoming/search','incoming/IncomingController.search').middleware(['auth','guard:incoming.read'])
    Route.get('/incoming/project/:id','incoming/IncomingController.addToProjectManganement').middleware(['auth','guard:incoming.update'])
    Route.get('/incoming/:id','incoming/IncomingController.details').middleware(['auth','guard:incoming.read'])
    Route.post('/incoming/:id/customer/:type','incoming/IncomingController.assignCustomer').middleware(['auth','guard:incoming.update'])
    Route.put('/incoming/:id','incoming/IncomingController.update').middleware(['auth','guard:incoming.update'])
    Route.post('/incoming/project_management/:id','incoming/IncomingController.addToProjectManagement').middleware(['auth','guard:incoming.update'])

    Route.post('/incoming/:id/schedule','incoming/IncomingController.schedule').middleware(['auth','guard:incoming.update'])

    Route.post('/incoming/:id/notes', 'incoming/IncomingController.addNotes').middleware(['auth','guard:incoming.update'])
    Route.delete('/incoming/:id/notes/:noteId', 'incoming/IncomingController.deleteNotes').middleware(['auth','guard:incoming.update'])

    Route.get('/incoming/:incomingId/season/:incomingSeasonId/episodes', 'incoming/IncomingController.indexIncomingEpisodes').middleware(['auth','guard:incoming.read'])
    Route.put('/incoming/episode/:id', 'incoming/IncomingController.updateEpisodeStatus').middleware(['auth','guard:incoming.update'])
    Route.post('/incoming/episode/:id/schedule', 'incoming/IncomingController.scheduleEpisode').middleware(['auth','guard:incoming.update'])
    Route.post('/incoming/:incomingId/season/:incomingSeasonId/addtoproject_management','incoming/IncomingController.addSeasonToProjectManagement').middleware(['auth','guard:incoming.update'])
}