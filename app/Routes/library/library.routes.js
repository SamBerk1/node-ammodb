module.exports = (Route) => {

    Route.get('library','Library/LibraryController.index').middleware(['auth','guard:library.read'])
    Route.post('library/export','Library/LibraryController.index').middleware(['auth','guard:library.read'])
    Route.delete('library/rights/manage','Library/LibraryController.RightsManagementRemoveTerritory').middleware(['auth','guard:library.delete'])
    Route.get('library/rights/manage/:id','Library/LibraryController.RightsManagementIndex').middleware(['auth','guard:library.update'])
    Route.post('library/rights/manage/:id','Library/LibraryController.RightsManagementStore').middleware(['auth','guard:library.update'])
    Route.delete('library/rights/:rightId/manage/:libraryId','Library/LibraryController.RightsManagementRemoveRight').middleware(['auth','guard:library.delete'])
    Route.get('library/rights/manage/edit/:id','Library/LibraryController.RightsManagementEdit').middleware(['auth','guard:library.update'])
    Route.get('library/add','Library/LibraryController.add').middleware(['auth','guard:library.create'])
    Route.get('library/:id/alternatetitle/:alternateTitleId/add/episode','Library/LibraryController.addEpisode').middleware(['auth','guard:library.create'])
    Route.get('library/:id/alternatetitle/:alternateTitleId/episodes','Library/LibraryController.indexEpisode').middleware(['auth','guard:library.read'])
    Route.get('library/:id/alternatetitle/:alternateTitleId/episodes/export','Library/LibraryController.exportEpisode').middleware(['auth','guard:library.read'])
    Route.post('library/episode','Library/LibraryController.storeEpisode').middleware(['auth','guard:library.create'])
    Route.delete('library/episode/:id', 'Library/LibraryController.deleteEpisode').middleware(['auth','guard:library.delete'])
    Route.get('library/edit/:id','Library/LibraryController.edit').middleware(['auth','guard:library.update'])
    Route.get('library/:id','Library/LibraryController.details').middleware(['auth','guard:library.read'])
    Route.post('library','Library/LibraryController.store').middleware(['auth','guard:library.create'])
    Route.put('library/:id','Library/LibraryController.update').middleware(['auth','guard:library.update']) 
    Route.delete('library/:id','Library/LibraryController.remove').middleware(['auth','guard:library.delete'])

    Route.post('library/rights/getCountries','Library/LibraryController.getRightsCountries').middleware(['auth','guard:library.read'])
    Route.put('library/rights/update/countries', 'Library/LibraryController.RightsManagementUpdate').middleware(['auth','guard:library.update'])
    Route.delete('library/alternatename/:id', 'Library/LibraryController.deleteAlternateTitle').middleware(['auth','guard:library.delete'])
    Route.put('/library/alternatename/:id','Library/LibraryController.updateAlternateTitle').middleware(['auth','guard:library.update'])

    Route.post('/library/import','Library/LibraryController.importLibrary').middleware(['auth','guard:library.create'])
    Route.post('/library/import/update','Library/LibraryController.importUpdateLibrary').middleware(['auth','guard:library.update'])

    Route.get('/library/:id/add/images','Library/LibraryController.addImages').middleware(['auth','guard:library.create'])
    Route.post('/library/:id/images','Library/LibraryController.storeImages').middleware(['auth','guard:library.create'])
    Route.get('/library/:id/images','Library/LibraryController.indexImages').middleware(['auth','guard:library.read'])
    Route.delete('/library/:id/images','Library/LibraryController.deleteImages').middleware(['auth','guard:library.delete'])

    Route.get('library/:id/alternatetitle/:alternateTitleId/seasons','Library/LibraryController.indexSeasons').middleware(['auth','guard:library.read'])
    Route.put('library/:id/alternatetitle/:alternateTitleId/seasons/:seasonId','Library/LibraryController.updateSeason').middleware(['auth','guard:library.update'])
    Route.get('library/:id/alternatetitle/:alternateTitleId/seasons/:seasonId/edit','Library/LibraryController.editSeason').middleware(['auth','guard:library.update'])
    Route.get('library/:id/alternatetitle/:alternateTitleId/seasons/add','Library/LibraryController.addSeason').middleware(['auth','guard:library.create'])
    Route.post('library/:id/alternatetitle/:alternateTitleId/seasons','Library/LibraryController.storeSeason').middleware(['auth','guard:library.create'])
    Route.delete('library/:id/alternatetitle/:alternateTitleId/seasons/:seasonId', 'Library/LibraryController.deleteSeason').middleware(['auth','guard:library.delete'])

    Route.get('/library/keywords/:libraryId/:alternateTitleId?/:seasonId?/:episodeId?', 'others/KeywordController.getKeywords').middleware(['auth','guard:library.read'])
    Route.put('/library/keywords/:libraryId/:alternateTitleId?/:seasonId?/:episodeId?', 'others/KeywordController.update').middleware(['auth','guard:library.create'])
}