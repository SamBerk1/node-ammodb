module.exports = (Route) => {
    Route.post('/login', 'Auth/AuthenticationController.login')
    Route.get('/logout', 'Auth/AuthenticationController.logout')
    Route.post('/updatepassword', 'Auth/AuthenticationController.updatePassword').middleware(['auth'])
}