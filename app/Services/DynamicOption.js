const AdBreak = use("App/Models/AdBreak");
const Award = use("App/Models/Award");
const Cast = use("App/Models/Cast");
const Delivery = use("App/Models/Delivery");
const Director = use("App/Models/Director");
const Format = use("App/Models/Format");
const Genre = use("App/Models/Genre");
const Holdback = use("App/Models/Holdback");
const Keyword = use("App/Models/Keyword");
const Rating = use("App/Models/Rating");
const Platform = use("App/Models/Platform");
const Producer = use("App/Models/Producer");
const Right = use("App/Models/Right");
const Screener = use("App/Models/Screener");
const Studio = use("App/Models/Studio");
const Subtitle = use("App/Models/Subtitle");
const Territory = use("App/Models/Territory");
const Trailer = use("App/Models/Trailer");
const Country = use("App/Models/Country");
const Language = use("App/Models/Language");


module.exports = {
    genre : async function (text,trx) {
        try {
            let id = ((await Genre.create({title:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }
    },
    cast : async function (text,trx) {
        try {
            let id = ((await Cast.create({actorName:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    studio : async function (text,trx) {
        try {
            let id = ((await Studio.create({title:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    director : async function (text,trx) {
        try {
            let id = ((await Director.create({name:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    producer : async function (text,trx) {
        try {
            let id = ((await Producer.create({name:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    rating : async function (text,trx) {
        try {
            let id = ((await Rating.create({name:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    format : async function (text,trx) {
        try {
            let id = ((await Format.create({format:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    territory : async function (text,trx) {
        try {
            let id = ((await Territory.create({territoryName:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    platform : async function (text,trx) {
        try {
            let id = ((await Platform.create({name:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    award : async function (text,trx) {
        try {
            let id = ((await Award.create({awardName:text},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    screener : async function (text,trx) {
        try {
            let id = ((await Screener.create({screenerLink:text.url,screenerPW:text.pw,name:text.name},trx)).toJSON()).id
            return id
        } catch (error) {

        }

    },
    trailer : async function (text,trx) {
        try {
            let id = ((await Trailer.create({trailerUrl:text.url,name:text.name},trx)).toJSON()).id
            return id
        } catch (error) {

        }

    },
    country : async function (text,trx) {
        try {
            let id = ((await Country.create({code:text.code,name:text.name},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
    language : async function (text,trx) {
        try {
            let id = ((await Language.create({code:text.code,name:text.name},trx)).toJSON()).id
            return id
        } catch (error) {
            
        }

    },
}