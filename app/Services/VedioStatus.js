
module.exports.incomingStatuses = () => {
    return {
        vedioStatus : [
            {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
            {name:"Ingesting",value:"0",icon:"assets/New_Icons/injesting-min.png"},
            {name:"Received",value:"1",icon:"assets/New_Icons/received-min.png"},
            {name:"Passed QC",value:"2",icon:"assets/New_Icons/ready-min.png"},
            {name:"Fail",value:"3",icon:"assets/New_Icons/rejected-min.png"},
            {name:"Needs QC",value:"4",icon:"assets/New_Icons/qc_needed-min.png"},
            { name:"Awaiting Approval", value:"6", icon:"assets/New_Icons/waiting_on_approval-min.png" }

        ],

        caps : [
            {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
            {name:"Ingesting",value:"0",icon:"assets/New_Icons/injesting-min.png"},
            {name:"Received",value:"1",icon:"assets/New_Icons/received-min.png"},
            {name:"Passed QC",value:"2",icon:"assets/New_Icons/ready-min.png"},
            {name:"Fail",value:"3",icon:"assets/New_Icons/rejected-min.png"},
            {name:"Needs QC",value:"4",icon:"assets/New_Icons/qc_needed-min.png"},
            { name:"Awaiting Approval", value:"6", icon:"assets/New_Icons/waiting_on_approval-min.png" }

        ],

        subs : [
            {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
            {name:"Ingesting",value:"0",icon:"assets/New_Icons/injesting-min.png"},
            {name:"Received",value:"1",icon:"assets/New_Icons/received-min.png"},
            {name:"Passed QC",value:"2",icon:"assets/New_Icons/ready-min.png"},
            {name:"Fail",value:"3",icon:"assets/New_Icons/rejected-min.png"},
            {name:"Needs QC",value:"4",icon:"assets/New_Icons/qc_needed-min.png"},
            { name:"Awaiting Approval", value:"6", icon:"assets/New_Icons/waiting_on_approval-min.png" }

        ],

        art : [
            {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
            {name:"Ingesting",value:"0",icon:"assets/New_Icons/injesting-min.png"},
            {name:"Received",value:"1",icon:"assets/New_Icons/received-min.png"},
            {name:"Passed QC",value:"2",icon:"assets/New_Icons/ready-min.png"},
            {name:"Fail",value:"3",icon:"assets/New_Icons/rejected-min.png"},
            {name:"Needs QC",value:"4",icon:"assets/New_Icons/qc_needed-min.png"},
            { name:"Awaiting Approval", value:"6", icon:"assets/New_Icons/waiting_on_approval-min.png" }
        ],

        dub : [
            {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
            {name:"Ingesting",value:"0",icon:"assets/New_Icons/injesting-min.png"},
            {name:"Received",value:"1",icon:"assets/New_Icons/received-min.png"},
            {name:"Passed QC",value:"2",icon:"assets/New_Icons/ready-min.png"},
            {name:"Fail",value:"3",icon:"assets/New_Icons/rejected-min.png"},
            {name:"Needs QC",value:"4",icon:"assets/New_Icons/qc_needed-min.png"},
            { name:"Awaiting Approval", value:"6", icon:"assets/New_Icons/waiting_on_approval-min.png" }

            
        ],

        metadata : [
            {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
            {name:"Ingesting",value:"0",icon:"assets/New_Icons/injesting-min.png"},
            {name:"Received",value:"1",icon:"assets/New_Icons/received-min.png"},
            {name:"Passed QC",value:"2",icon:"assets/New_Icons/ready-min.png"},
            {name:"Fail",value:"3",icon:"assets/New_Icons/rejected-min.png"},
            {name:"Needs QC",value:"4",icon:"assets/New_Icons/qc_needed-min.png"},
            {name:"Awaiting Approval", value:"6", icon:"assets/New_Icons/waiting_on_approval-min.png" }
        ]
    }
}

module.exports.projectManagementStatuses = () => {return {
    vedioStatus : [
        { name:"Encoding", value:"0", icon:"assets/New_Icons/transcoding-min.png" },
        { name:"In progress", value:"1", icon:"assets/New_Icons/in_progress-min.png" },
        { name:"Awaiting Approval", value:"2", icon:"assets/New_Icons/waiting_on_approval-min.png" },
        { name:"Needs QC", value:"3", icon:"assets/New_Icons/qc_needed-min.png" },
        { name:"Issue", value:"4", icon:"assets/New_Icons/issues-min.png" },
        { name:"Fail", value:"5", icon:"assets/New_Icons/rejected-min.png" },
        { name:"Uploading", value:"6", icon:"assets/New_Icons/uploading-min.png" },
        { name:"Delivered", value:"7", icon:"assets/New_Icons/delivered-min.png" },
        { name:"Removed/Expired", value:"8", icon:"assets/New_Icons/cancelled-min.png" },
        {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
        {name:"Passed QC",value:"10",icon:"assets/New_Icons/ready-min.png"},

    ],


    caps : [
        { name:"Needs Captions", value:"0", icon:"assets/New_Icons/caps_needed-min.png" },
        { name:"In progress", value:"1", icon:"assets/New_Icons/in_progress-min.png" },
        { name:"Awaiting Approval", value:"2", icon:"assets/New_Icons/waiting_on_approval-min.png" },
        { name:"Needs QC", value:"3", icon:"assets/New_Icons/qc_needed-min.png" },
        { name:"Issue", value:"4", icon:"assets/New_Icons/issues-min.png" },
        { name:"Fail", value:"5", icon:"assets/New_Icons/rejected-min.png" },
        { name:"Uploading", value:"6", icon:"assets/New_Icons/uploading-min.png" },
        { name:"Delivered", value:"7", icon:"assets/New_Icons/delivered-min.png" },
        { name:"Removed/Expired", value:"8", icon:"assets/New_Icons/cancelled-min.png" },
        {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
        {name:"Passed QC",value:"10",icon:"assets/New_Icons/ready-min.png"},

    ],


    subs : [
        { name:"Needs Subtitles", value:"0", icon:"assets/New_Icons/subs_needed-min.png" },
        { name:"In progress", value:"1", icon:"assets/New_Icons/in_progress-min.png" },
        { name:"Awaiting Approval", value:"2", icon:"assets/New_Icons/waiting_on_approval-min.png" },
        { name:"Needs QC", value:"3", icon:"assets/New_Icons/qc_needed-min.png" },
        { name:"Issue", value:"4", icon:"assets/New_Icons/issues-min.png" },
        { name:"Fail", value:"5", icon:"assets/New_Icons/rejected-min.png" },
        { name:"Uploading", value:"6", icon:"assets/New_Icons/uploading-min.png" },
        { name:"Delivered", value:"7", icon:"assets/New_Icons/delivered-min.png" },
        { name:"Removed/Expired", value:"8", icon:"assets/New_Icons/cancelled-min.png" },
        {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
        {name:"Passed QC",value:"10",icon:"assets/New_Icons/ready-min.png"},

    ],



    art : [
        { name:"Needs Artwork", value:"0", icon:"assets/New_Icons/art_needed-min.png" },
        { name:"In progress", value:"1", icon:"assets/New_Icons/in_progress-min.png" },
        { name:"Awaiting Approval", value:"2", icon:"assets/New_Icons/waiting_on_approval-min.png" },
        { name:"Needs QC", value:"3", icon:"assets/New_Icons/qc_needed-min.png" },
        { name:"Issue", value:"4", icon:"assets/New_Icons/issues-min.png" },
        { name:"Fail", value:"5", icon:"assets/New_Icons/rejected-min.png" },
        { name:"Uploading", value:"6", icon:"assets/New_Icons/uploading-min.png" },
        { name:"Delivered", value:"7", icon:"assets/New_Icons/delivered-min.png" },
        { name:"Removed/Expired", value:"8", icon:"assets/New_Icons/cancelled-min.png" },
        {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
        {name:"Passed QC",value:"10",icon:"assets/New_Icons/ready-min.png"},

    ],


    dub : [
        { name:"Needs Dubbing", value:"0", icon:"assets/New_Icons/dubs_needed-min.png" },
        { name:"In progress", value:"1", icon:"assets/New_Icons/in_progress-min.png" },
        { name:"Awaiting Approval", value:"2", icon:"assets/New_Icons/waiting_on_approval-min.png" },
        { name:"Needs QC", value:"3", icon:"assets/New_Icons/qc_needed-min.png" },
        { name:"Issue", value:"4", icon:"assets/New_Icons/issues-min.png" },
        { name:"Fail", value:"5", icon:"assets/New_Icons/rejected-min.png" },
        { name:"Uploading", value:"6", icon:"assets/New_Icons/uploading-min.png" },
        { name:"Delivered", value:"7", icon:"assets/New_Icons/delivered-min.png" },
        { name:"Removed/Expired", value:"8", icon:"assets/New_Icons/cancelled-min.png" },
        {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
        {name:"Passed QC",value:"10",icon:"assets/New_Icons/ready-min.png"},

    ],


    metadata : [
        { name:"Needs Metadata", value:"0", icon:"assets/New_Icons/metadata_needed-min.png"},
        { name:"In progress", value:"1", icon:"assets/New_Icons/in_progress-min.png"},
        { name:"Awaiting Approval", value:"2", icon:"assets/New_Icons/waiting_on_approval-min.png"},
        { name:"Needs QC", value:"3", icon:"assets/New_Icons/qc_needed-min.png"},
        { name:"Issue", value:"4", icon:"assets/New_Icons/issues-min.png"},
        { name:"Fail", value:"5", icon:"assets/New_Icons/rejected-min.png"},
        { name:"Uploading", value:"6", icon:"assets/New_Icons/uploading-min.png"},
        { name:"Delivered", value:"7", icon:"assets/New_Icons/delivered-min.png"},
        { name:"Removed/Expired", value:"8", icon:"assets/New_Icons/cancelled-min.png"},
        {name:"Not Applicable",value:"9",icon:"assets/New_Icons/not_applicable-min.png"},
        {name:"Passed QC",value:"10",icon:"assets/New_Icons/ready-min.png"},

    ],
}}


module.exports.outgoingStatuses = () =>  {
    return {
        scheduled : {name:"Scheduled",value:"0",icon:"assets/New_Icons/scheduled-min.png"},
        fail : {name:"Fail",value:"1",icon:"assets/New_Icons/rejected-min.png"},
        delivered : {name:"Delivered",value:"2",icon:"assets/New_Icons/delivered-min.png"}
    }

}

module.exports.pitchStatus = () => {
    return [{name:'Pitched',value:0},{name:'Rejected',value:1},{name:'Accepted',value:2},{name:'Under Review',value:3}]
}