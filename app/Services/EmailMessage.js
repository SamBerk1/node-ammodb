const Mail = use('Mail')
const Helpers = use('Helpers')
const Subscriber = use('App/Models/Subscriber')

formatMessage = (template,variables) => {
        let msg = template 
        msg = msg.replace('~user',variables.user)
        msg = msg.replace('~title',variables.title)
        msg = msg.replace('~createdAt',variables.createdAt)
        msg = msg.replace('~updatedAt',variables.updatedAt)
        msg = msg.replace('~statuses',variables.statuses)
        
        return msg
}

module.exports = async (email,data) => {
        try {
                let message = formatMessage(email.content,data)

                let subscribers = await Subscriber.query().where('emailId', email.id).fetch()

                subscribers = subscribers.toJSON()

                if (subscribers.length > 0) {
                        subscribers.map(async user => {
                                await Mail.send('emails.notification', {message:message,title:email.subject+"!"}, (message) => {
                                        message.to(user.email)
                                        message.embed(Helpers.publicPath('assets/logo/logo_inverted.png'), 'logo')
                                        .subject(email.subject)
                                })
                        })
                }
        } catch (error) {
                console.log(error)
                return
        }
}