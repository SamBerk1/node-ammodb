'use strict'
const Library = use("App/Models/Library");
const TitleStatus = use("App/Models/TitleStatus");
const Platform = use("App/Models/Platform");
const TitleStatusesSeason = use('App/Models/TitleStatusesSeason');
const TitlesTerritoriesRight = use('App/Models/TitlesTerritoriesRight');
const OutgoingTerritory = use('App/Models/OutgoingTerritory');
const Status = require('../../../Services/VedioStatus');
const Database = use('Database')
const load_ = require('lodash')
const OutgoingPolicy = require('../../../Policies/outgoingPolicy');
const exportFromJSON = require('export-from-json')
const moment = require("moment")


class OutgoingController {
  checkSelected(statusSelected, AllStatus) {
    for (let index = 0; index < AllStatus.length; index++) {
      if (AllStatus[index].value == statusSelected) {
        AllStatus[index].selected = true;
        return AllStatus[index]
      }
    }
  }


  assignOutgoingStatus(obj) {
    if (obj.platform_rightsId && obj.videoStatus.name == "Delivered" && obj.captions.name == "Delivered" && obj.subtitles.name == "Delivered" && obj.artwork.name == "Delivered" && obj.dubbing.name == "Delivered" && obj.metadata.name == "Delivered") {
      return Status.outgoingStatuses().delivered
    }
    else if (obj.platform_rightsId && obj.videoStatus.name == "Fail" || obj.captions.name == "Fail" || obj.subtitles.name == "Fail" || obj.artwork.name == "Fail" || obj.dubbing.name == "Fail" || obj.metadata.name == "Fail") {
      return Status.outgoingStatuses().fail
    }
    if (obj.platform_rightsId) {
      return Status.outgoingStatuses().scheduled
    }

  }

  async indexMovie({ request, auth, params, response, view }) {
    let searchColumns = [{ value: 'title', name: 'Title' }, { value: 'title', name: 'Alternate Title' }, { value: 'guid', name: 'GUID' }, { value: 'studioname', name: 'Studio' }, { value: 'platform', name: 'Platform' }, { value: 'type', name: 'Type' }]
    let statuses = Status.projectManagementStatuses()
    let platforms = await Platform.all()
    try {
      let user = await auth.getUser()
      let query = request.get()

      if (request.url().includes('export')) {
        let res = await OutgoingPolicy.getOutgoingMovies(user.toJSON(), query)
        let resSeason = await OutgoingPolicy.getOutgoingSeasons(user.toJSON(), query)
        res = res.map(item => {
          let obj = {
            title: item.alternateName.alternateName,
            guId: item.alternateName.guId,
            studio: item.library.studio.title,
            seasonName: "N/A",
            seasonNo: "N/A",
            parentTitle: item.library.title,
            movieORseries: item.library.is,
            platform: item.platform.name,
            dueDate: moment(item.dueDate).format("YYYY-MM-DD"),
            deliveryDate: moment(item.deliveryDate).format("YYYY-MM-DD"),
            launchDate: moment(item.launchDate).format("YYYY-MM-DD"),
            sunSetDate: moment(item.sunSetDate).format("YYYY-MM-DD"),
            videoStatus: item.videoStatus != null ? Status.projectManagementStatuses().vedioStatus.find(x => x.value == item.videoStatus).name : '',
            captions: item.captions != null ? Status.projectManagementStatuses().caps.find(x => x.value == item.captions).name : '',
            artwork: item.artwork != null ? Status.projectManagementStatuses().art.find(x => x.value == item.artwork).name : '',
            metadata: item.metadata != null ? Status.projectManagementStatuses().metadata.find(x => x.value == item.metadata).name : '',
            dubbing: item.dubbing != null ? Status.projectManagementStatuses().dub.find(x => x.value == item.dubbing).name : '',
            subtitles: item.subtitles != null ? Status.projectManagementStatuses().subs.find(x => x.value == item.subtitles).name : ''
          }

          obj.territories = item.territories.map(x => x.territory).join()

          return obj
        })

        resSeason.map(item => {
          let obj = {
            title: item.alternateName.alternateName,
            studio: item.alternateName.library.studio.title,
            seasonName: item.season.name,
            seasonNo: item.season.number,
            guId: item.season.guId,
            parentTitle: item.alternateName.library.title,
            movieORseries: item.series.is,
            platform: item.platform.name,
            dueDate: moment(item.dueDate).format("YYYY-MM-DD"),
            deliveryDate: moment(item.deliveryDate).format("YYYY-MM-DD"),
            launchDate: moment(item.launchDate).format("YYYY-MM-DD"),
            sunSetDate: moment(item.sunSetDate).format("YYYY-MM-DD"),
            videoStatus: item.videoStatus != null ? Status.projectManagementStatuses().vedioStatus.find(x => x.value == item.videoStatus).name : '',
            captions: item.captions != null ? Status.projectManagementStatuses().caps.find(x => x.value == item.captions).name : '',
            artwork: item.artwork != null ? Status.projectManagementStatuses().art.find(x => x.value == item.artwork).name : '',
            metadata: item.metadata != null ? Status.projectManagementStatuses().metadata.find(x => x.value == item.metadata).name : '',
            dubbing: item.dubbing != null ? Status.projectManagementStatuses().dub.find(x => x.value == item.dubbing).name : '',
            subtitles: item.subtitles != null ? Status.projectManagementStatuses().subs.find(x => x.value == item.subtitles).name : ''
          }
          obj.territories = item.territories.map(x => x.territory).join()
          res.push(obj)
        })

        try {
          const result = exportFromJSON({
            data: res,
            fileName: 'outgoing',
            exportType: 'csv',
            processor(content, type, fileName) {
              switch (type) {
                case 'txt':
                  response.header('Content-Type', 'text/plain')
                  break
                case 'json':
                  response.header('Content-Type', 'text/plain')
                  break
                case 'csv':
                  response.header('Content-Type', 'text/csv')
                  break
                case 'xls':
                  response.header('Content-Type', 'application/vnd.ms-excel')
                  break
              }
              response.header('Content-disposition', 'attachment;filename=' + fileName)
              return content
            }
          })
          return response.send(result)
        } catch (error) {
          // console.log(error.message)
          return response.redirect('back')
        }
      }

      let library = await OutgoingPolicy.getOutgoingMovies(user.toJSON(), query)
      let seasons = await OutgoingPolicy.getOutgoingSeasons(user.toJSON(), query)

      let libraryData = load_.groupBy(library, title => title.libraryId)

      let seasonsData = load_.groupBy(seasons, season => season.seriesId)

      for (const item in seasonsData) {
        seasonsData[item] = load_.groupBy(seasonsData[item], altenateTitle => altenateTitle.alternateTitleId)
      }

      let data = []
      for (const item in seasonsData) {
        let obj = seasonsData[item]
        let seasons = []
        let territories = await TitleStatusesSeason.query().select('id', 'seriesId', 'platformId').whereIn('outgoing', [1]).with('platform', (builder) => builder.select('id', 'name')).where('seriesId', item).with('territories', (builder) => builder.select('id', 'outgoingSeasonId', 'territory')).fetch()
        let customObj = []
        for (const key in obj) {
          seasons.push(obj[key])
        }
        for (const key in seasons) {
          let alternateName = JSON.parse(JSON.stringify(seasons[key][0].alternateName))
          alternateName['seasons'] = seasons[key]
          customObj.push(alternateName)
        }
        data.push({ "title": await Promise.all(customObj), "territories": territories.toJSON() })
      }


      for (let key in libraryData) {
        let territories = await TitleStatus.query().select('id', 'libraryId', 'platformId').whereIn('outgoing', [1]).with('platform', (builder) => builder.select('id', 'name')).where('libraryId', libraryData[key][0].libraryId).with('territories', (builder) => builder.select('id', 'outgoingMovieId', 'territory')).fetch()
        data.push({ "title": libraryData[key], "territories": territories.toJSON() })
      }

      let outgoing = {}
      outgoing.total = data.length
      outgoing.page = query.page ? parseInt(query.page) : 1
      outgoing.perPage = query.perpage ? parseInt(query.perpage) : 10
      outgoing.lastPage = Math.ceil(data.length / outgoing.perPage)
      outgoing.data = outgoing.page == 1 ? data.splice(0, outgoing.perPage) : data.splice(outgoing.perPage * outgoing.page - outgoing.perPage, outgoing.perPage * outgoing.page - 1)

      let paramsObj = Object.fromEntries(new URLSearchParams(request.get()))

      paramsObj.search && delete paramsObj.search
      paramsObj.searchcolumn && delete paramsObj.searchcolumn
      paramsObj.orderby && delete paramsObj.orderby
      paramsObj.order && delete paramsObj.order
      paramsObj.page && delete paramsObj.page
      paramsObj.perpage && delete paramsObj.perpage

      let filterQuery = []

      for (let [key, value] of Object.entries(paramsObj)) {
        filterQuery.push(`${key}=${value}`)
      }

      return view.render('outgoing.outgoing', {
        title: 'Latest Outgoing',
        outgoing: outgoing,
        url: '/outgoing/movies',
        subTitle: 'Movies / Series',
        searchColumns: searchColumns,
        search: !query.filter && Object.keys(query)[0] != 'page' ? { text: query[Object.keys(query)[0]], column: Object.keys(query)[0] } : undefined,
        filter: query.filter ? '&' + filterQuery.join('&') : undefined,
        order: { column: query.orderby, order: query.order },
        statuses: statuses,
        platforms: platforms.toJSON()
      })
    } catch (error) {
      console.log(error)
      return response.redirect('back')
    }
  }


  async indexSeason({ request, auth, response, view }) {
    let searchColumns = [{ value: 'title', name: 'Title' }, { value: 'guId', name: 'GUID' }, { value: 'studioname', name: 'Studio' }]
    let statuses = Status.projectManagementStatuses()
    let platforms = await Platform.all()
    try {
      let user = await auth.getUser()
      let query = request.get()

      let res = await OutgoingPolicy.getOutgoingSeasons(user.toJSON(), query)

      if (request.url().includes('export')) {

        res = res.map(item => {
          let obj = {
            title: item.alternateName.alternateName,
            studio: item.alternateName.library.studio.title,
            seasonName: item.season.name,
            seasonNo: item.season.number,
            guId: item.season.guId,
            parentTitle: item.alternateName.library.title,
            movieORseries: item.series.is,
            platform: item.platform.name,
            dueDate: item.dueDate,
            deliveryDate: item.deliveryDate,
            launchDate: item.launchDate,
            sunSetDate: item.sunSetDate,
            videoStatus: item.videoStatus != null ? Status.projectManagementStatuses().vedioStatus.find(x => x.value == item.videoStatus).name : '',
            captions: item.captions != null ? Status.projectManagementStatuses().caps.find(x => x.value == item.captions).name : '',
            artwork: item.artwork != null ? Status.projectManagementStatuses().art.find(x => x.value == item.artwork).name : '',
            metadata: item.metadata != null ? Status.projectManagementStatuses().metadata.find(x => x.value == item.metadata).name : '',
            dubbing: item.dubbing != null ? Status.projectManagementStatuses().dub.find(x => x.value == item.dubbing).name : '',
            subtitles: item.subtitles != null ? Status.projectManagementStatuses().subs.find(x => x.value == item.subtitles).name : ''
          }
          obj.territories = item.territories.map(x => x.territory).join()
          return obj
        })
        try {
          const result = exportFromJSON({
            data: res,
            fileName: 'outgoing',
            exportType: 'csv',
            processor(content, type, fileName) {
              switch (type) {
                case 'txt':
                  response.header('Content-Type', 'text/plain')
                  break
                case 'json':
                  response.header('Content-Type', 'text/plain')
                  break
                case 'csv':
                  response.header('Content-Type', 'text/csv')
                  break
                case 'xls':
                  response.header('Content-Type', 'application/vnd.ms-excel')
                  break
              }
              response.header('Content-disposition', 'attachment;filename=' + fileName)
              return content
            }
          })
          return response.send(result)
        } catch (error) {
          console.log(error)
          return response.redirect('back')
        }
      }

      let seasons = res

      let seasonsData = load_.groupBy(seasons, season => season.seriesId)

      seasonsData = Object.keys(seasonsData).reduce((a, b) => {
        a[seasonsData[b][0].alternateName.library.title] = seasonsData[b];
        return a;
      }, {});

      for (const item in seasonsData) {
        seasonsData[item] = load_.groupBy(seasonsData[item], altenateTitle => altenateTitle.alternateTitleId)
      }

      let data = []

      let keys = Object.keys(seasonsData)

      if (query.order == 'asc') {
        keys.sort()
      }
      else if (query.order == 'desc') {
        keys.sort().reverse()
      }

      await Promise.all(
        keys.map(async item => {

          let obj = seasonsData[item]
          let seasons = []
          let customObj = []
          for (const key in obj) {
            seasons.push(obj[key])
          }
          for (const key in seasons) {
            let alternateName = JSON.parse(JSON.stringify(seasons[key][0].alternateName))
            alternateName['seasons'] = seasons[key]
            customObj.push(alternateName)
          }
          data.push(await Promise.all(customObj))
        })
      )

      if (data.length > 0) {
        data = await Promise.all(
          data.map(async item => {
            let territories = await TitleStatusesSeason.query().where('outgoing', 1).select('id', 'seriesId', 'platformId').with('platform', (builder) => builder.select('id', 'name')).where('seriesId', item[0].libraryId).with('territories', (builder) => builder.select('id', 'outgoingSeasonId', 'territory')).fetch()
            item.territories = territories.toJSON()
            return item
          })
        )
      }


      let outgoing = {}
      outgoing.total = data.length
      outgoing.page = query.page ? parseInt(query.page) : 1
      outgoing.perPage = query.perpage ? parseInt(query.perpage) : 10
      outgoing.lastPage = Math.ceil(data.length / outgoing.perPage)
      outgoing.data = outgoing.page == 1 ? data.splice(0, outgoing.perPage) : data.splice(outgoing.perPage * outgoing.page - outgoing.perPage, outgoing.perPage * outgoing.page - 1)

      let paramsObj = Object.fromEntries(new URLSearchParams(request.get()))

      paramsObj.search && delete paramsObj.search
      paramsObj.searchcolumn && delete paramsObj.searchcolumn
      paramsObj.orderby && delete paramsObj.orderby
      paramsObj.order && delete paramsObj.order
      paramsObj.page && delete paramsObj.page
      paramsObj.perpage && delete paramsObj.perpage

      let filterQuery = []

      for (let [key, value] of Object.entries(paramsObj)) {
        filterQuery.push(`${key}=${value}`)
      }

      return view.render('outgoing.outgoing', {
        title: 'Latest Outgoing',
        outgoing: outgoing,
        url: '/outgoing/seasons',
        subTitle: 'Seasons',
        searchColumns: searchColumns,
        search: !query.filter && Object.keys(query)[0] != 'page' ? { text: query[Object.keys(query)[0]], column: Object.keys(query)[0] } : undefined,
        filter: query.filter ? '&' + filterQuery.join('&') : undefined,
        statuses: statuses,
        platforms: platforms.toJSON()
      })
    } catch (error) {
      console.log(error)
      return response.redirect('back')
    }

  }

  async details({ request, response, params, view }) {
    try {
      let data = await TitleStatus.query().where("id", params.id).where("outgoing", 1).with('alternateName').with('library').with('platformRights').first();
      data = data.toJSON()
      if (data) {
        data['videoStatus'] = data['videoStatus'] != null ? this.checkSelected(data['videoStatus'], Status.projectManagementStatuses().vedioStatus) : { name: '' }
        data['captions'] = data['captions'] != null ? this.checkSelected(data['captions'], Status.projectManagementStatuses().caps) : { name: '' }
        data['subtitles'] = data['subtitles'] != null ? this.checkSelected(data['subtitles'], Status.projectManagementStatuses().subs) : { name: '' }
        data['artwork'] = data['artwork'] != null ? this.checkSelected(data['artwork'], Status.projectManagementStatuses().art) : { name: '' }
        data['dubbing'] = data['dubbing'] != null ? this.checkSelected(data['dubbing'], Status.projectManagementStatuses().dub) : { name: '' }
        data['metadata'] = data['metadata'] != null ? this.checkSelected(data['metadata'], Status.projectManagementStatuses().metadata) : { name: '' }

        data['status'] = this.assignOutgoingStatus(data)
      }
      return view.render("outgoing.outgoingdetail",
        { data: data }
      );
    } catch (error) {
      response.redirect('back')
    }
  }

  async detailsSeason({ request, response, params, view }) {
    try {
      let data = await TitleStatusesSeason.query().where("id", params.id).where("outgoing", 1).with('season').with('alternateName').with('platformRights').first();
      data = data.toJSON()
      if (data) {
        data['videoStatus'] = data['videoStatus'] != null ? this.checkSelected(data['videoStatus'], Status.projectManagementStatuses().vedioStatus) : { name: '' }
        data['captions'] = data['captions'] != null ? this.checkSelected(data['captions'], Status.projectManagementStatuses().caps) : { name: '' }
        data['subtitles'] = data['subtitles'] != null ? this.checkSelected(data['subtitles'], Status.projectManagementStatuses().subs) : { name: '' }
        data['artwork'] = data['artwork'] != null ? this.checkSelected(data['artwork'], Status.projectManagementStatuses().art) : { name: '' }
        data['dubbing'] = data['dubbing'] != null ? this.checkSelected(data['dubbing'], Status.projectManagementStatuses().dub) : { name: '' }
        data['metadata'] = data['metadata'] != null ? this.checkSelected(data['metadata'], Status.projectManagementStatuses().metadata) : { name: '' }

        data['status'] = this.assignOutgoingStatus(data)
      }
      return view.render("outgoing.outgoingdetail",
        { data: data }
      );
    } catch (error) {
      response.redirect('back')
    }
  }

  async getTerritories({ request, view, response, auth, params }) {
    try {
      let outgoingTerritory = await OutgoingTerritory.query().where('outgoingMovieId', params.id).fetch()
      return response.json({ data: outgoingTerritory, flag: true })
    } catch (error) {
      return response.json({ data: null, flag: false })
    }
  }

  async getSeasonTerritories({ request, view, response, auth, params }) {
    try {
      let outgoingTerritory = await OutgoingTerritory.query().where('outgoingSeasonId', params.id).fetch()
      return response.json({ data: outgoingTerritory, flag: true })
    } catch (error) {
      return response.json({ data: null, flag: false })
    }
  }

  async updateTerritories({ request, session, response, auth, params }) {
    try {
      let territories = request.input('territories') ? request.input('territories') : []
      if (params.is.toLowerCase() == 'season') {
        await OutgoingTerritory.query().where('outgoingSeasonId', params.id).delete()
        let data = []
        if (territories.length > 0) {
          territories.map(item => {
            data.push({ outgoingSeasonId: params.id, territory: item })
          })
          await OutgoingTerritory.createMany(data)
        }

      }
      else {
        await OutgoingTerritory.query().where('outgoingMovieId', params.id).delete()
        let data = []
        if (territories.length > 0) {
          territories.map(item => {
            data.push({ outgoingMovieId: params.id, territory: item })
          })
          await OutgoingTerritory.createMany(data)
        }
      }
      session.flash({ notification: 'Territories Updated Successfully!' })
      response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: "Duplicate Entry For territories!" })
      response.redirect('back')
    }
  }
}

module.exports = OutgoingController
