'use strict'
const Role = use('App/Models/Role')
const Resource = use('Cerberus/Models/Resource')
const Permission = use('Cerberus/Models/Permission')
const Database = use('Database')


class RoleController {

    /**
     * Store a newly created resource in storage.
     */
    async store({request,session,response}) {
        try {
            const trx = await Database.beginTransaction()

            const data = request.only(['name','slug'])
            data.isStudio = request.input('isStudio') == 'on' ? 1 : 0
            const role = await Role.create(data,trx)

            let dummyPermissions = [
                {
                  "resource_id": (await Resource.findBy({slug:'financial'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'library'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'incoming'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'outgoing'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'projectManagement'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'customers'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'roles'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'permissions'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'users'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'activityLog'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                },
                {
                  "resource_id": (await Resource.findBy({slug:'others'})).id,
                  "role_id": role.id,
                  "create": "0",
                  "read": "0",
                  "update": "0",
                  "delete": "0"
                }
              ]

            await Permission.createMany(dummyPermissions,trx)

            await trx.commit()
            session.flash({ notification: 'Role Created Successfully.' })

            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: 'Role Creation Failed.' })
            return response.redirect('back')
        }
    }

    /**
     * Display a listing of the resource.
     */
    async index({request,view,response}) {
        try {
            const roles = await Role.all()
            return view.render('settings.roles.roles',{data:roles.toJSON()})
        } catch (error) {
            return response.redirect('back')
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    async remove({request,session, params,response}) {
        try {
            const role = await Role.find(params.id)
            await role.permissions().delete()
            await role.delete()
            session.flash({ notification: 'Role Deleted Successfully.' })
            return response.redirect('back')
        } catch (error) {
          session.flash({ errMsg: 'Role Delete Failed.' })
          return response.redirect('back')
        }
    }

    
    /**
     * Update the specified resource in storage.
     */
    async update({request,params ,response}) {

    }
    
    
}

module.exports = RoleController
