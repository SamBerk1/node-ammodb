'use strict'

const User = use('App/Models/User')
const Role = use('App/Models/Role')
const Studio = use('App/Models/Studio')
const Database = use('Database')
const StudioCustomer = use('App/Models/StudioCustomer')

class UserController {
    /**
     * Display a listing of the resource.
     */
    async index({request,view,response}) {
        try {
            const users = await User.query().with('role').fetch()
            const studios = await Studio.query().fetch()
            const roles = await Role.query().fetch()
            return view.render('settings.users.users',{data:users.toJSON(),studios:studios.toJSON(),roles:roles.toJSON()})
        } catch (error) {
            return response.redirect('back')
        }
    }


    
    /**
     * Show the form for editing the specified resource.
    */
    async edit({ params, view }) {
        const user = await User.query().with('role').with('studio').where('id',params.id).first()
        const roles = await Role.all()
        const studios = await Studio.all()
        return view.render('settings.users.edit', {user:user.toJSON(),roles:roles.toJSON(),studios:studios.toJSON()})
    }
    

    /**
     * Store a newly created resource in storage.
     */
    async store({request, session,response}) {
        let trx = await Database.beginTransaction()
        try {
            let data = request.all()
            let user = await User.create({fullname:data.fullname,role_id:data.role_id,email:data.email,password:data.password},trx)
            data.studioId && await StudioCustomer.create({userId:user.id,studioId:data.studioId},trx)
            await trx.commit()
            session.flash({ notification: 'User Created Successfully' })
            response.redirect('back')
        } catch (error) {
            await trx.rollback()
            session.flash({ errMsg: 'User Creation Failed' })
            response.redirect('back')
        }
    }

    
    /**
     * Update the specified resource in storage.
     */
    async update({request,session,params,response}) {
        try {
            const data = request.only(['fullname','email','role_id','password'])
            const user = await User.find(params.id)

            user.merge(data)

            await user.save()
            if (request.input('studioId')) {
                const userStudio = await StudioCustomer.findBy('userId',user.id)
                userStudio.studioId = request.input('studioId')
                await userStudio.save()
            }

            session.flash({ notification: 'User Updated Successfully' })
            response.redirect('back')

        } catch (error) {
            session.flash({ errMsg: 'User Updated Failed' })
            response.redirect('back')
        }
    }
    

    
    /**
     * Remove the specified resource from storage.
     */
    async remove({session,params,response}) {
        try {
            const user = await User.find(params.id)
            await user.delete()
            session.flash({ notification: 'User Deleted Successfully' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: 'User Delete Failed' })
            return response.redirect('back')
        }
    }

    async updatePassword({ request, response, params, session }){
        try {
            const user = await User.find(params.id)
            user.password = request.input('password')
            user.isNew = 1
            await user.save()
            session.flash({ notification: `"${user.email}"'s Password Updated Successfully` })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: `"${user.email}"'s Password Update Failed` })
            return response.redirect('back')
        }
    }

    async searchUser({ request, response }){
        try {
            let users = await User.query().where('fullname','LIKE',`%${request.get().search}%` ).orWhere('email','LIKE',`%${request.get().search}%` ).fetch()
            return response.json({flag:true,users:users})
        } catch (error) {
            console.log(error)
            return response.json({flag:false,users:[]})
        }
    }
}

module.exports = UserController
