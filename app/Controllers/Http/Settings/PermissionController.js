'use strict'
const Permission = use('Cerberus/Models/Permission')

class PermissionController {
    /**
     * Display a listing of the resource.
     */
    async index({request,view,response,params}) {
        try {
            const permissions = await Permission.query().where('role_id', params.roleId).with('role').with('resource').fetch()
            return view.render('settings.permissions.permissions',{data:permissions.toJSON(),role:permissions.toJSON()[0].role})
        } catch (error) {
            return response.redirect('back')
        }
    }

     
    /**
     * Update the specified resource in storage.
     */
    async update({request,view,params,session,response}) {
        let data = request.only(['role_id','resource_id','create','delete','read','update'])
        let permissionId = params.id

        data.hasOwnProperty('create') ? data.create = 1 : data.create = 0    
        data.hasOwnProperty('delete') ? data.delete = 1 : data.delete = 0
        data.hasOwnProperty('read') ? data.read = 1 : data.read = 0
        data.hasOwnProperty('update') ? data.update = 1 : data.update = 0
        try {
            const permission = await Permission.find(permissionId)
            permission.merge(data)
            await permission.save()
            session.flash({ notification: 'Permission Update Successfully.' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: 'Permission Update Failed.' })
            return response.redirect('back')
        }
    }
    
}

module.exports = PermissionController
