"use strict";

const AdBreak = use("App/Models/AdBreak");
const Award = use("App/Models/Award");
const Cast = use("App/Models/Cast");
const Delivery = use("App/Models/Delivery");
const Director = use("App/Models/Director");
const Format = use("App/Models/Format");
const Genre = use("App/Models/Genre");
const Holdback = use("App/Models/Holdback");
const Keyword = use("App/Models/Keyword");
const Rating = use("App/Models/Rating");
const Producer = use("App/Models/Producer");
const Right = use("App/Models/Right");
const Screener = use("App/Models/Screener");
const Studio = use("App/Models/Studio");
const Territory = use("App/Models/Territory");
const Trailer = use("App/Models/Trailer");
const Languages = use("App/Models/Language");
const Countries = use("App/Models/Country")

const Library = use("App/Models/Library");
const Incoming = use("App/Models/Incoming");

const TitlesGenre = use('App/Models/TitlesGenre');
const TitlesAward = use("App/Models/TitlesAward");
const TitlesCast = use("App/Models/TitlesCast");
const TitlesProducer = use("App/Models/TitlesProducer");
const TitlesDirector = use("App/Models/TitlesDirector");
const TitlesCountry = use("App/Models/TitlesCountry");
const TitlesScreener = use("App/Models/TitlesScreener");
const TitlesTrailer = use("App/Models/TitlesTrailer");
const TitlesTerritoriesRight = use("App/Models/TitlesTerritoriesRight");
const AlternateTitleLanguage = use("App/Models/AlternateTitleLanguage");
const TerritoryCountry = use('App/Models/TerritoryCountry');
const AlternateTitlesPlatform = use('App/Models/AlternateTitlesPlatform');

const TitleStatus = use("App/Models/TitleStatus");
const TitleStatusesSeason = use('App/Models/TitleStatusesSeason');

const Season = use("App/Models/Season");
const Episode = use('App/Models/Episode');
const IncomingEpisode = use('App/Models/IncomingEpisode');
const IncomingSeason = use('App/Models/IncomingSeason');
const Image = use('App/Models/Image');
const ExportToken = use('App/Models/ExportToken');
const Email = use('App/Models/Email');


const Database = use("Database");

const LibraryPolicy = require('../../../Policies/libraryPolicy');
const uuid = require('uuid-random');
const csv = require('csv-parser');
const fs = require('fs');
const path = require('path');
const fsPromise = require('fs').promises;
const Helpers = use('Helpers');

const readFile = Helpers.promisify(fs.readFile)

const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminWebp = require('imagemin-webp');
const sizeOf = require('image-size');

const Env = use('Env')

const DynamicOption = require('../../../Services/DynamicOption');
const libraryValidator = require('../../../validators/libraryValidator.js')

const exportFromJSON = require('export-from-json')

const Status = require('../../../Services/VedioStatus');


const sendMail = require('../../../Services/EmailMessage.js');
const { conforms } = require('lodash');


const parseCSV = (file) => {
  let results = []
  return new Promise(resolve => {
    fs.createReadStream('tmp/uploads/' + file)
      .pipe(csv())
      .on('data', (data) => results.push(data))
      .on('end', () => {
        resolve(results)
      })
  })
}

const UpdateStatusIncomingEpisode = async (incomingSeasonId) => {

  let incomingEpiosodes = await IncomingEpisode.query().select('incomingSeasonId', 'captions', 'videoStatus', 'subtitles', 'artwork', 'dubbing', 'metadata').where('incomingSeasonId', incomingSeasonId).fetch()

  let statuses = { // 2 for QC passed.

    captions: incomingEpiosodes.toJSON().filter(item => {
      if (item.captions != 2 && item.captions != 9) {
        return item
      }
    }).length,

    videoStatus: incomingEpiosodes.toJSON().filter(item => {
      if (item.videoStatus != 2 && item.videoStatus != 1) {
        return item
      }
    }).length,

    subtitles: incomingEpiosodes.toJSON().filter(item => {
      if (item.subtitles != 2 && item.subtitles != 9) {
        return item
      }
    }).length,

    artwork: incomingEpiosodes.toJSON().filter(item => {
      if (item.artwork != 2 && item.artwork != 1) {
        return item
      }
    }).length,

    dubbing: incomingEpiosodes.toJSON().filter(item => {
      if (item.dubbing != 2 && item.dubbing != 9) {
        return item
      }
    }).length,

    metadata: incomingEpiosodes.toJSON().filter(item => {
      if (item.metadata != 2 && item.metadata != 1) {
        return item
      }
    }).length
  }

  let incomingSeason = await IncomingSeason.find(incomingSeasonId)
  incomingSeason.captions = statuses.captions == 0 ? 2 : 4

  incomingSeason.videoStatus = statuses.videoStatus == 0 ? 2 : 4

  incomingSeason.subtitles = statuses.subtitles == 0 ? 2 : 4

  incomingSeason.artwork = statuses.artwork == 0 ? 2 : 4

  incomingSeason.dubbing = statuses.dubbing == 0 ? 2 : 4

  incomingSeason.metadata = statuses.metadata == 0 ? 2 : 4

  await incomingSeason.save()

  let incomingSeasons = await IncomingSeason.query().select('incomingId', 'captions', 'videoStatus', 'subtitles', 'artwork', 'dubbing', 'metadata').where('incomingId', incomingSeason.incomingId).fetch()

  let seasonStatuses = { // 2 for QC passed.

    captions: incomingSeasons.toJSON().filter(item => {
      if (item.captions != 2 && item.captions != null) {
        return item
      }
    }).length,

    videoStatus: incomingSeasons.toJSON().filter(item => {
      if (item.videoStatus != 2 && item.videoStatus != null) {
        return item
      }
    }).length,

    subtitles: incomingSeasons.toJSON().filter(item => {
      if (item.subtitles != 2 && item.subtitles != null) {
        return item
      }
    }).length,

    artwork: incomingSeasons.toJSON().filter(item => {
      if (item.artwork != 2 && item.artwork != null) {
        return item
      }
    }).length,

    dubbing: incomingSeasons.toJSON().filter(item => {
      if (item.dubbing != 2 && item.dubbing != null) {
        return item
      }
    }).length,

    metadata: incomingSeasons.toJSON().filter(item => {
      if (item.metadata != 2 && item.metadata != null) {
        return item
      }
    }).length
  }


  let incoming = await Incoming.find(incomingSeason.incomingId)
  incoming.captions = seasonStatuses.captions == 0 ? 2 : 4

  incoming.videoStatus = seasonStatuses.videoStatus == 0 ? 2 : 4

  incoming.subtitles = seasonStatuses.subtitles == 0 ? 2 : 4

  incoming.artwork = seasonStatuses.artwork == 0 ? 2 : 4

  incoming.dubbing = seasonStatuses.dubbing == 0 ? 2 : 4

  incoming.metadata = seasonStatuses.metadata == 0 ? 2 : 4
  await incoming.save()

  return new Promise((resolve) => {
    resolve(incomingSeason)
  })
}

class LibraryController {
  /**
   * Store a newly created resource in storage.
   */
  async store({ request, auth, session, response }) {
    
    let validation = await libraryValidator.movie(request.all())
    if (validation.fails()) {
      session.withErrors(validation.messages()).flashAll()
      return response.redirect('back')
    }
    const trx = await Database.beginTransaction();

    try {
      let libraryData = request.only([
        "title",
        "releaseDate",
        "is",
        "runtime",
        "duration",
        "synopsis",
        "alterSynopsis",
        "frameRate",
        "imdb",
        "year"
      ]);


      libraryData.releaseDate = (request.input("releaseDate") != '' ? request.input("releaseDate") : null);
      libraryData.userId = (await auth.getUser()).id

      libraryData["studioId"] = isNaN(request.input("studioId")) ? request.input("studioId") != 'null' ? (await DynamicOption.studio(request.input("studioId"), trx)) : null : request.input("studioId")
      libraryData["ratingId"] = isNaN(request.input("ratingId")) ? request.input("ratingId") != 'null' ? (await DynamicOption.rating(request.input("ratingId"), trx)) : null : request.input("ratingId")
      libraryData["formatId"] = isNaN(request.input("formatId")) ? request.input("formatId") != 'null' ? (await DynamicOption.format(request.input("formatId"), trx)) : null : request.input("formatId")
      libraryData["screenerId"] = isNaN(request.input("screenerId")) ? null : request.input("screenerId")
      libraryData["trailerId"] = isNaN(request.input("trailerId")) ? null : request.input("trailerId")
      libraryData["originalLanguageId"] = isNaN(request.input("originalLanguageId")) ? null : request.input("originalLanguageId")


      if (request.input('screener').url) {
        libraryData["screenerId"] = (await DynamicOption.screener(request.input('screener'), trx))
      }

      if (request.input('trailer').url) {
        libraryData["trailerId"] = (await DynamicOption.trailer(request.input('trailer'), trx))
      }

      libraryData['guId'] = request.input('guId')

      const library = await Library.create(libraryData, trx);


      let parentTitle = await AlternateTitleLanguage.create({ alternateName: libraryData.title, guId: libraryData.guId + '-' + (await Languages.find(parseInt(request.input('language')))).code + '-' + Date.now(), libraryId: library.id, languageId: request.input('language'), isParent: 1, alterSynopsis: library.synopsis, studioId: library.studioId, imdb: library.imdb, ratingId: library.ratingId }, trx)

      await Incoming.create({ libraryId: library.id, alternateTitleId: parentTitle.id, userId: library.userId, studioId: library.studioId }, trx)

      if (request.input('alternateNames')) {
        if (request.input('alternateNames').length > 0) {
          let alternateNames = request.input('alternateNames')
          let alternateNamesData = []
          await Promise.all(alternateNames.map(async val => {
            alternateNamesData.push({ alternateName: (val.split('/~/'))[0], guId: libraryData.guId + '-' + (val.split('/~/'))[1] + '-' + Date.now(), libraryId: library.id, languageId: (await Languages.findBy({ code: (val.split('/~/'))[1] })).id, isParent: 0, alterSynopsis: (val.split('/~/'))[2], studioId: library.studioId, imdb: library.imdb, ratingId: library.ratingId })
          }))
          alternateNamesData = await AlternateTitleLanguage.createMany(alternateNamesData, trx)

          let incomingData = []
          await Promise.all(alternateNamesData.map(async val => {
            incomingData.push({ libraryId: library.id, alternateTitleId: val.id, userId: library.userId, studioId: library.studioId })
          }))

          await Incoming.createMany(incomingData, trx)
        }
      }



      if (request.input("genres")) {
        let genres = request.input("genres");
        const genreData = [];
        await Promise.all(genres.map(async val => {
          if (!isNaN(val)) {
            genreData.push({ libraryId: library.id, genreId: val });
          }
          else {
            genreData.push({ libraryId: library.id, genreId: (await DynamicOption.genre(val, trx)) });
          }
        }));
        await TitlesGenre.createMany(genreData, trx)
      }

      if (request.input("countries")) {
        let countries = request.input("countries");
        const countriesData = [];
        await Promise.all(countries.map(async val => {
          countriesData.push({ libraryId: library.id, countryId: val });
        }));
        await TitlesCountry.createMany(countriesData, trx)
      }

      if (request.input("producers")) {
        let producers = request.input("producers");
        const producersData = [];
        await Promise.all(producers.map(async val => {
          if (!isNaN(val)) {
            producersData.push({ libraryId: library.id, producerId: val });
          }
          else {
            producersData.push({ libraryId: library.id, producerId: (await DynamicOption.producer(val, trx)) });
          }
        }));
        await TitlesProducer.createMany(producersData, trx)
      }

      if (request.input("directors")) {
        let directors = request.input("directors");
        const directorsData = [];
        await Promise.all(directors.map(async val => {
          if (!isNaN(val)) {
            directorsData.push({ libraryId: library.id, directorId: val });
          }
          else {
            directorsData.push({ libraryId: library.id, directorId: (await DynamicOption.director(val, trx)) });
          }
        }));
        await TitlesDirector.createMany(directorsData, trx)
      }

      if (request.input("cast")) {
        let cast = request.input("cast");
        const castData = [];
        await Promise.all(cast.map(async val => {
          if (!isNaN(val)) {
            castData.push({ libraryId: library.id, castId: val });
          }
          else {
            castData.push({ libraryId: library.id, castId: (await DynamicOption.cast(val, trx)) });
          }
        }));
        await TitlesCast.createMany(castData, trx);
      }

      if (request.input("awards")) {
        let awards = request.input("awards");
        const awardsData = [];
        await Promise.all(awards.map(async val => {
          if (!isNaN(val)) {
            awardsData.push({ libraryId: library.id, awardId: val });
          }
          else {
            awardsData.push({ libraryId: library.id, awardId: (await DynamicOption.award(val, trx)) });
          }
        }));
        await TitlesAward.createMany(awardsData, trx);
      }

      await trx.commit();

      let email = await Email.findBy('title', 'LibraryNotification')

      sendMail(email, { user: (await auth.getUser()).fullname, title: library.title, createdAt: library.created_at })

      let user = await auth.getUser()
      if (user.toJSON().role.isStudio == 1) {
        session.flash({ notification: "Title Saved" });
        return response.redirect('/library');
      } else {
        return response.redirect("/library/rights/manage/" + library.id);
      }

    } catch (error) {
      await trx.rollback();
      session.flash({ libraryErr: "Title not Saved Duplicate Entry" });
      session.flashAll()
      return response.redirect("back");
    }
  }

  async index({ request, view, auth, response }) {
    

    // Settings columns for search in library index page
    let searchColumns = [{ value: 'alternateNames_alternateName', name: 'Title' }, { value: 'alternateNames_alternateName', name: 'Alternate Title' }, { value: 'guId', name: 'GUID' }, { value: 'keyword_keyword', name: 'Keyword' }, { value: 'studio_title', name: 'Studio' }, { value: 'genres_title', name: 'Genre' }, { value: 'format_format', name: 'Format' }, { value: 'cast_actorName', name: 'Cast' }, { value: 'year', name: 'Year' }]
    
    try {
      // Getting all params from req query as {}
      let query = request.get()
      let user = await auth.getUser()
      
      // Handling export request at index()
      if (request.url().includes('export')) {

        // Getting all params from req as {}
        let columnsExported = request.all()
        try {
          // Creating new uuid
          let token = uuid()
          let batch = Date.now()
          ExportToken.create({ batch: batch, token: token })

          // Fetching all data from db -> library
          let libraries = await LibraryPolicy.exportLibrary(user.toJSON(), query, columnsExported)
          // console.log(libraries)
          // Reducing fetched data to required export state
          libraries = await Promise.all(libraries.map(async (item) => {
            let obj = {}

            let releaseDate = new Date(item.releaseDate)


            obj.title = item.title
            obj.MovieOrSeries = item.is
            obj.guId = item.guId
            obj.releaseDate = releaseDate.getFullYear() + '-' + (releaseDate.getMonth() + 1) + '-' + releaseDate.getDate()
            obj.year = item.year
            // Exporting ratLibraryControllerings if given in query and fetched
            item.ratings && (obj.ratings = item.ratings.name)
            obj.runtime = item.runtime
            obj.originalLanguage = item.originalLanguage ? `${item.originalLanguage.name}(${item.originalLanguage.code})` : ''

            columnsExported.export_imdb && (obj.imdb = item.imdb)
            // Exporting synopsis only if requested in req params
            columnsExported.export_synopsis && (obj.synopsis = item.synopsis)
            item.screener && (obj.screenerLink = item.screener.screenerLink)
            item.screener && (obj.screenerPW = item.screener.screenerPW)
            item.trailer && (obj.trailerUrl = item.trailer.trailerUrl)
            item.keyword && (obj.keywords = item.keyword.map(x => x.keyword).join())

            item.pitchTracker && (obj.pitchTracker = item.pitchTracker.map(x => {
              let pitchDate = new Date(x.pitchDate)

              return `${x.platform}(${pitchDate.getFullYear()}-${pitchDate.getMonth() + 1}-${pitchDate.getDate()},${x.status != null ? Status.pitchStatus().find(i => i.value == x.status).name : ''})`
            }).join())

            item.holdbacks && (obj.holdbacks = item.holdbacks.map(x => {
              let start_date = new Date(x.startDate)
              let end_date = new Date(x.endDate)
              return `(${x.name},${start_date.getFullYear()}-${start_date.getMonth() + 1}-${start_date.getDate()} to ${end_date.getFullYear()}-${end_date.getMonth() + 1}-${end_date.getDate()})`
            }).join())

            item.alternateNames && (obj.alternateName = item.alternateNames.map(x => {
              return `${x.alternateName} (${x.guId},${x.language ? x.language.name : 'Language : N/A'})`
            }).join())
            item.studio && (obj.studio = item.studio.title)
            item.format && (obj.format = item.format.format)
            item.genres && (obj.genre = item.genres.map(x => x.title).join())
            item.cast && (obj.cast = item.cast.map(x => x.actorName).join())
            item.directors && (obj.director = item.directors.map(x => x.name).join())
            item.producers && (obj.producer = item.producers.map(x => x.name).join())
            item.rightsName && (obj.rights = item.rightsName.map(x => x.name).join())
            item.territories && (obj.territories = item.territories.map(x => x.territoryName).join())
            item.languages && (obj.languages = item.languages.map(x => x.name).join())
            item.images && (obj.images = item.images.map(x => {
              return `${Env.get('BASE_URL')}/${x.compressed}?export=${batch}&token=${token}`
            }).join())

            // Fetching and exporting title country
            if (columnsExported.export_country) {
              obj.country = 'NA'
              let title_country = await TitlesCountry.findBy("libraryId", item.id)
              if (title_country) {
                title_country = await title_country.country().fetch()
                obj.country = title_country.$attributes.name
              }
            }

            return obj
          }))
          try {

            // console.log(libraries)

            // Getting export response ready 
            let result = exportFromJSON({
              data: libraries,
              fileName: 'library',
              exportType: 'csv',
              processor(content, type, fileName) {
                switch (type) {
                  case 'txt':
                    response.header('Content-Type', 'text/plain')
                    break
                  case 'json':
                    response.header('Content-Type', 'text/plain')
                    break
                  case 'csv':
                    response.header('Content-Type', 'text/csv')
                    break
                  case 'xls':
                    response.header('Content-Type', 'application/vnd.ms-excel')
                    break
                }
                response.header('Content-disposition', 'attachment;filename=' + fileName)
                return content
                
              }
            })


            // Sending export response in form of csv file
            
            return response.send(result)
          } catch (error) {
            console.log(error)
            return response.redirect('back')
          }

        } catch (error) {
          console.log(error)
          return response.redirect('back')
        }
      }

      // console.log("query ===========================")
      // console.log(query)
      // Fetching data for renderign view if req is not export request
      let libraries = await LibraryPolicy.getLibrary(user.toJSON(), query)

      let languages = await Languages.query().fetch()

      let paramsObj = Object.fromEntries(new URLSearchParams(request.get()))

      paramsObj.search && delete paramsObj.search
      paramsObj.searchcolumn && delete paramsObj.searchcolumn
      paramsObj.orderby && delete paramsObj.orderby
      paramsObj.order && delete paramsObj.order
      paramsObj.page && delete paramsObj.page
      paramsObj.perpage && delete paramsObj.perpage

      let filterQuery = []

      // reducing columns to only filtered values
      for (let [key, value] of Object.entries(paramsObj)) {
        filterQuery.push(`${key}=${value}`)
      }

      // Rendering view
      return view.render("library.library", {
        data: libraries,
        languages: languages.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
        filter: query.filter ? '&' + filterQuery.join('&') : undefined
      }
      );
    } catch (error) {
      console.log(error)
      return response.redirect("back");
    }
  }

  getValue(val1, val2) {
    if (val1.length <= 0) {
      return val2;
    } else {
      for (let index = 0; index < val1.length; index++) {
        let ind = val2.findIndex(x => x.id == val1[index].id);
        if (ind != -1) {
          val2[ind]["selected"] = true;
        }
        if (index == val1.length - 1) {
          return val2;
        }
      }
    }
  }

  async edit({ request, params, view }) {
    const adBreaks = await AdBreak.all();
    const awards = await Award.all();
    const casts = await Cast.all();
    const deliveries = await Delivery.all();
    const directors = await Director.all();
    const formats = await Format.all();
    const genres = await Genre.all();
    const ratings = await Rating.all();
    const producers = await Producer.all();
    const screeners = await Screener.all();
    const studios = await Studio.all();
    const trailers = await Trailer.all();
    const languages = await Languages.all();
    const countries = await Countries.all();
    const library = await Library.query()
      .where("id", params.id)
      .with("studio")
      .with("genres")
      .with("cast")
      .with("producers")
      .with("directors")
      .with("ratings")
      .with("awards")
      .with("trailers")
      .with("screeners")
      .with("format")
      .with('countries')
      .first();


    const alternateNames = await AlternateTitleLanguage.query().with('language').where('libraryId', library.id).fetch()
    const parentLanguageId = await AlternateTitleLanguage.query().with('language').where('libraryId', library.id).where('isParent', 1).pluck('languageId')

    let data = {
      library: library.toJSON(),
      adBreaks: adBreaks.toJSON(),
      awards: this.getValue(library.toJSON().awards, awards.toJSON()),
      casts: this.getValue(library.toJSON().cast, casts.toJSON()),
      deliveries: deliveries.toJSON(),
      formats: formats.toJSON(),
      genres: this.getValue(library.toJSON().genres, genres.toJSON()),
      ratings: ratings.toJSON(),
      producers: this.getValue(
        library.toJSON().producers,
        producers.toJSON()
      ),
      directors: this.getValue(
        library.toJSON().directors,
        directors.toJSON()
      ),
      screeners: this.getValue(
        library.toJSON().screeners,
        screeners.toJSON()
      ),
      trailers: this.getValue(
        library.toJSON().trailers,
        trailers.toJSON()
      ),
      studios: studios.toJSON(),
      languages: languages.toJSON(),
      countries: this.getValue(
        library.toJSON().countries,
        countries.toJSON()
      ),
      alternateNames: alternateNames.toJSON(),
      parentLanguageId: parentLanguageId
    }

    return view.render("library.edit", { data });
  }

  async update({ params, request, auth, response, session }) {
    try {
      const library = await Library.find(params.id);

      await TitlesCast.query()
        .where("libraryId", library.id)
        .delete();
      await TitlesAward.query()
        .where("libraryId", library.id)
        .delete();
      await TitlesTrailer.query()
        .where("libraryId", library.id)
        .delete();
      await TitlesGenre.query()
        .where("libraryId", library.id)
        .delete();
      await TitlesProducer.query()
        .where("libraryId", library.id)
        .delete();
      await TitlesDirector.query()
        .where("libraryId", library.id)
        .delete();
      await TitlesCountry.query()
        .where("libraryId", library.id)
        .delete();

      // format

      let data = request.only([
        "cast",
        "genres",
        "producers",
        "directors",
        "awards",
        "trailers",
        "alternateNames",
        "countries"
      ]);

      library.studioId = isNaN(request.input("studioId")) ? request.input("studioId") != 'null' ? (await DynamicOption.studio(request.input("studioId"))) : null : request.input("studioId")

      await Incoming.query().where('libraryId', library.id).update({ studioId: library.studioId })
      await TitleStatus.query().where('libraryId', library.id).update({ studioId: library.studioId })
      await TitleStatusesSeason.query().where('seriesId', library.id).update({ studioId: library.studioId })



      if (data.producers) {
        if (data.producers.length > 0) {
          const producersData = [];
          await Promise.all(data.producers.map(async val => {
            if (!isNaN(val)) {
              producersData.push({ libraryId: library.id, producerId: val });
            }
            else {
              producersData.push({ libraryId: library.id, producerId: (await DynamicOption.producer(val)) });
            }
          }));
          await TitlesProducer.createMany(producersData)
        }
      }

      if (data.directors) {
        if (data.directors.length > 0) {
          const directorsData = [];
          await Promise.all(data.directors.map(async val => {
            if (!isNaN(val)) {
              directorsData.push({ libraryId: library.id, directorId: val });
            }
            else {
              directorsData.push({ libraryId: library.id, directorId: (await DynamicOption.director(val)) });
            }
          }));
          await TitlesDirector.createMany(directorsData)
        }
      }

      if (data.countries) {
        if (data.countries.length > 0) {
          const countriesData = [];
          await Promise.all(data.countries.map(async val => {
            countriesData.push({ libraryId: library.id, countryId: val });
          }));
          await TitlesCountry.createMany(countriesData)
        }
      }

      if (data.cast) {
        if (data.cast.length > 0) {
          const castData = [];
          await Promise.all(data.cast.map(async val => {
            if (!isNaN(val)) {
              castData.push({ libraryId: library.id, castId: val });
            }
            else {
              castData.push({ libraryId: library.id, castId: (await DynamicOption.cast(val)) });
            }
          }));
          await TitlesCast.createMany(castData);
        }
      }

      if (data.genres) {
        if (data.genres.length > 0) {
          const genreData = [];
          await Promise.all(data.genres.map(async val => {
            if (!isNaN(val)) {
              genreData.push({ libraryId: library.id, genreId: val });
            }
            else {
              genreData.push({ libraryId: library.id, genreId: (await DynamicOption.genre(val)) });
            }
          }));
          await TitlesGenre.createMany(genreData)
        }
      }
      if (data.awards) {
        if (data.awards.length > 0) {
          const awardsData = [];
          await Promise.all(data.awards.map(async val => {
            if (!isNaN(val)) {
              awardsData.push({ libraryId: library.id, awardId: val });
            }
            else {
              awardsData.push({ libraryId: library.id, awardId: (await DynamicOption.award(val)) });
            }
          }));
          await TitlesAward.createMany(awardsData);
        }
      }

      if (data.trailers) {
        if (data.trailers.length > 0) {
          await Promise.all(
            data.trailers.map(item => {
              return TitlesTrailer.create({
                libraryId: library.id,
                trailerId: item
              });
            })
          );
        }
      }




      library.title = request.input("title");
      library.year = request.input("year");
      library.runtime = request.input("runtime");
      library.duration = request.input("duration");
      library.country = request.input("country");
      library.is = request.input("is");
      library.frameRate = request.input("frameRate");
      library.synopsis = request.input("synopsis");
      library.alterSynopsis = request.input("alterSynopsis");
      library.imdb = request.input("imdb");
      library.guId = (request.input("guId") != '' ? request.input("guId") : null);
      library.releaseDate = (request.input("releaseDate") != '' ? request.input("releaseDate") : null);
      library.ratingId = isNaN(request.input("ratingId")) ? request.input("ratingId") != 'null' ? (await DynamicOption.rating(request.input("ratingId"))) : null : request.input("ratingId")
      library.formatId = isNaN(request.input("formatId")) ? request.input("formatId") != 'null' ? (await DynamicOption.format(request.input("formatId"))) : null : request.input("formatId")

      library.screenerId = isNaN(request.input("screenerId")) ? null : request.input("screenerId")
      library.trailerId = isNaN(request.input("trailerId")) ? null : request.input("trailerId")
      library.originalLanguageId = isNaN(request.input("originalLanguageId")) ? null : request.input("originalLanguageId")


      if (request.input('screener').url) {
        library.screenerId = (await DynamicOption.screener(request.input('screener')))
      }

      if (request.input('trailer').url) {
        library.trailerId = (await DynamicOption.trailer(request.input('trailer')))
      }

      await AlternateTitleLanguage.query().where('libraryId', library.id).where('isParent', 1).update({ alternateName: request.input('title'), languageId: request.input('language'), alterSynopsis: request.input('synopsis') })

      if (data.alternateNames) {
        if (data.alternateNames.length > 0) {
          let alternateNamesData = []
          await Promise.all(data.alternateNames.map(async val => {

            alternateNamesData.push({ alternateName: (val.split('/~/'))[0], guId: val.split('/~/')[3] ? val.split('/~/')[3] : library.guId + '-' + (val.split('/~/'))[1] + '-' + Date.now(), libraryId: library.id, languageId: (await Languages.findBy({ code: (val.split('/~/'))[1] })).id, isParent: 0, alterSynopsis: val.split('/~/')[2], guIdOld: val.split('/~/')[3] ? true : false })
          }))
          alternateNamesData.map(async val => {
            if (!val.guIdOld) {
              let alternateTitleId = await AlternateTitleLanguage.create({ studioId: library.studioId, alternateName: val.alternateName, guId: val.guId, libraryId: val.libraryId, languageId: val.languageId, isParent: 0, alterSynopsis: val.alterSynopsis, studioId: library.studioId, imdb: library.imdb, ratingId: library.ratingId })
              await Incoming.create({ studioId: library.studioId, userId: (await auth.getUser()).id, libraryId: library.id, alternateTitleId: alternateTitleId.id })
            }
          })

          alternateNamesData = alternateNamesData.map(val => { return val.guIdOld && val.guId })
          let libraryAlternateNames = await AlternateTitleLanguage.query().select('id', 'guId', 'libraryId', 'isParent', 'guId').where('libraryId', library.id).where('isParent', 0).whereNotIn('guId', alternateNamesData).fetch()
          libraryAlternateNames.toJSON().map(async x => {
            let alternateTitle = await AlternateTitleLanguage.find(x.id)
            await alternateTitle.delete()
          })
        }
      }
      else {
        let libraryAlternateNames = await AlternateTitleLanguage.query().select('id', 'libraryId', 'isParent').where('libraryId', library.id).where('isParent', 0).fetch()
        libraryAlternateNames.toJSON().map(async x => {
          let alternateTitle = await AlternateTitleLanguage.find(x.id)
          await alternateTitle.delete()
        })
      }

      await library.save();

      session.flash({ notification: "Library Updated!" });

      return response.redirect("/library");
    } catch (error) {
      session.flash({ errMsg: "Library Not Updated!" });

      return response.redirect("back");
    }
  }

  async details({ request, response, params, view }) {
    try {
      const library = await Library.query()
        .where("id", params.id)
        .with("studio")
        .with("genres")
        .with("cast")
        .with("alternateNames")
        .with("directors")
        .with("producers")
        .with("ratings")
        .with("format")
        .with("awards")
        .with("countries")
        .first();

      return view.render("library.detail", { data: library.toJSON() });
    } catch (error) {
      return response.redirect("back");
    }
  }

  async remove({ session, response, params }) {

    try {
      const library = await Library.find(params.id);
      await library.holdbacks().delete()
      await library.delete();
      session.flash({ notification: "Title deleted Succesfully!" })
      response.redirect("back");
    } catch (error) {
      console.log(error.message)
      session.flash({ errMsg: "Title deleted Failed!" + error.message.toString() })
      response.redirect("back");
    }
  }

  async add({ request, view, params, response }) {
    try {
      const adBreaks = await AdBreak.all();
      const awards = await Award.all();
      const casts = await Cast.all();
      const deliveries = await Delivery.all();
      const directors = await Director.all();
      const formats = await Format.all();
      const genres = await Genre.all();
      const holdbacks = await Holdback.all();
      // const keywords = await Keyword.all();
      const ratings = await Rating.all();
      const producers = await Producer.all();
      const screeners = await Screener.all();
      const studios = await Studio.all();
      const trailers = await Trailer.all();
      const languages = await Languages.all();
      const countries = await Countries.all();

      
      // if (params.is == "movie") {
      return view.render("library.add", {
        data: {
          adBreaks: adBreaks.toJSON(),
          awards: awards.toJSON(),
          casts: casts.toJSON(),
          deliveries: deliveries.toJSON(),
          directors: directors.toJSON(),
          formats: formats.toJSON(),
          genres: genres.toJSON(),
          holdbacks: holdbacks.toJSON(),
          // keywords: keywords.toJSON(),
          ratings: ratings.toJSON(),
          producers: producers.toJSON(),
          screeners: screeners.toJSON(),
          studios: studios.toJSON(),
          languages: languages.toJSON(),
          trailers: trailers.toJSON(),
          countries: countries.toJSON(),
          guId: uuid()
        }
      });
    } catch (error) {
      return response.redirect("back");
    }
  }

  async indexEpisode({ request, view, response, params }) {
    try {
      let alternateName = await AlternateTitleLanguage.query().select('id', 'alternateName').where('id', params.alternateTitleId).first()
      let episodes = await Episode.query().where('seriesId', params.id).where('alternateTitleId', params.alternateTitleId).with('season').fetch()
      return view.render('library.episode', { alternateName: alternateName.toJSON(), libraryId: params.id, episodes: episodes.toJSON(), alternateTitleId: params.alternateTitleId })
    } catch (error) {
      return response.redirect('back')
    }
  }

  async addEpisode({ view, params }) {
    try {
      let library = await Library.query().select('id', 'guId', 'title').where('id', params.id).first()
      let seasons = await Season.query().where('seriesId', params.id).where('alternateTitleId', params.alternateTitleId).fetch()
      return view.render('library.addEpisode', { library: library.toJSON(), seasons: seasons.toJSON(), alternateTitleId: params.alternateTitleId })
    } catch (error) {
      response.redirect('back')
    }
  }

  async storeEpisode({ request, session, response, auth }) {
    let trx;
    try {
      let data = request.all()
      let library = await Library.find(data.seriesId)
      if (data.hasOwnProperty('seasonId')) {
        let season = await Season.query().select('id', 'number').where('id', data.seasonId).first()
        let guId = data.guId + '_' + (((data.title).match(/\b(\w)/g)).join('')) + '_s' + season.number + '_e' + data.episodeNumber

        trx = await Database.beginTransaction()

        let episode = await Episode.create({ number: data.episodeNumber, name: data.episodeName, guId: guId, seasonId: data.seasonId, seriesId: data.seriesId, alternateTitleId: data.alternateTitleId, alterSynopsis: data.alterSynopsis, ratingId: library.ratingId, studioId: library.studioId, imdb: library.imdb, userId: (await auth.getUser()).id }, trx)

        let incoming = await Incoming.query().where('libraryId', data.seriesId).where('alternateTitleId', data.alternateTitleId).first()

        let incomingSeason = await IncomingSeason.query().where('seriesId', data.seriesId).where('incomingId', incoming.id).where('seasonId', data.seasonId).first()

        await IncomingEpisode.create({ incomingId: incoming.id, userId: (await auth.getUser()).id, studioId: library.studioId, seriesId: data.seriesId, seasonId: season.id, episodeId: episode.id, incomingSeasonId: incomingSeason.id }, trx)
        await trx.commit()

        await UpdateStatusIncomingEpisode(incomingSeason.id)

        session.flash({ notification: "Episode Added Succesfully!" })
        return response.redirect("back")
      }
      else {
        trx = await Database.beginTransaction()

        let season = await Season.create({ guId: data.guId + '_' + (((data.title).match(/\b(\w)/g)).join('')) + '_s' + data.seasonNumber, name: data.seasonName, number: data.seasonNumber, seriesId: data.seriesId, alternateTitleId: data.alternateTitleId, studioId: library.studioId, userId: (await auth.getUser()).id }, trx)

        let guId = data.guId + '_' + (((data.title).match(/\b(\w)/g)).join('')) + '_s' + data.seasonNumber + '_e' + data.episodeNumber

        let episode = await Episode.create({ number: data.episodeNumber, name: data.episodeName, guId: guId, seasonId: season.id, seriesId: data.seriesId, alternateTitleId: data.alternateTitleId, alterSynopsis: data.alterSynopsis, ratingId: library.ratingId, studioId: library.studioId, imdb: library.imdb, userId: (await auth.getUser()).id }, trx)

        let incoming = await Incoming.query().where('libraryId', data.seriesId).where('alternateTitleId', data.alternateTitleId).first()

        let incomingSeason = await IncomingSeason.create({ incomingId: incoming.id, seriesId: data.seriesId, seasonId: season.id, alternateTitleId: data.alternateTitleId, studioId: library.studioId, userId: (await auth.getUser()).id }, trx)

        await IncomingEpisode.create({ incomingId: incoming.id, seriesId: data.seriesId, seasonId: season.id, episodeId: episode.id, incomingSeasonId: incomingSeason.id, studioId: library.studioId, userId: (await auth.getUser()).id }, trx)
        await trx.commit()

        await UpdateStatusIncomingEpisode(incomingSeason.id)


        session.flash({ notification: "Episode Added Succesfully!" })
        return response.redirect("back")
      }
    } catch (error) {
      await trx.rollback()
      session.flash({ errMsg: "Duplicate Entry!" })
      return response.redirect("back")
    }
  }

  async deleteEpisode({ session, response, params }) {
    try {
      let episode = await Episode.find(params.id)
      await episode.delete()
      let episodes = await Episode.query().where('seasonId', episode.seasonId).first()
      if (episodes == null) {
        let season = await Season.find(episode.seasonId)
        await season.delete()
      }
      let incomingSeason = await IncomingSeason.findBy('seasonId', episode.seasonId)
      await UpdateStatusIncomingEpisode(incomingSeason.id)

      session.flash({ notification: "Episode Deleted Succesfully!" })
      return response.redirect("back")
    } catch (error) {
      session.flash({ errMsg: "Failed to Delete Episode!" })
      return response.redirect("back")
    }
  }

  async RightsManagementIndex({ view, params }) {
    const territories = await Territory.query().with('countries').fetch();
    const rights = await Right.all();
    const library = await Library.query().select('id', 'title').where('id', params.id).first()
    try {
      const titles_territories_right = await Right.query()
        .with('territory', (builder) => builder.distinct('territoryName').where('libraryId', params.id))
        .with('dates', (builder) => builder.where('libraryId', params.id))
        .fetch()

      return view.render('library.rightsManagement', { data: { rights: rights.toJSON(), territories: territories.toJSON(), library: library.toJSON(), rightsData: titles_territories_right.toJSON() } })
    } catch (error) {
      return view.render('library.rightsManagement', { data: { rights: rights.toJSON(), territories: territories.toJSON(), library: library.toJSON() } })
    }
  }

  async getRightsCountries({ request, response }) {
    let data = request.all()
    try {
      const titles_territories_right = await TitlesTerritoriesRight.query().where('libraryId', data.libraryId).where('rightId', data.rightId).where('territoryId', data.territoryId).pluck('countryId')
      const dates = await TitlesTerritoriesRight.query().select('endDate', 'startDate').where('libraryId', data.libraryId).where('rightId', data.rightId).where('territoryId', data.territoryId).first()
      // console.log(dates.toJSON())
      let territoryCountries = await TerritoryCountry.query().where('territoryId', data.territoryId).fetch()
      territoryCountries = territoryCountries.toJSON().map(val => {
        if (titles_territories_right.includes(val.id)) {
          val.selected = true
        }
        return val
      })
      return response.json({ data: territoryCountries, dates: dates.toJSON(), success: true })
    } catch (error) {
      return response.json({ data: titles_territories_right, dates: dates.toJSON(), success: false })
    }
  }

  async RightsManagementStore({ request, session, response, params }) {
    let data = request.all()
    data.countries = JSON.parse('[' + data.countries + ']')
    try {

      data.countries.map(async val => {
        await Promise.all(data.rights.map(async x => {
          try {
            await TitlesTerritoriesRight.create({ territoryId: val.territoryId, countryId: val.id, libraryId: params.id, rightId: x, startDate: data.startDate, endDate: data.endDate })
          } catch (error) {
          }
        }))
      })

      session.flash({ notification: "Created Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split(' '))[0] + ' ' + ((error.sqlMessage).split(' '))[1] + ' ' + 'Skipped' })
      return response.redirect('back')
    }
  }

  async RightsManagementEdit({ request, params, view }) {
    const territories = await Territory.all();
    const rights = await Right.all();
    try {
      const titles_territories_right = await TitlesTerritoriesRight.query().with('territory').with('rights').where('id', params.id).first()
      return view.render('library.rightsManagementEdit', { data: titles_territories_right.toJSON(), territories: territories.toJSON(), rights: rights.toJSON() })
    } catch (error) {
      return response.redirect('back')
    }
  }


  async RightsManagementRemoveTerritory({ request, session, params, response }) {
    try {
      let data = request.all()
      await TitlesTerritoriesRight.query().where('libraryId', data.libraryId).where('rightId', data.rightId).where('territoryId', data.territoryId).delete()
      session.flash({ notification: "Deleted Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: "Delete Failed" })
      return response.redirect('back')
    }
  }

  async RightsManagementRemoveRight({ params, session, response }) {
    try {
      await TitlesTerritoriesRight.query().where('libraryId', params.libraryId).where('rightId', params.rightId).delete()
      session.flash({ notification: "Deleted Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: "Delete Failed" })
      return response.redirect('back')
    }
  }

  async RightsManagementUpdate({ request, params, session, response }) {
    try {
      let data = request.all()

      const titles_territories_right = await TitlesTerritoriesRight.query().where('libraryId', data.libraryId).where('rightId', data.rightId).where('territoryId', data.territoryId).fetch()

      data.countries ?
        await Promise.all(
          titles_territories_right.toJSON().map(async val => {
            if (!data.countries.includes((val.countryId))) {
              await TitlesTerritoriesRight.query().where('id', val.id).delete()
              data.countries = data.countries.filter(elm => elm != val.id)
            }
          })
        )

        :

        await TitlesTerritoriesRight.query().where('libraryId', data.libraryId).where('rightId', data.rightId).where('territoryId', data.territoryId).delete()

      data.countries && data.countries.map(async val => {
        try {
          await TitlesTerritoriesRight.create({ libraryId: data.libraryId, rightId: data.rightId, territoryId: data.territoryId, countryId: val, startDate: data.startDate, endDate: data.endDate })
        } catch (error) {
        }
      })

      await TitlesTerritoriesRight.query().where('libraryId', data.libraryId).where('rightId', data.rightId).where('territoryId', data.territoryId).update({ startDate: data.startDate, endDate: data.endDate })

      session.flash({ notification: "Updated Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split(' '))[0] + ' ' + ((error.sqlMessage).split(' '))[1] })
      return response.redirect('back')
    }
  }

  async updateAlternateTitle({ request, session, response, params }) {
    let data = request.all()
    try {

      let alternateName = await AlternateTitleLanguage.find(params.id)
      alternateName.alternateName = data.alternateName
      alternateName.languageId = data.languageId
      alternateName.alterSynopsis = data.alterSynopsis
      await alternateName.save()
      session.flash({ notification: "Updated Successfully" });
      return response.redirect('back')
    } catch (error) {
      return response.redirect('back')
    }
  }

  async deleteAlternateTitle({ session, response, params }) {
    try {
      let alternateName = await AlternateTitleLanguage.find(params.id)
      await alternateName.delete()
      session.flash({ notification: "Alternate Name Deleted Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: "Failed to Delete Alternate Name" })
      return response.redirect('back')
    }
  }

  async importLibrary({ request, auth, response, session, view }) {
    if (request.input('movie') == 'on') {
      try {
        const file = request.file('libraryFile')
        let fileName = uuid() + '.xlsx'
        await file.move(Helpers.tmpPath('uploads'), {
          name: fileName
        })

        let data = await parseCSV(fileName)

        if (data.length > 0 && data[0].hasOwnProperty('genre') && data[0].hasOwnProperty('type') && data[0].hasOwnProperty('director')) {

          data = data.map((item, index) => {
            item.created_at = index
            return item
          })

          let castData = ([].concat.apply([], data.map(x => x.cast.split(',')))).map(x => {
            if (x.trim() != '') {
              return { actorName: x.trim() }
            }

          })

          let directorsData = ([].concat.apply([], data.map(x => x.director.split(',')))).map(x => {
            if (x.trim() != '') {
              return { name: x.trim() }
            }
          })

          let producersData = ([].concat.apply([], data.map(x => x.producer.split(',')))).map(x => {
            if (x.trim() != '') {
              return { name: x.trim() }
            }
          })

          let genreData = ([].concat.apply([], data.map(x => x.genre.split(',')))).map(x => {
            if (x.trim() != '') {
              return { title: x.trim() }
            }
          })

          let awardsData = ([].concat.apply([], data.map(x => x.awards.split(';')))).map(x => {
            if (x.trim() != '') {
              return { awardName: x.trim() }
            }
          })

          let countryData = ([].concat.apply([], data.map(x => x.country.split(',')))).map(x => {
            if (x.trim() != '') {
              return { name: x.trim(), code: x.trim() }
            }
          })

          try {
            await Cast.createMany(castData)
          } catch (error) {

          }

          try {
            await Director.createMany(directorsData)
          } catch (error) {

          }

          try {
            await Producer.createMany(producersData)
          } catch (error) {

          }

          try {
            await Genre.createMany(genreData)
          } catch (error) {

          }

          try {
            await Award.createMany(awardsData)
          } catch (error) {

          }

          try {
            await Countries.createMany(countryData)
          } catch (error) {

          }

          let libraries = []
          data = await Promise.all(data.map(async (item, index) => {

            if (!item.title) return libraries.push({ error: { row: index + 2, message: "Title Missing", title: item.title } })

            if (!item.guid) item.guid = uuid()

            if (!item.guid.match(/[0-9a-z]{8}-/gi)) return libraries.push({ error: { row: index + 2, message: "GUID Invalid" } })

            let duplicate = await Library.query().where('guId', item.guid).first()
            if (duplicate == null) {

              let library = {
                title: item.title,
                year: item.year,
                runtime: item.runTime,
                is: item.type.toLowerCase(),
                synopsis: item.synopsis,
                guId: item.guid,
                imdb: item.imdb,
                frameRate: item.frame ? item.frame : 23.976,
                releaseDate: item.releaseDate ? item.releaseDate : null,
                startDate: item.startDate ? item.startDate : null,
                endDate: item.endDate ? item.startDate : null,
                created_at: item.created_at
              }

              try {
                let language = await Languages.query().where('name', 'like', `%${item.language.trim()}%`).orWhere('code', item.language.trim()).first()
                language != null ? library.languageId = language.id : await DynamicOption.format({ code: item.language.trim(), name: item.language.trim() })
                library.languageId = (await Languages.query().where('name', item.language.trim()).orWhere('code', item.language.trim()).first()).id
              } catch (error) {

              }

              try {
                let format = await Format.query().where('format', item.format).first()
                format != null ? library.formatId = format.id : await DynamicOption.format(item.format)
                library.formatId = (await Format.query().where('format', item.format).first()).id
              } catch (error) {

              }

              try {
                let studio = await Studio.query().where('title', item.studio).first()
                studio != null ? library.studioId = studio.id : await DynamicOption.studio(item.studio)
                library.studioId = (await Studio.query().where('title', item.studio).first()).id
              } catch (error) {

              }

              try {
                let screener = await Screener.query().where('screenerLink', item.screenerLink).first()
                screener != null ? library.screenerId = screener.id : item.screenerLink != '' && await DynamicOption.screener({ url: item.screenerLink, screenerPW: item.screenerPw, name: item.title })
                library.screenerId = (await Screener.query().where('screenerLink', item.screenerLink).first()).id
              } catch (error) {

              }

              try {
                let trailer = await Trailer.query().where('trailerUrl', item.trailerLink).first()
                trailer != null ? library.trailerId = trailer.id : item.trailerLink != '' && await DynamicOption.trailer({ url: item.trailerLink, name: item.title })
                library.trailerId = (await Trailer.query().where('trailerUrl', item.trailerLink).first()).id
              } catch (error) {

              }

              try {
                let rating = await Rating.query().where('name', item.mpaaRating).first()
                rating != null ? library.ratingId = rating.id : await DynamicOption.rating(item.mpaaRating)
                library.ratingId = (await Rating.query().where('name', item.mpaaRating).first()).id
              } catch (error) {

              }

              try {
                library.genres = await Promise.all(
                  item.genre && item.genre.split(',').map(async val => {
                    let obj = await Genre.query().where('title', val.trim()).first()
                    return obj.id
                  })
                )
              } catch (error) {

              }

              try {
                library.directors = await Promise.all(
                  item.director && item.director.split(',').map(async val => {
                    let obj = await Director.query().where('name', val.trim()).first()
                    return obj.id
                  })
                )
              } catch (error) {

              }

              try {
                library.producers = await Promise.all(
                  item.producer && item.producer.split(',').map(async val => {
                    let obj = await Producer.query().where('name', val.trim()).first()
                    return obj.id

                  })
                )

              } catch (error) {

              }

              try {
                library.casts = await Promise.all(
                  item.cast && item.cast.split(',').map(async val => {
                    if (val != '#N/A' && val != 'n/a') {
                      let obj = await Cast.query().where('actorName', val.trim()).first()
                      return obj.id
                    }
                  })
                )

              } catch (error) {

              }

              try {
                library.awards = await Promise.all(
                  item.awards && item.awards.split(';').map(async val => {
                    let obj = await Award.query().where('awardName', val.trim()).first()
                    return obj.id

                  })
                )

              } catch (error) {

              }

              try {
                library.countries = await Promise.all(
                  item.country && item.country.split(',').map(async val => {
                    let obj = await Countries.query().where('code', val.trim()).first()
                    return obj.id

                  })
                )

              } catch (error) {

              }

              try {
                library.THEATRICAL = []
                let right = await Right.findBy('name', 'THEATRICAL')
                item.THEATRICAL && item.THEATRICAL.slice(1, item.THEATRICAL.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.THEATRICAL.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.THEATRICAL.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.THEATRICAL.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
              }


              try {
                library.NONTHEATRICAL = []
                let right = await Right.findBy('name', 'Non-theatrical')
                item.NONTHEATRICAL && item.NONTHEATRICAL.slice(1, item.NONTHEATRICAL.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.NONTHEATRICAL.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.NONTHEATRICAL.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.NONTHEATRICAL.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }


              try {
                library.PTV = []
                let right = await Right.findBy('name', 'PTV')
                item.PTV && item.PTV.slice(1, item.PTV.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.PTV.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.PTV.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.PTV.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }

              try {
                library.AD_BASED_STREAMING = []
                let right = await Right.findBy('name', 'Ad-Based Streaming')
                item.AD_BASED_STREAMING && item.AD_BASED_STREAMING.slice(1, item.AD_BASED_STREAMING.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.AD_BASED_STREAMING.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.AD_BASED_STREAMING.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.AD_BASED_STREAMING.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }

              try {
                library.TVOD = []
                let right = await Right.findBy('name', 'TVOD')
                item.TVOD && item.TVOD.slice(1, item.TVOD.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.TVOD.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.TVOD.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.TVOD.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }


              try {
                library.PPV = []
                let right = await Right.findBy('name', 'PPV')
                item.PPV && item.PPV.slice(1, item.PPV.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.PPV.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.PPV.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.PPV.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }


              try {
                library.EST = []
                let right = await Right.findBy('name', 'EST')
                item.EST && item.EST.slice(1, item.EST.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.EST.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.EST.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.EST.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }


              try {
                library.SVOD = []
                let right = await Right.findBy('name', 'SVOD')
                item.SVOD && item.SVOD.slice(1, item.SVOD.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.SVOD.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.SVOD.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.SVOD.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }


              try {
                library.AVOD = []
                let right = await Right.findBy('name', 'AVOD')
                item.AVOD && item.AVOD.slice(1, item.AVOD.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.AVOD.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.AVOD.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.AVOD.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }


              try {
                library.DVD = []
                let right = await Right.findBy('name', 'DVD')
                item.DVD && item.DVD.slice(1, item.DVD.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.DVD.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.DVD.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.DVD.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }


              try {
                library.ANCILARY = []
                let right = await Right.findBy('name', 'ANCILARY')
                item.ANCILARY && item.ANCILARY.slice(1, item.ANCILARY.length).split(',-').map(async row => {
                  if (row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)

                    if (territoryId != null) {
                      if (territoryName.trim().toLowerCase() != 'world wide') {
                        let territoryCountries = await TerritoryCountry.query().where('territoryId', territoryId.id).fetch()
                        territoryCountries.toJSON().map(country => {
                          library.ANCILARY.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                        })
                      } else {
                        library.ANCILARY.push({ countryId: null, territoryId: territoryId.id, rightId: right.id })
                      }
                    }

                  } else if (!row.includes("(*)")) {
                    let territoryName = row.split('(')[0].trim()
                    let territoryId = await Territory.findBy('territoryName', territoryName)
                    let territoryCountriesName = row.slice(row.indexOf("(") + 1, row.lastIndexOf(")")).split(",")

                    if (territoryId != null && territoryCountriesName.length > 0) {
                      territoryCountriesName = territoryCountriesName.map(name => name.trim())
                      let territoriesCountryIds = await TerritoryCountry.query().whereIn('country', territoryCountriesName).fetch()
                      territoriesCountryIds.toJSON().map(country => {
                        library.ANCILARY.push({ countryId: country.id, territoryId: territoryId.id, rightId: right.id })
                      })
                    }
                  }
                })
              } catch (error) {
                console.log(error.message)
              }

              libraries.push(library)

            }
            else {
              libraries.push({ error: { row: index + 2, message: 'GUID should be unique', title: item.title } })
            }
          })
          )

          let errors = []

          await Promise.all(libraries.sort((a, b) => a.created_at - b.created_at).map(async (library, index) => {
            if (library.hasOwnProperty('error') == false) {
              let trx = await Database.beginTransaction()
              try {
                let lib = await Library.create({ userId: (await auth.getUser()).id, title: library.title, year: library.year, runtime: library.runtime, is: library.is, synopsis: library.synopsis, guId: library.guId, imdb: library.imdb, releaseDate: library.releaseDate, formatId: library.formatId, studioId: library.studioId, screenerId: library.screenerId, trailerId: library.trailerId, ratingId: library.ratingId, frameRate: library.frameRate }, trx)

                let genres = library.genres && library.genres.map(val => {
                  return { genreId: val, libraryId: lib.id }
                })

                try {
                  await TitlesGenre.createMany(genres, trx)
                } catch (error) {

                }

                let directors = library.directors && library.directors.map(val => {
                  return { directorId: val, libraryId: lib.id }
                })


                try {
                  await TitlesDirector.createMany(directors, trx)
                } catch (error) {

                }

                let producers = library.producers && library.producers.map(val => {
                  return { producerId: val, libraryId: lib.id }
                })

                try {
                  await TitlesProducer.createMany(producers, trx)
                } catch (error) {

                }

                let casts = library.casts && library.casts.map(val => {
                  return { castId: val, libraryId: lib.id }
                })


                try {
                  await TitlesCast.createMany(casts, trx)
                } catch (error) {

                }

                let awards = library.awards && library.awards.map(val => {
                  return { awardId: val, libraryId: lib.id }
                })


                try {
                  await TitlesAward.createMany(awards, trx)
                } catch (error) {

                }

                let countries = library.countries && library.countries.map(val => {
                  return { countryId: val, libraryId: lib.id }
                })


                try {
                  await TitlesCountry.createMany(countries, trx)
                } catch (error) {

                }

                let THEATRICALRights = library.THEATRICAL && library.THEATRICAL.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(THEATRICALRights, trx)
                } catch (error) {

                }

                let NONTHEATRICALRights = library.NONTHEATRICAL && library.NONTHEATRICAL.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(NONTHEATRICALRights, trx)
                } catch (error) {

                }

                let PTVRights = library.PTV && library.PTV.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(PTVRights, trx)
                } catch (error) {

                }

                let AD_BASED_STREAMINGRights = library.AD_BASED_STREAMING && library.AD_BASED_STREAMING.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(AD_BASED_STREAMINGRights, trx)
                } catch (error) {

                }

                let TVODRights = library.TVOD && library.TVOD.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(TVODRights, trx)
                } catch (error) {

                }

                let PPVRights = library.PPV && library.PPV.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(PPVRights, trx)
                } catch (error) {

                }

                let ESTRights = library.EST && library.EST.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(ESTRights, trx)
                } catch (error) {

                }

                let SVODRights = library.SVOD && library.SVOD.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(SVODRights, trx)
                } catch (error) {

                }

                let AVODRights = library.AVOD && library.AVOD.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(AVODRights, trx)
                } catch (error) {

                }

                let DVDRights = library.DVD && library.DVD.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(DVDRights, trx)
                } catch (error) {

                }

                let ANCILARYRights = library.ANCILARY && library.ANCILARY.map(val => {
                  return { countryId: val.countryId, libraryId: lib.id, rightId: val.rightId, territoryId: val.territoryId, startDate: library.startDate, endDate: library.endDate }
                })

                try {
                  await TitlesTerritoriesRight.createMany(ANCILARYRights, trx)
                } catch (error) {

                }

                try {
                  let parentTitle = await AlternateTitleLanguage.create({ alternateName: lib.title, guId: lib.guId, libraryId: lib.id, languageId: library.languageId, isParent: 1, alterSynopsis: lib.synopsis, studioId: lib.studioId, imdb: lib.imdb, ratingId: lib.ratingId }, trx)
                  await Incoming.create({ libraryId: lib.id, alternateTitleId: parentTitle.id, studioId: lib.studioId, userId: (await auth.getUser()).id }, trx)

                } catch (error) {

                }

                await library

                await trx.commit()

              } catch (error) {
                if (library.guid) {
                  let guId = await Library.query().where('guId', library.guId).first()
                  let msg = error.sqlMessage + ' ' + (guId != null ? 'GUID should be unique.' : '')
                  errors.push({ row: index + 2, message: msg, title: library.title })
                } else {
                  let msg = error.sqlMessage + ' ' + 'GUID Missing.'
                  errors.push({ row: index + 2, message: msg, title: library.title })
                }
                await trx.rollback()
              }
            }
            else {
              errors.push(library.error)
            }

          }))


          if (errors.length > 0) {
            return view.render('library.importError', { data: errors, back: '/library' })
          }
          else {
            session.flash({ notification: 'Titles Imported Successfully!' })
            return response.redirect('/library')
          }

        } else {
          session.flash({ errMsg: "Data Is Not in Correct Format!" });
          return response.redirect('/library')
        }

      } catch (error) {
        session.flash({ errMsg: "Titles Import Failed" });
        return response.redirect('/library')
      }
    }
    else {

      try {
        const file = request.file('libraryFile')
        let fileName = uuid() + '.xlsx'
        await file.move(Helpers.tmpPath('uploads'), {
          name: fileName
        })

        let data = await parseCSV(fileName)

        if (data.length > 0 && data[0].hasOwnProperty('episodeName') && data[0].hasOwnProperty('episodeNo') && data[0].hasOwnProperty('series')) {


          let episodes = []

          await Promise.all(data.map(async (item, index) => {

            let alternateName = item.series ? await AlternateTitleLanguage.query().with('library').where('guId', item.guid.trim()).first() : null
            if (alternateName != null) {
              let incoming = await Incoming.findBy('alternateTitleId', alternateName.id)
              let seasonObj = {}
              seasonObj.incoming = incoming.toJSON()
              seasonObj.alternateName = alternateName.toJSON()

              let episode = {
                name: item.episodeName,
                number: parseInt(item.episodeNo),
                alterSynopsis: item.synopsis,
                guId: item.guid.trim() + '_' + (((alternateName.alternateName).match(/\b(\w)/g)).join('')) + '_s' + item.seasonNo + '_e' + item.episodeNo,
                ratingId: alternateName.toJSON().library.ratingId,
                alternateTitleId: alternateName.toJSON().library.alternateTitleId,
                studioId: alternateName.toJSON().library.studioId,
                imdb: alternateName.toJSON().imdb,
                studioId: incoming.studioId,
                userId: (await auth.getUser()).id
              }

              try {
                let season = await Season.create({ studioId: incoming.studioId, userId: (await auth.getUser()).id, guId: item.guid.trim() + '_' + (((alternateName.toJSON().alternateName).match(/\b(\w)/g)).join('')) + '_s' + item.seasonNo, alternateTitleId: alternateName.id, seriesId: alternateName.toJSON().libraryId, imdb: alternateName.toJSON().library.imdb, studioId: alternateName.toJSON().library.studioId, ratingId: alternateName.toJSON().library.ratingId, name: item.seasonName, number: item.seasonNo })
                let incomingSeason = await IncomingSeason.create({ studioId: incoming.studioId, userId: (await auth.getUser()).id, incomingId: incoming.id, seriesId: alternateName.libraryId, seasonId: season.id, alternateTitleId: alternateName.id })
                episode.seasonId = season.toJSON().id
                seasonObj.seasonId = season.toJSON().id
                seasonObj.incomingSeasonId = incomingSeason.toJSON().id
              } catch (error) {
                let season = await Season.query().where('name', item.seasonName).where('number', item.seasonNo).where('alternateTitleId', alternateName.id).first()
                let incomingSeason = await IncomingSeason.findBy('alternateTitleId', alternateName.id)
                episode.seasonId = season.toJSON().id
                seasonObj.seasonId = season.toJSON().id
                seasonObj.incomingSeasonId = incomingSeason.toJSON().id
              }

              let duplicate = await Episode.query().where('name', item.episodeName).where('seasonId', seasonObj.seasonId).where('number', item.episodeNo).where('alternateTitleId', alternateName.id).first()
              if (duplicate == null) {
                (alternateName != null) && (
                  (episode.seriesId = alternateName.libraryId),
                  (episode.alternateTitleId = alternateName.id)
                )

                episodes.push({ episode, seasonObj })
              }
              else {
                (incoming != null && duplicate != null) ? episodes.push({ error: { row: index + 2, message: 'GUID provided is not in Library.Episode Should be unique' } })
                  :
                  (
                    (incoming == null && duplicate != null) && episodes.push({ error: { row: index + 2, message: 'GUID provided is not in Library' } })
                      (incoming != null && duplicate == null) && episodes.push({ error: { row: index + 2, message: 'Episode Should be unique.' } })
                  )
              }
            } else {
              episodes.push({ error: { row: index + 2, message: 'Series Title Provided Not in Library.' } })
            }

          })
          )

          let errors = []

          await Promise.all(episodes.map(async (episode, index) => {
            try {
              if (episode.hasOwnProperty('error') == false) {
                let episodeObj = await Episode.create(episode.episode)
                await IncomingEpisode.create({ studioId: episode.episode.studioId, userId: (await auth.getUser()).id, incomingId: episode.seasonObj.incoming.id, seriesId: episode.seasonObj.alternateName.libraryId, seasonId: episode.seasonObj.seasonId, episodeId: episodeObj.id, incomingSeasonId: episode.seasonObj.incomingSeasonId })
                await UpdateStatusIncomingEpisode(episode.seasonObj.incomingSeasonId)
              }
              else {
                errors.push(episode.error)
              }
            } catch (error) {
              errors.push({ row: index + 2, message: error.sqlMessage })
            }
          }))

          if (errors.length > 0) {
            session.flash({ errors: errors })
          }
          else {
            session.flash({ notification: "Seasons and Episode Data Imported Succesfully!" })
          }

          return response.redirect('/library')

        }
        else {
          session.flash({ errMsg: "Data Is Not in Correct Format!" });
          return response.redirect('/library')
        }


      } catch (error) {
        session.flash({ errMsg: "Titles Import Failed" });
        return response.redirect('/library')
      }
    }
  }

  async indexImages({ request, view, params, auth }) {
    try {
      const library = await Library.find(params.id)
      let images = await Image.query().where('libraryId', params.id).fetch()
      return view.render('library.indexImage', { data: images.toJSON(), library: library.toJSON() })
    } catch (error) {

    }
  }

  async addImages({ view, params }) {
    const library = await Library.find(params.id)
    return view.render('library.addImages', { data: library.toJSON() })
  }

  async deleteImages({ session, params, response }) {
    try {
      let image = await Image.find(params.id)
      await image.delete()
      fsPromise.unlink('public/' + image.path)
      fsPromise.unlink('public/' + image.compressed)
      fsPromise.unlink('public/' + image.thumbnail)
      session.flash({ notification: `Image "${image.name}" Deleted Successfully` })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: `Image "${image.name}" Delete Failed` })
      return response.redirect('back')
    }
  }

  async showImage({ response, params }) {
    return await readFile(decodeURIComponent(params.path))
  }

  async storeImages({ request, session, response, params, auth }) {
    try {
      let user = await auth.getUser()
      let studioId = parseInt(request.input('studioId'))
      let libraryId = parseInt(params.id)
      const images = request.file('photos', {
        types: ['image'],
        extnames: ['png', 'jpg', 'jpeg'],
        size: '2mb'
      })

      let paths = []
      let files = await images.moveAll(`public/storage/images/studio-${studioId}`, (file) => {

        let name = `${new Date().getTime()}-${libraryId}-${file.toJSON().clientName.replace(/[^a-zA-Z0-9-_\.]+/ig, "_")}`
        let ext= path.parse(name).ext
        if(ext == ".tiff")
        {
          throw err
        }
        let length=ext.length
        paths.push({
          userId: user.id,
          libraryId: libraryId,
          studioId: studioId,
          path: `storage/images/studio-${studioId}/${name}`,
          compressed: `storage/images/studio-${studioId}/compressed/${name}`,
          thumbnail: `storage/images/studio-${studioId}/thumbnail/${name}`,
          //thumbnail: `storage/images/studio-${studioId}/thumbnail/${name.substr(0, name.length - length)}.webp`,
          name: name
        })

        return {
          name: name
        }

      })

      let files_paths = paths.map(x => 'public/' + x.path)
      let thumbnails = []
      await imagemin(files_paths, {
        destination: `public/storage/images/studio-${studioId}/compressed`,
        plugins: [
          imageminJpegtran(),
          imageminPngquant({
            quality: [0.6, 0.8]
          })
        ]
      }).then(x => {
        x.map(img => {
          let imgSize = sizeOf(img.sourcePath)
          thumbnails.push({ path: img.sourcePath, size: { height: imgSize.height, width: imgSize.width } })
        })
      }).catch(e => console.log(e))

      thumbnails.map(img => {
        imagemin(img.path.split(), {
          destination: `public/storage/images/studio-${studioId}/thumbnail`,
          plugins: [
            imageminJpegtran()
            //imageminWebp({ resize: { height: img.size.height * 0.20, width: img.size.width * 0.20 } })
          ]
        })
      })



      await Image.createMany(paths)
      session.flash({ notification: 'Image Uploaded Successfully.' })

      return response.redirect('/library/' + libraryId + '/images')
    } catch (error) {
      session.flash({ errMsg: 'Image Upload Failed. Image in .tiff format are not accepted' })
      response.redirect('back')
    }
  }


  async importUpdateLibrary({ request, auth, response, session, view }) {
    try {
      const file = request.file('libraryFile')
      let fileName = uuid() + '.xlsx'
      await file.move(Helpers.tmpPath('uploads'), {
        name: fileName
      })

      let data = await parseCSV(fileName)

      let errors = []
      let errorFile = []
      const guIds = []

      data = data.map(async (item, index) => {

        let obj = {
          guId: item.guid,
          holdbacks: [],
          pitchs: [],
          keywords: []
        }

        if (!item.hasOwnProperty('guid') || item.guid.trim() == '') {
          errors.push({ row: index + 2, message: 'guid missing' })
          item.error = 'GUID missing'
          errorFile.push(item)
          return
        }

        if (data.filter(x => x.guid == item.guid).length >= 2) {
          errors.push({ row: index + 2, message: 'Duplicate GUID' })
          item.error = 'Duplicate GUID'
          errorFile.push(item)
          return
        }

        let library = await Library.findBy('guId', item.guid.trim())

        if (library != null) {
          obj.libraryId = library.id
        }
        else {
          errors.push({ row: index + 2, message: 'Title Not Found GUID Invalid' })
          item.error = 'Title Not Found GUID Invalid'
          errorFile.push(item)
          return
        }
        let userId = (await auth.getUser()).id

        // item.runTime.trim()!='' && (obj.runtime = item.runTime.trim())
        // item.duration.trim()!= '' && (obj.duration = item.duration.trim())
        item.frameRate && (item.frameRate.trim() != '' && (obj.frameRate = item.frameRate.trim()))

        // try {
        //   if (item.format.trim()!='') {
        //     let format = await Format.findBy('format',item.format.trim().toLowerCase())
        //     obj.format = format ? format.id : await DynamicOption.format(item.format.trim()) 
        //   } 
        // } catch (error) {
        // }


        try {
          if (item.holdbacks.trim() != '') {
            item.holdbacks.trim().split('),(').map(x => {
              let holback = x.replace(')', '').replace('(', '').trim().split(',')
              obj.holdbacks.push({ name: holback[0], startDate: holback[1], endDate: holback[2], libraryId: obj.libraryId })
            })
          }
        } catch (error) {
        }

        try {
          if (item.pitchs.trim() != '') {
            item.pitchs.trim().split('),(').map(x => {
              let pitch = x.replace(')', '').replace('(', '').trim().split(',')
              let status = Status.pitchStatus().find(status => status.name == pitch[2]) ? Status.pitchStatus().find(status => status.name == pitch[2]).value : 0
              obj.pitchs.push({ platform: pitch[0], pitchDate: pitch[1], status: status, libraryId: obj.libraryId, userId: userId })
            })
          }
        } catch (error) {
        }

        try {
          if (item.keywords.trim() != '') {
            item.keywords.trim().split(',').map(x => {
              obj.keywords.push({ keyword: x, libraryId: obj.libraryId })
            })
          }
        } catch (error) {
        }

        try {
          // obj.duration && (library.duration = obj.duration)
          // obj.runtime && (library.runtime = obj.runtime) 
          // obj.format && (library.formatId = obj.format)
          obj.frameRate && (library.frameRate = obj.frameRate)
          await library.save()
        } catch (error) {
          return
        }

        if (obj.holdbacks.length > 0) {
          try {
            await Holdback.createMany(obj.holdbacks)
          } catch (error) {

          }
        }

        if (obj.pitchs.length > 0) {
          try {
            await AlternateTitlesPlatform.createMany(obj.pitchs)
          } catch (error) {
          }
        }

        if (obj.keywords.length > 0) {
          try {
            await Keyword.createMany(obj.keywords)
          } catch (error) {
          }
        }
        guIds.push(item.guid.trim())
        return obj
      })

      await Promise.all(data)

      if ((await Promise.all(errors)).length > 0) {
        // return view.render('library.importError',{data:errors,back:'/library'})
        try {
          const result = exportFromJSON({
            data: errorFile,
            fileName: 'Errors in Library Update',
            exportType: 'csv',
            processor(content, type, fileName) {
              return content
            }
          })
          let fileName = uuid() + '.csv'
          fs.writeFileSync(`public/error/${fileName}`, result)
          return view.render('library.importError', { data: errors, back: '/library', errorFile: `/error/${fileName}` })
        } catch (error) {
          console.log(error)
          session.flash({ errMsg: error.message })
          response.redirect('back')
        }
      }
      else {
        session.flash({ notification: "Data Imported Succesfully!" })
        return response.redirect('back')
      }

    } catch (error) {
      console.log(error)
      return response.redirect('back')
    }
  }


  async exportEpisode({ request, params, response, auth }) {
    try {
      let alternateName = await AlternateTitleLanguage.find(params.alternateTitleId)
      let episodes = await Episode.query().where('episodes.alternateTitleId', params.alternateTitleId).andWhere('episodes.seriesId', params.id)
        .with('studio').with('season').with('alternateName').fetch()
      let fileName = `${alternateName.alternateName}_${alternateName.guId}_episodes`
      episodes = episodes.toJSON().map(item => {
        let obj = {}
        obj.guId = item.guId
        obj.seasonNo = item.season.number
        obj.seasonName = item.season.name
        obj.episodeNo = item.number
        obj.episodeName = item.name
        obj.studio = item.studio.title
        obj.series = item.alternateName.alternateName
        obj.episodeSynopsis = item.alterSynopsis
        return obj
      })
      try {
        const result = exportFromJSON({
          data: episodes,
          fileName: fileName,
          exportType: 'csv',
          processor(content, type, fileName) {
            switch (type) {
              case 'txt':
                response.header('Content-Type', 'text/plain')
                break
              case 'json':
                response.header('Content-Type', 'text/plain')
                break
              case 'csv':
                response.header('Content-Type', 'text/csv')
                break
              case 'xls':
                response.header('Content-Type', 'application/vnd.ms-excel')
                break
            }
            response.header('Content-disposition', 'attachment;filename=' + fileName)
            return content
          }
        })
        return response.send(result)
      } catch (error) {
        return response.redirect('back')
      }

    } catch (error) {
      console.log(error)
      return response.redirect('back')
    }
  }

  async indexSeasons({ request, view, params, auth }) {
    try {
      let seasons = await Season.query().where('seriesId', params.id).andWhere('alternateTitleId', params.alternateTitleId).fetch()
      let alternateName = await AlternateTitleLanguage.find(params.alternateTitleId)
      return view.render('library.seasons.index', { seasons: seasons.toJSON(), alternateName: alternateName })
    } catch (error) {

    }
  }

  async updateSeason({ request, session, response, params }) {
    try {
      let data = request.all()
      let season = await Season.find(params.seasonId)
      season.name = data.name
      season.number = data.number
      season.alterSynopsis = data.alterSynopsis
      await season.save()
      session.flash({ notification: `Season Updated!` });
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: `Season Update Failed` });
      return response.redirect('back')
    }
  }

  async editSeason({ request, view, response, params }) {
    try {
      let season = await Season.find(params.seasonId)
      let alternateName = await AlternateTitleLanguage.find(params.alternateTitleId)
      return view.render('library.seasons.edit', { season: season.toJSON(), alternateName: alternateName })
    } catch (error) {
      return response.redirect('back')
    }
  }

  async addSeason({ request, view, response, params }) {
    try {
      let alternateName = await AlternateTitleLanguage.find(params.alternateTitleId)
      return view.render('library.seasons.add', { alternateName: alternateName })
    } catch (error) {
      return response.redirect('back')
    }
  }

  async storeSeason({ request, session, response, auth, params }) {
    try {
      let data = request.all()
      let library = await Library.find(params.id)
      let guId = library.guId + '_' + (((library.title).match(/\b(\w)/g)).join('')) + '_s' + data.number
      let incoming = await Incoming.findBy('alternateTitleId', params.alternateTitleId)
      let season = await Season.create({ guId: guId, name: data.name, number: data.number, alterSynopsis: data.alterSynopsis, alternateTitleId: params.alternateTitleId, seriesId: params.id, imdb: library.imdb, studioId: library.studioId, ratingId: library.ratingId, userId: (await auth.getUser()).id })
      await IncomingSeason.create({ incomingId: incoming.id, seriesId: library.id, seasonId: season.id, alternateTitleId: params.alternateTitleId })
      session.flash({ notification: `Season created Successfully!` });
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: `Season creation Failed!` });
      return response.redirect('back')
    }
  }

  async deleteSeason({ request, session, response, params }) {
    try {
      console.log(params)
      let season = await Season.find(params.seasonId)
      await season.delete()
      session.flash({ notification: `Season No.${season.number} Deleted!` });
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: `Season No.${season.number} Delete Failed` });
      return response.redirect('back')
    }
  }


}

module.exports = LibraryController;
