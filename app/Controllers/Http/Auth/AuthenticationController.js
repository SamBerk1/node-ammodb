'use strict'

class AuthenticationController {

  async login ({request,view, response, auth, session}) {
    const userData = request.only(['email', 'password','remember'])
    try {
      await auth.attempt(userData.email,userData.password)
      return response.redirect('/')
    } catch (error) {
      if (error.message.includes("Cannot login multiple users")) {
        return  response.redirect('/')
      }
      return view.render('Auth.signin',{ signinErr:(error.message.split(':')[1])})
    }
  }

  async logout ({request, response, auth, session}) {
    try {
      session.clear()
      return response.redirect('/')
    }catch (error) {
      session.flash({ logout:(error.message.split(':')[1])})
      return response.redirect('/')
    }
  }

  async updatePassword ({request, response, auth, session}) {
    try {


      let data = {password:request.input('password'),password_confirm: request.input('password_confirm')}
      if (data.password == data.password_confirm) {
        let user = await auth.getUser()
        user.password = data.password
        user.isNew = 0
        await user.save()
        if (user.toJSON().role.slug != 'client_user') {  
            session.flash({ notification: 'Password Updated' })
            return response.redirect('/library')
        } else {
            session.flash({ notification: 'Password Updated' })
            return response.redirect('/incoming')
        }
        
      } else {
        session.flash({ err: 'Confirm Password Does not Match' })
        session.flashAll()
        return response.redirect('back')
      }
    }catch (error) {
      session.flash({ logout:(error.message.split(':')[1])})
      return response.redirect('back')
    }
  }
}

module.exports = AuthenticationController
