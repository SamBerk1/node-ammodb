'use strict'
const Language = use('App/Models/Language')
class LanguageController {
  //Get All
  async index({ request, response , view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10 

      let dataQuery = Language.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn,'LIKE',`%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby,query.order)
      }
      
      const languages = await dataQuery.paginate(parseInt(page),parseInt(perpage))
      const searchColumns = [{name:'Name',value:'name'},{name:'Code',value:'code'}]
      
      return view.render('others.forms.languages.languages', {
        title: 'Languages',
        data: languages.toJSON(),
        order:{column:query.orderby,order:query.order},
        search:{text:query.search,column:query.searchcolumn},
        searchColumns:searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }

  }
  


  async add({ view }) {
    return view.render('others.forms.languages.add')
  }

  async store({ request, response, session }) {


    const language = new Language()

    language.name = request.input('name')
    language.code = request.input('code')

    
    try {
      await language.save()
      session.flash({ notification: 'Language Added!' })
      return response.redirect('back')
    } catch (error) {
        session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
        return response.redirect('back')
    }     
  }

//edit
  async edit({ params, view }) {
    const language = await Language.find(params.id)
    
    return view.render('others.forms.languages.edit', {
      data: language
    })
  }
//Update
  async update({ params, request, response, session }) { 


    const language = await Language.find(params.id)

    language.name = request.input('name')
    language.code = request.input('code')

    await language.save()

    session.flash({ notification: 'Language Updated!' })

    return response.redirect('back')
  }

//Delete
async remove({ params, response, session }){
    const language = await Language.find(params.id)
    await language.delete()
    session.flash({ notification:"Language deleted successfully!!" })
    return  response.redirect('back')
}
}

module.exports = LanguageController
