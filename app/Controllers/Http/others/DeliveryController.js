'use strict'

const Delivery = use('App/Models/Delivery')
const Library = use('App/Models/Library')
class DeliveryController {
  
     //Get All
     async index({ request, response , view }) {
      const Deliverys = await Delivery.query().with('library').fetch()
         
         
         
        return view.render('others.forms.delivery.delivery', {
          title: 'Latest Deliverys',
          data: Deliverys.toJSON(),
           
        })
  
      }
      

    
      async add({ view }) {
        const library = await Library.all()
        let status = [
          {name:"Video", value:"video"},
          {name:"Captions", value:"captions"},
          {name:"Artwork", value:"artwork"},
          {name:"Meta", value:"meta"},
        ] 
        return view.render('others.forms.delivery.add',{data:{
          status:status,
          library:library.toJSON()
        }})
      }
    
      async store({ request, response, session }) {
    
    
        const delivery = new Delivery()
      
        delivery.deliveryDate = request.input('deliveryDate')
        delivery.launchDate = request.input('launchDate')
        delivery.status = request.input('status')
        // delivery.libraryId = request.input('libraryId')
        request.input("libraryId") != 'null' ? (delivery["libraryId"] = request.input("libraryId")) : "";
        
        try {
          await delivery.save()
          session.flash({ notification: 'Delivery Added!' })
          return response.redirect('back')
        } catch (error) {
          session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
          return response.redirect('back')
        }        
      }
    
    //edit
      async edit({ params, view }) {
        const library = await Library.all()
        const delivery = await Delivery.find(params.id)
        let status = [
          {name:"Video", value:"video"},
          {name:"Captions", value:"captions"},
          {name:"Artwork", value:"artwork"},
          {name:"Meta", value:"meta"},
        ] 
        return view.render('others.forms.delivery.edit', {data:{
          delivery:delivery.toJSON(),
          status:status,
          library:library.toJSON()
        }})
      }
    //Update
      async update({ params, request, response, session }) { 
  
    
        const delivery = await Delivery.find(params.id)
    
        delivery.deliveryDate = request.input('deliveryDate')
        delivery.launchDate = request.input('launchDate')
        delivery.status = request.input('status')
        // delivery.libraryId = request.input('libraryId')
        request.input("libraryId") != 'null' ? (delivery["libraryId"] = request.input("libraryId")) : "";

        await delivery.save()
    
        session.flash({ notification: 'Delivery Updated!' })
    
        return response.redirect('/others/delivery')
      }
    
    //Delete
    async remove({ params, response, session }){
        const delivery = await Delivery.find(params.id)
        await delivery.delete()
        session.flash({ notification:"Delivery deleted successfully!!" })
        return  response.redirect('/others/delivery')
    }

}

module.exports = DeliveryController
