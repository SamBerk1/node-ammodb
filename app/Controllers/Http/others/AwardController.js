'use strict'

const Award = use('App/Models/Award')

class AwardController {
  //Get All
  async index({ request, response, view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10
      console.log(query)

      let dataQuery = Award.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const Awards = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Name', value: 'awardName' }, { name: 'Type', value: 'awardType' }]

      return view.render('others.forms.award.award', {
        title: 'Latest Awards',
        data: Awards.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      console.log(error)
      return response.redirect('back')
    }
  }



  async add({ view }) {
    return view.render('others.forms.award.add')
  }

  async store({ request, response, session }) {

    const award = new Award()

    award.awardName = request.input('awardName')
    award.awardType = request.input('awardType')

    try {
      await award.save()
      session.flash({ notification: 'Award Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }
  }

  //edit
  async edit({ params, view }) {
    const award = await Award.find(params.id)

    return view.render('others.forms.award.edit', {
      data: award
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const award = await Award.find(params.id)

    award.awardName = request.input('awardName')
    award.awardType = request.input('awardType')

    await award.save()

    session.flash({ notification: 'Award Updated!' })

    return response.redirect('/others/award')
  }

  //Delete
  async remove({ params, response, session }) {
    const award = await Award.find(params.id)
    await award.delete()
    session.flash({ notification: "Award deleted successfully!!" })
    return response.redirect('/others/award')
  }

}

module.exports = AwardController
