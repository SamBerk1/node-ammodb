'use strict'

const Cast = use('App/Models/Cast')


class CastController {
  //Get All
  async index({ request, response, view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Cast.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const Casts = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Name', value: 'actorName' }]


      return view.render('others.forms.cast.cast', {
        title: 'Latest Casts',
        data: Casts.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }
  }



  async add({ view }) {
    return view.render('others.forms.cast.add')
  }

  async store({ request, response, session }) {


    const cast = new Cast()

    cast.actorName = request.input('actorName')

    try {
      await cast.save()
      session.flash({ notification: 'Cast Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }
  }

  //edit
  async edit({ params, view }) {
    const cast = await Cast.find(params.id)

    return view.render('others.forms.cast.edit', {
      data: cast
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const cast = await Cast.find(params.id)

    cast.actorName = request.input('actorName')

    await cast.save()

    session.flash({ notification: 'Cast Updated!' })

    return response.redirect('/others/cast')
  }

  //Delete
  async remove({ params, response, session }) {
    const cast = await Cast.find(params.id)
    await cast.delete()
    session.flash({ notification: "Cast deleted successfully!!" })
    return response.redirect('/others/cast')
  }

}

module.exports = CastController
