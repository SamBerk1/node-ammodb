'use strict'
const Territory = use('App/Models/Territory')
const TerritoryCountry = use('App/Models/TerritoryCountry')
class TerritoryController {
  //Get All
  async index({ request, response, view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Territory.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const Territorys = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Territory', value: 'territoryname' }]

      return view.render('others.forms.territory.territory', {
        title: 'Latest Territorys',
        data: Territorys.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }
  }

  async add({ view }) {
    return view.render('others.forms.territory.add')
  }

  async store({ request, response, session }) {


    const territory = new Territory()

    territory.territoryName = request.input('territoryName')


    try {
      await territory.save()
      session.flash({ notification: 'Territory Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }

  }

  //edit
  async edit({ params, view }) {
    const territory = await Territory.query().where('id', params.id).with('countries').first()
    // return territory
    return view.render('others.forms.territory.edit', {
      data: territory.toJSON()
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const territory = await Territory.find(params.id)

    territory.territoryName = request.input('territoryName')

    await territory.save()

    session.flash({ notification: 'Territory Updated!' })

    return response.redirect('/others/territory')
  }

  //Delete
  async remove({ params, response, session }) {
    const territory = await Territory.find(params.id)
    await territory.delete()
    session.flash({ notification: "Territory deleted successfully!!" })
    return response.redirect('/others/territory')
  }

  async storeCountry({ request, response, session, params }) {

    const country = new TerritoryCountry()

    country.territoryId = params.id
    country.country = request.input('country')


    try {
      await country.save()
      session.flash({ notification: 'Territory Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }

  }

  //Delete Country
  async removeCountry({ params, response, session }) {

    const country = await TerritoryCountry.query().where('id', params.id).where('territoryId', params.territoryId).first()
    try {
      await country.delete()
      session.flash({ notification: "Country deleted successfully!!" })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: "Country Failed to delete!!" })
      return response.redirect('back')
    }
  }

  //Update Country
  async updateCountry({ request, params, response, session }) {
    const country = await TerritoryCountry.query().where('id', params.id).where('territoryId', params.territoryId).first()
    country.country = request.input('country')
    try {
      await country.save()
      session.flash({ notification: "Country Updated successfully!!" })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: "Country Failed to update!!" })
      return response.redirect('back')
    }
  }

}

module.exports = TerritoryController
