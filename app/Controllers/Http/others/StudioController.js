'use strict'
// Bring in model
const Studio = use('App/Models/Studio')
 
// Bring in validator
const { validate } = use('Validator') 

class StudioController {
      //Get All
  async index({ request, response , view }) {
     
    try {

      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10 

      let dataQuery = Studio.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn,'LIKE',`%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby,query.order)
      }
      
      const studios = await dataQuery.paginate(parseInt(page),parseInt(perpage))
      const searchColumns = [{name:'Title',value:'title'},{name:'Location',value:'location'},{name:'Contact',value:'contactNumber'},{name:'Email',value:'contactEmail'}]     
     
      return view.render('others.forms.studio.studio', {
        title: 'Latest Studios',
        data: studios.toJSON(),
        order:{column:query.orderby,order:query.order},
        search:{text:query.search,column:query.searchcolumn},
        searchColumns:searchColumns,
      })
    } catch (error) {
      response.redirect('back')
    }
  }
  
  async add({ view }) {
    return view.render('others.forms.studio.add')
  }

  async store({ request, response, session }) {

    const studio = new Studio()
    studio.title = request.input('title')
    studio.location = request.input('location')
    studio.contactNumber = request.input('contactNumber')
    studio.contactEmail = request.input('contactEmail')
    studio.address = request.input('address')

    try {
      await studio.save()
      session.flash({ notification: 'Studio Added!' })
      return response.redirect('back')
    } catch (error) { 
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }

  }

//edit
  async edit({ params, view }) {
    const studio = await Studio.find(params.id)
    
    return view.render('others.forms.studio.edit', {
      data: studio
    })
  }
//Update
  async update({ params, request, response, session }) { 

    const studio = await Studio.find(params.id)

    studio.title = request.input('title')
    studio.location = request.input('location')
    studio.contactNumber = request.input('contactNumber')
    studio.contactEmail = request.input('contactEmail')
    studio.address = request.input('address')

    try {
      await studio.save()
      session.flash({ notification: 'Studio Updated!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }
  }

//Delete
async remove({ params, response, session }){
    const studio =await Studio.find(params.id)
    await studio.delete()
    session.flash({ notification:"Studio deleted successfully!!" })
    return  response.redirect('/others/studio')
}
}

module.exports = StudioController



