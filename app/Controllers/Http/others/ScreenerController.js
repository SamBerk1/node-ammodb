'use strict'
const Screener = use('App/Models/Screener')
class ScreenerController {
  //Get All
  async index({ request, response, view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Screener.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const Screeners = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Name', value: 'name' }, { name: 'Link', value: 'screenerLink' }]

      return view.render('others.forms.screener.screener', {
        title: 'Latest Screeners',
        data: Screeners.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }
  }



  async add({ view }) {
    return view.render('others.forms.screener.add')
  }

  async store({ request, response, session }) {


    const screener = new Screener()

    screener.screenerLink = request.input('screenerLink')
    screener.screenerPW = request.input('screenerPW')
    screener.name = request.input('name')


    try {
      await screener.save()
      session.flash({ notification: 'Screener Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }

  }

  //edit
  async edit({ params, view }) {
    const screener = await Screener.find(params.id)

    return view.render('others.forms.screener.edit', {
      data: screener
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const screener = await Screener.find(params.id)

    screener.screenerLink = request.input('screenerLink')
    screener.screenerPW = request.input('screenerPW')
    screener.name = request.input('name')

    await screener.save()

    session.flash({ notification: 'Screener Updated!' })

    return response.redirect('/others/screener')
  }

  //Delete
  async remove({ params, response, session }) {
    const screener = await Screener.find(params.id)
    await screener.delete()
    session.flash({ notification: "Screener deleted successfully!!" })
    return response.redirect('/others/screener')
  }

}

module.exports = ScreenerController
