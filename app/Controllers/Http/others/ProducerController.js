'use strict'

const Producer = use('App/Models/Producer')


class ProducerController {

  //Get All
  async index({ request, response, view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Producer.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const producers = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Name', value: 'name' }]


      return view.render('others.forms.producer.producer', {
        title: 'Latest Producers',
        data: producers.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {

    }

  }



  async add({ view }) {
    return view.render('others.forms.producer.add')
  }

  async store({ request, response, session }) {


    const producer = new Producer()

    producer.name = request.input('name')


    try {
      await producer.save()
      session.flash({ notification: 'Producer Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }
  }

  //edit
  async edit({ params, view }) {
    const producer = await Producer.find(params.id)

    return view.render('others.forms.producer.edit', {
      data: producer
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const producer = await Producer.find(params.id)

    producer.name = request.input('name')

    await producer.save()

    session.flash({ notification: 'Producer Updated!' })

    return response.redirect('/others/producer')
  }

  //Delete
  async remove({ params, response, session }) {
    const producer = await Producer.find(params.id)
    await producer.delete()
    session.flash({ notification: "Producer deleted successfully!!" })
    return response.redirect('/others/producer')
  }

}

module.exports = ProducerController
