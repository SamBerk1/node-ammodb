'use strict'
const Genre = use('App/Models/Genre')
class GenreController {
  //Get All
  async index({ request, response, view }) {

    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Genre.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const Genres = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Title', value: 'title' }]

      return view.render('others.forms.genre.genre', {
        title: 'Latest Genres',
        data: Genres.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }
  }



  async add({ view }) {
    return view.render('others.forms.genre.add')
  }

  async store({ request, response, session }) {


    const genre = new Genre()

    genre.title = request.input('title')
    try {
      await genre.save()
      session.flash({ notification: 'Genre Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }

  }

  //edit
  async edit({ params, view }) {
    const genre = await Genre.find(params.id)

    return view.render('others.forms.genre.edit', {
      data: genre
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const genre = await Genre.find(params.id)

    genre.title = request.input('title')

    await genre.save()

    session.flash({ notification: 'Genre Updated!' })

    return response.redirect('/others/genre')
  }

  //Delete
  async remove({ params, response, session }) {
    const genre = await Genre.find(params.id)
    await genre.delete()
    session.flash({ notification: "Genre deleted successfully!!" })
    return response.redirect('/others/genre')
  }

}

module.exports = GenreController
