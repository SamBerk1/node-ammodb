'use strict'
const Subtitle = use('App/Models/Subtitle')
class SubtitleController {
     //Get All
     async index({ request, response , view }) {
       
         
        const Subtitles = await Subtitle.all()
         
         
        return view.render('others.forms.subtitle.subtitle', {
          title: 'Latest Subtitles',
          data: Subtitles.toJSON(),
           
        })
  
      }
      

    
      async add({ view }) {
        return view.render('others.forms.subtitle.add')
      }
    
      async store({ request, response, session }) {
    
    
        const subtitle = new Subtitle()
    
        subtitle.subtitleLanguage = request.input('subtitleLanguage')
    
        
        try {
          await subtitle.save()
          session.flash({ notification: 'Subtitle Added!' })
          return response.redirect('back')
        } catch (error) {
          session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
          return response.redirect('back')
        } 
    
        
      }
    
    //edit
      async edit({ params, view }) {
        const subtitle = await Subtitle.find(params.id)
        
        return view.render('others.forms.subtitle.edit', {
          data: subtitle
        })
      }
    //Update
      async update({ params, request, response, session }) { 
  
    
        const subtitle = await Subtitle.find(params.id)
    
        subtitle.subtitleLanguage = request.input('subtitleLanguage')
    
        await subtitle.save()
    
        session.flash({ notification: 'Subtitle Updated!' })
    
        return response.redirect('/others/subtitle')
      }
    
    //Delete
    async remove({ params, response, session }){
        const subtitle = await Subtitle.find(params.id)
        await subtitle.delete()
        session.flash({ notification:"Subtitle deleted successfully!!" })
        return  response.redirect('/others/subtitle')
    }

}

module.exports = SubtitleController
