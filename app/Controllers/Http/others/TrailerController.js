'use strict'
const Trailer = use('App/Models/Trailer')
class TrailerController {
  //Get All
  async index({ request, response, view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Trailer.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const Trailers = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Name', value: 'name' }, { name: 'Url', value: 'trailerUrl' }]

      return view.render('others.forms.trailer.trailer', {
        title: 'Latest Trailers',
        data: Trailers.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }
  }



  async add({ view }) {
    return view.render('others.forms.trailer.add')
  }

  async store({ request, response, session }) {


    const trailer = new Trailer()

    trailer.trailerUrl = request.input('trailerUrl')
    trailer.name = request.input('name')


    try {
      await trailer.save()
      session.flash({ notification: 'Trailer Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }
  }

  //edit
  async edit({ params, view }) {
    const trailer = await Trailer.find(params.id)

    return view.render('others.forms.trailer.edit', {
      data: trailer
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const trailer = await Trailer.find(params.id)

    trailer.trailerUrl = request.input('trailerUrl')
    trailer.name = request.input('name')

    await trailer.save()

    session.flash({ notification: 'Trailer Updated!' })

    return response.redirect('/others/trailer')
  }

  //Delete
  async remove({ params, response, session }) {
    const trailer = await Trailer.find(params.id)
    await trailer.delete()
    session.flash({ notification: "Trailer deleted successfully!!" })
    return response.redirect('/others/trailer')
  }

}

module.exports = TrailerController
