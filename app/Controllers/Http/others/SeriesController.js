'use strict'
const Series = use('App/Models/Series')

const uuid = require('uuid-random');


class SeriesController {
    /**
     * Display a listing of the resource.
     */
    async index({request, view,response}) {
        try {
            const series = await Series.all()
            return view.render('others.forms.series.series',{data:series.toJSON()})
        } catch (error) {
            response.redirect('back')
        }
    }

    async add({view}){
        return view.render('others.forms.series.add',{guid:uuid()})
    }
 
    /**
     * Store a newly created resource in storage.
     */
    async store({request, session,response}) {
        try {
            const series = new Series()
            series.title = request.input('title')
            series.seasons = request.input('seasons')
            series.guid = request.input('guid')
            await series.save()
            session.flash({ notification: 'Series Added!' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
            return response.redirect('back')
        }
    }

    async edit({request,params,view,response}){
        try {
            const series = await Series.find(params.id)
            return view.render('others.forms.series.edit',{data:series.toJSON()})
        } catch (error) {
            return response.redirect('back')
        }
    }

    async update({request,response,params}){
        try {
            const series = await Series.find(params.id)
            series.title = request.input('title')
            series.seasons = request.input('seasons')
            series.guid = request.input('guid')
            await series.save()
            response.redirect('/others/series')
        } catch (error) {
            response.redirect('back')
        }
    }
    

    async remove({request,params,response}){
        try {
            const series = await Series.find(params.id)
            await series.delete()
            response.redirect('back')
        } catch (error) {
            response.redirect('back')
        }
    }

    async selectAdd({request,response,params,view,session}){
        if(params.is=='selected'){
            response.redirect('/library/add/episode/series/'+request.input('seriesId'))
        }

        if(params.is=='added'){
            try {
                const series = new Series()
                series.title = request.input('title')
                series.seasons = request.input('seasons')
                series.guid = request.input('guid')
                await series.save()
                response.redirect('/library/add/episode/series/'+(series.toJSON()).id)

            } catch (error) {
                session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
                return response.redirect('back')
            }
        }
    }


    async selectEdit({request,response,params,view,session}){
       
        if(params.is=='selected'){
            // library/edit/:is/:id/series/:seriesid
            response.redirect('/library/edit/Episode/'+params.id+'/series/'+request.input('seriesId'))
        }

        if(params.is=='added'){
            try {
                const series = new Series()
                series.title = request.input('title')
                series.seasons = request.input('seasons')
                series.guid = request.input('guid')
                await series.save()
                response.redirect('/library/edit/Episode/'+params.id+'/series/'+(series.toJSON()).id)

            } catch (error) {
                session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
                return response.redirect('back')
            }
        }
    }
    
}

module.exports = SeriesController
