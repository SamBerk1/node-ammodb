'use strict'
const Right = use('App/Models/Right')
class RightController {
  //Get All
  async index({ request, response, view }) {

    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Right.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const Rights = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Name', value: 'name' }, { name: 'Descritpion', value: 'descritpion' }]

      return view.render('others.forms.rights.rights', {
        title: 'Latest Rights',
        data: Rights.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }
  }



  async add({ view }) {
    return view.render('others.forms.rights.add')
  }

  async store({ request, response, session }) {


    const right = new Right()

    right.name = request.input('name')
    right.description = request.input('description')


    try {
      await right.save()
      session.flash({ notification: 'Right Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }
  }

  //edit
  async edit({ params, view }) {
    const right = await Right.find(params.id)

    return view.render('others.forms.rights.edit', {
      data: right
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const right = await Right.find(params.id)

    right.name = request.input('name')
    right.description = request.input('description')

    await right.save()

    session.flash({ notification: 'Right Updated!' })

    return response.redirect('/others/right')
  }

  //Delete
  async remove({ params, response, session }) {
    const right = await Right.find(params.id)
    await right.delete()
    session.flash({ notification: "Right deleted successfully!!" })
    return response.redirect('/others/right')
  }

}

module.exports = RightController
