const Episode = use('App/Models/Episode')

class EpisodeController {

    async getEpisodes({request,response,params}){
        try {
            let episodes = await Episode.query().where('incomingId',params.id).fetch()
            return response.json({data:episodes.toJSON(),success:true})
        } catch (error) {
            return response.json({data:null,success:false})
        }
    }
}
module.exports = EpisodeController