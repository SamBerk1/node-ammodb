'use strict'
const Format = use('App/Models/Format')
class FormatController {
  //Get All
  async index({ request, response, view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Format.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby, query.order)
      }

      const Formats = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Format', value: 'format' }]

      return view.render('others.forms.formats.formats', {
        title: 'Latest Formats',
        data: Formats.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }
  }



  async add({ view }) {
    return view.render('others.forms.formats.add')
  }

  async store({ request, response, session }) {


    const format = new Format()

    format.format = request.input('format')
    try {
      await format.save()
      session.flash({ notification: 'Format Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }
  }

  //edit
  async edit({ params, view }) {
    const format = await Format.find(params.id)

    return view.render('others.forms.formats.edit', {
      data: format
    })
  }
  //Update
  async update({ params, request, response, session }) {


    const format = await Format.find(params.id)

    format.format = request.input('format')

    await format.save()

    session.flash({ notification: 'Format Updated!' })

    return response.redirect('/others/format')
  }

  //Delete
  async remove({ params, response, session }) {
    const format = await Format.find(params.id)
    await format.delete()
    session.flash({ notification: "Format deleted successfully!!" })
    return response.redirect('/others/format')
  }

}

module.exports = FormatController
