'use strict'

const AdBreak = use('App/Models/AdBreak')
const Platform = use('App/Models/Platform')
const Library = use('App/Models/Library')
const Timecode = require('smpte-timecode')
const moment = require("moment");
const momentDurationFormatSetup = require("moment-duration-format");

const adbreakFormat = [
  { name: "SMPTE Timecode", value: "smpte", format: 'hh:mm:ss:ms' },
  { name: "ROKU Ad Break", value: "roku", format: 'hh:mm:ss' },
  { name: "XUMO Ad Break", value: "xumo", format: 'sss' },
  { name: "Ad Break Milliseconds", value: "milliseconds", format: 'ms' }
]


function formatTime(time, formatName, format, frameRate) {

  let timeCode = Timecode(time, parseFloat(frameRate))

  if (formatName != 'SMPTE Timecode') {
    let timeSeconds = Math.floor(timeCode.frameCount / timeCode.frameRate)
    return moment.duration(timeSeconds, "seconds").format(format, {
      trim: false
    });
  }
  else {
    return time
  }
}

class AdBreakController {
  //Get All
  async index({ request, response, params, view }) {

    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = AdBreak.query().with('library', (builder) => builder.select('id', 'title')).with('platform')
      if (query.search && query.searchcolumn) {
        const searchColumn = query.searchcolumn.toLowerCase()
        switch (searchColumn) {
          case 'title':
            dataQuery.whereHas('library', (builder) => builder.where('title', 'LIKE', `%${query.search}%`))
            break;
          case 'platform':
            dataQuery.whereHas('platform', (builder) => builder.where('name', 'LIKE', `%${query.search}%`))
            break;
          default:
            dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
            break;
        }
      }

      if (query.order && query.orderby) {
        const orderBy = query.orderby.toLowerCase()
        switch (orderBy) {
          case 'title':
            dataQuery.innerJoin('libraries', 'ad_breaks.libraryId', 'libraries.id').orderBy('libraries.title', query.order)
            break;
          case 'platform':
            dataQuery.innerJoin('platforms', 'ad_breaks.platformId', 'platforms.id').orderBy('platforms.name', query.order)
            break;
          default:
            dataQuery.orderBy(query.orderby, query.order)
            break;
        }
      }

      const adBreaks = await dataQuery.paginate(parseInt(page), parseInt(perpage))
      const searchColumns = [{ name: 'Title', value: 'title' }, { name: 'Adbreak Format', value: 'adbreakFormat' }, { name: 'Platform', value: 'platform' }]

      return view.render('others.forms.adBreak.adBreak', {
        title: 'Latest AdBreaks',
        data: adBreaks.toJSON(),
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      return response.redirect('back')
    }



  }

  async add({ view }) {
    const platforms = await Platform.all()
    const library = await Library.query().select('id', 'title').fetch()

    return view.render('others.forms.adBreak.add',
      {
        data: {
          platforms: platforms.toJSON(),
          library: library.toJSON(),
          adbreakFormat: adbreakFormat
        }
      }
    )
  }

  async store({ request, response, session }) {

    const adBreak = new AdBreak()
    adBreak.time = request.input('time')
    request.input("libraryId") == 'null' ? (adBreak.libraryId = null) : (adBreak.libraryId = request.input("libraryId"))
    request.input("platformId") == 'null' ? (adBreak.platformId = null) : (adBreak.platformId = request.input("platformId"))
    adBreak.adbreakFormat = (request.input('adbreakFormat').split('_'))[1]

    let frameRate = ((await Library.find(adBreak.libraryId)).toJSON()).frameRate

    adBreak.timeFormated = formatTime(adBreak.time, adBreak.adbreakFormat, (request.input('adbreakFormat').split('_'))[0], frameRate)




    try {
      await adBreak.save()
      session.flash({ notification: 'AdBreak Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }

  }

  //edit
  async edit({ params, view }) {
    const platforms = await Platform.all()
    const library = await Library.query().select('id', 'title').fetch()
    const adBreak = await AdBreak.query().where('id', params.id).with('library').with('platform').fetch()
    // const adBreak = await AdBreak.find(params.id

    return view.render('others.forms.adBreak.edit', {
      data: adBreak.toJSON(),
      formData: {
        platforms: platforms.toJSON(),
        library: library.toJSON(),
        adbreakFormat: adbreakFormat
      }
    })
  }
  //Update
  async update({ params, request, response, session }) {

    let adBreak = await AdBreak.find(params.id)

    adBreak['time'] = request.input('time')
    adBreak['timeFormated'] = formatTime(request.input('time'), (request.input('adbreakFormat').split('_'))[0])
    adBreak.adbreakFormat = (request.input('adbreakFormat').split('_'))[1]
    request.input("libraryId") != 'null' ? (adBreak["libraryId"] = request.input("libraryId")) : "";
    request.input("platformId") != 'null' ? (adBreak["platformId"] = request.input("platformId")) : "";


    await adBreak.save()

    session.flash({ notification: 'AdBreak Updated!' })

    return response.redirect('/others/ad-break')
  }

  //Delete
  async remove({ params, response, session }) {
    const adBreak = await AdBreak.find(params.id)
    await adBreak.delete()
    session.flash({ notification: "AdBreak deleted successfully!!" })
    return response.redirect('/others/ad-break')
  }

}

module.exports = AdBreakController
