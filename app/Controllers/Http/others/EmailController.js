'use strict'

const Email = use('App/Models/Email')
const Subscriber = use('App/Models/Subscriber')

class EmailController {
    async index({ request, view, response, auth }) {
        try {
            const query = request.get()
            let page = query.page ? query.page : 1
            let perpage = request.get().perpage ? request.get().perpage : 10

            let dataQuery = Email.query()
            if (query.search && query.searchcolumn) {
                dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
            }

            if (query.order && query.orderby) {
                dataQuery.orderBy(query.orderby, query.order)
            }

            const emails = await dataQuery.paginate(parseInt(page), parseInt(perpage))
            const searchColumns = [{ name: 'Title', value: 'title' }, { name: 'Subject', value: 'subject' }]

            return view.render('others.forms.email.index', {
                data: emails.toJSON(),
                order: { column: query.orderby, order: query.order },
                search: { text: query.search, column: query.searchcolumn },
                searchColumns: searchColumns,
            })
        } catch (error) {
            return response.redirect('back')
        }
    }

    async add({ request, view, response, auth }) {
        return view.render('others.forms.email.add')
    }

    async store({ request, session, response, auth }) {
        try {
            let data = request.all()
            let email = {
                title: data.title,
                subject: data.subject,
                content: data.content,
                userId: (await auth.getUser()).id
            }
            await Email.create(email)
            session.flash({ notification: 'Email Added!' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
            return response.redirect('back')
        }
    }

    async edit({ request, view, response, params }) {
        try {
            let email = await Email.find(params.id)
            return view.render('others.forms.email.edit', { data: email.toJSON() })
        } catch (error) {
            return response.redirect('back')
        }
    }

    async update({ request, session, response, params }) {
        try {
            let data = request.all()
            let email = await Email.find(params.id)
            email.subject = data.subject
            email.content = data.content
            await email.save()
            session.flash({ notification: 'Email Updated!' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: 'Email Updated Failed!' })
            return response.redirect('back')
        }
    }

    async subscribers({ view, params }) {
        try {
            let subscribers = await Subscriber.query().where('emailId', params.emailId).with('emailTemplate').fetch()
            return view.render('others.forms.subscribers.index', { data: subscribers.toJSON(), back: '/others/emails', add: `/others/emails/${params.emailId}/subscribers/add`, edit: `/others/emails/${params.emailId}/subscribers/edit` })
        } catch (error) {
            console.log(error)
        }
    }
}

module.exports = EmailController
