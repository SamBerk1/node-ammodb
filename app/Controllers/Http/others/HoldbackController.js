'use strict'
const Holdback = use('App/Models/Holdback')
const Library = use('App/Models/Library')
class HoldbackController {
  //Get All
  async index({ request, response, params, view }) {
    try {
      let data = {}
      const query = request.get()
      const searchColumns = [{ name: 'Title', value: 'title' }, { name: 'Name', value: 'name' }, { name: 'Start Date', value: 'startDate' }, { name: 'End Date', value: 'endDate' }]

      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10

      let dataQuery = Holdback.query().with('library', (builder) => builder.select('id', 'title'))
      if (query.search && query.searchcolumn) {
        if (query.searchcolumn.toLowerCase() == 'title') {
          dataQuery.whereHas('library', (builder) => builder.select('id', 'title').where(query.searchcolumn, 'LIKE', `%${query.search}%`))
        } else {
          dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
        }
      }

      if (query.order && query.orderby) {
        if (query.orderby.toLowerCase() == 'title') {
          dataQuery.innerJoin('libraries', 'holdbacks.libraryId', 'libraries.id').orderBy('libraries.title', query.order)
        } else {
          dataQuery.orderBy(query.orderby, query.order)
        }
      }

      if (params.libraryId) {
        data.holdbacks = (await dataQuery.where('libraryId', params.libraryId).paginate(parseInt(page), parseInt(perpage))).toJSON()
        data.library = (await Library.query().where('id', params.libraryId).select('id', 'title').fetch()).toJSON()
        data.back = '/library'
      }
      else {
        data.holdbacks = (await dataQuery.paginate(parseInt(page), parseInt(perpage))).toJSON()
        data.library = (await Library.query().select('id', 'title').fetch()).toJSON()
      }

      return view.render('others.forms.holdback.holdback', {
        title: 'Latest Holdbacks',
        data: data,
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
      })
    } catch (error) {
      console.log(error)
      return response.redirect('back')
    }

  }

  async store({ request, response, session }) {
    try {
      let data = request.all()
      await Holdback.create({ name: data.name, libraryId: data.libraryId, startDate: data.startDate, endDate: data.endDate })
      session.flash({ notification: 'Holdback Added!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
      return response.redirect('back')
    }
  }
  //Update
  async update({ params, request, response, session }) {

    try {
      let data = request.all()
      const holdback = await Holdback.find(params.id)
      holdback.name = data.name
      holdback.libraryId = data.libraryId
      holdback.startDate = data.startDate
      holdback.endDate = data.endDate
      await holdback.save()
      session.flash({ notification: 'Holdback Updated!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: 'Holdback Update Failed!' })
      return response.redirect('back')
    }
  }

  //Delete
  async remove({ params, response, session }) {
    try {
      const holdback = await Holdback.find(params.id)
      await holdback.delete()
      session.flash({ notification: "Holdback deleted successfully!!" })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: 'Holdback Delete Failed!' })
      return response.redirect('back')
    }
  }

}

module.exports = HoldbackController
