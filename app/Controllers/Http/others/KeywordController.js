'use strict'
const Keyword = use('App/Models/Keyword')
const Library = use('App/Models/Library')
const Platform = use('App/Models/Platform')
class KeywordController {
     //Get All
  async index({ request, response , view }) {
    try {
      const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10 

      let dataQuery = Keyword.query().with('library',(builder) => builder.select('id','title'))
      if (query.search && query.searchcolumn) {
        if (query.searchcolumn.toLowerCase() == 'title') {
          dataQuery.whereHas('library', (builder) => builder.select('id','title').where('title','LIKE',`%${query.search}%` ))
        }
        else{
          dataQuery.where(query.searchcolumn,'LIKE',`%${query.search}%`)
        }
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby,query.order)
      }
      
      const Keywords = await dataQuery.paginate(parseInt(page),parseInt(perpage))
      const searchColumns = [{name:'Title',value:'title'},{name:'Keyword',value:'keyword'},{name:'LibraryId',value:'libraryId'}]
      
      return view.render('others.forms.keyword.keyword', {
        title: 'Latest Keywords',
        data: Keywords.toJSON(),
        order:{column:query.orderby,order:query.order},
        search:{text:query.search,column:query.searchcolumn},
        searchColumns:searchColumns,
      })
    } catch (error) {
      console.log(error)
      return response.redirect('back')
    }
  
  }
      


  async add({ view }) {
    const platforms = await Platform.all()
    const library = await Library.all()
    return view.render('others.forms.keyword.add',{
      data:{
        platform:platforms.toJSON(),
        library:library.toJSON(),
        order:{column:query.orderby,order:query.order},
        search:{text:query.search,column:query.searchcolumn},
        searchColumns:searchColumns,
      }
    })
  }

  async store({ request, response, session }) {


    const keyword = new Keyword()
    
    keyword.keyword = request.input('keyword')
    request.input("libraryId") != 'null' ? (keyword["libraryId"] = request.input("libraryId")) : "";
    request.input("platformId") != 'null' ? (keyword["platformId"] = request.input("platformId")) : "";

    
    try {
      await keyword.save()
      session.flash({ notification: 'Keyword Added!' })
      return response.redirect('back')
    } catch (error) {
        session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
        return response.redirect('back')
    } 
      
  }

//edit
  async edit({ params, view }) {
    const platforms = await Platform.all()
    const library = await Library.all()

    const keyword = await Keyword.find(params.id)
    
    return view.render('others.forms.keyword.edit', {
      data: {
        keyword:keyword,
        platform:platforms.toJSON(),
        library:library.toJSON()
      }
    })
  }
//Update
  async update({ params, request, response, session }) { 

    try {
      let data = request.input('keywords')

      await Keyword.query().where('libraryId',params.libraryId).where('alternateTitleId',params.alternateTitleId).where('seasonId',params.seasonId).where('episodeId',params.episodeId).delete()

      data = data.map( item => {
        let obj = {}
        obj.libraryId = params.libraryId
        obj.alternateTitleId = params.alternateTitleId
        obj.seasonId = params.seasonId
        obj.episodeId = params.episodeId
        obj.keyword = item
        return obj
      })

      await Keyword.createMany(data)

      session.flash({ notification: 'Keyword Updated!' })

      return response.redirect('back')
    } catch (error) {
      session.flash({ notification: 'Keyword Updated!' })

      return response.redirect('back')
    }
  }

//Delete
async remove({ params, response, session }){
    const keyword = await Keyword.find(params.id)
    await keyword.delete()
    session.flash({ notification:"Keyword deleted successfully!!" })
    return  response.redirect('/others/keyword')
}

async getKeywords({ request, params, response }){
  const keywords = await Keyword.query().where('libraryId',params.libraryId).where('alternateTitleId',params.alternateTitleId).where('seasonId',params.seasonId).where('episodeId',params.episodeId).fetch()
  return response.json({data:keywords.toJSON(),flag:true})
}

}

module.exports = KeywordController
