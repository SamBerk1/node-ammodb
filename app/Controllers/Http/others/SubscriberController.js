'use strict'
const Subscriber = use('App/Models/Subscriber')
const Email = use('App/Models/Email')


class SubscriberController {
    async index({ request, view, response, auth }) {
        try {
            const query = request.get()
            let page = query.page ? query.page : 1
            let perpage = request.get().perpage ? request.get().perpage : 10

            let dataQuery = Subscriber.query().with('emailTemplate')
            if (query.search && query.searchcolumn) {
                if (query.searchcolumn.toLowerCase() == 'title') {
                    dataQuery.whereHas('emailTemplate', (builder) => builder.where(query.searchcolumn, 'LIKE', `%${query.search}%`))
                } else {
                    dataQuery.where(query.searchcolumn, 'LIKE', `%${query.search}%`)
                }
            }

            if (query.order && query.orderby) {
                dataQuery.orderBy(query.orderby, query.order)
            }

            const subscribers = await dataQuery.paginate(parseInt(page), parseInt(perpage))
            const searchColumns = [{ name: 'User Email', value: 'email' }, { name: 'Template', value: 'title' }]

            return view.render('others.forms.subscribers.index', {
                data: subscribers.toJSON(),
                add: `/others/subscribers/add`,
                edit: '/others/subscribers/edit',
                order: { column: query.orderby, order: query.order },
                search: { text: query.search, column: query.searchcolumn },
                searchColumns: searchColumns,
            })
        } catch (error) {
            return response.redirect('back')
        }
    }

    async add({ request, view, response, params }) {
        let emails;

        if (params.emailId) {
            emails = await Email.query().where('id', params.emailId).fetch()
        }
        else {
            emails = await Email.all()
        }
        return view.render('others.forms.subscribers.add', { emails: emails.toJSON(), back: params.emailId ? `/others/emails/${params.emailId}/subscribers` : '/others/subscribers' })
    }

    async store({ request, session, response, params }) {
        try {
            let data = request.all()
            let subcriber = {
                email: data.email,
                emailId: data.emailId
            }
            await Subscriber.create(subcriber)
            session.flash({ notification: 'Subscriber Added!' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
            return response.redirect('back')
        }
    }

    async edit({ request, view, response, params }) {
        let emails;
        try {
            if (params.emailId) {
                emails = await Email.query().where('id', params.emailId).fetch()
            }
            else {
                emails = await Email.all()
            }
            let subcriber = await Subscriber.query().where('id', params.id).with('emailTemplate').first()
            return view.render('others.forms.subscribers.edit', { data: subcriber.toJSON(), emails: emails.toJSON(), back: params.emailId ? `/others/emails/${params.emailId}/subscribers` : '/others/subscribers' })
        } catch (error) {
            console.log(error)
            return response.redirect('back')
        }
    }

    async update({ request, session, response, params }) {
        try {
            let data = request.all()
            let subcriber = await Subscriber.find(params.id)
            subcriber.email = data.email
            subcriber.emailId = data.emailId
            await subcriber.save()
            session.flash({ notification: 'Subscriber Updated!' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: 'Subscriber Updated Failed!' })
            return response.redirect('back')
        }
    }

    async remove({ request, session, response, params }) {
        try {
            let subscriber = await Subscriber.find(params.id)
            await subscriber.delete()
            session.flash({ notification: 'Subscriber Deleted!' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: 'Subscriber Delete Failed!' })
            return response.redirect('back')
        }
    }

}

module.exports = SubscriberController
