'use strict'
const Platform = use('App/Models/Platform')
const PlatfromsRight = use('App/Models/PlatfromsRight')
const Territory = use('App/Models/Territory')
const Right = use('App/Models/Right')
const TerritoryCountry = use('App/Models/TerritoryCountry')
class PlatformController {
    //Get All
    async index({ request, response , view }) {
      try {
        const query = request.get()
      let page = query.page ? query.page : 1
      let perpage = request.get().perpage ? request.get().perpage : 10 

      let dataQuery = Platform.query()
      if (query.search && query.searchcolumn) {
        dataQuery.where(query.searchcolumn,'LIKE',`%${query.search}%`)
      }

      if (query.order && query.orderby) {
        dataQuery.orderBy(query.orderby,query.order)
      }
      
      const Platforms = await dataQuery.paginate(parseInt(page),parseInt(perpage))
      const searchColumns = [{name:'Name',value:'name'}]
         
         
        return view.render('others.forms.platform.platform', {
          title: 'Latest Platforms',
          data: Platforms.toJSON(),
          order:{column:query.orderby,order:query.order},
          search:{text:query.search,column:query.searchcolumn},
          searchColumns:searchColumns,
        })
      } catch (error) {
        response.redirect('back')
      }
    }
    

  
    async add({ view }) {
      return view.render('others.forms.platform.add')
    }
  
    async store({ request, response, session }) {
  
  
      const platform = new Platform()
  
      platform.name = request.input('name')
  
      
      try {
        await platform.save()
        session.flash({ notification: 'Platform Added!' })
        return response.redirect('back')
      } catch (error) {
        session.flash({ errMsg: ((error.sqlMessage).split("'"))[0] })
        return response.redirect('back')
      }         
    }
  
  //edit
    async edit({ params, view }) {
      const platform = await Platform.find(params.id)
      
      return view.render('others.forms.platform.edit', {
        data: platform
      })
    }
  //Update
    async update({ params, request, response, session }) { 

  
      const platform = await Platform.find(params.id)
  
      platform.name = request.input('name')
  
      await platform.save()
  
      session.flash({ notification: 'Platform Updated!' })
  
      return response.redirect('/others/platform')
    }
  
  //Delete
  async remove({ params, response, session }){
      const platform = await Platform.find(params.id)
      await platform.delete()
      session.flash({ notification:"Platform deleted successfully!!" })
      return  response.redirect('/others/platform')
  }

  async RightsManagementIndex({view,params}){
    const territories = await Territory.query().with('countries').fetch();
    const rights = await Right.all();
    const platform = await Platform.find(params.id)
    try {
      const titles_platforms_right = await Right.query().with('platformTerritories',(builder) => builder.distinct('territoryName').where('platformId', params.id)).fetch()
      // return titles_platforms_right
      return view.render('others.forms.platform.rightsManagement',{data:{rights:rights.toJSON(),territories:territories.toJSON(),platform:platform.toJSON(),rightsData:titles_platforms_right.toJSON()}})

    } catch (error) {
      return view.render('others.forms.platform.rightsManagement',{data:{rights:rights.toJSON(),territories:territories.toJSON(),platform:platform.toJSON()}})
    }
  }

  async getRightsCountries({request,response}){
    let data = request.all()
    try {
      const titles_platforms_right = await PlatfromsRight.query().where('platformId', data.libraryId).where('rightId', data.rightId).where('territoryId',data.territoryId).pluck('countryId')
      let territoryCountries = await TerritoryCountry.query().where('territoryId',data.territoryId).fetch()
      territoryCountries = territoryCountries.toJSON().map( val => {
        if (titles_platforms_right.includes(val.id)) {
          val.selected = true
        }
        return val
      })
      return response.json({data:territoryCountries,success:true})
    } catch (error) {
      return response.json({data:titles_territories_right,success:false})
    }
  }

  async RightsManagementStore({request,session,response,params}){
    let data = request.all()
    // return data
    try {
      
      if (data.countries) {
        data.countries.map( async val => {
          await Promise.all(data.rights.map( async x => {
            try {
              await PlatfromsRight.create({territoryId:data.territoryId,countryId:val,platformId:params.id,rightId:x})
              }catch (error) {
            }
          }))
        })
      } else {
        await Promise.all(
          data.rights.map( async x => {
            try {
              await PlatfromsRight.create({territoryId:data.territoryId,platformId:params.id,rightId:x})
              }catch (error) {
            }
          })
        )
      }
      
      session.flash({ notification: "Created Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({errMsg : ((error.sqlMessage).split(' '))[0]+' '+((error.sqlMessage).split(' '))[1]+' '+'Skipped'})
      return response.redirect('back')
    }
  }

  async RightsManagementEdit({request,params,view}){
    const territories = await Territory.all();
    const rights = await Right.all();
    try {
      const titles_platforms_right = await PlatfromsRight.query().with('territory').with('rights').where('id',params.rightId).first()
      return view.render('others.forms.platform.rightsManagementEdit',{data:titles_platforms_right.toJSON(),territories:territories.toJSON(),rights:rights.toJSON(),platformId:params.platformId})
    } catch (error) {
      return response.redirect('back')
    }
  }

  async RightsManagementRemoveTerritory({request,session,params,response}){
    try {
      let data = request.all()
      await PlatfromsRight.query().where('platformId', data.libraryId).where('rightId', data.rightId).where('territoryId',data.territoryId).delete()
      session.flash({ notification: "Deleted Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({errMsg : "Delete Failed"})
      return response.redirect('back')
    }
  }

  async RightsManagementRemoveRight({params,session,response}){
    try {
      await PlatfromsRight.query().where('platformId', params.platformId).where('rightId', params.rightId).delete()
      session.flash({ notification: "Deleted Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({errMsg : "Delete Failed"})
      return response.redirect('back')
    }
  }

  async RightsManagementUpdate({request,params,session,response}){
    try {
      let data = request.all()
      const titles_platforms_right = await PlatfromsRight.query().where('platformId', data.libraryId).where('rightId', data.rightId).where('territoryId',data.territoryId).fetch()
      
      data.countries ? await Promise.all(
        titles_platforms_right.toJSON().map(async val => {
          if(!data.countries.includes((val.countryId).toString())){
            await PlatfromsRight.query().where('id', val.id).delete()
            data.countries = data.countries.filter(elm => elm != val.id)
          }
        })
      ) : await PlatfromsRight.query().where('platformId', data.libraryId).where('rightId', data.rightId).where('territoryId',data.territoryId).delete()

      data.countries && data.countries.map( async val => {
        try {
          await PlatfromsRight.create({platformId:data.libraryId,rightId:data.rightId,territoryId:data.territoryId,countryId:val})
        } catch (error) {
        }
      })
      session.flash({ notification: "Updated Successfully" });
      return response.redirect('back')
    } catch (error) {
      session.flash({errMsg : ((error.sqlMessage).split(' '))[0]+' '+((error.sqlMessage).split(' '))[1]})
      return response.redirect('back')
    }
  }
}

module.exports = PlatformController
