'use strict'

const Platform = use("App/Models/Platform");
const Library = use("App/Models/Library");
const AlternateTitlesPlatform = use("App/Models/AlternateTitlesPlatform");
const DynamicOption = require('../../../Services/DynamicOption.js')
const Status = require('../../../Services/VedioStatus');
const pitchStatus = Status.pitchStatus()

class PitchTrackerController {
    async index({ request, view, response, auth }){
        let pitchs = await AlternateTitlesPlatform.query().with('library',(builder) => builder.select('id','guId','title','studioId').with('studio')).fetch()
        let libraries = await Library.query().select('id','title').fetch()
        return view.render('PitchTracker/index',{data:pitchs.toJSON(),pitchStatus:pitchStatus,libraries:libraries.toJSON()})
    }

    async indexTitle({ request, view, response, params }){
        let pitchs = await AlternateTitlesPlatform.query().where('libraryId',params.libraryId, ).with('library',(builder) => builder.select('id','title','studioId').with('studio')).fetch()
        let libraries = await Library.query().where('id', params.libraryId).select('id','title').fetch()
        return view.render('PitchTracker/index',{data:pitchs.toJSON(),pitchStatus:pitchStatus,libraries:libraries.toJSON(),back:'/library'})
    }

    async store({ request, params, session, auth, response }){
        let data = request.all()
        try {

            await AlternateTitlesPlatform.create({libraryId:data.libraryId,platform:data.platform,userId:(await auth.getUser()).id,status:data.status,pitchDate:data.pitchDate})
            session.flash({ notification: 'Title Pitched Successfully.' })
            return response.redirect('back')
        } catch (error) {
            console.log(error)
            session.flash({ errMsg: error.sqlMessage })
            return response.redirect('back')
        }
    }

    async update({ request, params, session, auth, response }){
        let data = request.all()
        let alternateTitlesPlatform = await AlternateTitlesPlatform.find(params.id)
        alternateTitlesPlatform.status = data.status
        alternateTitlesPlatform.platform = data.platform
        alternateTitlesPlatform.pitchDate = data.pitchDate
        alternateTitlesPlatform.libraryId = data.libraryId
        try {
            await alternateTitlesPlatform.save()
            session.flash({ notification: 'Title Pitch updated Successfully.' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: error.sqlMessage })
            return response.redirect('back')
        }
    }

    async remove({ request, session, response, auth, params }){
        try {
            let alternateTitlesPlatform = await AlternateTitlesPlatform.find(params.id)
            await alternateTitlesPlatform.delete()
            session.flash({ notification: 'Title Pitch Deleted Successfully.' })
            return response.redirect('back')
        } catch (error) {
            session.flash({ errMsg: error.sqlMessage })
            return response.redirect('back')
        }
    }
}

module.exports = PitchTrackerController
