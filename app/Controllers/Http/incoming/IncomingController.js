'use strict'
const User = use("App/Models/User");
const Role = use("App/Models/Role");
const Incoming = use("App/Models/Incoming");
const Platform = use("App/Models/Platform")
const ProjectManagement = use("App/Models/TitleStatus");
const ProjectManagementSeason = use('App/Models/TitleStatusesSeason');
const CustomAssetsIncoming = use("App/Models/CustomAssetsIncoming");
const Database = use("Database");
const Note = use("App/Models/Note");
const IncomingNote = use("App/Models/IncomingNote");
const AlternateTitleLanguage = use("App/Models/AlternateTitleLanguage");
const TitlesTerritoriesRight = use("App/Models/TitlesTerritoriesRight");
const IncomingEpisode = use('App/Models/IncomingEpisode');
const PlatfromsRight = use("App/Models/PlatfromsRight");
const IncomingSeason = use('App/Models/IncomingSeason');
const Status = require("../../../Services/VedioStatus");
const IncomingPolicy = require("../../../Policies/incomingPolicy");
const exportFromJSON = require('export-from-json')

const Email = use('App/Models/Email');
const sendMail = require('../../../Services/EmailMessage.js')

const uuid = require('uuid-random');
const csv = require('csv-parser');
const fs = require('fs');
const { title } = require("process");
const Helpers = use('Helpers');

const parseCSV = (file) => {
  let results = []
  return new Promise(resolve => {
    fs.createReadStream('tmp/uploads/' + file)
      .pipe(csv())
      .on('data', (data) => results.push(data))
      .on('end', () => {
        resolve(results)
      })
  })
}

const cartesianArray = (data) => {
  return data.reduce((acc, curr) =>
    acc.flatMap(c => curr.map(n => [].concat(c, n)))
  )
}


class IncomingController {

  async UpdateStatusIncomingEpisode(incomingSeasonId) {

    let incomingEpiosodes = await IncomingEpisode.query().select('incomingSeasonId', 'captions', 'videoStatus', 'subtitles', 'artwork', 'dubbing', 'metadata').where('incomingSeasonId', incomingSeasonId).fetch()

    let statuses = { // 2 for QC passed.

      captions: incomingEpiosodes.toJSON().filter(item => {
        if (item.captions != 2 && item.captions != 9) {
          return item
        }
      }).length,

      videoStatus: incomingEpiosodes.toJSON().filter(item => {
        if (item.videoStatus != 2 && item.videoStatus != 1) {
          return item
        }
      }).length,

      subtitles: incomingEpiosodes.toJSON().filter(item => {
        if (item.subtitles != 2 && item.subtitles != 9) {
          return item
        }
      }).length,

      artwork: incomingEpiosodes.toJSON().filter(item => {
        if (item.artwork != 2 && item.artwork != 1) {
          return item
        }
      }).length,

      dubbing: incomingEpiosodes.toJSON().filter(item => {
        if (item.dubbing != 2 && item.dubbing != 9) {
          return item
        }
      }).length,

      metadata: incomingEpiosodes.toJSON().filter(item => {
        if (item.metadata != 2 && item.metadata != 1) {
          return item
        }
      }).length
    }

    let incomingSeason = await IncomingSeason.find(incomingSeasonId)
    incomingSeason.captions = statuses.captions == 0 ? 2 : 4

    incomingSeason.videoStatus = statuses.videoStatus == 0 ? 2 : 4

    incomingSeason.subtitles = statuses.subtitles == 0 ? 2 : 4

    incomingSeason.artwork = statuses.artwork == 0 ? 2 : 4

    incomingSeason.dubbing = statuses.dubbing == 0 ? 2 : 4

    incomingSeason.metadata = statuses.metadata == 0 ? 2 : 4

    await incomingSeason.save()

    let incomingSeasons = await IncomingSeason.query().select('incomingId', 'captions', 'videoStatus', 'subtitles', 'artwork', 'dubbing', 'metadata').where('incomingId', incomingSeason.incomingId).fetch()

    let seasonStatuses = { // 2 for QC passed.

      captions: incomingSeasons.toJSON().filter(item => {
        if (item.captions != 2 && item.captions != null) {
          return item
        }
      }).length,

      videoStatus: incomingSeasons.toJSON().filter(item => {
        if (item.videoStatus != 2 && item.videoStatus != null) {
          return item
        }
      }).length,

      subtitles: incomingSeasons.toJSON().filter(item => {
        if (item.subtitles != 2 && item.subtitles != null) {
          return item
        }
      }).length,

      artwork: incomingSeasons.toJSON().filter(item => {
        if (item.artwork != 2 && item.artwork != null) {
          return item
        }
      }).length,

      dubbing: incomingSeasons.toJSON().filter(item => {
        if (item.dubbing != 2 && item.dubbing != null) {
          return item
        }
      }).length,

      metadata: incomingSeasons.toJSON().filter(item => {
        if (item.metadata != 2 && item.metadata != null) {
          return item
        }
      }).length
    }


    let incoming = await Incoming.find(incomingSeason.incomingId)
    incoming.captions = seasonStatuses.captions == 0 ? 2 : 4

    incoming.videoStatus = seasonStatuses.videoStatus == 0 ? 2 : 4

    incoming.subtitles = seasonStatuses.subtitles == 0 ? 2 : 4

    incoming.artwork = seasonStatuses.artwork == 0 ? 2 : 4

    incoming.dubbing = seasonStatuses.dubbing == 0 ? 2 : 4

    incoming.metadata = seasonStatuses.metadata == 0 ? 2 : 4
    await incoming.save()

    return new Promise((resolve) => {
      resolve(incomingSeason)
    })
  }

  checkSelected(statusSelected, AllStatus) {
    for (let index = 0; index < AllStatus.length; index++) {
      if (AllStatus[index].value == statusSelected) {
        AllStatus[index].selected = true;
      }
    }

    return AllStatus
  }

  async getSuggestedPlatforms(libraryId, platformRights) {
    let titleRights = await TitlesTerritoriesRight.query().where('libraryId', libraryId)
    let applicablePlatforms = []



    platformRights.map(platform => {
      return (titleRights.some((titleRight) => {
        if ((titleRight.rightId == platform.rightId) && (titleRight.territoryId == platform.territoryId)) {
          !applicablePlatforms.some(item => item.name == platform.platform.name) && applicablePlatforms.push({ id: platform.platform.id, name: platform.platform.name })
        }
        else if ((titleRight.rightId == platform.rightId) && (platform.territory.territoryName == 'world wide' || platform.territory.territoryName == 'World Wide')) {
          !applicablePlatforms.some(item => item.name == platform.platform.name) && applicablePlatforms.push({ id: platform.platform.id, name: platform.platform.name })
        }
      }))
    })


    return applicablePlatforms
  }

  async index({ request, response, auth, view }) {
    let searchColumns = [{ value: 'title', name: 'Title' }, { value: 'title', name: 'Alternate Title' }, { value: 'guId', name: 'GUID' }, { value: 'studio', name: 'Studio' }]

    try {
      let query = Object.create(request.get(), {})
      query.searchColumns = searchColumns
      let user = await auth.getUser()


      if (request.url().includes('export')) {
        let exportData = await IncomingPolicy.exportIncoming(user.toJSON(), query)
        exportData = exportData.map(item => {
          let obj = {
            title: item.alternateName.alternateName,
            studio: item.library.studio.title,
            guId: item.alternateName.guId,
            parentTitle: item.library.title,
            movieORseries: item.library.is,
            videoStatus: item.videoStatus != null ? Status.incomingStatuses().vedioStatus.find(x => x.value == item.videoStatus).name : '',
            captions: item.captions != null ? Status.incomingStatuses().caps.find(x => x.value == item.captions).name : '',
            artwork: item.artwork != null ? Status.incomingStatuses().art.find(x => x.value == item.artwork).name : '',
            metadata: item.metadata != null ? Status.incomingStatuses().metadata.find(x => x.value == item.metadata).name : '',
            dubbing: item.dubbing != null ? Status.incomingStatuses().dub.find(x => x.value == item.dubbing).name : '',
            subtitles: item.subtitles != null ? Status.incomingStatuses().subs.find(x => x.value == item.subtitles).name : ''
          }

          item.incomingSeasons.length > 0 && (obj.season = item.incomingSeasons.map(x => {
            return `${x.season.name}(No:${x.season.number})`
          }).join())

          item.incomingSeasons.length > 0 && (obj.totalEpisodes = item.incomingSeasons[0].__meta__.incomingEpisodes_count)

          item.incomingNotes && (obj.notes = item.incomingNotes.map(x => {
            return `${x.user.fullname} (${x.note.text})`
          }).join())

          return obj
        })

        try {
          const result = exportFromJSON({
            data: exportData,
            fileName: 'incoming',
            exportType: 'xls',
            processor(content, type, fileName) {
              switch (type) {
                case 'txt':
                  response.header('Content-Type', 'text/plain')
                  break
                case 'json':
                  response.header('Content-Type', 'text/plain')
                  break
                case 'csv':
                  response.header('Content-Type', 'text/csv')
                  break
                case 'xls':
                  response.header('Content-Type', 'application/vnd.ms-excel')
                  break
              }
              response.header('Content-disposition', 'attachment;filename=' + fileName)
              return content
            }
          })
          return response.send(result)
        } catch (error) {
          console.log(error)
          return response.redirect('back')
        }

      }


      let libraryData = await IncomingPolicy.getIncoming(user.toJSON(), query)

      let platforms = await Platform.all()

      await Promise.all(
        libraryData.data.map(async val => {
          val['videoStatus'] = Status.incomingStatuses().vedioStatus.find(x => x.value == val['videoStatus'])
          val['captions'] = Status.incomingStatuses().caps.find(x => x.value == val['captions'])
          val['subtitles'] = Status.incomingStatuses().subs.find(x => x.value == val['subtitles'])
          val['artwork'] = Status.incomingStatuses().art.find(x => x.value == val['artwork'])
          val['dubbing'] = Status.incomingStatuses().dub.find(x => x.value == val['dubbing'])
          val['metadata'] = Status.incomingStatuses().metadata.find(x => x.value == val['metadata'])

          // val['videoStatus'] = this.checkSelected(val['videoStatus'],Status.incomingStatuses().vedioStatus)
          // val['captions'] = this.checkSelected(val['captions'],Status.incomingStatuses().caps)
          // val['subtitles'] = this.checkSelected(val['subtitles'],Status.incomingStatuses().subs)
          // val['artwork'] = this.checkSelected(val['artwork'],Status.incomingStatuses().art)
          // val['dubbing'] = this.checkSelected(val['dubbing'],Status.incomingStatuses().dub)
          // val['metadata'] = this.checkSelected(val['metadata'],Status.incomingStatuses().metadata)
          // val['customAssets'] = (await CustomAssetsIncoming.query().where('incomingId',val.id).fetch()).toJSON()
          val['incomingSeasons'] = await Promise.all(val['incomingSeasons'].map(season => {
            season.videoStatus = Status.incomingStatuses().vedioStatus.find(x => x.value == season['videoStatus'])
            season.captions = Status.incomingStatuses().caps.find(x => x.value == season['captions'])
            season.subtitles = Status.incomingStatuses().subs.find(x => x.value == season['subtitles'])
            season.artwork = Status.incomingStatuses().art.find(x => x.value == season['artwork'])
            season.dubbing = Status.incomingStatuses().dub.find(x => x.value == season['dubbing'])
            season.metadata = Status.incomingStatuses().metadata.find(x => x.value == season['metadata'])
            // season.videoStatus = this.checkSelected(season['videoStatus'],Status.incomingStatuses().vedioStatus)
            // season.captions = this.checkSelected(season['captions'],Status.incomingStatuses().caps)
            // season.subtitles = this.checkSelected(season['subtitles'],Status.incomingStatuses().subs)
            // season.artwork = this.checkSelected(season['artwork'],Status.incomingStatuses().art)
            // season.dubbing = this.checkSelected(season['dubbing'],Status.incomingStatuses().dub)
            // season.metadata = this.checkSelected(season['metadata'],Status.incomingStatuses().metadata)
            return season
          })
          )

        })
      )

      // return libraryData

      let paramsObj = Object.fromEntries(new URLSearchParams(request.get()))

      paramsObj.search && delete paramsObj.search
      paramsObj.searchcolumn && delete paramsObj.searchcolumn
      paramsObj.orderby && delete paramsObj.orderby
      paramsObj.order && delete paramsObj.order
      paramsObj.page && delete paramsObj.page
      paramsObj.perpage && delete paramsObj.perpage

      let filterQuery = []

      for (let [key, value] of Object.entries(paramsObj)) {
        filterQuery.push(`${key}=${value}`)
      }

      return view.render('incoming.incomingIndex', {
        title: 'Incoming',
        library: libraryData,
        searchColumns: searchColumns,
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        filter: query.filter ? '&' + filterQuery.join('&') : undefined,
        data: {
          statuses: Status.incomingStatuses(),
          platforms: platforms.toJSON(),
          customAssetOpt: Status.incomingStatuses().vedioStatus,
        }
      })
    } catch (error) {
      return response.redirect('back')
    }
  }

  /**
   * Update the specified resource in storage.
   */
  async update({ request, auth, session, response, params }) {
    try {

      let incomingStatus = await Incoming.query().where('id', params.id).with('alternateName').first()

      let data = request.only(['captions', 'subtitles', 'artwork', 'dubbing', 'metadata', 'videoStatus'])

      let incomingFailStatuses = []

      if (incomingStatus.captions != 3 && data.captions == 3) {
        incomingFailStatuses.push('Captions')
      }
      if (incomingStatus.subtitles != 3 && data.subtitles == 3) {
        incomingFailStatuses.push('Subtitles')
      }
      if (incomingStatus.artwork != 3 && data.artwork == 3) {
        incomingFailStatuses.push('Artwork')
      }
      if (incomingStatus.dubbing != 3 && data.dubbing == 3) {
        incomingFailStatuses.push('Dubbing')
      }
      if (incomingStatus.metadata != 3 && data.metadata == 3) {
        incomingFailStatuses.push('Metadata')
      }
      if (incomingStatus.videoStatus != 3 && data.videoStatus == 3) {
        incomingFailStatuses.push('Video Status')
      }

      let customAsset = request.input('customAsset')
      for (let key in data) {
        if (data[key] == 'null' || data[key] == '') {
          delete data[key]
        }
      }
      let customAssetData = []
      if (customAsset) {
        if (Array.isArray(customAsset.name) && Array.isArray(customAsset.status)) {
          for (let i = 0; i < customAsset.name.length; i++) {
            customAssetData.push({ name: customAsset.name[i], status: customAsset.status[i], incomingId: params.id, studioId: incomingStatus.studioId, userId: (await auth.getUser()).id })
          }
        } else {
          customAssetData.push({ name: customAsset.name, status: customAsset.status, incomingId: params.id, studioId: incomingStatus.studioId, userId: (await auth.getUser()).id })
        }
      }
      incomingStatus.merge(data)
      await incomingStatus.save()
      await CustomAssetsIncoming.query()
        .where("incomingId", params.id)
        .delete();
      await CustomAssetsIncoming.createMany(customAssetData)


      if ((incomingStatus.videoStatus == 1 || incomingStatus.videoStatus == 2) && (incomingStatus.captions == 2 || incomingStatus.captions == 9) && (incomingStatus.subtitles == 2 || incomingStatus.subtitles == 9) && (incomingStatus.artwork == 1 || incomingStatus.artwork == 2) && (incomingStatus.dubbing == 2 || incomingStatus.dubbing == 9) && (incomingStatus.metadata == 1 || incomingStatus.metadata == 2)) {

        let email = await Email.findBy('title', 'IncomingNotification')

        sendMail(email, { user: (await auth.getUser()).fullname, title: incomingStatus.toJSON().alternateName.alternateName, updatedAt: incomingStatus.updated_at })
      }

      if (incomingFailStatuses.length > 0) {
        let email = await Email.findBy('title', 'IncomingFailNotification')
        sendMail(email, { user: (await auth.getUser()).fullname, title: incomingStatus.toJSON().alternateName.alternateName, updatedAt: incomingStatus.updated_at, statuses: incomingFailStatuses.join() })
      }

      session.flash({ notification: 'Statuses Updated!' })
      return response.redirect('back')
    } catch (error) {
      console.log(error)
      session.flash({ errMsg: 'Updated Failed!' })
      return response.redirect('back')
    }
  }

  async addToProjectManagement({ auth, request, session, response, params }) {
    let data = request.all()
    try {
      let incomingStatus = await Incoming.query().with('incomingSeasons', (builder) => builder.select('id', 'incomingId', 'seriesId', 'seasonId')).where('id', params.id).first()
      if ((incomingStatus.videoStatus == 1 || incomingStatus.videoStatus == 2) && (incomingStatus.captions == 2 || incomingStatus.captions == 9) && (incomingStatus.subtitles == 2 || incomingStatus.subtitles == 9) && (incomingStatus.artwork == 1 || incomingStatus.artwork == 2) && (incomingStatus.dubbing == 2 || incomingStatus.dubbing == 9) && (incomingStatus.metadata == 1 || incomingStatus.metadata == 2)) {
        let titlestatusesSeason = []
        let userId = (await auth.getUser()).id
        await Promise.all(data.platforms.map(async x => {
          let titleStatus = await ProjectManagement.create({ userId: userId, studioId: incomingStatus.studioId, platformId: parseInt(x), alternateTitleId: incomingStatus.alternateTitleId, incomingId: incomingStatus.id, libraryId: incomingStatus.libraryId })
          incomingStatus.toJSON().incomingSeasons && incomingStatus.toJSON().incomingSeasons.map(async season => {
            titlestatusesSeason.push({ titleStatusId: titleStatus.id, platformId: parseInt(x), alternateTitleId: incomingStatus.alternateTitleId, incomingId: incomingStatus.id, seriesId: season.seriesId, incomingSeasonId: season.id, seasonId: season.seasonId, userId: userId, studioId: incomingStatus.studioId })
          })
        }))

        await ProjectManagementSeason.createMany(titlestatusesSeason)

        session.flash({ notification: 'Added To Programming Successfully!' })
        return response.redirect('/project-management/movie')
      } else {
        session.flash({ errMsg: "Subtitles,Captions and dubbing should be QC Passed or N/A. Artwork,Metadata should be QC Passed or Received!" })
        return response.redirect('back')
      }
    } catch (error) {
      session.flash({ errMsg: 'Duplicate Entries Skipped!' })
      return response.redirect('/project-management/movie')
    }
  }


  async addSeasonToProjectManagement({ session, request, auth, response, params }) {
    let data = request.all()
    try {
      let incomingSeason = await IncomingSeason.query().where('id', params.incomingSeasonId).with('incoming').first()
      let userId = (await auth.getUser()).id
      incomingSeason = incomingSeason.toJSON()
      if ((incomingSeason.videoStatus == 1 || incomingSeason.videoStatus == 2) && (incomingSeason.captions == 2 || incomingSeason.captions == 9) && (incomingSeason.subtitles == 2 || incomingSeason.subtitles == 9) && (incomingSeason.artwork == 1 || incomingSeason.artwork == 2) && (incomingSeason.dubbing == 2 || incomingSeason.dubbing == 9) && (incomingSeason.metadata == 1 || incomingSeason.metadata == 2)) {
        let titlestatusesSeason = []
        let titleStatuses = []

        await Promise.all(
          data.platforms.map(async item => {
            let titleStatus = await ProjectManagement.query().select('id', 'incomingId', 'platformId').where('incomingId', incomingSeason.incomingId).andWhere('platformId', parseInt(item))
            if (titleStatus.length == 0) {
              titleStatus = await ProjectManagement.create({ userId: userId, studioId: incomingSeason.studioId, platformId: parseInt(item), alternateTitleId: incomingSeason.alternateTitleId, incomingId: incomingSeason.incomingId, libraryId: incomingSeason.seriesId })
              titleStatuses.push(titleStatus.id)
            }
            if (titleStatus.length == 1) {
              titleStatuses.push(titleStatus[0].id)
            }
          })
        )
        titleStatuses = data.titleStatus ? titleStatuses.concat(data.titleStatus) : titleStatuses

        cartesianArray([titleStatuses, data.platforms]).map(x => {
          titlestatusesSeason.push({ platformId: parseInt(x[1]), titleStatusId: parseInt(x[0]), alternateTitleId: incomingSeason.incoming.alternateTitleId, incomingId: incomingSeason.incoming.id, seriesId: incomingSeason.seriesId, incomingSeasonId: incomingSeason.id, seasonId: incomingSeason.seasonId, userId: userId, studioId: incomingSeason.studioId })
        })
        await ProjectManagementSeason.createMany(titlestatusesSeason)

        session.flash({ notification: 'Season To Programming Successfully!' })
        return response.redirect('/project-management/movie')
      } else {
        session.flash({ errMsg: 'Subtitles,Captions and dubbing should be QC Passed or N/A. Artwork,Metadata should be QC Passed or Received!' })
        return response.redirect('back')
      }
    } catch (error) {
      console.log(error)
      session.flash({ errMsg: 'Duplicate Entries Skipped!' })
      return response.redirect('/project-management/movie')
    }
  }

  async assignCustomer({ request, session, response, params }) {
    if (params.type == 'new') {
      try {
        const user = new User
        user.fullname = request.input('fullname')
        user.email = request.input('email')
        user.password = request.input('password')
        user.role_id = (await Role.findBy({ slug: 'customer' })).id
        await user.save()
        let incomingStatus = await Incoming.find(params.id)
        incomingStatus.customerId = user.id
        await incomingStatus.save()
        session.flash({ notification: 'Customer Assigned Successfully!' })
        return response.redirect('back')
      } catch (error) {
        session.flash({ errMsg: 'Duplicate Entry User Email Should be Unique!' })
        return response.redirect('back')
      }
    }
    if (params.type == 'select') {
      try {
        let incomingStatus = await Incoming.find(params.id)
        incomingStatus.customerId = request.input('customerId')
        await incomingStatus.save()
        session.flash({ notification: 'Customer Assigned Successfully!' })
        return response.redirect('back')
      } catch (error) {
        session.flash({ errMsg: 'Failed to Assigne Customer!' })
        return response.redirect('back')
      }
    }
  }




  async addNotes({ request, auth, session, response, params }) {
    const trx = await Database.beginTransaction();
    let studioId = await Incoming.query().where('id', params.id).pluck('studioId')
    try {
      const note = await Note.create({ text: request.input('text') }, trx)
      await IncomingNote.create({ incomingId: params.id, noteId: note.id, userId: (await auth.getUser()).id, studioId: studioId }, trx)
      trx.commit()
      session.flash({ notification: 'Note Added Successfully!' })
      return response.redirect('back')

    } catch (error) {
      session.flash({ errMsg: 'Failed to Add!' })
      return response.redirect('back')
    }
  }

  async deleteNotes({ request, session, response, params }) {
    try {
      const note = await Note.find(params.noteId)
      await note.delete()

      session.flash({ notification: 'Note Deleted Successfully!' })
      return response.redirect('back')

    } catch (error) {
      session.flash({ errMsg: 'Failed to Delete!' })
      return response.redirect('back')
    }
  }

  async indexIncomingEpisodes({ view, params }) {
    try {
      let incomingEpisodes = await IncomingEpisode.query().where('incomingId', params.incomingId).where('incomingSeasonId', params.incomingSeasonId).with('episode').fetch()
      let incoming = await Incoming.query().select('id', 'libraryId', 'alternateTitleId').where('id', params.incomingId).with('library', (builder) => builder.select('id', 'title', 'is')).with('alternateName', (builder) => builder.select('id', 'alternateName')).first()
      let incomingSeason = await IncomingSeason.query().where('id', params.incomingSeasonId).with('season').first()
      incomingEpisodes = await Promise.all(incomingEpisodes.toJSON().map(episode => {
        episode.videoStatus = this.checkSelected(episode['videoStatus'], Status.incomingStatuses().vedioStatus)
        episode.captions = this.checkSelected(episode['captions'], Status.incomingStatuses().caps)
        episode.subtitles = this.checkSelected(episode['subtitles'], Status.incomingStatuses().subs)
        episode.artwork = this.checkSelected(episode['artwork'], Status.incomingStatuses().art)
        episode.dubbing = this.checkSelected(episode['dubbing'], Status.incomingStatuses().dub)
        episode.metadata = this.checkSelected(episode['metadata'], Status.incomingStatuses().metadata)
        return episode
      })
      )
      return view.render('incoming.incomingEpiosodes.incomingEpisode', { incoming: incoming.toJSON(), incomingEpisodes: incomingEpisodes, incomingSeason: incomingSeason.toJSON() })
    } catch (error) {
      return response.redirect('back')
    }
  }

  async updateEpisodeStatus({ request, response, session, params }) {
    try {
      let data = request.only(['captions', 'subtitles', 'artwork', 'dubbing', 'metadata', 'videoStatus'])
      let customAsset = request.input('customAsset')
      for (let key in data) {
        if (data[key] == 'null' || data[key] == '') {
          delete data[key]
        }
      }
      let customAssetData = []
      if (customAsset) {
        if (Array.isArray(customAsset.name) && Array.isArray(customAsset.status)) {
          for (let i = 0; i < customAsset.name.length; i++) {
            customAssetData.push({ name: customAsset.name[i], status: customAsset.status[i], incomingId: params.id })
          }
        } else {
          customAssetData.push({ name: customAsset.name, status: customAsset.status, incomingId: params.id })
        }
      }

      let incomingEpisodeStatus = await IncomingEpisode.find(params.id)
      incomingEpisodeStatus.merge(data)
      await incomingEpisodeStatus.save()

      await this.UpdateStatusIncomingEpisode(incomingEpisodeStatus.incomingSeasonId)

      session.flash({ notification: 'Statuses For Episode Updated!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: 'Updated Failed For Episode Status!' })
      return response.redirect('back')
    }
  }




  async search({ request, response }) {
    try {
      let data = await AlternateTitleLanguage.query().select('id', 'alternateName').where('alternateName', 'LIKE', '%' + request.get().term + '%').with('incoming', (builder) => builder.select('id', 'alternateTitleId', 'libraryId')).fetch()
      // let platformRights = await PlatfromsRight.query().select('id','platformId','rightId','territoryId','countryId').with('platform', (builder) => {builder.select('id','name') }).with('territory', (builder) => {builder.select('id','territoryName') }).with('rights', (builder) => {builder.select('id','name') }).with('country', (builder) => {builder.select('id','country') }).fetch()
      let platforms = await Platform.all()
      data = data.toJSON()
      await Promise.all(
        data.map(async val => {
          // val.incoming['applicablePlatforms'] = (await Platform.query().select('id','name').fetch()).toJSON() //await this.getSuggestedPlatforms(val.incoming.libraryId,platformRights.toJSON())
          // if(val.incoming['applicablePlatforms'].length > 0){
          //   val.incoming['length'] = true
          // }
          // else{
          //   val.incoming['length'] = false
          // }
        })
      )
      response.json({ success: true, data: { titles: data, platforms: platforms } });
    } catch (error) {
      response.json({ success: false, data: null })
    }
  }

  async import({ request, session, response, auth, view }) {
    try {
      const file = request.file('importFile')
      let fileName = uuid() + '.xlsx'
      await file.move(Helpers.tmpPath('uploads'), {
        name: fileName
      })
      let data = await parseCSV(fileName)
      let errorFile = []
      let errors = []

      await Promise.all(
        data.map(async (item, index) => {
          let incoming = await Incoming.query().whereHas('alternateName', (builder) => { builder.where('guId', 'LIKE', `%${item.guid.trim()}%`) }).first()
          if (incoming != null) {
            incoming.videoStatus = item.videoStatus ? Status.incomingStatuses().vedioStatus.find(x => x.name.toLocaleLowerCase() == item.videoStatus.trim().toLocaleLowerCase()).value : null
            incoming.captions = item.captions ? Status.incomingStatuses().caps.find(x => x.name.toLocaleLowerCase() == item.captions.trim().toLocaleLowerCase()).value : null
            incoming.subtitles = item.subtitles ? Status.incomingStatuses().subs.find(x => x.name.toLocaleLowerCase() == item.subtitles.trim().toLocaleLowerCase()).value : null
            incoming.artwork = item.artwork ? Status.incomingStatuses().art.find(x => x.name.toLocaleLowerCase() == item.artwork.trim().toLocaleLowerCase()).value : null
            incoming.dubbing = item.dubbing ? Status.incomingStatuses().dub.find(x => x.name.toLocaleLowerCase() == item.dubbing.trim().toLocaleLowerCase()).value : null
            incoming.metadata = item.metadata ? Status.incomingStatuses().metadata.find(x => x.name.toLocaleLowerCase() == item.metadata.trim().toLocaleLowerCase()).value : null
            await incoming.save()
          }
          else {
            item.error = (incoming == null ? 'GUID not found in incoming!' : '')
            errorFile.push({
              guid: item.guid ? item.guid.trim() : '',
              videoStatus: item.videoStatus ? item.videoStatus.trim() : '',
              captions: item.captions ? item.captions.trim() : '',
              subtitles: item.subtitles ? item.subtitles.trim() : '',
              artwork: item.artwork ? item.artwork.trim() : '',
              dubbing: item.dubbing ? item.dubbing.trim() : '',
              metadata: item.metadata ? item.metadata.trim() : '',
              error: item.error
            })
            errors.push({ row: index + 2, message: item.error })
          }
        })
      )

      if (errors.length > 0) {

        try {
          const result = exportFromJSON({
            data: errorFile,
            fileName: 'Errors in Incoming Import',
            exportType: 'csv',
            processor(content, type, fileName) {
              return content
            }
          })
          let fileName = uuid() + '.csv'
          fs.writeFileSync(`public/error/${fileName}`, result)
          return view.render('library.importError', { data: errors, back: '/incoming', errorFile: `/error/${fileName}` })
        } catch (error) {
          console.log(error)
          session.flash({ errMsg: error.message })
          response.redirect('back')
        }

      } else {
        session.flash({ notification: 'Updated Successfully.' })
        return response.redirect('back')
      }



    } catch (error) {
      session.flash({ errMsg: error.message })
      response.redirect('back')
    }
  }


  async importEpisodes({ request, view, response, session }) {
    try {
      const file = request.file('importFile')
      let fileName = uuid() + '.xlsx'
      await file.move(Helpers.tmpPath('uploads'), {
        name: fileName
      })
      let data = await parseCSV(fileName)
      let errorFile = []
      let errors = []
      let seasonIds = []

      await Promise.all(
        data.map(async (item, index) => {
          let incoming_episode = await IncomingEpisode.query().whereHas('episode', (builder) => builder.where('guId', 'LIKE', `%${item.guid}%`)).first()
          if (incoming_episode != null) {
            seasonIds.push(incoming_episode.incomingSeasonId)
            incoming_episode.videoStatus = item.videoStatus ? Status.incomingStatuses().vedioStatus.find(x => x.name.toLocaleLowerCase() == item.videoStatus.trim().toLocaleLowerCase()).value : null
            incoming_episode.captions = item.captions ? Status.incomingStatuses().caps.find(x => x.name.toLocaleLowerCase() == item.captions.trim().toLocaleLowerCase()).value : null
            incoming_episode.subtitles = item.subtitles ? Status.incomingStatuses().subs.find(x => x.name.toLocaleLowerCase() == item.subtitles.trim().toLocaleLowerCase()).value : null
            incoming_episode.artwork = item.artwork ? Status.incomingStatuses().art.find(x => x.name.toLocaleLowerCase() == item.artwork.trim().toLocaleLowerCase()).value : null
            incoming_episode.dubbing = item.dubbing ? Status.incomingStatuses().dub.find(x => x.name.toLocaleLowerCase() == item.dubbing.trim().toLocaleLowerCase()).value : null
            incoming_episode.metadata = item.metadata ? Status.incomingStatuses().metadata.find(x => x.name.toLocaleLowerCase() == item.metadata.trim().toLocaleLowerCase()).value : null
            await incoming_episode.save()
          }
          else {
            item.error = (incoming_episode == null ? 'GUID not found in incoming episodes!' : '')
            errorFile.push(item)
            errors.push({ row: index + 2, message: item.error })
          }
        })
      )
      seasonIds.map(x => this.UpdateStatusIncomingEpisode(x))

      if (errors.length > 0) {

        try {
          const result = exportFromJSON({
            data: errorFile,
            fileName: 'Errors in Incoming Episodes Import',
            exportType: 'csv',
            processor(content, type, fileName) {
              return content
            }
          })
          let fileName = uuid() + '.csv'
          fs.writeFileSync(`public/error/${fileName}`, result)
          return view.render('library.importError', { data: errors, back: '/incoming', errorFile: `/error/${fileName}` })
        } catch (error) {
          session.flash({ errMsg: error.message })
          response.redirect('back')
        }

      } else {
        session.flash({ notification: 'Epsiodes Updated Successfully.' })
        return response.redirect('back')
      }
    } catch (error) {
      session.flash({ errMsg: error.message })
      response.redirect('back')
    }
  }
  async incomingDetail({ params, response }) {
    try {
      let data = await Incoming.query().with('customAssetsIncoming').where('id', params.id).first()
      return response.json({ success: true, data: data })
    } catch (error) {
      return response.json({ success: false, data: error.message })
    }
  }
}

module.exports = IncomingController
