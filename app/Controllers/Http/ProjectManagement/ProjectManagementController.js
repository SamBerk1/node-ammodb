'use strict'
const Platform = use('App/Models/Platform');
const TitleStatus = use("App/Models/TitleStatus");
const TitleStatusesSeason = use('App/Models/TitleStatusesSeason');
const AlternateTitleLanguage = use("App/Models/AlternateTitleLanguage")
const CustomAssetsOutgoing = use("App/Models/CustomAssetsOutgoing")
const Incoming = use('App/Models/Incoming');
const IncomingSeason = use('App/Models/IncomingSeason');
const OutgoingNote = use("App/Models/OutgoingNote");
const OutgoingSeasonNote = use('App/Models/OutgoingSeasonNote');
const Database = use('Database');
const Note = use("App/Models/Note")
const OutgoingTerritory = use("App/Models/OutgoingTerritory")
const PlatfromsRight = use("App/Models/PlatfromsRight")
const TitlesTerritoriesRight = use("App/Models/TitlesTerritoriesRight")
const Status = require('../../../Services/VedioStatus')
const moment = require('moment')
const Email = use('App/Models/Email');
const uuid = require('uuid-random');
const csv = require('csv-parser');
const fs = require('fs');
const Helpers = use('Helpers');
const exportFromJSON = require('export-from-json')
const sendMail = require('../../../Services/EmailMessage.js')

const parseCSV = (file) => {
  let results = []
  return new Promise(resolve => {
    fs.createReadStream('tmp/uploads/' + file)
      .pipe(csv())
      .on('data', (data) => results.push(data))
      .on('end', () => {
        resolve(results)
      })
  })
}

class ProjectManagementController {
  checkSelected(statusSelected, AllStatus) {
    for (let index = 0; index < AllStatus.length; index++) {
      if (AllStatus[index].value == statusSelected) {
        AllStatus[index].selected = true;
      }
    }
    return AllStatus
  }
  checkSelected2(statusSelected, AllStatus) {
    for (let index = 0; index < AllStatus.length; index++) {
      if (AllStatus[index].value == statusSelected) {
        AllStatus[index].selected = true;
      }
    }
    return AllStatus
  }

  recentTitle(createdAt) {
    if (moment(createdAt).fromNow().includes('minute') || moment(createdAt).fromNow().includes('second')) {
      return { flag: true, time: moment(createdAt).fromNow() }
    }
    else {
      return { flag: false, time: null }
    }
  }


  async getSuggestedPlatforms(libraryId, platformRights) {
    let titleRights = await TitlesTerritoriesRight.query().where('libraryId', libraryId)
    let applicablePlatforms = []



    platformRights.map(platform => {
      return (titleRights.some((titleRight) => {
        if ((titleRight.rightId == platform.rightId) && (titleRight.territoryId == platform.territoryId)) {
          !applicablePlatforms.some(item => item.name == platform.platform.name) && applicablePlatforms.push({ id: platform.platform.id, name: platform.platform.name })
        }
        else if ((titleRight.rightId == platform.rightId) && (platform.territory.territoryName == 'world wide' || platform.territory.territoryName == 'World Wide')) {
          !applicablePlatforms.some(item => item.name == platform.platform.name) && applicablePlatforms.push({ id: platform.platform.id, name: platform.platform.name })
        }
      }))
    })


    return applicablePlatforms
  }
  async exportData({ request, response, view}){
    
    let searchColumns = [{ value: 'title', name: 'Title' }, { value: 'title', name: 'Alternate Title' }, { value: 'guId', name: 'GUID' }, { value: 'studioname', name: 'Studio' }]
    try {
      
      let query = Object.create(request.get())

      let orderby = query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby

      let searchcolumn = query.searchcolumn ? query.searchcolumn : ''
      let search = query.search ? query.search : ''
      query[searchcolumn] = search
      
      let data;


      if (orderby) {
        data = await TitleStatus.query().select('title_statuses.*')
          .where(function () {
            this
              .where('outgoing', null)
              .orWhere('outgoing', 2)
          })
          .with('alternateName').with('territories').with('platform').with('notes')
          .join('alternate_title_languages', 'title_statuses.alternateTitleId', '=', 'alternate_title_languages.id')
          .with('library', (builder) => { builder.with('studio').select('id', 'is', 'title', 'studioId') }).filter(query)
          .orderBy(orderby, query.order).fetch();

      } else {
        
        data = await TitleStatus.query()
          .where(function () {
            this
              .where('outgoing', null)
              .orWhere('outgoing', 2)
          })
          .with('alternateName').with('territories').with('platform').with('notes')
          .with('library', (builder) => { builder.with('studio').select('id', 'is', 'title', 'studioId') })
          .filter(query).fetch();
      }
      let dData= data.toJSON()
      dData=dData.map(item=>{
        let obj={
        GUID:item.alternateName.guId,
        Type:item.library.is,
        Title:item.library.title,
        Studio:item.library.studio.title,
        Platform:item.platform.name,
        DueDate:item.dueDate,
        VideoStatus:item.vedioStatus,
        Captions:item.captions,
        Subs:item.subtitles,
        ArtWork:item.artwork,
        Dubbing:item.dubbing,
        MetaData:item.metadata,
        Notes:item.notes,
        createdAt:item.created_at.split(' ')[0],
        updatedAt:item.updated_at.split(' ')[0],
        }
        return obj
      })
     
      if (request.url().includes('export')) {
      let exportData= dData
      const result = exportFromJSON({
        data: exportData,
        fileName: 'programming',
        exportType: 'xls',
        processor(content, type, fileName) {
          switch (type) {
            case 'txt':
              response.header('Content-Type', 'text/plain')
              break
            case 'json':
              response.header('Content-Type', 'text/plain')
              break
            case 'csv':
              response.header('Content-Type', 'text/csv')
              break
            case 'xls':
              response.header('Content-Type', 'application/vnd.ms-excel')
              break
          }
          response.header('Content-disposition', 'attachment;filename=' + fileName)
          return content
        }
      })
      return response.send(result)
    }
    return response.redirect('/project-management/movie') 
      
    } 
    
    
    catch (error) {
      console.log(error)
      return response.redirect('back')
    }
    
  }


  async index({ request, response, view }) {
    let searchColumns = [{ value: 'title', name: 'Title' }, { value: 'title', name: 'Alternate Title' }, { value: 'guId', name: 'GUID' }, { value: 'studioname', name: 'Studio' }]
    try {
      let query = Object.create(request.get())

      let page = query.page ? query.page : 1
      let perpage = query.perpage ? query.perpage : 10

      let orderby = query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby

      let searchcolumn = query.searchcolumn ? query.searchcolumn : ''
      let search = query.search ? query.search : ''
      query[searchcolumn] = search

      let data;


      if (orderby) {
        data = await TitleStatus.query().select('title_statuses.*')
          .where(function () {
            this
              .where('outgoing', null)
              .orWhere('outgoing', 2)
          })
          .with('alternateName').with('territories').with('platform').with('notes')
          .join('alternate_title_languages', 'title_statuses.alternateTitleId', '=', 'alternate_title_languages.id')
          .with('library', (builder) => { builder.with('studio').select('id', 'is', 'title', 'studioId') }).filter(query)
          .orderBy(orderby, query.order).paginate(parseInt(page), parseInt(perpage));
      } else {
        

        data = await TitleStatus.query()
          .where(function () {
            this
              .where('outgoing', null)
              .orWhere('outgoing', 2)
          })
          .with('alternateName').with('territories').with('platform').with('notes')
          .with('library', (builder) => { builder.with('studio').select('id', 'is', 'title', 'studioId') })
          .filter(query).paginate(parseInt(page), parseInt(perpage));
      }
  
      let platforms = await Platform.all()
      data = data.toJSON()
     
      await Promise.all(
        data.data.map(async val => {
          val['videoStatus'] = Status.projectManagementStatuses().vedioStatus.find(x => x.value == val['videoStatus']),
            val['captions'] = Status.projectManagementStatuses().caps.find(x => x.value == val['captions']),
            val['subtitles'] = Status.projectManagementStatuses().subs.find(x => x.value == val['subtitles']),
            val['artwork'] = Status.projectManagementStatuses().art.find(x => x.value == val['artwork']),
            val['dubbing'] = Status.projectManagementStatuses().dub.find(x => x.value == val['dubbing']),
            val['metadata'] = Status.projectManagementStatuses().metadata.find(x => x.value == val['metadata']),
            // val['videoStatus'] = this.checkSelected(val['videoStatus'],Status.projectManagementStatuses().vedioStatus)
            // val['captions'] = this.checkSelected(val['captions'],Status.projectManagementStatuses().caps)
            // val['subtitles'] = this.checkSelected(val['subtitles'],Status.projectManagementStatuses().subs)
            // val['artwork'] = this.checkSelected(val['artwork'],Status.projectManagementStatuses().art)
            // val['dubbing'] = this.checkSelected(val['dubbing'],Status.projectManagementStatuses().dub)
            // val['metadata'] = this.checkSelected(val['metadata'],Status.projectManagementStatuses().metadata)
            // val['customAssets'] = (await CustomAssetsOutgoing.query().where('outgoingId',val.id).fetch()).toJSON()
            val['applicablePlatforms'] = platforms.toJSON()
          val['seasons'] = (await TitleStatusesSeason.query().where('platformId', val.platformId).where(function () { this.where('titleStatusId', val.id).orWhere('titleStatusId', null) }).andWhere('outgoing', null).andWhere('incomingId', val.incomingId).select('id', 'incomingId', 'seasonId', 'alternateTitleId', 'platformId').with('season', (builder) => builder.select('id', 'name', 'number', 'guId')).fetch()).toJSON()
          val['recent'] = this.recentTitle(val['created_at'])
          val['nearDueDate'] = { tillDueDate: moment(val['dueDate']).fromNow(), flag: (moment(val['dueDate']).fromNow().includes('day') && moment(val['dueDate']).fromNow().includes('in') && moment(val['dueDate']).fromNow().split(' ')[1] < 7) || (moment(val['dueDate']).fromNow().includes('hour') && moment(val['dueDate']).fromNow().includes('in')) || (moment(val['dueDate']).fromNow().includes('minute') && moment(val['dueDate']).fromNow().includes('in')) ? true : false }
        })
      )

      // return data

      let paramsObj = Object.fromEntries(new URLSearchParams(request.get()))

      paramsObj.search && delete paramsObj.search
      paramsObj.searchcolumn && delete paramsObj.searchcolumn
      paramsObj.orderby && delete paramsObj.orderby
      paramsObj.order && delete paramsObj.order
      paramsObj.page && delete paramsObj.page
      paramsObj.perpage && delete paramsObj.perpage

      let filterQuery = []
      
      for (let [key, value] of Object.entries(paramsObj)) {
        filterQuery.push(`${key}=${value}`)
      }
       
      return view.render('projectmanagement.projectmanagement', {
        data: {
          platforms: platforms.toJSON(),
          statuses: Status.projectManagementStatuses()
        },
        titlestatuses: data,
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
        filter: query.filter ? '&' + filterQuery.join('&') : undefined,
      })
    } catch (error) {
      console.log(error)
      return response.redirect('back')
    }
  }

  async store({ request, response, session, params }) {
    let titleStausData = request.only([
      'videoStatus',
      'captions',
      'subtitles',
      'artwork',
      'dubbing',
      'metadata',
      "deliveryDate",
      "launchDate",
      "dueDate",
      "alternateTitleId",
      "status",
      "customerId"
    ]);

    if (params.id) {
      titleStausData['platformId'] = params.id
    }
    else {
      titleStausData['platformId'] = request.input('platformId')
    }
    titleStausData['libraryId'] = (((await AlternateTitleLanguage.query().with('library').where('id', request.input('alternateTitleId')).first()).toJSON()).library).id

    try {
      await TitleStatus.create(titleStausData);
      session.flash({ notification: 'Title Assigned to Platfrom Successfully!' })
      response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: 'Duplicate Entry!' })
      response.redirect('back')
    }

  }

  async update({ request, auth, response, session, params }) {

    let titlestatus = await TitleStatus.query().where('id', params.id).with('alternateName').first()
    let data = request.only([
      'videoStatus',
      'captions',
      'subtitles',
      'artwork',
      'dubbing',
      'metadata',
      "deliveryDate",
      "launchDate",
      "dueDate"
    ])
    let customAsset = request.input('customAsset')

    for (let key in data) {
      if (data[key] == 'null' || data[key] == '') {
        delete data[key]
      }
    }
    if (request.input('platformId') != 'null') {
      data['platformId'] = request.input('platformId')
    }

    titlestatus.merge(data)

    if (titlestatus.deliveryDate != (null || undefined) && titlestatus.platformId != (null || undefined)) {
      titlestatus.outgoing = 1;
    }

    let customAssetData = []
    if (customAsset) {
      if (Array.isArray(customAsset.name) && Array.isArray(customAsset.status)) {
        for (let i = 0; i < customAsset.name.length; i++) {
          customAssetData.push({ userId: (await auth.getUser()).id, studioId: titlestatus.studioId, name: customAsset.name[i], status: customAsset.status[i], outgoingId: params.id })
        }
      }
      else {
        customAssetData.push({ userId: (await auth.getUser()).id, studioId: titlestatus.studioId, name: customAsset.name, status: customAsset.status, outgoingId: params.id })
      }
    }


    try {
      await titlestatus.save()
      await CustomAssetsOutgoing.query()
        .where("outgoingId", params.id)
        .delete();
      await CustomAssetsOutgoing.createMany(customAssetData)

      if (titlestatus.outgoing == 1) {
        let email = await Email.findBy('title', 'OutgoingNotification')
        sendMail(email, { user: (await auth.getUser()).fullname, title: titlestatus.toJSON().alternateName.alternateName, updatedAt: titlestatus.updated_at })
      }

      session.flash({ notification: 'Updated Successfully!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: "Duplicate Entry!" })
      return response.redirect('back')
    }
  }

  async remove({ session, response, params }) {
    try {
      let titlestatus = await TitleStatus.find(params.id)
      await titlestatus.delete()
      session.flash({ notification: 'Title removed from Project Managnement Successfully!' })
      response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: 'Failed to remove from Project Managnement!' })
      response.redirect('back')
    }
  }


  async scheduleTitle({ response, session, params }) {
    try {
      let titlestatus = await TitleStatus.find(params.id)
      titlestatus.outgoing = 1;
      await titlestatus.save()
      session.flash({ notification: 'Scheduled Successfully!' })
      response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: 'Failed to Scheduled!' })
      response.redirect('back')
    }
  }


  async pullIncoming({ request, auth, response, session }) {
    try {

      let data = request.only(['incomingTitle', 'platforms'])
      let incomingTitle = await Incoming.find(data.incomingTitle)
      let titlestatuses = []

      if ((incomingTitle.videoStatus == 1 || incomingTitle.videoStatus == 2) && (incomingTitle.captions == 2 || incomingTitle.captions == 9) && (incomingTitle.subtitles == 2 || incomingTitle.subtitles == 9) && (incomingTitle.artwork == 1 || incomingTitle.artwork == 2) && (incomingTitle.dubbing == 2 || incomingTitle.dubbing == 9) && (incomingTitle.metadata == 1 || incomingTitle.metadata == 2)) {
        await Promise.all(data.platforms.map(async x => {
          titlestatuses.push({ userId: (await auth.getUser()).id, studioId: incomingTitle.studioId, platformId: parseInt(x), alternateTitleId: incomingTitle.alternateTitleId, incomingId: incomingTitle.id, libraryId: incomingTitle.libraryId })
        }))
        await TitleStatus.createMany(titlestatuses)
        session.flash({ notification: 'Successfull! Please Assign Date in Schedule Section.' })
        response.redirect('/project-management/movie')
      }
      else {
        session.flash({ errMsg: 'Subtitles,Captions and dubbing should be QC Passed or N/A. Artwork,Metadata should be QC Passed or Received!' })
        response.redirect('back')
      }


    } catch (error) {
      session.flash({ errMsg: (error.sqlMessage).split(' ')[0] + ' ' + (error.sqlMessage).split(' ')[1] + '!' })
      response.redirect('back')
    }
  }

  async schedule({ request, session, auth, response, params }) {
    let data = request.all()
    for (let key in data) {
      if (data[key] == 'null' || data[key] == '') {
        delete data[key]
      }
    }
    try {
      let titlestatus = await TitleStatus.query().where('id', params.id).with('alternateName').with('library', (builder) => builder.select('id', 'is')).first()
      titlestatus.dueDate = data.dueDate
      titlestatus.launchDate = data.launchDate
      titlestatus.deliveryDate = data.deliveryDate
      if (data.sunSetDate) {
        titlestatus.sunSetDate = data.sunSetDate
      }
      if (titlestatus.deliveryDate != (null || undefined) && titlestatus.platformId != (null || undefined)) {
        if (titlestatus.toJSON().library.is.toLocaleLowerCase() == 'movie') {
          titlestatus.outgoing = 1
        } else {
          titlestatus.outgoing = 2
          await TitleStatusesSeason.query().where('incomingId', titlestatus.incomingId).andWhere('platformId', titlestatus.platformId).update({ 'outgoing': 1, dueDate: titlestatus.dueDate, launchDate: titlestatus.launchDate, deliveryDate: titlestatus.deliveryDate, sunSetDate: titlestatus.sunSetDate })
        }
      }
      await titlestatus.save()

      if (titlestatus.outgoing == 1) {
        let email = await Email.findBy('title', 'OutgoingNotification')
        sendMail(email, { user: (await auth.getUser()).fullname, title: titlestatus.toJSON().alternateName.alternateName, updatedAt: titlestatus.updated_at })
      }

      session.flash({ notification: 'Updated Successfully!' })
      return response.redirect('back')
    } catch (error) {
      console.log(error)
      session.flash({ errMsg: 'Failed to Update!' })
      return response.redirect('back')
    }

  }


  async addNotes({ request, auth, session, response, params }) {
    let studioId = await TitleStatus.query().where('id', params.id).pluck('studioId')
    const trx = await Database.beginTransaction();
    try {
      const note = await Note.create({ text: request.input('text') }, trx)
      await OutgoingNote.create({ outgoingId: params.id, noteId: note.id, studioId: studioId, userId: (await auth.getUser()).id }, trx)
      trx.commit()
      session.flash({ notification: 'Note Added Successfully!' })
      return response.redirect('back')

    } catch (error) {
      session.flash({ errMsg: 'Failed to Add!' })
      return response.redirect('back')
    }
  }

  async deleteNotes({ request, session, response, params }) {
    try {
      const note = await Note.find(params.noteId)
      await note.delete()

      session.flash({ notification: 'Note Deleted Successfully!' })
      return response.redirect('back')

    } catch (error) {
      session.flash({ errMsg: 'Failed to Delete!' })
      return response.redirect('back')
    }
  }

  async addSeasonNotes({ request, session, response, params }) {
    const trx = await Database.beginTransaction();
    try {
      const note = await Note.create({ text: request.input('text') }, trx)
      await OutgoingSeasonNote.create({ outgoingSeasonId: params.id, noteId: note.id }, trx)
      trx.commit()
      session.flash({ notification: 'Note Added Successfully!' })
      return response.redirect('back')

    } catch (error) {
      session.flash({ errMsg: 'Failed to Add!' })
      return response.redirect('back')
    }
  }

  async deleteSeasonNotes({ request, session, response, params }) {
    try {
      const note = await Note.find(params.noteId)
      await note.delete()

      session.flash({ notification: 'Note Deleted Successfully!' })
      return response.redirect('back')

    } catch (error) {
      session.flash({ errMsg: 'Failed to Delete!' })
      return response.redirect('back')
    }
  }


  /*TitleStatusesSeason*/
  async indexSeason({ request, view, response, params }) {
    let searchColumns = [{ value: 'title', name: 'Series Title' }, { value: 'guId', name: 'GUID' }, { value: 'studioname', name: 'Studio' }]

    try {
      let query = Object.create(request.get())

      let page = query.page ? query.page : 1
      let perpage = query.perpage ? query.perpage : 10

      let orderby = query.orderby == 'title' ? 'alternate_title_languages.alternateName' : query.orderby

      let searchcolumn = query.searchcolumn ? query.searchcolumn : ''
      let search = query.search ? query.search : ''
      query[searchcolumn] = search

      let data;

      if (orderby) {
        let Query = TitleStatusesSeason.query().select('title_statuses_seasons.*')
        if (params.alternateTitleId) {
          Query.where('alternateTitleId', params.alternateTitleId)
        }
        if (params.platformId) {
          Query.where('platformId', params.platformId)
        }
        data = await Query.where('outgoing', null)
          .with('alternateName').with('territories').with('season').with('platform').with('notes')
          .join('alternate_title_languages', 'title_statuses_seasons.alternateTitleId', '=', 'alternate_title_languages.id')
          .with('series', (builder) => { builder.with('studio').select('id', 'title', 'studioId') })
          .orderBy(orderby, query.order).filter(query).paginate(parseInt(page), parseInt(perpage));

      } else {
        let Query = TitleStatusesSeason.query()
        if (params.alternateTitleId) {
          Query.where('alternateTitleId', params.alternateTitleId)
        }
        if (params.platformId) {
          Query.where('platformId', params.platformId)
        }
        data = await Query.where('outgoing', null)
          .with('alternateName').with('territories').with('season').with('platform').with('notes')
          .with('series', (builder) => { builder.with('studio').select('id', 'title', 'studioId') })
          .filter(query).paginate(parseInt(page), parseInt(perpage));
      }

      let platforms = await Platform.all()

      data = data.toJSON()

      await Promise.all(
        data.data.map(async val => {
          val['videoStatus'] = this.checkSelected(val['videoStatus'], Status.projectManagementStatuses().vedioStatus)
          val['captions'] = this.checkSelected(val['captions'], Status.projectManagementStatuses().caps)
          val['subtitles'] = this.checkSelected(val['subtitles'], Status.projectManagementStatuses().subs)
          val['artwork'] = this.checkSelected(val['artwork'], Status.projectManagementStatuses().art)
          val['dubbing'] = this.checkSelected(val['dubbing'], Status.projectManagementStatuses().dub)
          val['metadata'] = this.checkSelected(val['metadata'], Status.projectManagementStatuses().metadata)
          val['customAssets'] = (await CustomAssetsOutgoing.query().where('outgoingId', val.id).fetch()).toJSON()
          // val['applicablePlatforms'] = platforms.toJSON() //await this.getSuggestedPlatforms(val.seriesId,platformRights.toJSON())
          val['recent'] = this.recentTitle(val['created_at'])
          val['nearDueDate'] = { tillDueDate: moment(val['dueDate']).fromNow(), flag: (moment(val['dueDate']).fromNow().includes('day') && moment(val['dueDate']).fromNow().includes('in') && moment(val['dueDate']).fromNow().split(' ')[1] < 7) || (moment(val['dueDate']).fromNow().includes('hour') && moment(val['dueDate']).fromNow().includes('in')) || (moment(val['dueDate']).fromNow().includes('minute') && moment(val['dueDate']).fromNow().includes('in')) ? true : false }
        })
      )


      let paramsObj = Object.fromEntries(new URLSearchParams(request.get()))

      paramsObj.search && delete paramsObj.search
      paramsObj.searchcolumn && delete paramsObj.searchcolumn
      paramsObj.orderby && delete paramsObj.orderby
      paramsObj.order && delete paramsObj.order
      paramsObj.page && delete paramsObj.page
      paramsObj.perpage && delete paramsObj.perpage

      let filterQuery = []

      for (let [key, value] of Object.entries(paramsObj)) {
        filterQuery.push(`${key}=${value}`)
      }

      return view.render('projectmanagement.projectmanagement_seasons', {
        data: {
          incomingSeasons: [],
          applicablePlatforms: platforms.toJSON(),
          statuses: Status.projectManagementStatuses()
        },
        titlestatuses: data,
        order: { column: query.orderby, order: query.order },
        search: { text: query.search, column: query.searchcolumn },
        searchColumns: searchColumns,
        filter: query.filter ? '&' + filterQuery.join('&') : undefined,
      })
    } catch (error) {
      return response.redirect('back')
    }
  }

  async updateSeason({ request, response, session, params }) {
    let titlestatus = await TitleStatusesSeason.find(params.id)
    let data = request.only([
      'videoStatus',
      'captions',
      'subtitles',
      'artwork',
      'dubbing',
      'metadata',
      "deliveryDate",
      "launchDate",
      "dueDate"
    ])
    // let customAsset = request.input('customAsset')

    for (let key in data) {
      if (data[key] == 'null' || data[key] == '') {
        delete data[key]
      }
    }

    if (request.input('platformId') != 'null') {
      data['platformId'] = request.input('platformId')
    }

    titlestatus.merge(data)

    if (titlestatus.deliveryDate != (null || undefined) && titlestatus.platformId != (null || undefined)) {
      titlestatus.outgoing = 1;
    }

    // let customAssetData = []
    //   if(customAsset){
    //     if (Array.isArray(customAsset.name) && Array.isArray(customAsset.status)) {
    //     for(let i=0;i < customAsset.name.length;i++){
    //       customAssetData.push({name:customAsset.name[i],status:customAsset.status[i],outgoingId:params.id})
    //     }
    //   }
    //     else{
    //       customAssetData.push({name:customAsset.name,status:customAsset.status,outgoingId:params.id})
    //     }
    //   }


    try {
      await titlestatus.save()
      // await CustomAssetsOutgoing.query()
      // .where("outgoingId", params.id)
      // .delete();
      // await CustomAssetsOutgoing.createMany(customAssetData)
      session.flash({ notification: 'Updated Successfully!' })
      response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: "Duplicate Entry!" })
      response.redirect('back')
    }
  }

  async scheduleSeason({ request, session, response, params }) {
    let data = request.all()
    for (let key in data) {
      if (data[key] == 'null' || data[key] == '') {
        delete data[key]
      }
    }
    try {
      let titlestatus = await TitleStatusesSeason.find(params.id)
      titlestatus.dueDate = data.dueDate
      titlestatus.launchDate = data.launchDate
      titlestatus.deliveryDate = data.deliveryDate
      if (data.sunSetDate) {
        titlestatus.sunSetDate = data.sunSetDate
      }
      if (titlestatus.deliveryDate != (null || undefined) && titlestatus.platformId != (null || undefined)) {
        titlestatus.outgoing = 1;
      }
      await titlestatus.save()
      session.flash({ notification: 'Updated Successfully!' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: 'Failed to Update!' })
      return response.redirect('back')
    }

  }

  async removeSeason({ session, response, params }) {
    try {
      let titlestatus = await TitleStatusesSeason.find(params.id)
      await titlestatus.delete()
      session.flash({ notification: 'Season removed from Project Managnement Successfully!' })
      response.redirect('back')
    } catch (error) {
      session.flash({ errMsg: 'Failed to remove from Project Managnement!' })
      response.redirect('back')
    }
  }

  async pullIncomingSeason({ request, response, session }) {
    try {

      let data = request.only(['incomingTitle', 'platforms'])
      let incomingTitle = await IncomingSeason.find(data.incomingTitle)
      let titlestatuses = []

      if ((incomingTitle.videoStatus == 1 || incomingTitle.videoStatus == 2) && (incomingTitle.captions == 2 || incomingTitle.captions == 9) && (incomingTitle.subtitles == 2 || incomingTitle.subtitles == 9) && (incomingTitle.artwork == 1 || incomingTitle.artwork == 2) && (incomingTitle.dubbing == 2 || incomingTitle.dubbing == 9) && (incomingTitle.metadata == 1 || incomingTitle.metadata == 2)) {
        await Promise.all(data.platforms.map(x => {

          titlestatuses.push({ platformId: parseInt(x), alternateTitleId: incomingTitle.alternateTitleId, incomingId: incomingTitle.incomingId, incomingSeasonId: incomingTitle.id, seasonId: incomingTitle.seasonId, seriesId: incomingTitle.seriesId })
        }))
        await TitleStatusesSeason.createMany(titlestatuses)
        session.flash({ notification: 'Successfull! Please Assign Date in Schedule Section.' })
        response.redirect('/project-management/season')
      }
      else {
        session.flash({ errMsg: 'Subtitles,Captions and dubbing should be QC Passed or N/A. Artwork,Metadata should be QC Passed or Received!' })
        response.redirect('back')
      }


    } catch (error) {
      session.flash({ errMsg: (error.sqlMessage).split(' ')[0] + ' ' + (error.sqlMessage).split(' ')[1] + '!' })
      response.redirect('back')
    }
  }


  async undoMovie({ session, response, params }) {
    try {
      let title_status = await TitleStatus.query().where('id', params.id).with('alternateName').first()
      title_status.outgoing = null
      await title_status.save()
      return response.redirect(`/project-management/movie?search=${title_status.toJSON().alternateName.alternateName}&searchcolumn=title`)
    } catch (error) {
      console.log(error)
      session.flash({ errMsg: 'Edit Failed' })
      return response.redirect('back')
    }
  }

  async undoSeason({ session, response, params }) {
    try {
      let title_status = await TitleStatusesSeason.query().where('id', params.id).with('alternateName').first()
      title_status.outgoing = null
      await title_status.save()
      return response.redirect(`/project-management/season?search=${title_status.toJSON().alternateName.alternateName}&searchcolumn=title`)
    } catch (error) {
      console.log(error)
      session.flash({ errMsg: 'Edit Failed' })
      return response.redirect('back')
    }
  }

  async checkDuplicates({ request, view, response, auth }) {
    try {
      let data = request.all()
      if (request.url().includes('season')) {
        let title_statuses_seasons = await TitleStatusesSeason.query().where('incomingSeasonId', parseInt(data.incomingSeasonId)).andWhere('platformId', 'IN', data.platforms).pluck('platformId')
        let title_statuses_seasons_incomingIds = await TitleStatusesSeason.query().where('incomingSeasonId', parseInt(data.incomingSeasonId)).andWhere('platformId', 'IN', data.platforms).pluck('incomingId')
        let title_status = await TitleStatus.query().select('id', 'incomingId', 'platformId', 'alternateTitleId', 'created_at').with('platform').with('alternateName', (builder) => builder.select('id', 'alternateName')).whereIn('incomingId', title_statuses_seasons_incomingIds).andWhere('platformId', 'IN', data.platforms).fetch()
        return response.json({ flag: true, duplicate: [...new Set(title_statuses_seasons)], titleStatus: title_status })
      } else {
        let title_status = await TitleStatus.query().where('incomingId', parseInt(data.incomingId)).andWhere('platformId', 'IN', data.platforms).pluck('platformId')
        return response.json({ flag: true, duplicate: [...new Set(title_status)] })
      }
    } catch (error) {
      console.log(error.message)
      return response.json({ flag: false, duplicate: null })
    }
  }

  async import({ request, session, response, auth, view }) {
    try {
      const file = request.file('importFile')
      let fileName = uuid() + '.xlsx'
      await file.move(Helpers.tmpPath('uploads'), {
        name: fileName
      })
      let data = await parseCSV(fileName)
      let userId = (await auth.getUser()).id
      let errorFile = []
      let errors = []

      await Promise.all(
        data.map(async (item, index) => {
          let incoming = await Incoming.query().whereHas('alternateName', (builder) => { builder.where('guId', 'LIKE', `%${item.guid.trim()}%`) }).first()
          let platform = await Platform.findBy('name', item.platform.trim())
          let statuses = (incoming.videoStatus == 1 || incoming.videoStatus == 2) && (incoming.captions == 2 || incoming.captions == 9) && (incoming.subtitles == 2 || incoming.subtitles == 9) && (incoming.artwork == 1 || incoming.artwork == 2) && (incoming.dubbing == 2 || incoming.dubbing == 9) && (incoming.metadata == 1 || incoming.metadata == 2)
          let outgoing = (!isNaN(item.outgoing) && item.outgoing == '1') ? 1 : null
          if (incoming != null && platform != null && statuses) {
            let title_status = await TitleStatus.create({
              outgoing: outgoing, incomingId: incoming.id, alternateTitleId: incoming.alternateTitleId, libraryId: incoming.libraryId, platformId: platform.id,
              userId: userId, studioId: incoming.studioId,
              dueDate: item.duedate ? item.duedate.trim() != '' ? item.duedate : null : null,
              launchDate: item.launchdate ? item.launchdate.trim() != '' ? item.launchdate : null : null,
              sunSetDate: item.sunsetdate ? item.sunsetdate.trim() != '' ? item.sunsetdate : null : null,
              deliveryDate: item.deliverydate ? item.deliverydate.trim() != '' ? item.deliverydate : null : null,
              videoStatus: item.videoStatus ? Status.projectManagementStatuses().vedioStatus.find(x => x.name.toLocaleLowerCase() == item.videoStatus.trim().toLocaleLowerCase()).value : null,
              captions: item.captions ? Status.projectManagementStatuses().caps.find(x => x.name.toLocaleLowerCase() == item.captions.trim().toLocaleLowerCase()).value : null,
              subtitles: item.subtitles ? Status.projectManagementStatuses().subs.find(x => x.name.toLocaleLowerCase() == item.subtitles.trim().toLocaleLowerCase()).value : null,
              artwork: item.artwork ? Status.projectManagementStatuses().art.find(x => x.name.toLocaleLowerCase() == item.artwork.trim().toLocaleLowerCase()).value : null,
              dubbing: item.dubbing ? Status.projectManagementStatuses().dub.find(x => x.name.toLocaleLowerCase() == item.dubbing.trim().toLocaleLowerCase()).value : null,
              metadata: item.metadata ? Status.projectManagementStatuses().metadata.find(x => x.name.toLocaleLowerCase() == item.metadata.trim().toLocaleLowerCase()).value : null,
            })
            if (item.territories.length > 0) {
              let territories = item.territories.split(',').map(x => {
                return { outgoingMovieId: title_status.id, territory: x.trim() }
              })
              await OutgoingTerritory.createMany(territories)
            }
          }
          else {
            item.error = (incoming == null ? 'GUID not found in incoming!' : '') + (platform == null ? 'Platform Not found!' : '') + (!statuses ? 'Subtitles,Captions and dubbing should be QC Passed or N/A. Artwork,Metadata should be QC Passed or Received!' : '')
            errorFile.push(item)
            errors.push({ row: index + 2, message: item.error })
          }
        })
      )

      if (errors.length > 0) {

        try {
          const result = exportFromJSON({
            data: errorFile,
            fileName: 'Errors in Programming Import',
            exportType: 'csv',
            processor(content, type, fileName) {
              return content
            }
          })
          let fileName = uuid() + '.csv'
          fs.writeFileSync(`public/error/${fileName}`, result)
          return view.render('library.importError', { data: errors, back: '/project-management/movie', errorFile: `/error/${fileName}` })
        } catch (error) {
          console.log(error)
          session.flash({ errMsg: 'Some Titles Could not be scheduled.See Deatils in dowloaded File.' })
          response.redirect('back')
        }

      } else {
        session.flash({ notification: 'Scheduled Successfully.' })

        return response.redirect('back')
      }



    } catch (error) {
      session.flash({ errMsg: error.message })
      response.redirect('back')
    }
  }

  async importSeasons({ request, view, response, session, auth }) {
    try {
      const file = request.file('importFile')
      let fileName = uuid() + '.xlsx'
      await file.move(Helpers.tmpPath('uploads'), {
        name: fileName
      })
      let data = await parseCSV(fileName)
      let userId = (await auth.getUser()).id
      let errorFile = []
      let errors = []

      await Promise.all(
        data.map(async (item, index) => {
          let incoming_season = await IncomingSeason.query().whereHas('season', (builder) => builder.where('guId', 'LIKE', `%${item.guid.trim()}%`)).first()
          let platform = await Platform.findBy('name', item.platform.trim())
          let statuses = (incoming_season.videoStatus == 1 || incoming_season.videoStatus == 2) && (incoming_season.captions == 2 || incoming_season.captions == 9) && (incoming_season.subtitles == 2 || incoming_season.subtitles == 9) && (incoming_season.artwork == 1 || incoming_season.artwork == 2) && (incoming_season.dubbing == 2 || incoming_season.dubbing == 9) && (incoming_season.metadata == 1 || incoming_season.metadata == 2)
          let outgoing = (!isNaN(item.outgoing) && item.outgoing == '1') ? 1 : null
          if (incoming_season != null && platform != null && statuses) {
            let title_status_season = await TitleStatusesSeason.create({
              outgoing: outgoing, platformId: platform.id, alternateTitleId: incoming_season.alternateTitleId, incomingId: incoming_season.incomingId, seriesId: incoming_season.seriesId, incomingSeasonId: incoming_season.id, seasonId: incoming_season.seasonId,
              userId: userId, studioId: incoming_season.studioId,
              dueDate: item.duedate ? item.duedate.trim() != '' ? item.duedate : null : null,
              launchDate: item.launchdate ? item.launchdate.trim() != '' ? item.launchdate : null : null,
              sunSetDate: item.sunsetdate ? item.sunsetdate.trim() != '' ? item.sunsetdate : null : null,
              deliveryDate: item.deliverydate ? item.deliverydate.trim() != '' ? item.deliverydate : null : null,
              videoStatus: item.videoStatus ? Status.projectManagementStatuses().vedioStatus.find(x => x.name.toLocaleLowerCase() == item.videoStatus.trim().toLocaleLowerCase()).value : null,
              captions: item.captions ? Status.projectManagementStatuses().caps.find(x => x.name.toLocaleLowerCase() == item.captions.trim().toLocaleLowerCase()).value : null,
              subtitles: item.subtitles ? Status.projectManagementStatuses().subs.find(x => x.name.toLocaleLowerCase() == item.subtitles.trim().toLocaleLowerCase()).value : null,
              artwork: item.artwork ? Status.projectManagementStatuses().art.find(x => x.name.toLocaleLowerCase() == item.artwork.trim().toLocaleLowerCase()).value : null,
              dubbing: item.dubbing ? Status.projectManagementStatuses().dub.find(x => x.name.toLocaleLowerCase() == item.dubbing.trim().toLocaleLowerCase()).value : null,
              metadata: item.metadata ? Status.projectManagementStatuses().metadata.find(x => x.name.toLocaleLowerCase() == item.metadata.trim().toLocaleLowerCase()).value : null,
            })

            if (item.territories.length > 0) {
              let territories = item.territories.split(',').map(x => {
                return { outgoingSeasonId: title_status_season.id, territory: x.trim() }
              })
              await OutgoingTerritory.createMany(territories)
            }
          }
          else {
            item.error = (incoming_season == null ? 'GUID not found in incoming seasons!' : '') + (platform == null ? 'Platform Not found!' : '') + (!statuses ? 'Subtitles,Captions and dubbing should be QC Passed or N/A. Artwork,Metadata should be QC Passed or Received!' : '')
            errorFile.push(item)
            errors.push({ row: index + 2, message: item.error })
          }
        })
      )


      if (errors.length > 0) {

        try {
          const result = exportFromJSON({
            data: errorFile,
            fileName: 'Errors in Programming Season Import',
            exportType: 'csv',
            processor(content, type, fileName) {
              return content
            }
          })
          let fileName = uuid() + '.csv'
          fs.writeFileSync(`public/error/${fileName}`, result)
          return view.render('library.importError', { data: errors, back: '/project-management/season', errorFile: `/error/${fileName}` })
        } catch (error) {
          console.log(error)
          session.flash({ errMsg: 'Some Seasons Could not be scheduled.See Deatils in dowloaded File.' })
          response.redirect('back')
        }

      } else {
        session.flash({ notification: 'Seasons Scheduled Successfully.' })

        return response.redirect('back')
      }

    } catch (error) {
      session.flash({ errMsg: error.message })
      response.redirect('back')
    }
  }


  async getDetails({ request, view, response, params }) {
    try {
      let data = await TitleStatus.query().with('customAssetsOutgoing').where('id', params.id).with('territories').first()
      return response.json({ success: true, data: data })
    } catch (error) {
      return response.json({ success: false, data: null })
    }
  }


  async getDetailsSeason({ request, view, response, params }) {
    try {
      let data = await TitleStatusesSeason.query().where('id', params.id).with('territories').first()
      return response.json({ success: true, data: data })
    } catch (error) {
      return response.json({ success: false, data: null, error: error })
    }
  }
}

module.exports = ProjectManagementController

