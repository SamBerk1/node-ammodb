'use strict'
const ActivityLog = use("App/Models/ActivityLog")

class ActivityLogController {
    async index ({ view }){
        let data = await ActivityLog.query().with('user').fetch()
        return view.render('ActivityLog.activityLog',{data:data.toJSON()})
    }


    async destroy ({ request, session ,response }){
        try {
            let log = await ActivityLog.find(request.params.id)
            await log.delete()
            session.flash({ notification:"Log deleted successfully!" })
            return response.redirect('/settings/activity_log')
        } catch (error) {
            session.flash({ errMsg:"Log deleted successfully!" })
            return response.redirect('/settings/activity_log')
        }
    }
}
module.exports = ActivityLogController
